function fnSaveLookUp() {
	if(valOnSubmit()){
		startLoading();
		$.post({
			url : path + '/lookup/save',
			data : $('form[name=lookupform]').serialize(),
			success : function(response) {
				if(response == 'SuccessFullySaved'){
					showSuccess('<b>Success!</b><br> LookUp record is sucessfully created');					
					afterSaveDataGet();
				}else{
					showError('<b>ERROR!</b><br> LookUp record is not sucessfully created');	
					hideLoading();
				}				
			}
		});
	}
}

function afterSaveDataGet(){
	$.get({
		url : path + '/lookup/fetchAll',
		success : function(response) {			
			if(response != ''){
				resetForm();
				prepareGrid(response);
			}else{
				showError('<b>ERROR!</b><br> LookUp records are not fetch sucessfully');	
			}				
		}
	})
}

function prepareGrid(leadData){			
console.log(leadData);
	
	destroyTable('myTable');
	var headArr = ['Id','Name','Name Native','Keyword','Abbreviation','abbreviation Native','SI No','isActive','Action' ];
	createHeader('theadLookUp',headArr);

		$('#tbodyLookUp').empty();

		if(checkValue(leadData) != '-'){
			leadData = JSON.parse(leadData);
			var count = 0;
			$.each(leadData, function( index, object ) {
				count++;
				var trTableBody = $('<tr class="text-center"></tr>').appendTo($('#tbodyLookUp'));
				$('<td data-sort="'+object['lookUpPk']+'">'+count+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['lookUpName'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['lookUpNativeName'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['keyword'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['abbreviation'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['abbreviationNative'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['siNo'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['isActive'])+'</td>').appendTo(trTableBody);
				$('<td><button class="btn btn-info" type="button" module="lookup" value="'+object['lookUpPk']+'" onclick="fnEdit(this)" ><b><i class="fa fa-edit"></i></b></button> &nbsp;&nbsp;<button class="btn btn-danger" type="button" module="lookup" value="'+object['lookUpPk']+'" onclick="fnDelete(this)" ><b><i class="fa fa-trash-o"></i></b></button>').appendTo(trTableBody);
			
			});
		}

	createTable('myTable');	
	hideLoading();
}

function setupForm(response) {
	var json = JSON.parse(response);
	$('#lookUpName').val(json['lookUpName']);
	$('#name_native').val(json['lookUpNativeName']);
	$('#keyword').val(json['keyword']);
	$('#abbreviation').val(json['abbreviation']);
	$('#abbreviationNative').val(json['abbreviationNative']);
	$('#siNo').val(json['siNo']);	
	$('#isactive').val(json['isActive']);
	$('#lookUpPk').val(json['lookUpPk'])
}

function resetForm(){
	$('#lookUpName').val('');
	$('#name_native').val('');
	$('#keyword').val('');
	$('#abbreviation').val('');
	$('#abbreviationNative').val('');
	$('#siNo').val('');	
	$('#isactive').val('-1');
}
