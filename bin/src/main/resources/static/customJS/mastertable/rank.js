function fnSaveRank() {
	if(valOnSubmit()){
		startLoading();
		$.post({
			url : path + '/rank/save',
			data : $('form[name=rankform]').serialize(),
			success : function(response) {
				if(response == 'SuccessFullySaved'){
					showSuccess('<b>Success!</b><br> Rank record is sucessfully created');					
					afterSaveDataGet();
				}else{
					showError('<b>ERROR!</b><br> Rank record is not sucessfully created');	
					hideLoading();
				}				
			}
		});
	}
}

function afterSaveDataGet(){
	$.get({
		url : path + '/rank/fetchAll',
		success : function(response) {			
			if(response != ''){
				resetForm();
				prepareGrid(response);
			}else{
				showError('<b>ERROR!</b><br> Rank records are not fetch sucessfully');	
			}				
		}
	})
}

function prepareGrid(leadData){			
console.log(leadData);
	
	destroyTable('myTable');
	var headArr = ['Id','Name','Name Native','Abbreviation','Abbreviation Native','Pims Pay Scale','Action' ];
	createHeader('theadRank',headArr);

		$('#tbodyRank').empty();

		if(checkValue(leadData) != '-'){
			leadData = JSON.parse(leadData);
			var count = 0;
			$.each(leadData, function( index, object ) {
				count++;
				var trTableBody = $('<tr class="text-center"></tr>').appendTo($('#tbodyRank'));
				$('<td data-sort="'+object['rankPk']+'">'+count+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['rankName'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['rankNativeName'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['abbreviation'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['abbreviationNative'])+'</td>').appendTo(trTableBody);
				$('<td>'+'Pims Pay Scale Id :  '+checkValue(object['pimsPayScaleId']['payScalePk'])+'<br>  Scale : '+checkValue(object['pimsPayScaleId']['scale'])+'<br>  Grade : '+checkValue(object['pimsPayScaleId']['grade'])+'<br> Year : '+checkValue(object['pimsPayScaleId']['year'])+'</td>').appendTo(trTableBody);
				$('<td><button class="btn btn-info" type="button" module="rank" value="'+object['rankPk']+'" onclick="fnEdit(this)" ><b><i class="fa fa-edit"></i></b></button> &nbsp;&nbsp;<button class="btn btn-danger" type="button" module="rank" value="'+object['rankPk']+'" onclick="fnDelete(this)" ><b><i class="fa fa-trash-o"></i></b></button>').appendTo(trTableBody);
			});
		}

	createTable('myTable');	
	hideLoading();
}

function setupForm(response) {
	var json = JSON.parse(response);
	$('#rankName').val(json['rankName']);
	$('#rankNativeName').val(json['rankNativeName']);
	$('#abbreviation').val(json['abbreviation']);
	$('#abbreviationNative').val(json['abbreviationNative']);
	//alert(json['pimsPayScaleId']['payScalePk']);
	$('#pimsPayScaleId').val(json['pimsPayScaleId']['payScalePk']);	
	$('#isactive').val(json['isActive']);	
	$('#rankPk').val(json['rankPk'])
}

function resetForm(){
	$('#rankName').val('');
	$('#rankNativeName').val('');
	$('#abbreviation').val('');
	$('#abbreviationNative').val('');
	$('#pimsPayScaleId').val('-1');	
	$('#isactive').val('-1');
}
