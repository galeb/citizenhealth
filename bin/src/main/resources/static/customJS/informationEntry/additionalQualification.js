function fnSaveQualification() {
    if(valOnSubmit()){
        startLoading();
        $.post({
            url : path + '/qualification/save',
            data : $('form[name=qualificationform]').serialize(),
            success : function(response) {
                if(response == 'SuccessFullySaved'){
                    resetForm();
                    showSuccess('<b>Success!</b><br> Qualification record is sucessfully saved');
                    hideShow();
                    afterSaveDataGet();
                }else{
                    showError('<b>ERROR!</b><br> Qualification record is not sucessfully saved');	
                    hideLoading();
                }				
            }
        });
    }
}

function afterSaveDataGet(){
    $.get({
        url : path + '/qualification/fetchAll',
        success : function(response) {			
            if(response != ''){
                prepareGrid(response);
            }else{
                showError('<b>ERROR!</b><br> Qualification records are not fetch sucessfully');	
            }				
        }
    });
}

function prepareGrid(leadData){			
    destroyTable('myTable');
    var headArr = ['Id','Employee Details','Qualification Serial','Additional Qualification','Remarks','Action' ];
    createHeader('theadQualification',headArr);
    $('#tbodyQualification').empty();
    if(checkValue(leadData) != '-'){
        leadData = JSON.parse(leadData);
        var count = 0;
        $.each(leadData, function( index, object ) {
            count++;
            var trTableBody = $('<tr class="text-center"></tr>').appendTo($('#tbodyQualification'));
            $('<td data-sort="'+object['qualificationPk']+'">'+count+'</td>').appendTo(trTableBody);
            $('<td>-</td>').appendTo(trTableBody);
            $('<td>'+checkValue(object['qualificationSerial'])+'</td>').appendTo(trTableBody);
            $('<td>'+checkValue(object['additionalQualification'])+'</td>').appendTo(trTableBody);
            $('<td>'+checkValue(object['remarks'])+'</td>').appendTo(trTableBody);
            $('<td><button class="btn btn-info" type="button" module="qualification" value="'+object['qualificationPk']+'" onclick="fnEdit(this);showHide()" ><b><i class="fa fa-edit"></i></b></button> &nbsp;&nbsp;<button class="btn btn-danger" type="button" module="qualification" value="'+object['qualificationPk']+'" onclick="fnDelete(this)" ><b><i class="fa fa-trash-o"></i></b></button>').appendTo(trTableBody);
        });
    }
    createTable('myTable');	
    hideLoading();
}

function setupForm(response) {
    var json = JSON.parse(response);
    $('#qualificationPk').val(json['qualificationPk']);
    $('#qualificationSerial').val(json['qualificationSerial']);
    $('#additionalQualification').val(json['additionalQualification']);
    $('#remarks').val(json['remarks']);
}

function resetForm(){
    $('#qualificationPk').val('');
    $('#qualificationSerial').val('');
    $('#additionalQualification').val('');
    $('#remarks').val('');
}

function fnSubmit(){
//    debugger;
//    var requiredMessage = "Please Enter value in Searchbar";
//  alert($('#search').val());
//    if($('#search').is(":empty")){
//        alert("fill");
//      //  $('<span class="error">' + requiredMessage + '</span>');
//    }else{
//        alert("noo");
//    }
}



