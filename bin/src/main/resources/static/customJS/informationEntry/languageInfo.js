function fnSaveLanguage() {
	if(valOnSubmit()){
		startLoading();
		$.post({
			url : path + '/languageinfo/save',
			data : $('form[name=languageinfoform]').serialize(),
			success : function(response) {
				if(response == 'SuccessFullySaved'){
					showSuccess('<b>Success!</b><br> Language record is sucessfully created');					
					afterSaveDataGet();
				}else{
					showError('<b>ERROR!</b><br> Language record is not sucessfully created');	
					hideLoading();
				}				
			}
		});
	}
}

function afterSaveDataGet(){
	$.get({
		url : path + '/languageinfo/fetchAll',
		success : function(response) {			
			if(response != ''){
				resetForm();
				prepareGrid(response);
			}else{
				showError('<b>ERROR!</b><br> Language records are not fetch sucessfully');	
			}				
		}
	})
}

function prepareGrid(leadData){			
console.log(leadData);
	
	destroyTable('myTable');
	var headArr = ['Id','Employee Details','Language','Proficiency Level','Remarks','Action' ];
	createHeader('theadLanguageInfo',headArr);

		$('#tbodyLanguageInfo').empty();

		if(checkValue(leadData) != '-'){
			leadData = JSON.parse(leadData);
			var count = 0;
			$.each(leadData, function( index, object ) {
				count++;
				var trTableBody = $('<tr class="text-center"></tr>').appendTo($('#tbodyLanguageInfo'));
				$('<td data-sort="'+object['languageInfoPk']+'">'+count+'</td>').appendTo(trTableBody);
				$('<td>-</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['language'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['proficiencylevel'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['remarks'])+'</td>').appendTo(trTableBody);
				$('<td><button class="btn btn-info" type="button" module="languageinfo" value="'+object['languageInfoPk']+'" onclick="fnEdit(this)" ><b><i class="fa fa-edit"></i></b></button> &nbsp;&nbsp;<button class="btn btn-danger" type="button" module="languageinfo" value="'+object['languageInfoPk']+'" onclick="fnDelete(this)" ><b><i class="fa fa-trash-o"></i></b></button>').appendTo(trTableBody);
			
			});
		}

	createTable('myTable');	
	hideLoading();
}

function setupForm(response) {
	var json = JSON.parse(response);
	$('#language').val(json['language']);
	$('#proficiencylevel').val(json['proficiencylevel']);	
	$('#remarks').val(json['remarks']);
	$('#languageInfoPk').val(json['languageInfoPk'])
}

function resetForm(){
	$('#language').val('-1');
	$('#proficiencylevel').val('-1');	
	$('#remarks').val('');
}