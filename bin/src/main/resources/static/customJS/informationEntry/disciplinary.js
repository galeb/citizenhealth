function fnSaveDisciplinary() {
	if(valOnSubmit()){
		startLoading();
		$.post({
			url : path + '/disciplinary/save',
			data : $('form[name=disciplinaryform]').serialize(),
			success : function(response) {
				if(response == 'SuccessFullySaved'){
					showSuccess('<b>Success!</b><br> Disciplinary record is sucessfully created');					
					afterSaveDataGet();
				}else{
					showError('<b>ERROR!</b><br>  record is not sucessfully created');	
					hideLoading();
				}				
			}
		});
	}
}

function afterSaveDataGet(){
	$.get({
		url : path + '/disciplinary/fetchAll',
		success : function(response) {			
			if(response != ''){
				resetForm();
				prepareGrid(response);
			}else{
				showError('<b>ERROR!</b><br>  records are not fetch sucessfully');	
			}				
		}
	})
}

function prepareGrid(leadData){			
console.log(leadData);
	
	destroyTable('myTable');
	var headArr = ['Id','Employee Details','Go Date','Offence','Nature of Punishment','Punishment Line 1','Punishment Line 2','Remarks','Action' ];
	createHeader('theadDisciplinary',headArr);

		$('#tbodyDisciplinary').empty();

		if(checkValue(leadData) != '-'){
			leadData = JSON.parse(leadData);
			var count = 0;
			$.each(leadData, function( index, object ) {
				count++;
				var trTableBody = $('<tr class="text-center"></tr>').appendTo($('#tbodyDisciplinary'));
				$('<td data-sort="'+object['disciplinaryPk']+'">'+count+'</td>').appendTo(trTableBody);
				$('<td>-</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['goDate'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['offence'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['natureofPunishment'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['punishmentLine1'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['punishmentLine2'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['remarks'])+'</td>').appendTo(trTableBody);
				$('<td><button class="btn btn-info" type="button" module="disciplinary" value="'+object['disciplinaryPk']+'" onclick="fnEdit(this)" ><b><i class="fa fa-edit"></i></b></button> &nbsp;&nbsp;<button class="btn btn-danger" type="button" module="disciplinary" value="'+object['disciplinaryPk']+'" onclick="fnDelete(this)" ><b><i class="fa fa-trash-o"></i></b></button>').appendTo(trTableBody);
			
			});
		}

	createTable('myTable');	
	hideLoading();
}

function setupForm(response) {
	var json = JSON.parse(response);
	$('#goDate').val(json['goDate']);
	$('#offence').val(json['offence']);
	$('#natureofPunishment').val(json['natureofPunishment']);
	$('#punishmentLine1').val(json['punishmentLine1']);
	$('#punishmentLine2').val(json['punishmentLine2']);
	$('#remarks').val(json['remarks']);
	$('#disciplinaryPk').val(json['disciplinaryPk'])
}

function resetForm(){
	$('#goDate').val('');
	$('#offence').val('');
	$('#natureofPunishment').val('');
	$('#punishmentLine1').val('');
	$('#punishmentLine2').val('');
	$('#remarks').val('');
}