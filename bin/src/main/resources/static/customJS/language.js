function fnSaveLanguage() {
	if(valOnSubmit()){
		startLoading();
		$.post({
			url : path + '/language/save',
			data : $('form[name=languageform]').serialize(),
			success : function(response) {
				if(response == 'SUCCESSFULLYINSERT'){
					showSuccess('<b>Success!</b><br> Your records are sucessfully created');					
					afterSaveDataGet();
				}else{
					showError('<b>ERROR!</b><br> Your records are not sucessfully created');	
					hideLoading();
				}				
			}
		});
	}
}

function checkValSession(ctrl) {	
	$(ctrl).parent().find('.usedError').remove();
	var value = $(ctrl).val();
	if (checkValue(value) != '-' && dict.hasOwnProperty(value)) {
		setUsedError(ctrl);
	}
}

function setUsedError(ctrl){	
	var inputParent = $(ctrl).parent();
	$('<lable style="color: red" class="usedError"><span class="trn"> '+$(ctrl).val()+' is allready Saved.</span></lable>').appendTo(inputParent);
}

function afterSaveDataGet(){
	
	$.get({
		url : path + '/language/fetchAll',
		success : function(response) {			
			if(response != ''){
				resetForm();
				prepareGrid(response);
			}else{
				showError('<b>ERROR!</b><br> Language records are not fetch sucessfully');	
			}				
		}
	})
}

function prepareGrid(leadData){			
	
	destroyTable('myTable');
	var headArr = ['Id','English Name','Bengali Name','Action'];
	createHeader('theadLanguage',headArr);

		$('#tbodyLanguage').empty();

		if(checkValue(leadData) != '-'){
			leadData = JSON.parse(leadData);
			var count = 0;
			$.each(leadData, function( index, object ) {
				count++;
				var trTableBody = $('<tr class="text-center"></tr>').appendTo($('#tbodyLanguage'));
				$('<td data-sort="'+object['languagePk']+'">'+count+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['englishName'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['bengaliName'])+'</td>').appendTo(trTableBody);
				$('<td><button class="btn btn-info" type="button" module="language" value="'+object['languagePk']+'" onclick="fnEdit(this)" ><b><i class="fa fa-edit"></i></b></button> &nbsp;&nbsp;<button class="btn btn-danger" type="button" module="language" value="'+object['languagePk']+'" onclick="fnDelete(this)" ><b><i class="fa fa-trash-o"></i></b></button>').appendTo(trTableBody);
			
			});
		}

	createTable('myTable');	
	hideLoading();
}

function setupForm(response) {
	
	deleteAllMultiKeypair();
	var json = JSON.parse(response);
	$('#lstLanguage0languagePk').val(json['languagePk']);
	$('#lstLanguage0englishName').val(json['englishName']);
	$('#lstLanguage0bengaliName').val(json['bengaliName']);	
}

function resetForm(){

	deleteAllMultiKeypair();
	$('#lstLanguage0englishName').val('');
	$('#lstLanguage0bengaliName').val('');
}

function addMultipleKeyandValuePair(){
	
	var $lastDiv = $('div[id^="divKeyValue"]:last');
	var num = parseInt( $lastDiv.prop("id").match(/\d+/g), 10 ) +1;
    var $newDiv = $('#divKeyValue0').clone().prop('id', 'divKeyValue'+num );
    
    var engId= 'lstLanguage['+num+'].englishName';
    $newDiv.find('input[formcontrolname="englishName"]').prop('id','lstLanguage'+num+'englishName').prop('path',engId).prop('name',engId).val("");   
    
    var bengaliId = 'lstLanguage['+num+'].bengaliName';
    $newDiv.find('input[formcontrolname="bengaliName"]').prop('id','lstLanguage'+num+'bengaliName').prop('path',bengaliId).prop('name',bengaliId).val("");   
    
    $($lastDiv).after( $newDiv );    
    $('#btnMultiKeyPair').show();
	
}

function deleteMultiKeypair() {	
	var $lastDiv = $('div[id^="divKeyValue"]:last');
	var num = parseInt($lastDiv.prop("id").match(/\d+/g), 10);
	if (num > 0) {
		$($lastDiv).remove();
	}else if(num==0){
		$('#btnMultiKeyPair').hide();
	}	
}

function deleteAllMultiKeypair(){
	
	var $lastDiv = $('div[id^="divKeyValue"]:last');
	var num = parseInt($lastDiv.prop("id").match(/\d+/g), 10);
	for(i=0; i< num ;i++){		
		$('div[id^="divKeyValue"]:last').remove();
	}
	deleteMultiKeypair();
}
