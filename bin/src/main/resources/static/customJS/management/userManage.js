function fnSaveUser() {
	if(valOnSubmit()){
		startLoading();
		$.post({
			url : path + '/user/save',
			data : $('form[name=userform]').serialize(),
			success : function(response) {
				if(response == 'SuccessFullySaved'){
					showSuccess('<b>Success!</b><br> User record is sucessfully created');					
					afterSaveDataGet();
				}else if(response == 'USEREXCIST'){
					showError('<b>ERROR!</b><br> Username is already excisted, so User record is not sucessfully created');					
					afterSaveDataGet();
				}else{
					showError('<b>ERROR!</b><br> User record is not sucessfully created');	
					hideLoading();
				}				
			}
		});
	}
}

function afterSaveDataGet(){	
	$.get({
		url : path + '/user/fetchAll',
		success : function(response) {			
			if(response != ''){
				resetForm();
				prepareGrid(response);
			}else{
				showError('<b>ERROR!</b><br> User records are not fetch sucessfully');	
			}				
		}
	})
}

function prepareGrid(leadData){			

	destroyTable('myTable');
	var headArr = ['Id','Name','Email','Username','Action'];
	createHeader('theadUser',headArr);

		$('#tbodyUser').empty();

		if(checkValue(leadData) != '-'){
			leadData = JSON.parse(leadData);
			var count = 0;
			$.each(leadData, function( index, object ) {
				count++;
				var trTableBody = $('<tr class="text-center"></tr>').appendTo($('#tbodyUser'));
                                $('<td data-sort="'+object['id']+'">'+count+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['name'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['email'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['userName'])+'</td>').appendTo(trTableBody);
				$('<td><button class="btn btn-info" type="button" module="user" value="'+object['id']+'" onclick="fnEdit(this)" ><b><i class="fa fa-edit"></i></b></button> &nbsp;&nbsp;<button class="btn btn-danger" type="button" module="user" value="'+object['id']+'" onclick="fnDelete(this)" ><b><i class="fa fa-trash-o"></i></b></button>').appendTo(trTableBody);
			
			});
		}
	createTable('myTable');	
	hideLoading();
}

function setupForm(response) {
	var json = JSON.parse(response);
	$('#id').val(json['id']);
	$('#name').val(json['name']);
	$('#email').val(json['email']);
	$('#userName').val(json['userName']);
	$('#password').val(json['password']);
	$('#roleFK').val(json['roleFK']);
}

function resetForm(){
	$('#name').val('');
	$('#email').val('');
	$('#userName').val('');
	$('#password').val('');
	$('#roleFK').val('-1');
}
