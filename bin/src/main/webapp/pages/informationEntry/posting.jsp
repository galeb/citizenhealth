<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<script	type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/customJS/common.js"></script>
<script>
   startLoading();
</script>
<style>
    
    .nav-tabs .nav-link.active, .nav-tabs .nav-item.show .nav-link {
    color: #5c6873;
    background-color: rgba(255, 255, 255, 0.9);
    border-color: #c8ced3 #c8ced3 rgba(255, 255, 255, 0.9);
}
</style>
<script type='text/javascript'>
   function hideShow()
   {
    	document.getElementById("aNew").classList.remove("active");
    	document.getElementById("aShow").classList.add("active");
    	document.getElementById("show").style.display = "block";
    	document.getElementById("create").style.display = "none";
   }
   function showHide()
   {
   	
   	document.getElementById("aShow").classList.remove("active");
   	document.getElementById("aNew").classList.add("active");
   	document.getElementById("show").style.display = "none";
   	document.getElementById("create").style.display = "block";				
   }
   
</script>
      <main class="main">
         <!-- Breadcrumb-->
         <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="${pageContext.servletContext.contextPath}/index">Home</a></li>
            <li class="breadcrumb-item active">Posting Information</li>
         </ol>
         <div class="container-fluid">
            <div  id="ui-view">
               <div >
                  <div  class="animated fadeIn">
                     <div  class="row">
                        <div  class="col-md-12 mb-12">
                           <ul  class="nav nav-tabs" role="tablist">
                              <li  class="nav-item"><a  id="aShow" aria-controls="home" class="nav-link active" data-toggle="tab" role="tab" onclick='hideShow()'>Posting Information</a></li>
                              <li  class="nav-item"><a  id="aNew"aria-controls="profile" class="nav-link" data-toggle="tab" role="tab" onclick='showHide()'>Create New</a></li>
                           </ul>
                           <div id="show"  class="tab-content">
                              <div  class="tab-pane active" id="home" role="tabpanel">
                                 <div  class="col-sm-12">
                                    <div  class="card">
                                       <div  class="card-header"><strong >Posting Information</strong></div>
                                       <div  class="card-body">
                                          <div  class="row">
                                             <div  class="col-sm-12">
                                                <form  action="" class="form-horizontal ng-untouched ng-pristine ng-valid" method="post" novalidate="">
                                                   <div  class="row">
                                                      <div  class="col-md-12">
                                                         <div  class="form-group row">
                                                            <label  class="col-md-3 col-form-label" for="remarks">Search By Employee Code</label>
                                                            <div  class="col-md-9">
                                                               <div  class="input-group"><input  class="form-control" id="input2-group2" name="input2-group2" placeholder="Search By Employee Code" type="email"><span  class="input-group-append"><button  class="btn btn-primary" type="button"><i  class="fa fa-search"></i> Search</button></span></div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </form>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                  
                                   	 <div  class="row">
										<div class="col-sm-12 table-responsive" id="divDatatable">
											<table aria-describedby="DataTables_Table_0_info"
												class="table table-striped table-bordered datatable dataTable no-footer "
												id="myTable" role="grid"
												style="border-collapse: collapse !important">
												<thead id="theadPosting"></thead>								
												<tbody id="tbodyPosting"></tbody>
											</table>
										</div>
									</div>	
                                    
                                 </div>
                              </div>
                              
                           
                           
                           </div>
                           <div id="create" class="tab-content" style="display:none">
                              
                              <div  class="tab-pane active" id="profile" role="tabpanel">
                                 <div >
                                    <form  class="form-horizontal ng-untouched ng-pristine ng-invalid" method="post" novalidate="">
                                       <div  class="row">
                                          <div  class="col-sm-12">
                                             <div  class="card">
                                                <div  class="card-header"><strong >Posting Information</strong></div>
                                                <div  class="card-body">
                                                   <div  class="row">
                                                      <div  class="col-sm-12 col-md-6"></div>
                                                      <div  class="col-sm-12 col-md-6"></div>
                                                   </div>
                                                   <div  class="row">
                                                      <div  class="col-sm-12">
                                                         <table  aria-describedby="DataTables_Table_0_info" class="table table-striped table-bordered datatable dataTable no-footer" id="DataTables_Table_0" role="grid" style="border-collapse: collapse !important">
                                                            <tbody >
                                                               <tr >
                                                                  <th >Employee Code:</th>
                                                                  <td ><input  aria-controls="DataTables_Table_0" class="form-control form-control-sm" placeholder="Search..." type="search"></td>
                                                                  <th ></th>
                                                                  <td  rowspan="4" style="width:150px;"><img  alt="admin@bootstrapmaster.com" height="150px" src="/resources/img/avatars/6.jpg" width="150px"></td>
                                                               </tr>
                                                               <tr >
                                                                  <th >Name:</th>
                                                                  <td ></td>
                                                                  <th >Rank:</th>
                                                               </tr>
                                                               <tr >
                                                                  <th >Designation:</th>
                                                                  <td ></td>
                                                                  <th >Organisation:</th>
                                                               </tr>
                                                               <tr >
                                                                  <th >Location:</th>
                                                                  <td ></td>
                                                                  <th >SPDB:</th>
                                                               </tr>
                                                            </tbody>
                                                         </table>
                                                      </div>
                                                   </div>
                                                   <div  class="row mrg-top-15">
                                                      <div  class="col-sm-6">
                                                         <div  class="form-group row">
                                                            <label  class="col-md-3 col-form-label" for="order_date">Order Date</label>
                                                            <div  class="col-md-9">
                                                               <input  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="order_date" id="order_date" name="order_date" placeholder="date" type="date"><!---->
                                                               <div  class="ng-star-inserted">
                                                                  <!---->
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div  class="col-sm-6">
                                                         <div  class="form-group row">
                                                            <label  class="col-md-3 col-form-label" for="location">Location</label>
                                                            <div  class="col-md-9">
                                                               <select  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="location" id="location" name="location">
                                                                  <option  selected="selected" value="0">Select Location</option>
                                                                  <option  value="Location 1">Location 1</option>
                                                                  <option  value="Location 2">Location 2</option>
                                                               </select>
                                                               <!---->
                                                               <div  class="ng-star-inserted">
                                                                  <!---->
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div  class="row" >
                                                      <div  class="col-sm-6">
                                                         <div  class="form-group row">
                                                            <label  class="col-md-3 col-form-label" for="designation">Designation</label>
                                                            <div  class="col-md-9">
                                                               <select  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="designation" id="designation" name="designation">
                                                                  <option  selected="selected" value="0">Select Designation</option>
                                                                  <option  value="Designation 1">Designation 1</option>
                                                                  <option  value="Designation 2">Designation 2</option>
                                                               </select>
                                                               <!---->
                                                               <div  class="ng-star-inserted">
                                                                  <!---->
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div  class="col-sm-6">
                                                         <div  class="form-group row">
                                                            <label  class="col-md-3 col-form-label" for="rank">Rank</label>
                                                            <div  class="col-md-9">
                                                               <select  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="rank" id="rank" name="rank">
                                                                  <option  selected="selected" value="0">Select Rank</option>
                                                                  <option  value="Rank 1">Rank 1</option>
                                                                  <option  value="Rank 2">Rank 2</option>
                                                               </select>
                                                               <!---->
                                                               <div  class="ng-star-inserted">
                                                                  <!---->
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div  class="row">
                                                      <div  class="col-sm-6">
                                                         <div  class="form-group row">
                                                            <label  class="col-md-3 col-form-label" for="organisation">Organisation</label>
                                                            <div  class="col-md-9">
                                                               <select  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="organisation" id="organisation" name="organisation">
                                                                  <option  selected="selected" value="0">Select Organisation</option>
                                                                  <option  value="Organisation 1">Organisation 1</option>
                                                                  <option  value="Organisation 2">Organisation 2</option>
                                                               </select>
                                                               <!---->
                                                               <div  class="ng-star-inserted">
                                                                  <!---->
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div  class="col-sm-6">
                                                         <div  class="form-group row">
                                                            <label  class="col-md-3 col-form-label" for="from_date">From Date</label>
                                                            <div  class="col-md-9">
                                                               <input  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="from_date" id="from_date" name="from_date" placeholder="date" type="date"><!---->
                                                               <div  class="ng-star-inserted">
                                                                  <!---->
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div  class="row">
                                                      <div  class="col-sm-6">
                                                         <div  class="form-group row">
                                                            <label  class="col-md-3 col-form-label" for="release_date">Release Date</label>
                                                            <div  class="col-md-9">
                                                               <input  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="release_date" id="release_date" name="release_date" placeholder="date" type="date"><!---->
                                                               <div  class="ng-star-inserted">
                                                                  <!---->
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div  class="col-sm-6">
                                                         <div  class="form-group row">
                                                            <label  class="col-md-3 col-form-label" for="end_date">End Date</label>
                                                            <div  class="col-md-9">
                                                               <input  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="end_date" id="end_date" name="end_date" placeholder="date" type="date"><!---->
                                                               <div  class="ng-star-inserted">
                                                                  <!---->
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div  class="row">
                                                      <div  class="col-sm-6">
                                                         <div  class="form-group row">
                                                            <label  class="col-md-3 col-form-label" for="joining_date">Joininng Date</label>
                                                            <div  class="col-md-9">
                                                               <input  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="joining_date" id="joining_date" name="joining_date" placeholder="date" type="date"><!---->
                                                               <div  class="ng-star-inserted">
                                                                  <!---->
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div  class="col-sm-6">
                                                         <div  class="form-group row">
                                                            <label  class="col-md-3 col-form-label" for="pay_scale">Pay Scale</label>
                                                            <div  class="col-md-9">
                                                               <select  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="pay_scale" id="pay_scale" name="pay_scale">
                                                                  <option  selected="selected" value="">Select Pay Scale</option>
                                                                  <!---->
                                                               </select>
                                                               <!---->
                                                               <div  class="ng-star-inserted">
                                                                  <!---->
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div  class="row">
                                                      <div  class="col-sm-6">
                                                         <div  class="form-group row">
                                                            <label  class="col-md-3 col-form-label" for="status">Status</label>
                                                            <div  class="col-md-9">
                                                               <select  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="status" id="status" name="status">
                                                                  <option  selected="selected" value="">Select Status</option>
                                                                  <!---->
                                                                  <option  value="17" class="ng-star-inserted">Home deputation</option>
                                                               </select>
                                                               <!---->
                                                               <div  class="ng-star-inserted">
                                                                  <!---->
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div  class="col-sm-6">
                                                         <div  class="form-group row">
                                                            <label  class="col-md-3 col-form-label" for="placement">Placement</label>
                                                            <div  class="col-md-9">
                                                               <select  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="placement" id="placement" name="placement">
                                                                  <option  selected="selected" value="">Select Placement</option>
                                                                  <!---->
                                                                  <option  value="20" class="ng-star-inserted">N</option>
                                                               </select>
                                                               <!---->
                                                               <div  class="ng-star-inserted">
                                                                  <!---->
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div  class="row">
                                                      <div  class="col-sm-6">
                                                         <div  class="form-group row">
                                                            <label  class="col-md-3 col-form-label" for="code">Code</label>
                                                            <div  class="col-md-9">
                                                               <select  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="code" id="code" name="code">
                                                                  <option  selected="selected" value="">Select Code</option>
                                                                  <!---->
                                                                  <option  value="60" class="ng-star-inserted">SEC</option>
                                                                  <option  value="61" class="ng-star-inserted">DC</option>
                                                               </select>
                                                               <!---->
                                                               <div  class="ng-star-inserted">
                                                                  <!---->
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div  class="col-sm-6">
                                                         <div  class="form-group row">
                                                            <label  class="col-md-3 col-form-label" for="last_pay_scale">Last Pay Scale</label>
                                                            <div  class="col-md-9">
                                                               <input  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="last_pay_scale" id="last_pay_scale" name="last_pay_scale" placeholder="Last Pay Scale" type="text"><!---->
                                                               <div  class="ng-star-inserted">
                                                                  <!---->
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div  class="row">
                                                      <div  class="col-sm-6">
                                                         <div  class="form-group row">
                                                            <label  class="col-md-3 col-form-label" for="remarks">Remarks</label>
                                                            <div  class="col-md-9">
                                                               <input  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="remarks" id="remarks" name="remarks" placeholder="Remarks" type="text"><!---->
                                                               <div  class="ng-star-inserted">
                                                                  <!---->
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div  class="col-sm-6"></div>
                                                   </div>
                                                   <div  class="row">
                                                      <div  class="col-sm-12">
                                                         <table  aria-describedby="DataTables_Table_0_info" class="table table-striped table-bordered datatable dataTable no-footer" id="Table1" role="grid" style="border-collapse: collapse !important">
                                                            <tbody >
                                                               <tr >
                                                                  <th >Attachments</th>
                                                                  <th ></th>
                                                                  <th ></th>
                                                                  <th ></th>
                                                               </tr>
                                                               <tr >
                                                                  <td ></td>
                                                                  <td >Govt Order</td>
                                                                  <td ></td>
                                                                  <td ></td>
                                                               </tr>
                                                               <tr >
                                                                  <td ></td>
                                                                  <td >Release Order</td>
                                                                  <td ></td>
                                                                  <td ></td>
                                                               </tr>
                                                               <tr >
                                                                  <td ></td>
                                                                  <td >Joining Letter</td>
                                                                  <td ></td>
                                                                  <td ></td>
                                                               </tr>
                                                               <tr >
                                                                  <td ></td>
                                                                  <td ></td>
                                                                  <td ></td>
                                                                  <td ><input  formcontrolname="posting_id" id="posting_id" name="posting_id" type="hidden" value="0" class="ng-untouched ng-pristine ng-valid"><button  class="btn btn-sm btn-success float-right" type="button" disabled=""><i  class="fa fa-dot-circle-o"></i> Save </button></td>
                                                               </tr>
                                                            </tbody>
                                                         </table>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </form>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div ></div>
         </div>
      </main>
<script	type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/customJS/informationEntry/posting.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script>
var leadData = '${lstAward}';
prepareGrid(leadData);
</script>