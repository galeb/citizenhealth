<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<script	type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/customJS/common.js"></script>
<script>
    startLoading();
</script>
<style>

    .nav-tabs .nav-link.active, .nav-tabs .nav-item.show .nav-link {
        color: #5c6873;
        background-color: rgba(255, 255, 255, 0.9);
        border-color: #c8ced3 #c8ced3 rgba(255, 255, 255, 0.9);
    }

/*    .timeline-item {
        -webkit-box-shadow: 0 1px 1px rgba(0,0,0,0.1);
        box-shadow: 0 1px 1px rgba(0,0,0,0.1);
        border-radius: 3px;
        margin-top: 0;
        background: #fff;
        color: #444;
        margin: -3px;
        margin-right: 48px;
        padding: 0;
        position: relative;
        width: 0px;
        height: 0px;

    }*/
/*    .imgcontainer {
        position: relative;
        float:left;
        margin:5px;
        width: 150px;
        height: 100px;
    }*/

    .margin {
        margin: 10px;
    }
    img#productImg1{
        vertical-align: middle;
        width: 75%;
        height: 6%;
    }

    .imgcontainer:hover img {
        opacity:0.5;
    }

    .imgcontainer:hover input {
        display: block;
    }

    .imgcontainer input {
        position:absolute;
        display:none;
    }

    .imgcontainer input.delete {
        top:0;
        left:65%;
    }

</style>
<script type='text/javascript'>
    function hideShow()
    {
        document.getElementById("aNew").classList.remove("active");
        document.getElementById("aShow").classList.add("active");
        document.getElementById("show").style.display = "block";
        document.getElementById("create").style.display = "none";
    }
    function showHide()
    {

        document.getElementById("aShow").classList.remove("active");
        document.getElementById("aNew").classList.add("active");
        document.getElementById("show").style.display = "none";
        document.getElementById("create").style.display = "block";
    }

</script>

<main class="main">
    <!-- Breadcrumb-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="../login.html"><span class="trn">Home</span></a></li>
        <li class="breadcrumb-item active"><span class="trn">General Information Component</span></li>
    </ol>
    <div class="container-fluid">
        <div  id="ui-view">
            <div >
                <div  class="animated fadeIn">
                    <div  class="row">
                        <div  class="col-md-12 mb-12">
                            <ul  class="nav nav-tabs" role="tablist">
                                <li  class="nav-item"><a  id="aShow" aria-controls="home" class="nav-link active" data-toggle="tab" role="tab" onclick='hideShow()'>General Information Details</a></li>
                                <li  class="nav-item"><a  id="aNew"aria-controls="profile" class="nav-link" data-toggle="tab" role="tab" onclick='showHide()'>Create New</a></li>
                            </ul>
                            <div id="show" class="tab-content">
                                <div  class="tab-pane active" id="home" role="tabpanel">
                                    <div  class="col-sm-12">
                                        <div  class="card">
                                            <div  class="card-header"><strong ><span class="trn">General Information Section</span></strong></div>
                                        </div>
                                        <div  class="row">
                                            <div class="col-sm-12 table-responsive" id="divDatatable">
                                                <table aria-describedby="DataTables_Table_0_info"
                                                       class="table table-striped table-bordered datatable dataTable no-footer "
                                                       id="myTable" role="grid"
                                                       style="border-collapse: collapse !important">
                                                    <thead id="theadGeneralInformation"></thead>
                                                    <tbody id="tbodyGeneralInformation"></tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="create" class="tab-content" style="display:none">
                                <div  class="tab-pane active" id="profile" role="tabpanel">
                                    <form:form  class="form-horizontal ng-untouched ng-pristine ng-invalid" enctype="multipart/form-data" action="${pageContext.servletContext.contextPath}/generalinformation/save" method="post" modelAttribute="modelEmployeeImageMapping">
                                        <div  class="row">
                                            <div  class="col-sm-12">
                                                <div  class="card">
                                                    <div  class="card-header"><strong ><span class="trn">General Information</span></strong></div>
                                                    <div  class="card-body">
                                                        <div  class="row">
                                                            <div  class="col-sm-6">
                                                                <div  class="form-group row">
                                                                    <label  class="col-md-3 col-form-label" for="organisation_address"><span class="trn">Govt ID</span></label>
                                                                    <div  class="col-md-9">
                                                                        <form:input path="tblEmployee.govId" class="form-control ng-untouched ng-pristine ng-invalid" 
                                                                                    formcontrolname="govId" id="govId" name="govId" placeholder="Enter Govt ID" />
                                                                        <div  class="ng-star-inserted">
                                                                            <!---->
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div  class="col-sm-6">
                                                                <div  class="form-group row">
                                                                    <label  class="col-md-3 col-form-label" for="organisation_address"><span class="trn">Photo</span></label>
                                                                    <div  class="col-md-6">
                                                                        <input path="fileimage"  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="organisation_address" id="showImage" name="fileImage" placeholder="Enter Orgainisation Address" type="file"><!---->
                                                                    </div>
                                                                    <div  class="col-md-3">
                                                                    <div class="timeline-item" id="dvImg" style="display: none">
                                                                        <div class="timeline-body" style="padding: 5px;" id="dvProductImgs">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div  class="row">
                                                        <div  class="col-sm-6">
                                                            <div  class="form-group row">
                                                                <label  class="col-md-3 col-form-label" for="organisation_address"><span class="trn">Name(English)</span></label>
                                                                <div  class="col-md-9">
                                                                    <form:input path="tblEmployee.engName"  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="engName"
                                                                                id="engName" name="engName" placeholder="Enter Name(English)" /><!---->
                                                                    <div  class="ng-star-inserted">
                                                                        <!---->
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div  class="col-sm-6">
                                                            <div  class="form-group row">
                                                                <label  class="col-md-3 col-form-label" for="organisation_mobile"><span class="trn">Name(Bangla)</span></label>
                                                                <div  class="col-md-9">
                                                                    <form:input path="tblEmployee.bngName"  class="form-control ng-untouched ng-pristine ng-invalid" 
                                                                                formcontrolname="bngName" id="bngName" name="bngName" placeholder="Enter Name(Bangla)" />
                                                                    <div  class="ng-star-inserted">
                                                                        <!---->
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div  class="row">
                                                        <div  class="col-sm-6">
                                                            <div  class="form-group row">
                                                                <label  class="col-md-3 col-form-label" for="organisation_name"><span class="trn">Father's Name</span></label>
                                                                <div  class="col-md-9">
                                                                    <form:input path="tblEmployee.engFatherName" class="form-control ng-untouched ng-pristine ng-invalid"
                                                                                formcontrolname="organisation_name" id="engFatherName" name="engFatherName" placeholder="Enter Father's Name" /><!---->
                                                                    <div  class="ng-star-inserted">
                                                                        <!---->
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div  class="col-sm-6">
                                                            <div  class="form-group row">
                                                                <label  class="col-md-3 col-form-label" for="organisation_telephone"><span class="trn">Father's Name(Bangla)</span></label>
                                                                <div  class="col-md-9">
                                                                    <form:input path="tblEmployee.bngFatherName" class="form-control ng-untouched ng-pristine ng-invalid" 
                                                                                formcontrolname="bngFatherName" id="bngFatherName" name="bngFatherName" placeholder="Enter Father's Name(Bangla)" /><!---->
                                                                    <div  class="ng-star-inserted">
                                                                        <!---->
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div  class="row">
                                                        <div  class="col-sm-6">
                                                            <div  class="form-group row">
                                                                <label  class="col-md-3 col-form-label" for="organisation_name_native"><span class="trn">Mother's Name</span></label>
                                                                <div  class="col-md-9">
                                                                    <form:input path="tblEmployee.engMotherName" class="form-control ng-untouched ng-pristine ng-invalid" 
                                                                                formcontrolname="engMotherName" id="engMotherName" name="engMotherName" placeholder="Enter Mother's Name" /><!---->
                                                                    <div  class="ng-star-inserted">
                                                                        <!---->
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div  class="col-sm-6">
                                                            <div  class="form-group row">
                                                                <label  class="col-md-3 col-form-label" for="organisation_email"><span class="trn">Mother's Name(Bangla)</span></label>
                                                                <div  class="col-md-9">
                                                                    <form:input path="tblEmployee.bngMotherName"  class="form-control ng-untouched ng-pristine ng-invalid" 
                                                                                formcontrolname="bngMotherName" id="bngMotherName" name="bngMotherName" placeholder="Enter Mother's Name(Bangla)" /><!---->
                                                                    <div  class="ng-star-inserted">
                                                                        <!---->
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div  class="row">
                                                        <div  class="col-sm-6">
                                                            <div  class="form-group row">
                                                                <label  class="col-md-3 col-form-label" for="remarks"><span class="trn">Date of Birth</span></label>
                                                                <div  class="col-md-9">
                                                                    <jsp:useBean id="today" class="java.util.Date"/>
                                                                    <fmt:formatDate value="${today}" pattern="yyyy-MM-dd" var="currentDate"/>
                                                                    <form:input path="tblEmployee.birthDate" class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="remarks" 
                                                                                id="remarks" name="remarks" value="${currentDate}" placeholder="Enter Remarks" type="date"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div  class="col-sm-6">
                                                            <div  class="form-group row">
                                                                <label  class="col-md-3 col-form-label" for="organisation_type"><span class="trn">Sex</span></label>
                                                                <div  class="col-md-9">
                                                                    <form:select path="tblEmployee.gender" class="form-control ng-untouched ng-pristine ng-invalid" 
                                                                                 formcontrolname="gender" id="gender" name="gender">
                                                                        <form:option  selected="selected" value="">Select Sex</form:option>
                                                                        <form:option  value="Male" class="ng-star-inserted">Male</form:option>
                                                                        <form:option  value="Female" class="ng-star-inserted">Female</form:option>
                                                                    </form:select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div  class="row">
                                                        <div  class="col-sm-6">
                                                            <div  class="form-group row">
                                                                <label  class="col-md-3 col-form-label" for="organisation_type"><span class="trn">Prefix Name</span></label>
                                                                <div  class="col-md-9">
                                                                    <form:select path="tblEmployee.preFixName" class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="organisation_type" 
                                                                                 id="preFixName" name="preFixName">
                                                                        <form:option  selected="selected" value="">Select Prefix Name</form:option>
                                                                        <form:option  value="Prefix 1" class="ng-star-inserted">Prefix 1</form:option>
                                                                        <form:option  value="Prefix 2" class="ng-star-inserted">Prefix 2</form:option>
                                                                    </form:select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div  class="col-sm-6">
                                                            <div  class="form-group row">
                                                                <label  class="col-md-3 col-form-label" for="organisation_type"><span class="trn">Suffix Name</span></label>
                                                                <div  class="col-md-9">
                                                                    <form:select path="tblEmployee.suffixName" class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="suffixName"
                                                                                 id="suffixName" name="suffixName">
                                                                        <form:option  selected="selected" value="">Select Suffix Name</form:option>
                                                                        <form:option  value="1" class="ng-star-inserted">Suffix 1</form:option>
                                                                        <form:option  value="2" class="ng-star-inserted">Suffix 2</form:option>
                                                                    </form:select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div  class="row">
                                                        <div  class="col-sm-6">
                                                            <div  class="form-group row">
                                                                <label  class="col-md-3 col-form-label" for="organisation_type"><span class="trn">Marital Status</span></label>
                                                                <div  class="col-md-9">
                                                                    <form:select path="tblEmployee.matirialStatus" class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="organisation_type" id="organisation_type" name="organisation_type">
                                                                        <form:option  selected="selected" value="">Select Marital Status</form:option>
                                                                        <form:option  value="1" class="ng-star-inserted">married</form:option>
                                                                        <form:option  value="2" class="ng-star-inserted">unmarried</form:option>
                                                                    </form:select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div  class="col-sm-6">
                                                            <div  class="form-group row">
                                                                <label  class="col-md-3 col-form-label" for="organisation_type"><span class="trn">Home District</span></label>
                                                                <div  class="col-md-9">
                                                                    <form:select path="tblEmployee.homeDistrict" class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="organisation_type" id="organisation_type" name="organisation_type">
                                                                        <form:option  selected="selected" value="">Select Home District</form:option>
                                                                        <form:option  value="1" class="ng-star-inserted">Ahmedabad</form:option>
                                                                        <form:option  value="2" class="ng-star-inserted">Baroda</form:option>
                                                                    </form:select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div  class="row">
                                                        <div  class="col-sm-6">
                                                            <div  class="form-group row">
                                                                <label  class="col-md-3 col-form-label" for="organisation_type"><span class="trn">Religion</span></label>
                                                                <div  class="col-md-9">
                                                                    <form:select path="tblEmployee.religion" class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="organisation_type" id="organisation_type" name="organisation_type">
                                                                        <form:option  selected="selected" value="">Select Religion</form:option>
                                                                        <form:option  value="1" class="ng-star-inserted">Hindu</form:option>
                                                                        <form:option  value="2" class="ng-star-inserted">Muslim</form:option>
                                                                    </form:select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div  class="col-sm-6">
                                                            <div  class="form-group row">
                                                                <label  class="col-md-3 col-form-label" for="organisation_type"><span class="trn">Rank</span></label>
                                                                <div  class="col-md-9">
                                                                    <label  class="col-md-3 col-form-label" for="organisation_name_native"><span class="trn">Label</span></label>  
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div  class="row">
                                                        <div  class="col-sm-6">
                                                            <div  class="form-group row">
                                                                <label  class="col-md-3 col-form-label" for="organisation_name_native"><span class="trn">Designation</span></label>
                                                                <div  class="col-md-9">
                                                                    <label  class="col-md-3 col-form-label" for="organisation_name_native"><span class="trn">Label</span></label>  
                                                                    <div  class="ng-star-inserted">
                                                                        <!---->
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div  class="col-sm-6">
                                                            <div  class="form-group row">
                                                                <label  class="col-md-3 col-form-label" for="organisation_email"><span class="trn">location</span></label>
                                                                <div  class="col-md-9">
                                                                    <label  class="col-md-3 col-form-label" for="organisation_name_native"><span class="trn">Label</span></label>  
                                                                    <div  class="ng-star-inserted">
                                                                        <!---->
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div  class="row">
                                                        <div  class="col-sm-6">
                                                            <div  class="form-group row">
                                                                <label  class="col-md-3 col-form-label" for="organisation_name_native"><span class="trn">Organization</span></label>
                                                                <div  class="col-md-9">
                                                                    <label  class="col-md-3 col-form-label" for="organisation_name_native"><span class="trn">Label</span></label>  
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div  class="col-sm-6">
                                                            <div  class="form-group row">
                                                                <label  class="col-md-3 col-form-label" for="organisation_type"><span class="trn">Order/Join Type</span></label>
                                                                <div  class="col-md-9">
                                                                    <form:select path="tblEmployee.joinType" class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="organisation_type" id="organisation_type" name="organisation_type">
                                                                        <form:option  selected="selected" value="">Select Order/Join Type</form:option>
                                                                        <form:option  value="1" class="ng-star-inserted">Join Type 1</form:option>
                                                                        <form:option  value="2" class="ng-star-inserted">Join Type 2 </form:option>
                                                                    </form:select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div  class="row">
                                                        <div  class="col-sm-6">
                                                            <div  class="form-group row">
                                                                <label  class="col-md-3 col-form-label" for="organisation_name_native"><span class="trn">O/J Date</span></label>
                                                                <div  class="col-md-9">
                                                                    <form:input path="tblEmployee.joinDate" class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="joinDate" 
                                                                                id="joinDate" value="${currentDate}" name="joinDate" placeholder="Enter O/J Date" type="date"/><!---->
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div  class="col-sm-6">
                                                            <div  class="form-group row">
                                                                <label  class="col-md-3 col-form-label" for="organisation_type"><span class="trn">Freedom Fighter</span></label>
                                                                <div  class="col-md-9">
                                                                    <form:select path="tblEmployee.freedomFighter" class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="organisation_type" id="organisation_type" name="organisation_type">
                                                                        <form:option  selected="selected" value="">Select Freedom Fighter</form:option>
                                                                        <form:option  value="1" class="ng-star-inserted">Yes</form:option>
                                                                        <form:option  value="2" class="ng-star-inserted">No</form:option>
                                                                    </form:select>
                                                                    <input  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="organisation_name_native" id="c" name="organisation_name_native" value="if Yes" disabled />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div  class="row">
                                                        <div  class="col-sm-6">
                                                            <div  class="form-group row">
                                                                <label  class="col-md-3 col-form-label" for="organisation_name_native"><span class="trn">PRL Date</span></label>
                                                                <div  class="col-md-9">
                                                                    <label  class="col-md-3 col-form-label" for="organisation_name_native"><span class="trn">Label</span></label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div  class="col-sm-6">
                                                            <div  class="form-group row">
                                                                <label  class="col-md-3 col-form-label" for="organisation_type"><span class="trn">No. of Spouse</span></label>
                                                                <div  class="col-md-9">
                                                                    <label  class="col-md-3 col-form-label" for="organisation_name_native"><span class="trn">Label</span></label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div  class="row">
                                                        <div  class="col-sm-6">
                                                            <div  class="form-group row">
                                                                <label  class="col-md-3 col-form-label" for="organisation_type"><span class="trn">Batch</span></label>
                                                                <div  class="col-md-9">
                                                                    <form:select path="tblEmployee.batch" class="form-control ng-untouched ng-pristine ng-invalid"
                                                                                 formcontrolname="batch" id="batch" name="batch">
                                                                        <form:option  selected="selected" value="">Select Batch</form:option>
                                                                        <form:option  value="1" class="ng-star-inserted">Batch 1</form:option>
                                                                        <form:option  value="2" class="ng-star-inserted">Batch 2</form:option>
                                                                    </form:select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div  class="col-sm-6">
                                                            <div  class="form-group row">
                                                                <label  class="col-md-3 col-form-label" for="organisation_type"><span class="trn">Confirmation Go Date</span></label>
                                                                <div  class="col-md-9">
                                                                    <form:input path="tblEmployee.confirmationGoDate" value="${currentDate}" class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="organisation_name_native" id="c" name="organisation_name_native"  type="date"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div  class="row">
                                                        <div  class="col-sm-6">
                                                            <div  class="form-group row">
                                                                <label  class="col-md-3 col-form-label" for="organisation_type"><span class="trn">Confirmation Date</span></label>
                                                                <div  class="col-md-9">
                                                                    <form:input path="tblEmployee.confirmationDate" value="${currentDate}" class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="organisation_name_native" id="c" name="organisation_name_native"  type="date"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div  class="col-sm-6">
                                                            <div  class="form-group row">
                                                                <label  class="col-md-3 col-form-label" for="organisation_type"><span class="trn">Expire Date</span></label>
                                                                <div  class="col-md-9">
                                                                    <form:input path="tblEmployee.expiryDate" value="${currentDate}" class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="organisation_name_native" id="expiryDate" name="expiryDate"  type="date"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div  class="row">
                                                        <div  class="col-sm-6">
                                                            <div  class="form-group row">
                                                                <label  class="col-md-3 col-form-label" for="organisation_type"><span class="trn">National Seniority</span></label>
                                                                <div  class="col-md-9">
                                                                    <label  class="col-md-3 col-form-label" for="organisation_type"><span class="trn">Label</span></label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div  class="col-sm-6">
                                                            <div  class="form-group row">
                                                                <label  class="col-md-3 col-form-label" for="organisation_type"><span class="trn">Status</span></label>
                                                                <div  class="col-md-9">
                                                                    <label  class="col-md-3 col-form-label" for="organisation_type"><span class="trn">Label</span></label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div  class="row">
                                                        <div  class="col-sm-6">
                                                            <div  class="form-group row">
                                                                <label  class="col-md-3 col-form-label" for="organisation_type"><span class="trn">Promote</span></label>
                                                                <div  class="col-md-9">
                                                                    <form:select path="tblEmployee.promote" class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="organisation_type" id="organisation_type" name="organisation_type">
                                                                        <form:option  selected="selected" value="">Select Promote</form:option>
                                                                        <form:option  value="1" class="ng-star-inserted">Promote 1</form:option>
                                                                        <form:option  value="2" class="ng-star-inserted">Promote 2</form:option>
                                                                    </form:select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div  class="col-sm-6">
                                                            <div  class="form-group row">
                                                                <label  class="col-md-3 col-form-label" for="organisation_type"><span class="trn">Cadre</span></label>
                                                                <div  class="col-md-9">
                                                                    <label  class="col-md-3 col-form-label" for="organisation_type"><span class="trn">Label</span></label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div  class="row">
                                                        <div  class="col-sm-6">
                                                            <div  class="form-group row">
                                                                <label  class="col-md-3 col-form-label" for="organisation_type"><span class="trn">Cadre Date</span></label>
                                                                <div  class="col-md-9">
                                                                    <label  class="col-md-3 col-form-label" for="organisation_type"><span class="trn">Label</span></label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div  class="col-sm-6">
                                                            <div  class="form-group row">
                                                                <label  class="col-md-3 col-form-label" for="organisation_type"><span class="trn">Activity Status</span></label>
                                                                <div  class="col-md-9">
                                                                    <form:select path="tblEmployee.activeStatus" class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="organisation_type" id="organisation_type" name="organisation_type">
                                                                        <form:option  selected="selected" value="">Select Promote</form:option>
                                                                        <form:option  value="1" class="ng-star-inserted">Promote 1</form:option>
                                                                        <form:option  value="2" class="ng-star-inserted">Promote 2</form:option>
                                                                    </form:select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div  class="row">
                                                        <div  class="col-sm-6">
                                                            <div  class="form-group row">
                                                                <label  class="col-md-3 col-form-label" for="organisation_type"><span class="trn">Placement</span></label>
                                                                <div  class="col-md-9">
                                                                    <form:select path="tblEmployee.placement" class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="organisation_type" id="organisation_type" name="organisation_type">
                                                                        <form:option  selected="selected" value="">Select Promote</form:option>
                                                                        <form:option  value="1" class="ng-star-inserted">Promote 1</form:option>
                                                                        <form:option  value="2" class="ng-star-inserted">Promote 2</form:option>
                                                                    </form:select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div  class="col-sm-6">
                                                            <div  class="form-group row">
                                                                <label  class="col-md-3 col-form-label" for="organisation_type"><span class="trn">Code</span></label>
                                                                <div  class="col-md-9">
                                                                    <label  class="col-md-3 col-form-label" for="organisation_type"><span class="trn">Label</span></label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div  class="row">
                                                        <div  class="col-sm-6">
                                                            <div  class="form-group row">
                                                                <label  class="col-md-3 col-form-label" for="organisation_type"><span class="trn">Additional Personal Info</span></label>
                                                                <div  class="col-md-9">
                                                                    <form:input path="tblEmployee.additionalPersonalInfo" class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="additionalPersonalInfo"
                                                                                id="additionalPersonalInfo" name="additionalPersonalInfo" placeholder="Enter Additional Personal Info"  />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div  class="col-sm-6">
                                                            <div  class="form-group row">
                                                                <label  class="col-md-3 col-form-label" for="organisation_type"><span class="trn">Remarks</span></label>
                                                                <div  class="col-md-9">
                                                                    <form:input path="tblEmployee.remarks"  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="remarks" 
                                                                                id="remarks" name="remarks" placeholder="Enter Remarks" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <input  formcontrolname="organisation_id" id="organisation_id" name="organisation_id" type="hidden" value="0" class="ng-untouched ng-pristine ng-valid">
                                            <div class="card-footer"><button  class="btn btn-sm btn-success float-right" type="submit"><i  class="fa fa-dot-circle-o"></i> Save </button></div>
                                        </div>
                                    </div>
                                </form:form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</main>
<script	type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/customJS/informationEntry/generalinformation.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script>
                                    var leadData = '${lstAward}';
                                    prepareGrid(leadData);

                                    function readURL(input) {
                                        console.log(input.files.length);
                                        $('#dvProductImgs').html('');
                                        for (var i = 0; i < input.files.length; i++) {
                                            console.log('in loop');
                                            if (input.files && input.files[i]) {
                                                var name = input.files[i]['name'];
                                                var reader = new FileReader();

                                                reader.onload = function (e) {

                                                    var divImage = "<div class='imgcontainer' >";
                                                    divImage = divImage + '<img class="img-responsive img-circle" id="productImg' + i + '" src="' + e.target.result + '" alt="your image"/>';
                                                    //  divImage = divImage + '<input class="delete" type="button" value="Delete" onclick="remove(this)" name="' + name + '"/></div></div></div>';
                                                    $('#dvProductImgs').append(divImage);
                                                }
                                                reader.readAsDataURL(input.files[i]);
                                            }
                                        }
                                    }
//                                    function remove(e) {
//                                        //alert("in function");
//                                        $(e).parent().remove();
//                                    }

                                    $("#showImage").change(function () {
                                        readURL(this);
                                    });

                                    $('input[type=file]').change(function () {
                                        $("#dvImg").show();
                                    });

                                    $('input.delete').change(function () {
                                        console.log("doneeeeee")
                                        if ($("#dvImg").checkEmpty()) {
                                            console.log("Empty");
                                        } else {
                                            console.log("Not Empty");
                                        }
                                    });
</script>