<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<script	type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/customJS/common.js"></script>
<script>
   startLoading();
</script>
<style>
    
    .nav-tabs .nav-link.active, .nav-tabs .nav-item.show .nav-link {
    color: #5c6873;
    background-color: rgba(255, 255, 255, 0.9);
    border-color: #c8ced3 #c8ced3 rgba(255, 255, 255, 0.9);
}
</style>
<script type='text/javascript'>
   function hideShow()
   {
    	document.getElementById("aNew").classList.remove("active");
    	document.getElementById("aShow").classList.add("active");
    	document.getElementById("show").style.display = "block";
    	document.getElementById("create").style.display = "none";
   }
   function showHide()
   {
   	
   	document.getElementById("aShow").classList.remove("active");
   	document.getElementById("aNew").classList.add("active");
   	document.getElementById("show").style.display = "none";
   	document.getElementById("create").style.display = "block";				
   }
   
</script>
      <main class="main">
         <!-- Breadcrumb-->
         <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="${pageContext.servletContext.contextPath}/index">Home</a></li>
            <li class="breadcrumb-item active">Foreign travel Section</li>
         </ol>
         <div class="container-fluid">
            <div  id="ui-view">
               <div >
                  <div  class="animated fadeIn">
                     <div  class="row">
                        <div  class="col-md-12 mb-12">
                           <ul  class="nav nav-tabs" role="tablist">
                              <li  class="nav-item"><a  id="aShow" aria-controls="home" class="nav-link active" data-toggle="tab" role="tab" onclick='hideShow()'>Foreign Travel</a></li>
                              <li  class="nav-item"><a  id="aNew"aria-controls="profile" class="nav-link" data-toggle="tab" role="tab" onclick='showHide()'>Create New</a></li>
                           </ul>
                           <div id="show" class="tab-content">
                              <div  class="tab-pane active" id="home" role="tabpanel">
                                 <div  class="col-sm-12">
                                    <div  class="card">
                                       <div  class="card-header"><strong >Foreign Travel</strong></div>
                                       <div  class="card-body">
                                          <div  class="row">
                                             <div  class="col-sm-12">
                                                <form  action="" class="form-horizontal ng-untouched ng-pristine ng-valid" method="post" novalidate="">
                                                   <div  class="row">
                                                      <div  class="col-md-12">
                                                         <div  class="form-group row">
                                                            <label  class="col-md-3 col-form-label" for="remarks">Search By Employee Code</label>
                                                            <div  class="col-md-9">
                                                               <div  class="input-group"><input  class="form-control" id="input2-group2" name="input2-group2" placeholder="Search By Employee Code" type="email"><span  class="input-group-append"><button  class="btn btn-primary" type="button"><i  class="fa fa-search"></i> Search</button></span></div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </form>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
									
									<div  class="row">
										<div class="col-sm-12 table-responsive" id="divDatatable">
											<table aria-describedby="DataTables_Table_0_info"
												class="table table-striped table-bordered datatable dataTable no-footer "
												id="myTable" role="grid"
												style="border-collapse: collapse !important">
												<thead id="theadForeignTravel"></thead>								
												<tbody id="tbodyForeignTravel"></tbody>
											</table>
										</div>
									</div>	
									
									
                                 </div>
                              </div>
							  
                           </div>
                          


						  <div id="create" class="tab-content" style="display:none">
                            
                              <div  class="tab-pane active" id="profile" role="tabpanel">
                                 <div >
                                   <form:form role="form" name="foreigntravelform" action="${pageContext.servletContext.contextPath}/foreigntravel/save" onsubmit="return false;" method="post" modelAttribute="modelForeignTravel" >                                                      
                                     <form:hidden name="foreigntravelPk" path="foreigntravelPk" id="foreigntravelPk" />
                                     
                                       <div  class="row">
                                          <div  class="col-sm-12">
                                             <div  class="card">
                                                <div  class="card-header"><strong >Foreign Travel Information in Group</strong></div>
                                                <div  class="card-body">
                                                   <div  class="row">
                                                      <div  class="col-sm-12 col-md-6"></div>
                                                      <div  class="col-sm-12 col-md-6"></div>
                                                   </div>
                                                   <div  class="row">
                                                      <div  class="col-sm-12">
                                                         <table  aria-describedby="DataTables_Table_0_info" class="table table-striped table-bordered datatable dataTable no-footer" id="DataTables_Table_0" role="grid" style="border-collapse: collapse !important">
                                                            <tbody >
                                                               <tr >
                                                                  <th >Employee Code:</th>
                                                                  <td ><input  aria-controls="DataTables_Table_0" class="form-control form-control-sm" placeholder="Search..." type="search"></td>
                                                                  <th ></th>
                                                                  <td  rowspan="4" style="width:150px;"><img  alt="admin@bootstrapmaster.com" height="150px" src="/resources/img/avatars/6.jpg" width="150px"></td>
                                                               </tr>
                                                               <tr >
                                                                  <th >Name:</th>
                                                                  <td ></td>
                                                                  <th >Rank:</th>
                                                               </tr>
                                                               <tr >
                                                                  <th >Designation:</th>
                                                                  <td ></td>
                                                                  <th >Organisation:</th>
                                                               </tr>
                                                               <tr >
                                                                  <th >Location:</th>
                                                                  <td ></td>
                                                                  <th >SPDB:</th>
                                                               </tr>
                                                            </tbody>
                                                         </table>
                                                      </div>
                                                   </div>
                                                   <div  class="row">
                                                      <div  class="col-sm-6">
                                                         <div  class="form-group row">
                                                         
                                                         </div>
                                                      </div>
                                                      <div  class="col-sm-6"></div>
                                                   </div>
                                                   
                                                   <div class="row">
															<div class="col-sm-6">
																<div class="form-group row">
																	<label class="col-md-3 col-form-label" for="country"><span class="trn">Country</span></label>
																	<div class="col-md-9">
																		<form:select path="country"
																			class="form-control ng-untouched ng-pristine ng-valid"
																			formcontrolname="country" id="country" name="country" onblur="validateCombo(this);" isrequired="true" title="Country">
																			<form:option selected="true" disabled="true" value="-1">Select Country</form:option>
																			<form:option value="Bangladesh">Bangladesh</form:option>
																			<form:option value="India">India</form:option>
																		</form:select>
																	</div>
																</div>
															</div>
													</div>
													
													
													<div class="row">
															<div class="col-sm-6">
																<div class="form-group row">
																	<label class="col-md-3 col-form-label" for="purposeoftravel"><span class="trn">Purpose of Travel</span></label>
																	<div class="col-md-9">
																		<form:select path="purposeoftravel"
																			class="form-control ng-untouched ng-pristine ng-valid"
																			formcontrolname="purposeoftravel" id="purposeoftravel" name="purposeoftravel" onblur="validateCombo(this);" isrequired="true" title="Purpose of Travel">
																			<form:option selected="true" disabled="true" value="-1">Select Purpose of Travel</form:option>
																			<form:option value="PPM STUDY TOUR">PPM STUDY TOUR</form:option>
																			<form:option value="INTL. CONFERENCE">INTL. CONFERENCE</form:option>
																		</form:select>
																	</div>
																</div>
															</div>
													</div>
                                                   
                                                   <div class="row">
															<div class="col-sm-6">
																<div class="form-group row">
																	<label class="col-md-3 col-form-label" for="status"><span class="trn">Status</span></label>
																	<div class="col-md-9">
																		<form:select path="status"
																			class="form-control ng-untouched ng-pristine ng-valid"
																			formcontrolname="status" id="status" name="status" onblur="validateCombo(this);" isrequired="true" title="Status">
																			<form:option selected="true" disabled="true" value="-1">Select Status</form:option>
																			<form:option value="START">START</form:option>
																			<form:option value="END">END</form:option>
																		</form:select>
																	</div>
																</div>
															</div>
													</div>
													
													<div  class="row" >
			                                             <div  class="col-sm-6">
			                                                <div  class="form-group row">
			                                                   <label  class="col-md-3 col-form-label" for="startDate">Start Date</label>
			                                                   <div  class="col-md-9">
			                                                       <form:input path="startDate"
			                                                        title="Start Date" validarr="required@@" tovalid="true" onblur="validateTextComponent(this)"
			                                                                class="form-control ng-untouched ng-pristine ng-invalid"
			                                                                formcontrolname="startDate" id="startDate"
			                                                                name="startDate" placeholder="Enter Start Date"
			                                                                type="date"/>
			                                                   </div>
			                                                </div>
			                                             </div>
			                                      </div>
                                                   
                                                   <div  class="row" >
			                                             <div  class="col-sm-6">
			                                                <div  class="form-group row">
			                                                   <label  class="col-md-3 col-form-label" for="endDate">End Date</label>
			                                                   <div  class="col-md-9">
			                                                       <form:input path="endDate"
			                                                        title="End Date" validarr="required@@" tovalid="true" onblur="validateTextComponent(this)"
			                                                                class="form-control ng-untouched ng-pristine ng-invalid"
			                                                                formcontrolname="endDate" id="endDate"
			                                                                name="endDate" placeholder="Enter End Date"
			                                                       type="date"/>
			                                                   </div>
			                                                </div>
			                                             </div>
			                                      </div>
                                                   
                                                   <div  class="row" >
			                                             <div  class="col-sm-6">
			                                                <div  class="form-group row">
			                                                   <label  class="col-md-3 col-form-label" for="duration">Duration</label>
			                                                   <div  class="col-md-9">
			                                                       <form:input path="duration"
			                                                        title="Duration" validarr="required@@" tovalid="true" onblur="validateTextComponent(this)"
			                                                                class="form-control ng-untouched ng-pristine ng-invalid"
			                                                                formcontrolname="duration" id="duration"
			                                                                name="duration" placeholder="Enter Duration"
			                                                       type="text"/>
			                                                   </div>
			                                                </div>
			                                             </div>
			                                      </div>
			                                      
			                                      
			                                      <div  class="row" >
			                                             <div  class="col-sm-6">
			                                                <div  class="form-group row">
			                                                   <label  class="col-md-3 col-form-label" for="remarks">Remarks</label>
			                                                   <div  class="col-md-9">
			                                                       <form:input path="remarks"
			                                                        title="Remarks" validarr="required@@" tovalid="true" onblur="validateTextComponent(this)"
			                                                                class="form-control ng-untouched ng-pristine ng-invalid"
			                                                                formcontrolname="remarks" id="remarks"
			                                                                name="remarks" placeholder="Enter Remarks"
			                                                       type="text"/>
			                                                   </div>
			                                                </div>
			                                             </div>
			                                      </div>
                                                   
                                                   
                                                </div>
                                                
                                                <div class="card-footer" style="height: 52px;">
												<form:button class="btn btn-sm btn-success float-right" type="button" onclick="fnSaveForeignTravel()" disabled="">
														<i class="fa fa-dot-circle-o"></i> <span class="trn">Save</span>
												</form:button>
												</div>
												
                                             </div>
                                          </div>
                                       </div>
                                    </form:form>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div ></div>
         </div>
      </main>
<script	type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/customJS/informationEntry/foreignTravel.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script>
var leadData = '${lstForeignTravel}';
prepareGrid(leadData);
</script>