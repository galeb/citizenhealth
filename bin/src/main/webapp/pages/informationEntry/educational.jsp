<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<script	type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/customJS/common.js"></script>
<script>
   startLoading();
</script>
<style>
    
    .nav-tabs .nav-link.active, .nav-tabs .nav-item.show .nav-link {
    color: #5c6873;
    background-color: rgba(255, 255, 255, 0.9);
    border-color: #c8ced3 #c8ced3 rgba(255, 255, 255, 0.9);
}
</style>
<script type='text/javascript'>
   function hideShow()
   {
    	document.getElementById("aNew").classList.remove("active");
    	document.getElementById("aShow").classList.add("active");
    	document.getElementById("show").style.display = "block";
    	document.getElementById("create").style.display = "none";
   }
   function showHide()
   {
   	
   	document.getElementById("aShow").classList.remove("active");
   	document.getElementById("aNew").classList.add("active");
   	document.getElementById("show").style.display = "none";
   	document.getElementById("create").style.display = "block";				
   }
   
</script>
<main class="main">
   <!-- Breadcrumb-->
   <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="${pageContext.servletContext.contextPath}/index">Home</a></li>
      <li class="breadcrumb-item active">Educational Information Section</li>
   </ol>
   <div class="container-fluid">
      <div  id="ui-view">
         <div >
            <div  class="animated fadeIn">
               <div  class="row">
                  <div  class="col-md-12 mb-12">
                     <ul  class="nav nav-tabs" role="tablist">
                        <li  class="nav-item"><a  id="aShow" aria-controls="home" class="nav-link active" data-toggle="tab" role="tab" onclick='hideShow()'>Educational Information</a></li>
                        <li  class="nav-item"><a  id="aNew"aria-controls="profile" class="nav-link" data-toggle="tab" role="tab" onclick='showHide()'>Create New</a></li>
                     </ul>
                     <div  id="show" class="tab-content">
                        <div  class="tab-pane active" id="home" role="tabpanel">
                           <div  class="col-sm-12">
                              <div  class="card">
                                 <div  class="card-header"><strong >Educational Information</strong></div>
                                 <div  class="card-body">
                                    <div  class="row">
                                       <div  class="col-sm-12">
                                          <form  action="" class="form-horizontal ng-untouched ng-pristine ng-valid" method="post" novalidate="">
                                             <div  class="row">
                                                <div  class="col-md-12">
                                                   <div  class="form-group row">
                                                      <label  class="col-md-3 col-form-label" for="remarks">Search By Employee Code</label>
                                                      <div  class="col-md-9">
                                                         <div  class="input-group"><input  class="form-control" id="input2-group2" name="input2-group2" placeholder="Search By Employee Code" type="email"><span  class="input-group-append"><button  class="btn btn-primary" type="button"><i  class="fa fa-search"></i> Search</button></span></div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </form>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                               <div  class="row">
										<div class="col-sm-12 table-responsive" id="divDatatable">
											<table aria-describedby="DataTables_Table_0_info"
												class="table table-striped table-bordered datatable dataTable no-footer "
												id="myTable" role="grid"
												style="border-collapse: collapse !important">
												<thead id="theadEducational"></thead>								
												<tbody id="tbodyEducational"></tbody>
											</table>
										</div>
									</div>	
                           </div>
                        </div>
                     </div>
                     <div  id="create" class="tab-content" style="display:none">
                        <div  class="tab-pane active" id="profile" role="tabpanel">
                           <form  class="form-horizontal ng-untouched ng-pristine ng-invalid" method="post" novalidate="">
                              <div  class="row">
                                 <div  class="col-sm-12">
                                    <div  class="card">
                                       <div  class="card-header"><strong >Educational Information</strong></div>
                                       <div  class="card-body">
                                          <div  class="row">
                                             <div  class="col-sm-12 col-md-6"></div>
                                             <div  class="col-sm-12 col-md-6"></div>
                                          </div>
                                          <div  class="row">
                                             <div  class="col-sm-12">
                                                <table  aria-describedby="DataTables_Table_0_info" class="table table-striped table-bordered datatable dataTable no-footer" id="DataTables_Table_0" role="grid" style="border-collapse: collapse !important">
                                                   <tbody >
                                                      <tr >
                                                         <th >Employee Code:</th>
                                                         <td ><input  aria-controls="DataTables_Table_0" class="form-control form-control-sm" placeholder="Search..." type="search"></td>
                                                         <th ></th>
                                                         <td  rowspan="4" style="width:150px;"><img  alt="admin@bootstrapmaster.com" height="150px" src="/resources/img/avatars/6.jpg" width="150px"></td>
                                                      </tr>
                                                      <tr >
                                                         <th >Name:</th>
                                                         <td ></td>
                                                         <th >Rank:</th>
                                                      </tr>
                                                      <tr >
                                                         <th >Designation:</th>
                                                         <td ></td>
                                                         <th >Organisation:</th>
                                                      </tr>
                                                      <tr >
                                                         <th >Location:</th>
                                                         <td ></td>
                                                         <th >SPDB:</th>
                                                      </tr>
                                                   </tbody>
                                                </table>
                                             </div>
                                          </div>
                                          <div  class="row">
                                             <div  class="col-sm-6">
                                                <div  class="form-group row">
                                                   <label  class="col-md-3 col-form-label" for="degree_code">Degree Code</label>
                                                   <div  class="col-md-9">
                                                      <select  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="degree_code" id="degree_code" name="degree_code">
                                                         <option  selected="selected" value="">Select Degree Code</option>
                                                         <option  value="Degree Code i">Degree Code i</option>
                                                         <option  value="Degree Code ii">Degree Code ii</option>
                                                      </select>
                                                      <!---->
                                                      <div  class="ng-star-inserted">
                                                         <!---->
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div  class="col-sm-6">
                                                <div  class="form-group row">
                                                   <label  class="col-md-3 col-form-label" for="institute_name">Institute Name</label>
                                                   <div  class="col-md-9">
                                                      <input  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="institute_name" id="institute_name" name="institute_name" type="text"><!---->
                                                      <div  class="ng-star-inserted">
                                                         <!---->
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div  class="row">
                                             <div  class="col-sm-6">
                                                <div  class="form-group row">
                                                   <label  class="col-md-3 col-form-label" for="passing_year">Passing Year</label>
                                                   <div  class="col-md-9">
                                                      <input  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="passing_year" id="passing_year" name="passing_year" type="text"><!---->
                                                      <div  class="ng-star-inserted">
                                                         <!---->
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div  class="col-sm-6">
                                                <div  class="form-group row">
                                                   <label  class="col-md-3 col-form-label" for="degree_level">Degree Level</label>
                                                   <div  class="col-md-9">
                                                      <select  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="degree_level" id="degree_level" name="degree_level">
                                                         <option  selected="selected" value="">Select Any One</option>
                                                         <!---->
                                                         <option  value="30" class="ng-star-inserted">M</option>
                                                         <option  value="31" class="ng-star-inserted">UM</option>
                                                      </select>
                                                      <!---->
                                                      <div  class="ng-star-inserted">
                                                         <!---->
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div  class="row">
                                             <div  class="col-sm-6">
                                                <div  class="form-group row">
                                                   <label  class="col-md-3 col-form-label" for="principal_subject">Principal Subject</label>
                                                   <div  class="col-md-9">
                                                      <select  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="principal_subject" id="principal_subject" name="principal_subject">
                                                         <option  selected="selected" value="">Select Principal Subject</option>
                                                         <!---->
                                                         <option  value="32" class="ng-star-inserted">URBAN LAND APPRAISAL</option>
                                                         <option  value="33" class="ng-star-inserted">RESOURCES MOBILI IN LOCAL GOVT</option>
                                                      </select>
                                                      <!---->
                                                      <div  class="ng-star-inserted">
                                                         <!---->
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div  class="col-sm-6">
                                                <div  class="form-group row">
                                                   <label  class="col-md-3 col-form-label" for="gpa_cgpa">GPA/CGPA</label>
                                                   <div  class="col-md-9">
                                                      <select  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="gpa_cgpa" id="gpa_cgpa" name="gpa_cgpa">
                                                         <option  selected="selected" value="">Select GPA/CGPA</option>
                                                         <!---->
                                                         <option  value="34" class="ng-star-inserted">GPA</option>
                                                         <option  value="35" class="ng-star-inserted">CGPA</option>
                                                      </select>
                                                      <!---->
                                                      <div  class="ng-star-inserted">
                                                         <!---->
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div  class="row">
                                             <div  class="col-sm-6">
                                                <div  class="form-group row">
                                                   <label  class="col-md-3 col-form-label" for="obtain_gpa_cgpa">Obtain GPA/CGPA</label>
                                                   <div  class="col-md-9">
                                                      <input  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="obtain_gpa_cgpa" id="obtain_gpa_cgpa" name="obtain_gpa_cgpa" type="text"><!---->
                                                      <div  class="ng-star-inserted">
                                                         <!---->
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div  class="col-sm-6">
                                                <div  class="form-group row">
                                                   <label  class="col-md-3 col-form-label" for="out_of_gpa_cgpa">Out of GPA/CGPA</label>
                                                   <div  class="col-md-9">
                                                      <input  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="out_of_gpa_cgpa" id="out_of_gpa_cgpa" name="out_of_gpa_cgpa" type="text"><!---->
                                                      <div  class="ng-star-inserted">
                                                         <!---->
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div  class="row">
                                             <div  class="col-sm-6">
                                                <div  class="form-group row">
                                                   <label  class="col-md-3 col-form-label" for="result">Result</label>
                                                   <div  class="col-md-9">
                                                      <select  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="result" id="result" name="result">
                                                         <option  selected="selected" value="">Select Result</option>
                                                         <option  value="Result i">Result i</option>
                                                         <option  value="Result ii">Result ii</option>
                                                      </select>
                                                      <!---->
                                                      <div  class="ng-star-inserted">
                                                         <!---->
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div  class="col-sm-6">
                                                <div  class="form-group row">
                                                   <label  class="col-md-3 col-form-label" for="result_level">Result Level</label>
                                                   <div  class="col-md-9">
                                                      <select  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="result_level" id="result_level" name="result_level">
                                                         <option  selected="selected" value="">Select Result Level</option>
                                                         <!---->
                                                         <option  value="36" class="ng-star-inserted">ONE</option>
                                                         <option  value="37" class="ng-star-inserted">TWO</option>
                                                      </select>
                                                      <!---->
                                                      <div  class="ng-star-inserted">
                                                         <!---->
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div  class="row">
                                             <div  class="col-sm-6">
                                                <div  class="form-group row">
                                                   <label  class="col-md-3 col-form-label" for="is_foreign">Is Foreign</label>
                                                   <div  class="col-md-9">
                                                      <select  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="is_foreign" id="is_foreign" name="is_foreign">
                                                         <option  selected="selected" value="">Select Is Foreign</option>
                                                         <option  value="Is Foreign i">Is Foreign i</option>
                                                         <option  value="Is Foreign ii">Is Foreign ii</option>
                                                      </select>
                                                      <!---->
                                                   </div>
                                                </div>
                                             </div>
                                             <div  class="col-sm-6">
                                                <div  class="form-group row">
                                                   <label  class="col-md-3 col-form-label" for="position">Position</label>
                                                   <div  class="col-md-9">
                                                      <select  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="position" id="position" name="position">
                                                         <option  selected="selected" value="">Select Position</option>
                                                         <!---->
                                                         <option  value="38" class="ng-star-inserted">FIRST</option>
                                                         <option  value="39" class="ng-star-inserted">SECOND</option>
                                                      </select>
                                                      <!---->
                                                      <div  class="ng-star-inserted">
                                                         <!---->
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div  class="row">
                                             <div  class="col-sm-6">
                                                <div  class="form-group row">
                                                   <label  class="col-md-3 col-form-label" for="distinction">Distinction</label>
                                                   <div  class="col-md-9">
                                                      <select  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="distinction" id="distinction" name="distinction">
                                                         <option  selected="selected" value="">Select Distinction</option>
                                                         <!---->
                                                         <option  value="40" class="ng-star-inserted">STAND</option>
                                                         <option  value="41" class="ng-star-inserted">START</option>
                                                      </select>
                                                      <!---->
                                                      <div  class="ng-star-inserted">
                                                         <!---->
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <input  formcontrolname="Educationalinfo_id" id="Educationalinfo_id" name="Educationalinfo_id" type="hidden" value="" class="ng-untouched ng-pristine ng-valid">
                                       <div  class="card-footer"><button  class="btn btn-sm btn-success float-right" type="button" disabled=""><i  class="fa fa-dot-circle-o"></i> Save </button></div>
                                    </div>
                                 </div>
                              </div>
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div ></div>
   </div>
</main>
<script	type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/customJS/informationEntry/educational.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script>
   var leadData = '${lstAward}';
   prepareGrid(leadData);
</script>