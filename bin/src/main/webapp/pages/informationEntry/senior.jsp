<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<script	type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/customJS/common.js"></script>
<script>
   startLoading();
</script>
<style>
    
    .nav-tabs .nav-link.active, .nav-tabs .nav-item.show .nav-link {
    color: #5c6873;
    background-color: rgba(255, 255, 255, 0.9);
    border-color: #c8ced3 #c8ced3 rgba(255, 255, 255, 0.9);
}
</style>
<script type='text/javascript'>
   function hideShow()
   {
    	document.getElementById("aNew").classList.remove("active");
    	document.getElementById("aShow").classList.add("active");
    	document.getElementById("show").style.display = "block";
    	document.getElementById("create").style.display = "none";
   }
   function showHide()
   {
   	
   	document.getElementById("aShow").classList.remove("active");
   	document.getElementById("aNew").classList.add("active");
   	document.getElementById("show").style.display = "none";
   	document.getElementById("create").style.display = "block";				
   }
   
</script>
      <main class="main">
         <!-- Breadcrumb-->
         <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="${pageContext.servletContext.contextPath}/index">Home</a></li>
            <li class="breadcrumb-item active">Senior Information Section</li>
         </ol>
         <div class="container-fluid">
            <div  id="ui-view">
               <div >
                  <div  class="animated fadeIn">
                     <div  class="row">
                        <div  class="col-md-12 mb-12">
                           <ul  class="nav nav-tabs" role="tablist">
                              <li  class="nav-item"><a  id="aShow" aria-controls="home" class="nav-link active" data-toggle="tab" role="tab" onclick='hideShow()'>Senior Scale</a></li>
                              <li  class="nav-item"><a  id="aNew"aria-controls="profile" class="nav-link" data-toggle="tab" role="tab" onclick='showHide()'>Create New</a></li>
                           </ul>
                           <div id="show" class="tab-content">
                              <div  class="tab-pane active" id="home" role="tabpanel">
                                 <div  class="col-sm-12">
                                    <div  class="card">
                                       <div  class="card-header"><strong >Senior Scale Exam Entry in Group</strong></div>
                                       <div  class="card-body">
                                          <div  class="row">
                                             <div  class="col-sm-12">
                                                <form  action="" class="form-horizontal ng-untouched ng-pristine ng-valid" method="post" novalidate="">
                                                   <div  class="row">
                                                      <div  class="col-md-12">
                                                         <div  class="form-group row">
                                                            <label  class="col-md-3 col-form-label" for="remarks">Search By Employee Code</label>
                                                            <div  class="col-md-9">
                                                               <div  class="input-group"><input  class="form-control" id="input2-group2" name="input2-group2" placeholder="Search By Employee Code" type="email"><span  class="input-group-append"><button  class="btn btn-primary" type="button"><i  class="fa fa-search"></i> Search</button></span></div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </form>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div  class="row">
                                       <div class="col-sm-12 table-responsive" id="divDatatable">
                                          <table aria-describedby="DataTables_Table_0_info"
                                             class="table table-striped table-bordered datatable dataTable no-footer "
                                             id="myTable" role="grid"
                                             style="border-collapse: collapse !important">
                                             <thead id="theadSenior"></thead>
                                             <tbody id="tbodySenior"></tbody>
                                          </table>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div id="create" class="tab-content" style="display:none">
                              <div  class="tab-pane active" id="profile" role="tabpanel">
                                 <form  class="form-horizontal ng-untouched ng-pristine ng-invalid" novalidate="">
                                    <div  class="row">
                                       <div  class="col-sm-12">
                                          <div  class="card">
                                             <div  class="card-header"><strong >Senior Scale Exam Entry in Group</strong></div>
                                             <div  class="card-body">
                                                <div  class="row">
                                                   <div  class="col-sm-12"><strong >Add New Scale Exam</strong></div>
                                                </div>
                                                <div  class="row">
                                                   <!---->
                                                   <div  class="col-sm-12 ng-untouched ng-pristine ng-invalid ng-star-inserted" formarrayname="items">
                                                      <table  class="table table-striped table-bordered datatable dataTable no-footer ng-untouched ng-pristine ng-invalid">
                                                         <thead >
                                                            <tr  role="row">
                                                               <th  class="sorting" colspan="1" rowspan="1" style="width: 201px;" tabindex="0"> Paper (1) Gazette Date </th>
                                                               <th  class="sorting" colspan="1" rowspan="1" style="width: 158px;" tabindex="0"></th>
                                                               <th  class="sorting" colspan="1" rowspan="1" style="width: 150px;" tabindex="0"></th>
                                                               <th  class="sorting" colspan="1" rowspan="1" style="width: 150px;" tabindex="0"></th>
                                                               <th  class="sorting" colspan="1" rowspan="1" style="width: 150px;" tabindex="0"></th>
                                                               <th  class="sorting" colspan="1" rowspan="1" style="width: 88px;" tabindex="0"></th>
                                                            </tr>
                                                         </thead>
                                                         <tbody >
                                                            <tr  class="odd" role="row">
                                                               <td >Remarks</td>
                                                               <td  colspan="2"><input  class="form-control form-control-sm ng-untouched ng-pristine ng-invalid" formcontrolname="remark" placeholder="Remarks" type="text"></td>
                                                               <td >Date</td>
                                                               <td ><input  class="form-control form-control-sm ng-untouched ng-pristine ng-invalid" formcontrolname="date" placeholder="dd/mm/yyyy" type="date"></td>
                                                               <td ></td>
                                                            </tr>
                                                            <tr  class="odd" role="row">
                                                               <td >Detail Information</td>
                                                               <td ></td>
                                                               <td ></td>
                                                               <td ></td>
                                                               <td ></td>
                                                               <td ></td>
                                                            </tr>
                                                            <tr  class="odd" role="row">
                                                               <td >Govt. Id</td>
                                                               <td ><input  class="form-control form-control-sm ng-untouched ng-pristine ng-invalid" formcontrolname="govt_id" placeholder="Govt. Id" type="text"></td>
                                                               <td >Roll No</td>
                                                               <td ><input  class="form-control form-control-sm ng-untouched ng-pristine ng-invalid" formcontrolname="roll_no" placeholder="Roll No" type="text"></td>
                                                               <td ><a  href="#" style="margin-right: 50px">SPDS</a><a  href="#">LPDS</a></td>
                                                               <td >
                                                                  <!----><a  class="cursor-pointer ng-star-inserted"><i  class="fa fa-search-plus"></i>Add More</a><!---->
                                                               </td>
                                                            </tr>
                                                         </tbody>
                                                      </table>
                                                   </div>
                                                </div>
                                             </div>
                                             <div  class="card-footer"><button  class="btn btn-sm btn-success float-right" type="submit"><i  class="fa fa-dot-circle-o"></i> Save </button></div>
                                          </div>
                                       </div>
                                    </div>
                                 </form>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div ></div>
         </div>
      </main>
<script	type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/customJS/informationEntry/senior.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script>
var leadData = '${lstAward}';
prepareGrid(leadData);
</script>