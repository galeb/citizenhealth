<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<script	type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/customJS/common.js"></script>
<script>
   startLoading();
</script>
<style>
    
    .nav-tabs .nav-link.active, .nav-tabs .nav-item.show .nav-link {
    color: #5c6873;
    background-color: rgba(255, 255, 255, 0.9);
    border-color: #c8ced3 #c8ced3 rgba(255, 255, 255, 0.9);
}
</style>
<script type='text/javascript'>
   function hideShow()
   {
    	document.getElementById("aNew").classList.remove("active");
    	document.getElementById("aShow").classList.add("active");
    	document.getElementById("show").style.display = "block";
    	document.getElementById("create").style.display = "none";
   }
   function showHide()
   {
	   	document.getElementById("aShow").classList.remove("active");
	   	document.getElementById("aNew").classList.add("active");
	   	document.getElementById("show").style.display = "none";
	   	document.getElementById("create").style.display = "block";				
   }
   
</script>
      <main class="main">
         <!-- Breadcrumb-->
         <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="${pageContext.servletContext.contextPath}/index">Home</a></li>
            <li class="breadcrumb-item active">Promotion Information</li>
         </ol>
         <div class="container-fluid">
            <div  id="ui-view">
               <div >
                  <div  class="animated fadeIn">
                     <div  class="row">
                        <div  class="col-md-12 mb-12">
                           <ul  class="nav nav-tabs" role="tablist">
                              <li  class="nav-item"><a  id="aShow" aria-controls="home" class="nav-link active" data-toggle="tab" role="tab" onclick='hideShow()'>Promotion</a></li>
                              <li  class="nav-item"><a  id="aNew"aria-controls="profile" class="nav-link" data-toggle="tab" role="tab" onclick='showHide()'>Create New</a></li>
                           </ul>
                           <div id="show" class="tab-content">
                              <div  class="tab-pane active" id="home" role="tabpanel">
                                 <div  class="col-sm-12">
                                    <div  class="card">
                                       <div  class="card-header"><strong >Promotion</strong></div>
                                       <div  class="card-body">
                                          <div  class="row">
                                             <div  class="col-sm-12">
                                                <form  action="" class="form-horizontal ng-untouched ng-pristine ng-valid" method="post" novalidate="">
                                                   <div  class="row">
                                                      <div  class="col-md-12">
                                                         <div  class="form-group row">
                                                            <label  class="col-md-3 col-form-label" for="remarks">Search By Employee Code</label>
                                                            <div  class="col-md-9">
                                                               <div  class="input-group"><input  class="form-control" id="input2-group2" name="input2-group2" placeholder="Search By Employee Code" type="email"><span  class="input-group-append"><button  class="btn btn-primary" type="button"><i  class="fa fa-search"></i> Search</button></span></div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </form>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    
									 <div  class="row">
										<div class="col-sm-12 table-responsive" id="divDatatable">
											<table aria-describedby="DataTables_Table_0_info"
												class="table table-striped table-bordered datatable dataTable no-footer "
												id="myTable" role="grid"
												style="border-collapse: collapse !important">
												<thead id="theadPromotion"></thead>								
												<tbody id="tbodyPromotion"></tbody>
											</table>
										</div>
									</div>	
									
                                 </div>
                              </div>
						  </div>


                           <div id="create" class="tab-content" style="display:none">
                              <div  class="tab-pane active" id="profile" role="tabpanel">
                                 <div>
                                  <form:form role="form" name="promotionform" action="${pageContext.servletContext.contextPath}/promotion/save" 
                                  method="post" modelAttribute="modelPromotion" enctype="multipart/form-data" > 
                                   <form:hidden path="promotionPk" name="promotionPk"  id="promotionPk"/>
                                   
                                       <div  class="row">
                                          <div  class="col-sm-12">
                                             <div  class="card">
                                                <div  class="card-header"><strong >Promotion</strong></div>
                                                <div  class="card-body">
                                                   <div  class="row">
                                                      <div  class="col-sm-6">
                                                         <div  class="form-group row">
                                                            <label  class="col-md-3 col-form-label" for="natureofPromotion">Nature of Promotion</label>
                                                            <div  class="col-md-9">
                                                            <form:select path="natureofPromotion"
																class="form-control ng-untouched ng-pristine ng-valid"
																formcontrolname="natureofPromotion" id="natureofPromotion" name="natureofPromotion" onblur="validateCombo(this);" isrequired="true" title="Nature of Promotion">
																<form:option selected="selected" value="-1">Select Nature of Promotion</form:option>
																<form:option value="FIRST CLASS">FIRST CLASS</form:option>
																<form:option value="SECON CLASS">SECON CLASS</form:option>
															</form:select>
															
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div  class="col-sm-6">
                                                         <div  class="form-group row">
                                                            <label  class="col-md-3 col-form-label" for="promotionDate">Promotion Date</label>
                                                            <div  class="col-md-9">
                                                              <form:input path="promotionDate"
																title="Promotion Date" validarr="required@@" tovalid="true"  onblur="validateTextComponent(this)"
																	class="form-control ng-untouched ng-pristine ng-invalid"
																	formcontrolname="promotionDate" id="promotionDate"
																	name="promotionDate" placeholder="Enter Promotion Date"
																	type="date"/>
                                                               <div  class="ng-star-inserted">
                                                                  <!---->
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div  class="row">
                                                      <div  class="col-sm-6">
                                                         <div  class="form-group row">
                                                            <label  class="col-md-3 col-form-label" for="goDate">GO Date</label>
                                                            <div  class="col-md-9">
                                                              <form:input path="goDate"
															title="GO Date" validarr="required@@" tovalid="true"  onblur="validateTextComponent(this)"
																class="form-control ng-untouched ng-pristine ng-invalid"
																formcontrolname="goDate" id="goDate"
																name="goDate" placeholder="Enter GO Date"
																type="date"/>
                                                               <div  class="ng-star-inserted">
                                                                  <!---->
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div  class="col-sm-6">
                                                         <div  class="form-group row">
                                                            <label  class="col-md-3 col-form-label" for="payScale">Pay Scale</label>
                                                            <div  class="col-md-9">
                                                              <form:select path="payScaleFk.payScalePk" 
																class="form-control ng-untouched ng-pristine ng-valid"
																formcontrolname="payScaleFk" id="payScaleFk" name="payScaleFk.payScalePk" onblur="validateCombo(this);" isrequired="true" title="Pay Scale">
																<form:option disabled="true" selected="selected"  value="-1">Select Pay Scale</form:option>
																<c:forEach items="${allPayScale}" var="entry">
																	<form:option value="${entry.payScalePk}">${entry.scale}</form:option>	
																</c:forEach>
															</form:select>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div  class="row">
                                                      <div  class="col-sm-6">
                                                         <div  class="form-group row">
                                                            <label  class="col-md-3 col-form-label" for="rank">Rank</label>
                                                            <div  class="col-md-9">
                                                            <form:select path="rankFk.rankPk" 
																class="form-control ng-untouched ng-pristine ng-valid"
																formcontrolname="rankFk" id="rankFk" name="rankFk.rankPk" onblur="validateCombo(this);" isrequired="true" title="Rank">
																<form:option disabled="true" selected="selected"  value="-1">Select Rank</form:option>
																<c:forEach items="${allRank}" var="entry">
																	<form:option value="${entry.rankPk}">${entry.rankName}</form:option>	
																</c:forEach>
															</form:select>
															
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div  class="col-sm-6">
                                                         <div  class="form-group row">
                                                            <label  class="col-md-3 col-form-label" for="rule">Rule</label>
                                                            <div  class="col-md-9">
                                                            <form:select path="rule"
																	class="form-control ng-untouched ng-pristine ng-valid"
																	formcontrolname="rule" id="rule" name="rule" onblur="validateCombo(this);" isrequired="true" title="Rule">
																	<form:option selected="selected" value="-1">Select Rule</form:option>
																	<form:option value="275">275</form:option>
																	<form:option value="380">380</form:option>
															</form:select>
                                                            
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div  class="row">
                                                      <div  class="col-sm-6">
                                                         <div  class="form-group row">
                                                            <label  class="col-md-3 col-form-label" for="remarks">Remarks</label>
                                                            <div  class="col-md-9">
                                                             <form:input path="remarks"
															title="Remarks" validarr="required@@" tovalid="true"  onblur="validateTextComponent(this)"
																class="form-control ng-untouched ng-pristine ng-invalid"
																formcontrolname="remarks" id="remarks"
																name="remarks" placeholder="Enter Remarks"
																type="text"/>
                                                               <div  class="ng-star-inserted">
                                                                  <!---->
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div  class="col-sm-6">
                                                         <div  class="form-group row">
                                                            <label  class="col-md-3 col-form-label" for="details">Details</label>
                                                            <div  class="col-md-9">
                                                               <form:input path="details"
															title="Details" validarr="required@@" tovalid="true"  onblur="validateTextComponent(this)"
																class="form-control ng-untouched ng-pristine ng-invalid"
																formcontrolname="details" id="details"
																name="details" placeholder="Enter Details"
																type="text"/>
                                                               <div  class="ng-star-inserted">
                                                                  <!---->
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div  class="row">
                                                      <div  class="col-sm-6">
                                                         <div  class="form-group row">
                                                            <label  class="col-md-3 col-form-label" for="Image">Image</label>
                                                            <div  class="col-md-9">
                                                               <form:input id="inpFileDoc" path="fileDoc" type="file" name="fileDoc" />
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div  class="col-sm-6"></div>
                                                   </div>
                                                </div>
                                                
                                              <div class="card-footer" style="height: 52px;">
													<form:button class="btn btn-sm btn-success float-right" type="button" onclick="fnSavePromotion()"
														disabled="">
														<i class="fa fa-dot-circle-o"></i> <span class="trn">Save</span>
													</form:button>
											</div>
                                             </div>
                                          </div>
                                       </div>
                                    </form:form>
                                 </div>
                              </div>
                           </div>
                        
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div ></div>
         </div>
      </main>
<script	type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/customJS/informationEntry/promotion.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script>
var leadData = '${lstPromotion}';
prepareGrid(leadData);
</script>