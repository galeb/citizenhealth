<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<script	type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/customJS/common.js"></script>
<script>
   startLoading();
</script>
<style>
   .nav-tabs .nav-link.active, .nav-tabs .nav-item.show .nav-link {
   color: #5c6873;
   background-color: rgba(255, 255, 255, 0.9);
   border-color: #c8ced3 #c8ced3 rgba(255, 255, 255, 0.9);
   }
</style>
<script type='text/javascript'>
   function hideShow()
   {
    	document.getElementById("aNew").classList.remove("active");
    	document.getElementById("aShow").classList.add("active");
    	document.getElementById("show").style.display = "block";
    	document.getElementById("create").style.display = "none";
   }
   function showHide()
   {
   	
   	document.getElementById("aShow").classList.remove("active");
   	document.getElementById("aNew").classList.add("active");
   	document.getElementById("show").style.display = "none";
   	document.getElementById("create").style.display = "block";				
   }
   
</script>
<main class="main">
   <!-- Breadcrumb-->
   <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="${pageContext.servletContext.contextPath}/index"><span class="trn">Home</span></a></li>
      <li class="breadcrumb-item active"><span class="trn">Organisation Component</span></li>
   </ol>
   <div class="container-fluid">
      <div  id="ui-view">
         <div >
            <div  class="animated fadeIn">
               <div  class="row">
                  <div  class="col-md-12 mb-12">
                     <ul  class="nav nav-tabs" role="tablist">
                        <li  class="nav-item"><a  id="aShow" aria-controls="home" class="nav-link active" data-toggle="tab" role="tab" onclick='hideShow()'>Organisation Details</a></li>
                        <li  class="nav-item"><a  id="aNew"aria-controls="profile" class="nav-link" data-toggle="tab" role="tab" onclick='showHide()'>Create New</a></li>
                     </ul>
                     <div id="show" class="tab-content">
                        <div  class="tab-pane active" id="home" role="tabpanel">
                           <div  class="col-sm-12">
                              <div  class="card">
                                 <div  class="card-header"><strong >Organisation Section</strong></div>
                              </div>
                              <div  class="row">
                                 <div class="col-sm-12 table-responsive" id="divDatatable">
                                    <table aria-describedby="DataTables_Table_0_info"
                                       class="table table-striped table-bordered datatable dataTable no-footer "
                                       id="myTable" role="grid"
                                       style="border-collapse: collapse !important">
                                       <thead id="theadOrganisation"></thead>
                                       <tbody id="tbodyOrganisation"></tbody>
                                    </table>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     
                     <div id="create" class="tab-content" style="display:none">
                        <div  class="tab-pane active" id="profile" role="tabpanel">
                           <form:form role="form" name="organisationform" action="${pageContext.servletContext.contextPath}/organisation/save" onsubmit="return false;" method="post" modelAttribute="modelOrganisation" >
                              <div  class="row">
                                 <div  class="col-sm-12">
                                    <div  class="card">
                                       <div  class="card-body">
                                          <form:hidden path="organisationPk" name="organisationPk"  id="organisationPk"/>
                                          <div  class="row">
                                             <div class="col-sm-6">
                                                <div class="form-group row">
                                                   <label class="col-md-3 col-form-label" for="organisationType"><span class="trn">Organization Type</span></label>
                                                   <div class="col-md-9">
                                                      <form:select path="organisationType"
                                                         class="form-control ng-untouched ng-pristine ng-valid"
                                                         formcontrolname="organisationType" id="organisationType" name="organisationType" onblur="validateCombo(this);" isrequired="true" title="Organisation Type">
                                                         <form:option selected="selected" value="-1">Select Organisation Type</form:option>
                                                         <form:option value="Ministry">Ministry</form:option>
                                                         <form:option value="Divison">Divison</form:option>
                                                         <form:option value="Department">Department</form:option>
                                                      </form:select>
                                                      <!---->
                                                      <div class="ng-star-inserted">
                                                         <!---->
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div  class="col-sm-6">
                                                <div  class="form-group row">
                                                   <label  class="col-md-3 col-form-label" for="organisationAddress"><span class="trn">Organisation Address</span></label>
                                                   <div  class="col-md-9">
                                                      <form:input path="organisationAddress"
                                                         title="Organisation Address" validarr="required@@" tovalid="true"  onblur="validateTextComponent(this)"
                                                         class="form-control ng-untouched ng-pristine ng-invalid"
                                                         formcontrolname="organisationAddress" id="organisationAddress"
                                                         name="siNo" placeholder="Enter Organisation Address"
                                                         type="text"/>
                                                      <div  class="ng-star-inserted">
                                                         <!---->
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div  class="row">
                                             <div class="col-sm-6">
                                                <div class="form-group row">
                                                   <label class="col-md-3 col-form-label" for="parentOrganisation"><span class="trn">Parent Organisation</span></label>
                                                   <div class="col-md-9">
                                                      <form:select path="parentOrganisation"
                                                         class="form-control ng-untouched ng-pristine ng-valid"
                                                         formcontrolname="parentOrganisation" id="parentOrganisation" name="parentOrganisation" onblur="validateCombo(this);" isrequired="true" title="Parent Organisation">
                                                         <form:option selected="selected" value="-1">Select Parent Organisation</form:option>
                                                         <form:option value="test">test</form:option>
                                                      </form:select>
                                                      <!---->
                                                      <div class="ng-star-inserted">
                                                         <!---->
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div  class="col-sm-6">
                                                <div  class="form-group row">
                                                   <label  class="col-md-3 col-form-label" for="organisationMobile"><span class="trn">Organisation Mobile</span></label>
                                                   <div  class="col-md-9">
                                                      <form:input path="organisationMobile"
                                                         title="Organisation Mobile" validarr="required@@numeric" tovalid="true"  onblur="validateTextComponent(this)"
                                                         class="form-control ng-untouched ng-pristine ng-invalid"
                                                         formcontrolname="organisationMobile" id="organisationMobile"
                                                         name="organisationMobile" placeholder="Enter Organisation Mobile"
                                                         type="text"/>
                                                      <div  class="ng-star-inserted">
                                                         <!---->
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div  class="row">
                                             <div  class="col-sm-6">
                                                <div  class="form-group row">
                                                   <label  class="col-md-3 col-form-label" for="organisationName"><span class="trn">Organisation Name</span></label>
                                                   <div  class="col-md-9">
                                                      <form:input path="organisationName"
                                                         title="Organisation Name" validarr="required@@" tovalid="true"  onblur="validateTextComponent(this)"
                                                         class="form-control ng-untouched ng-pristine ng-invalid"
                                                         formcontrolname="organisationName" id="organisationName"
                                                         name="organisationName" placeholder="Enter Organisation Name"
                                                         type="text"/>
                                                      <div  class="ng-star-inserted">
                                                         <!---->
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div  class="col-sm-6">
                                                <div  class="form-group row">
                                                   <label  class="col-md-3 col-form-label" for="organisationTelephone"><span class="trn">Organisation Telephone</span></label>
                                                   <div  class="col-md-9">
                                                      <form:input path="organisationTelephone"
                                                         title="Organisation Telephone" validarr="required@@numeric" tovalid="true"  onblur="validateTextComponent(this)"
                                                         class="form-control ng-untouched ng-pristine ng-invalid"
                                                         formcontrolname="organisationTelephone" id="organisationTelephone"
                                                         name="siNo" placeholder="Enter Organisation Telephone"
                                                         type="text"/>
                                                      <div  class="ng-star-inserted">
                                                         <!---->
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div  class="row">
                                             <div  class="col-sm-6">
                                                <div  class="form-group row">
                                                   <label  class="col-md-3 col-form-label" for="nameNative"><span class="trn">Name Native</span></label>
                                                   <div  class="col-md-9">
                                                      <form:input path="nameNative"
                                                         title="Name Native" validarr="required@@" tovalid="true"  onblur="validateTextComponent(this)"
                                                         class="form-control ng-untouched ng-pristine ng-invalid"
                                                         formcontrolname="nameNative" id="nameNative"
                                                         name="nameNative" placeholder="Enter Name Native"
                                                         type="text"/>
                                                      <div  class="ng-star-inserted">
                                                         <!---->
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div  class="col-sm-6">
                                                <div  class="form-group row">
                                                   <label  class="col-md-3 col-form-label" for="organisationEmail"><span class="trn">Organisation Email</span></label>
                                                   <div  class="col-md-9">
                                                      <form:input path="organisationEmail"
                                                         title="Organisation Email" validarr="required@@email" tovalid="true"  onblur="validateTextComponent(this)"
                                                         class="form-control ng-untouched ng-pristine ng-invalid"
                                                         formcontrolname="Organisation Email" id="organisationEmail"
                                                         name="organisationEmail" placeholder="Enter Organisation Email"
                                                         type="text"/>
                                                      <div  class="ng-star-inserted">
                                                         <!---->
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div  class="row">
                                             <div  class="col-sm-6">
                                                <div  class="form-group row">
                                                   <label  class="col-md-3 col-form-label" for="location"><span class="trn">Location</span></label>
                                                   <div  class="col-md-9">
                                                      <form:input path="location"
                                                         title="Location" validarr="required@@" tovalid="true"  onblur="validateTextComponent(this)"
                                                         class="form-control ng-untouched ng-pristine ng-invalid"
                                                         formcontrolname="location" id="location"
                                                         name="location" placeholder="Enter Location"
                                                         type="text"/>
                                                      <div  class="ng-star-inserted">
                                                         <!---->
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div  class="col-sm-6">
                                                <div  class="form-group row">
                                                   <label  class="col-md-3 col-form-label" for="remarks"><span class="trn">Remarks</span></label>
                                                   <div  class="col-md-9">
                                                      <form:input path="remarks"
                                                         title="Remarks" validarr="required@@" tovalid="true"  onblur="validateTextComponent(this)"
                                                         class="form-control ng-untouched ng-pristine ng-invalid"
                                                         formcontrolname="remarks" id="remarks"
                                                         name="remarks" placeholder="Enter Remarks"
                                                         type="text"/>
                                                      <div  class="ng-star-inserted">
                                                         <!---->
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="card-footer" style="height: 52px;">
                                          <form:button class="btn btn-sm btn-success float-right" type="button" onclick="fnSaveOrganisation()"
                                             disabled="">
                                             <i class="fa fa-dot-circle-o"></i> <span class="trn">Save</span>
                                          </form:button>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </form:form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div ></div>
   </div>
</main>
<script	type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/customJS/mastertable/organisation.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script>
   var leadData = '${lstOrganisation}';
   prepareGrid(leadData);
</script>