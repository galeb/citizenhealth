<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="keywords" content="Creative , Creative Agency template, Creative, Agency, Parallax, parallax template, landing page, material design, Corporate, Multipurpose, Business">
        <link rel="icon" type="image/png" href="${pageContext.servletContext.contextPath}/resources/img/brand/favicon.png">
    	<title>Health Information System</title>

        <!-- favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="${pageContext.servletContext.contextPath}/resources/login/assets/img/favicon.ico">


        <!-- style css -->
        <link rel="stylesheet" href="${pageContext.servletContext.contextPath}/resources/login/style.css">
        <link rel="stylesheet" href="bootstrap.min.css">
        <link rel="stylesheet" href="responsiveslides.css">        
        <!-- modernizr js -->
        <script src="${pageContext.servletContext.contextPath}/resources/login/assets/js/vendor/modernizr-2.8.3.min.js"></script>

        <style>   
            #cssmenu ul ul {
                display: none;
            }
			
        </style>

    </head>
    <body>
        <div class="wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="full-body">
                            <header class="header">
                                <div class="header_img">
                                            <a href="#"><img src="${pageContext.servletContext.contextPath}/resources/login/assets/img/mopa-banner.jpg"></a>
                                 </div>                   
                                
                            </header>
                            <div class="maincontent">
                                <div class="row">
                                    <div class="col-md-5">
										<div class="fstprt">
											<h3>To get support please contact</h3>
											<p>Senior Systems Analyst,
												<br>
												Public Administration Computer Center,
												<br>
												Ministry of Public Administration, Dhaka-1000
												<br>
												Telephone: +88029540485
												<br>
												E-mail: Info@mopa.gov.bd
											</p>
										</div>
									</div>
                                    <div class="col-md-7">
										<div class="inner-content">
											<div class="content">
												<div class="row">
													<form:form role="form" action="${pageContext.servletContext.contextPath}/user/authenticate" method="post" modelAttribute="userModel" >
                
														<label class="tess"for="name">User Name:</label>
														  <form:input class="box" path="userName" name="userName" placeholder="Username"/></br>  
														
														<label class="tess"for="name">Password:</label>
														<form:password class="box" path="password" name="password" placeholder="Password"/></br>  
														
														<label class="errormsg" id="lblInvalidCredentials" style="color:#e61a1a"></label>        
														
														<div class="header_img">
															<a href="#"><img src="${pageContext.servletContext.contextPath}/resources/login/assets/img/captcha.png"></a>
														</div>  
														<div class="input-group" style="margin-left: 24px">
															<div class="checkbox">
																<label>
																  <input id="login-remember" type="checkbox" name="remember" value="1"> Remember me
																</label>
															  </div>
														</div>
														
<!-- 														<div class="col-md-12"> -->
<%-- 															<span class='forgot-pass'><a href="${pageContext.servletContext.contextPath}/signup">Create new user?</a></span> --%>
<!-- 														</div> -->
														<div class="col-md-12">
															<div class="log-btn">
																<input class="inlog" type="submit" value="Login"/>
															</div>
														</div>
														
													</form:form>
												</div>					
											</div>					
										</div>      
											
										
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="footer_area">
                                <div class="row">
                                    <div class="col-md-12">
                                        <p>© 2019 Ministry of Public Administration, Government of the People's Republic of Bangladesh. All rights reserved.<br>
												Design & Developed By IBCS-PRIMAX</p>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="${pageContext.servletContext.contextPath}/resources/login/assets/js/bootstrap.min.js"></script>
        <script>
    var resposneMessage =  '${resposneMessage}';
    if(resposneMessage !== ''){
    	$('#lblInvalidCredentials').html('<b>' + resposneMessage + '</b>');
    } else {
      $('#lblInvalidCredentials').html('');
    }
    </script>
    </body>

</html>
