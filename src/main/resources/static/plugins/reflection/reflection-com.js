/**
 @Created By : Reflection
 @Date       : 2019.05.01 11:28:55 AM

 @Author     : Md. Hoshen Mahmud Khan
 @Email      : saif_hmk@live.com
 @Phone      : +88 01672 036757
 **/
//======================================================================================================================================================
function hideAll() {
    $(".hidden").hide();
}

function validateRequiredFields() {
    clearErrors();
    setTimeout(function(){
        //waiting for initialization...
    }, 500);
    var isValid = true;
    $(".required").each(function (e) {
        var v = $(this).val();
        if (v == null || v == "") {
            isValid = false;
            $(this).addClass("has-field-error");
            $(this).parent().parent().addClass("has-error");
        }
    });
    return isValid;
}

function clearErrors() {
    $(".has-field-error").each(function () {
        $(this).removeClass("has-field-error");
    });
    $(".has-error").each(function () {
        $(this).removeClass("has-error");
    });
}

function generateDatePicker() {
    $('.dtp-date').datepicker({
        autoclose: true,
        multidate: false,
        format: 'dd/mm/yyyy',
        clearBtn: true,
        keyboardNavigation: true,
        todayHighlight: true,
        weekStart: 0//,
        // startDate         : new Date()
    })
        .prop('placeholder', "DD/MM/YYYY")
        .prop('autocomplete', "OFF");
}

function generateDataTable(){
    $('.dt-default').DataTable();
}

$(document).ready(function (f) {
    validateFormElements();
});

function validateFormElements() {
    $(".required").prop("required", true);
    $(".disabled").prop("disabled", true);
    $(".dtp-time").prop("placeholder", "HH:MM");
    generateDatePicker();
    generateDataTable();
}

function showStatusError(message){
    $('#errorMsg').html('');
    $('#errorMsg').html(message);
    $('.errorMsgBlock').stop().fadeIn(500).delay(180000).fadeOut(400);
}

function showStatusWarning(message){
    $('#warningMsg').html('');
    $('#warningMsg').html(message);
    $('.warningMsgBlock').stop().fadeIn(500).delay(15000).fadeOut(400);
}

function showStatusSuccess(message){
    $('#successMsg').html('');
    $('#successMsg').html(message);
    $('.successMsgBlock').stop().fadeIn(500).delay(15000).fadeOut(400);
}

//======================================================================================================================================================
