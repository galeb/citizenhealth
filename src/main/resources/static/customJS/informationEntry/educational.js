var dataTable='';
function fnSaveEducational() {
	if(valOnSubmit()){
		startLoading();
		$.post({
			url : path + '/educational/save',
			data : $('form[name=educationalform]').serialize(),
			success : function(response) {
				if(response == 'SuccessFullySaved'){
					showSuccess('<b>Success!</b><br> educational record is sucessfully created');					
					afterSaveDataGet();
				}else{
					showError('<b>ERROR!</b><br> Educational record is not sucessfully created');	
					hideLoading();
				}				
			}
		});
	}
}

function afterSaveDataGet(){
	$.get({
		url : path + '/educational/fetchAll',
		success : function(response) {			
			if(response != ''){
				resetForm();
				hideShow();
				prepareGrid(response);
			}else{
				showError('<b>ERROR!</b><br> Educational records are not fetch sucessfully');	
			}				
		}
	})
}

function prepareGrid(leadData){			
console.log(leadData);
	
	destroyTable('myTable');
	var headArr = ['Id','Employee Details','Degree Code','Institute Name','Passing Year','Degree Level','Principal Subject','Gpa Cgpa','Obtain Gpa Cgpa','Out of Gpa Cgpa','Result','Result Level','Is Foreign','Position','Distinction','Action' ];
	createHeader('theadEducational',headArr);

		$('#tbodyEducational').empty();
		if(checkValue(leadData) != '-'){
			leadData = JSON.parse(leadData);
			var count = 0;
			$.each(leadData, function( index, object ) {
				count++;
				var trTableBody = $('<tr class="text-center"></tr>').appendTo($('#tbodyEducational'));
				$('<td data-sort="'+object['educationalPk']+'">'+count+'</td>').appendTo(trTableBody);
				$('<td>-</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['degreeCode'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['instituteName'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['passingYear'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['degreeLevel'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['principalSubject'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['gpaCgpa'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['obtainGpaCgpa'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['outofGpaCgpa'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['result'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['resultLevel'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['isForeign'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['position'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['distinction'])+'</td>').appendTo(trTableBody);
				$('<td><button class="btn btn-info edit-btn" type="button" module="educational" value="'+object['educationalPk']+'" onclick="fnEdit(this)" ><b><i class="fa fa-edit"></i></b></button> &nbsp;&nbsp;<button class="btn btn-danger del-btn" type="button" module="educational" value="'+object['educationalPk']+'" onclick="fnDelete(this)" ><b><i class="fa fa-trash-o"></i></b></button>').appendTo(trTableBody);
			});
		}
	dataTable = createTable('myTable');	
	hideLoading();
}

function customSearch(){
	var inputVal= $('#searchByEmplCode').val();
	dataTable.search( inputVal ).draw();	
}

function setupForm(response) {
	var json = JSON.parse(response);
	$('#degreeCode').val(json['degreeCode']);
	$('#instituteName').val(json['instituteName']);
	$('#passingYear').val(json['passingYear']);
	$('#degreeLevel').val(json['degreeLevel']);
	$('#principalSubject').val(json['principalSubject']);
	$('#gpaCgpa').val(json['gpaCgpa']);	
	$('#obtainGpaCgpa').val(json['obtainGpaCgpa']);
	$('#outofGpaCgpa').val(json['outofGpaCgpa']);
	$('#result').val(json['result']);
	$('#resultLevel').val(json['resultLevel']);
	$('#isForeign').val(json['isForeign']);
	$('#position').val(json['position']);
	$('#distinction').val(json['distinction']);
	$('#educationalPk').val(json['educationalPk'])
}


function resetForm(){
	$('#degreeCode').val('-1');
	$('#instituteName').val('');
	$('#passingYear').val('');
	$('#degreeLevel').val('-1');
	$('#principalSubject').val('');
	$('#gpaCgpa').val('');	
	$('#obtainGpaCgpa').val('');
	$('#outofGpaCgpa').val('');
	$('#result').val('');
	$('#resultLevel').val('');
	$('#isForeign').val('');
	$('#position').val('');
	$('#distinction').val('');	
}