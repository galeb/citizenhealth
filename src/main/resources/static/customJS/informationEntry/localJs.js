/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var dataTable='';
function fnSaveLocal() {
	
    if (valOnSubmit() && checkMultiInstitute()) {
        startLoading();
        $.post({
            url: path + '/local/save',
            data: $('form[name=localform]').serialize(),
            success: function (response) {
                if (response == 'SuccessFullySaved') {
                    resetForm();
                    showSuccess('<b>Success!</b><br> Local record is sucessfully saved');
                    hideShow();
                    afterSaveDataGet();
                } else {
                    showError('<b>ERROR!</b><br> Local record is not sucessfully saved');
                    hideLoading();
                }
            }
        });
    }
}

function afterSaveDataGet() {
    $.get({
        url: path + '/local/fetchAll',
        success: function (response) {
            if (response != '') {
                prepareGrid(response);
            } else {
                showError('<b>ERROR!</b><br> Local records are not fetch sucessfully');
            }
        }
    });
}

function prepareGrid(leadData) {
    destroyTable('myTable');
    var headArr = ['Id','Category','Title', 'Institution','Start Date', 'End Date', 'Duration', 'Action'];
    createHeader('theadLocal', headArr);
    $('#tbodyLocal').empty();
    if (checkValue(leadData) != '-') {
        leadData = JSON.parse(leadData);
        var count = 0;
        $.each(leadData, function (index, object) {
            count++;
            var trTableBody = $('<tr class="text-center"></tr>').appendTo($('#tbodyLocal'));
            $('<td data-sort="' + object['locakPk'] + '">' + count + '</td>').appendTo(trTableBody);
             $('<td>' + checkValue(object['localcategory']) + '</td>').appendTo(trTableBody);
            $('<td>' + checkValue(object['localtitle']) + '</td>').appendTo(trTableBody);
            $('<td>' + checkValue(object['localinstitution']) + '</td>').appendTo(trTableBody);
            $('<td>' + checkValue(object['startdate']) + '</td>').appendTo(trTableBody);
            $('<td>' + checkValue(object['enddate']) + '</td>').appendTo(trTableBody);
            $('<td>' + checkValue(object['duration']) + '</td>').appendTo(trTableBody);
            $('<td><button class="btn btn-info" type="button" module="local" value="' + object['locakPk'] + '" onclick="fnEdit(this);showHide()" ><b><i class="fa fa-edit"></i></b></button> &nbsp;&nbsp;<button class="btn btn-danger" type="button" module="local" value="' + object['locakPk'] + '" onclick="fnDelete(this)" ><b><i class="fa fa-trash-o"></i></b></button>').appendTo(trTableBody);
        });
    }
    dataTable=createTable('myTable');
    hideLoading();
}

function customSearch(){
	var inputVal= $('#searchByEmplCode').val();
	dataTable.search( inputVal ).draw();	
}

function setupForm(response) {
    var json = JSON.parse(response);
    $('#locakPk').val(json['locakPk']);
    $('#localcategory').val(json['localcategory']);
    $('#localtitle').val(json['localtitle']);
    $('#localistitution').val(json['localistitution']);
    $('#startdate').val(json['startdate']);
    $('#enddate').val(json['enddate']);
    $('#duration').val(json['duration']);
}

function resetForm() {
    $('#locakPk').val('');
    $('#localcategory').val('');
    $('#localtitle').val('');
    $('#sellocalinstitution').html('-1');
    $('#txtlocalistitution').html('');
    $('#startdate').val('');
    $('#enddate').val('');
    $('#duration').val('');
}


function checkMultiInstitute() {
	$('.institute').remove();
	if (checkValue($('#sellocalinstitution').val()) != '-'
			&& checkValue($('#txtlocalistitution').val()) != '-') {
		$('#txtlocalistitution')
				.parent()
				.append(
						"<label class='errtitle validationMsg clearfix institute'><span class='trn'>Please Select Either Institution OR Enter Institution name.</span></label>");
		return false;
	} else if (checkValue($('#sellocalinstitution').val()) != '-'
			|| checkValue($('#txtlocalistitution').val()) != '-') {
		return true;
	} else {

		$('#txtlocalistitution')
				.parent()
				.append(
						"<label class='errtitle validationMsg clearfix institute'>Please Select Institution OR Enter Institution name.</label>");
		return false;
	}
}