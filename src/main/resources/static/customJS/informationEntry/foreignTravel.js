function fnSaveForeignTravel() {
	if(valOnSubmit()){
		startLoading();
		$.post({
			url : path + '/foreigntravel/save',
			data : $('form[name=foreigntravelform]').serialize(),
			success : function(response) {
				if(response == 'SuccessFullySaved'){
					showSuccess('<b>Success!</b><br> ForeignTravel record is sucessfully created');					
					afterSaveDataGet();
				}else{
					showError('<b>ERROR!</b><br> ForeignTravel record is not sucessfully created');	
					hideLoading();
				}				
			}
		});
	}
}

function afterSaveDataGet(){
	$.get({
		url : path + '/foreigntravel/fetchAll',
		success : function(response) {			
			if(response != ''){
				resetForm();
				prepareGrid(response);
			}else{
				showError('<b>ERROR!</b><br> ForeignTravel records are not fetch sucessfully');	
			}				
		}
	})
}

function prepareGrid(leadData){			
console.log(leadData);
	
	destroyTable('myTable');
	var headArr = ['Id','Employee Details','Country','Purpose of Travel','Status','Start Date','End Date','Duration','Remarks','Action' ];
	createHeader('theadForeignTravel',headArr);

		$('#tbodyForeignTravel').empty();

		if(checkValue(leadData) != '-'){
			leadData = JSON.parse(leadData);
			var count = 0;
			$.each(leadData, function( index, object ) {
				count++;
				var trTableBody = $('<tr class="text-center"></tr>').appendTo($('#tbodyForeignTravel'));
				$('<td data-sort="'+object['foreigntravelPk']+'">'+count+'</td>').appendTo(trTableBody);
				$('<td>-</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['country'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['status'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['purposeoftravel'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['startDate'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['endDate'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['duration'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['remarks'])+'</td>').appendTo(trTableBody);
				$('<td><button class="btn btn-info" type="button" module="foreigntravel" value="'+object['foreigntravelPk']+'" onclick="fnEdit(this)" ><b><i class="fa fa-edit"></i></b></button> &nbsp;&nbsp;<button class="btn btn-danger" type="button" module="foreigntravel" value="'+object['foreigntravelPk']+'" onclick="fnDelete(this)" ><b><i class="fa fa-trash-o"></i></b></button>').appendTo(trTableBody);
			
			});
		}

	createTable('myTable');	
	hideLoading();
}

function setupForm(response) {
	var json = JSON.parse(response);
	$('#country').val(json['country']);
	$('#status').val(json['status']);
	$('#purposeoftravel').val(json['purposeoftravel']);
	$('#startDate').val(json['startDate']);
	$('#endDate').val(json['endDate']);
	$('#duration').val(json['duration']);	
	$('#remarks').val(json['remarks']);
	$('#foreigntravelPk').val(json['foreigntravelPk'])
}

function resetForm(){
	$('#country').val('-1');
	$('#status').val('-1');
	$('#purposeoftravel').val('-1');
	$('#startDate').val('');
	$('#endDate').val('');
	$('#duration').val('');	
	$('#remarks').val('');
}