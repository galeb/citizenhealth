var dataTable='';
function fnSaveaward() {
	if(valOnSubmit()){
		startLoading();
		$.post({
			url : path + '/generalinformation/save',
			data : $('form[name=awardform]').serialize(),
			success : function(response) {
				if(response == 'SuccessFullySaved'){
					showSuccess('<b>Success!</b><br> Award record is sucessfully saved');					
					afterSaveDataGet();
				}else{
					showError('<b>ERROR!</b><br> Award record is not sucessfully created');	
					hideLoading();
				}				
			}
		});
	}
}

function afterSaveDataGet(){
	$.get({
		url : path + '/generalinformation/fetchAll',
		success : function(response) {			
			if(response != ''){
				resetForm();
                                hideShow();
				prepareGrid(response);
			}else{
				showError('<b>ERROR!</b><br> Award records are not fetch sucessfully');	
			}				
		}
	})
}

function prepareGrid(leadData){			
	destroyTable('myTable');
	var headArr = ['Id','Employee Details','Award Name','Award Serial','Ground','Remarks','Action' ];
	createHeader('theadGeneralInformation',headArr);

		$('#tbodyGeneralInformation').empty();

		if(checkValue(leadData) != '-'){
			leadData = JSON.parse(leadData);
			var count = 0;
			$.each(leadData, function( index, object ) {
				count++;
				var trTableBody = $('<tr class="text-center"></tr>').appendTo($('#tbodyGeneralInformation'));
				$('<td data-sort="'+object['employeePk']+'">'+count+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['govId'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['engName'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['bngName'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['birthDate'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['gender'])+'</td>').appendTo(trTableBody);				
				$('<td><button class="btn btn-info" type="button" module="generalinformation" value="'+object['employeePk']+'" onclick="fnEdit(this);showHide()" ><b><i class="fa fa-edit"></i></b></button> &nbsp;&nbsp;<button class="btn btn-danger" type="button" module="generalinformation" value="'+object['employeePk']+'" onclick="fnDelete(this)" ><b><i class="fa fa-trash-o"></i></b></button>').appendTo(trTableBody);
			
			});
		}

	dataTable =createTable('myTable');	
	hideLoading();
}

function customSearch(){
	var inputVal= $('#searchByEmplCode').val();
	dataTable.search( inputVal ).draw();	
}

function setupForm(response) {
	var json = JSON.parse(response);
	$('#employeeForm :input, #employeeForm select').each(function() {		
		if ($(this).attr('type') == 'date') {			
			if (checkValue(json[$(this).attr('name')]) != '-') {
				$(this).val(formatDate(json[$(this).attr('name')]));
			}
		} else if($(this).attr('type') == 'file'){			
			var path = json['imagePath'];            
			editImage(path);
		}
		else {
			$(this).val(json[$(this).attr('name')]);
		}

	});
}

function editImage(path){
	var divImage = "<div class='imgcontainer' >";
	divImage = divImage
			+ '<img class="img-responsive img-circle" id="productImg" src="' + path + '" alt="your image"/>';
	//  divImage = divImage + '<input class="delete" type="button" value="Delete" onclick="remove(this)" name="' + name + '"/></div></div></div>';
	$('#dvProductImgs').append(divImage);
	$("#dvImg").show();	
}


function resetForm() {
	$('#employeeForm :input, #employeeForm select').each(function() {

		if ($(this).is('input')) {
			$(this).val('');
		}
		if ($(this).is('select')) {
			$(this).val('-1');
		}
	});
}

function submitForm() {
	var status = valOnSubmit();
	if (status == true) {
		startLoading();
	}
	return status;
}

function prepareSelect(lookupJson) {
	var jsonLookup = JSON.parse(lookupJson);
	console.log(jsonLookup);
	$("select").each(function() {
		console.log((jsonLookup['keyword'])+'--'+$(this).attr("name"));
		if(checkValue((jsonLookup['keyword'])) != '-' && checkValue($(this).attr("name")) != '-'){
			console.log('in if '+(jsonLookup['keyword'])+'--'+$(this).attr("name"));
			if((jsonLookup['keyword']).toLowerCase() == $(this).attr("name")){
				console.log($(this).attr("name"));
			}
		}		
		
	});
}