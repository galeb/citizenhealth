/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function fnSaveProfessional() {
    if (valOnSubmit()) {
        startLoading();
        $.post({
            url: path + '/professional/save',
            data: $('form[name=professionalform]').serialize(),
            success: function (response) {
                if (response == 'SuccessFullySaved') {
                    resetForm();
                    showSuccess('<b>Success!</b><br> Professional record is sucessfully saved');
                    hideShow();
                    afterSaveDataGet();
                } else {
                    showError('<b>ERROR!</b><br> Professional record is not sucessfully saved');
                    hideLoading();
                }
            }
        });
    }
}

function afterSaveDataGet() {
    $.get({
        url: path + '/professional/fetchAll',
        success: function (response) {
            if (response != '') {
                prepareGrid(response);
            } else {
                showError('<b>ERROR!</b><br> Professional records are not fetch sucessfully');
            }
        }
    });
}

function prepareGrid(leadData) {
    destroyTable('myTable');
    var headArr = ['Id','Serial', 'Membership No','Institution', 'Location No', 'Remarks', 'Action'];
    createHeader('theadProfessional', headArr);
    $('#tbodyProfessional').empty();
    if (checkValue(leadData) != '-') {
        leadData = JSON.parse(leadData);
        var count = 0;
        $.each(leadData, function (index, object) {
            count++;
            var trTableBody = $('<tr class="text-center"></tr>').appendTo($('#tbodyProfessional'));
            $('<td data-sort="' + object['professionalPk'] + '">' + count + '</td>').appendTo(trTableBody);
              $('<td>' + checkValue(object['serial']) + '</td>').appendTo(trTableBody);
            $('<td>' + checkValue(object['membership']) + '</td>').appendTo(trTableBody);
            $('<td>' + checkValue(object['institution']) + '</td>').appendTo(trTableBody);
            $('<td>' + checkValue(object['locationNumber']) + '</td>').appendTo(trTableBody);
            $('<td>' + checkValue(object['remarks']) + '</td>').appendTo(trTableBody);
            $('<td><button class="btn btn-info" type="button" module="professional" value="' + object['professionalPk'] + '" onclick="fnEdit(this);showHide()" ><b><i class="fa fa-edit"></i></b></button> &nbsp;&nbsp;<button class="btn btn-danger" type="button" module="professional" value="' + object['professionalPk'] + '" onclick="fnDelete(this)" ><b><i class="fa fa-trash-o"></i></b></button>').appendTo(trTableBody);
        });
    }
    createTable('myTable');
      hideLoading();
}

function setupForm(response) {
    var json = JSON.parse(response);
    $('#professionalPk').val(json['professionalPk']);
    $('#serial').val(json['serial']);
    $('#membership').val(json['membership']);
    $('#institution').val(json['institution']);
    $('#locationNumber').val(json['locationNumber']);
    $('#remarks').val(json['remarks']);
}

function resetForm() {
    $('#professionalPk').val('');
    $('#serial').val('');
    $('#membership').val('');
    $('#institution').html('');
    $('#locationNumber').val('');
    $('#remarks').val('');
}


