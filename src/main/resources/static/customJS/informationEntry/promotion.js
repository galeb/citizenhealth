function fnSavePromotion() {
	if(valOnSubmit()){
		startLoading();
		$.post({
			url : path + '/promotion/save',
			data : $('form[name=promotionform]').serialize(),
			success : function(response) {
				showSuccess('<b>Success!</b><br> Promotion record is sucessfully created');					
				afterSaveDataGet();			
			}
		});
	}
}

function afterSaveDataGet(){
	$.get({
		url : path + '/promotion/fetchAll',
		success : function(response) {			
			if(response != ''){
				resetForm();
				prepareGrid(response);
			}else{
				showError('<b>ERROR!</b><br> Promotion records are not fetch sucessfully');	
			}				
		}
	})
}

function prepareGrid(leadData){			
console.log(leadData);
	
	destroyTable('myTable');
	var headArr = ['Id','Nature of Promotion','Promotion Date','GO Date','Pay Scale','Rank','Rule','Remarks','Details','Image','Action' ];
	createHeader('theadPromotion',headArr);

		$('#tbodyPromotion').empty();

		if(checkValue(leadData) != '-'){
			leadData = JSON.parse(leadData);
			var count = 0;
			$.each(leadData, function( index, object ) {
				count++;
				var trTableBody = $('<tr class="text-center"></tr>').appendTo($('#tbodyPromotion'));
				$('<td data-sort="'+object['promotionPk']+'">'+count+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['natureofPromotion'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['promotionDate'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['goDate'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['payScaleFk']['scale'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['rankFk']['rankName'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['rule'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['remarks'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['details'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['fileDoc'])+'</td>').appendTo(trTableBody);
				$('<td><button class="btn btn-info" type="button" module="promotion" value="'+object['promotionPk']+'" onclick="fnEdit(this)" ><b><i class="fa fa-edit"></i></b></button> &nbsp;&nbsp;<button class="btn btn-danger" type="button" module="promotion" value="'+object['promotionPk']+'" onclick="fnDelete(this)" ><b><i class="fa fa-trash-o"></i></b></button>').appendTo(trTableBody);
			});
		}
	createTable('myTable');	
	hideLoading();
}

function setupForm(response) {
	var json = JSON.parse(response);
	$('#natureofPromotion').val(json['natureofPromotion']);
	$('#promotionDate').val(json['promotionDate']);
	$('#goDate').val(json['goDate']);
	$('#payScaleFk').val(json['payScaleFk']['scale']);
	$('#rankFk').val(json['rankFk']['rankName']);
	$('#rule').val(json['rule']);	
	$('#remarks').val(json['remarks']);
	$('#details').val(json['details']);
	$('#inpFileDoc').val(json['fileDoc']);
	$('#promotionPk').val(json['promotionPk'])
}

function resetForm(){
	$('#natureofPromotion').val('');
	$('#promotionDate').val('');
	$('#goDate').val('');
	$('#payScaleFk').val('-1');
	$('#rankFk').val('-1');
	$('#rule').val('');	
	$('#remarks').val('');
	$('#details').val('');
	$('#image').val('');
}