var dataTable='';
function fnSaveForeignTraining() {
	checkMultiInstitute()
	if(valOnSubmit() && checkMultiInstitute()){
		startLoading();
		$.post({
			url : path + '/foreigntraining/save',
			data : $('form[name=foreignTrainingform]').serialize(),
			success : function(response) {
				if(response == 'SuccessFullySaved'){
					showSuccess('<b>Success!</b><br> ForeignTraining record is sucessfully created');					
					afterSaveDataGet();
				}else{
					showError('<b>ERROR!</b><br> ForeignTraining record is not sucessfully created');	
					hideLoading();
				}				
			}
		});
	}
}

function afterSaveDataGet(){
	$.get({
		url : path + '/foreigntraining/fetchAll',
		success : function(response) {			
			if(response != ''){
				resetForm();
				hideShow();
				prepareGrid(response);
			}else{
				showError('<b>ERROR!</b><br> ForeignTraining records are not fetch sucessfully');	
			}				
		}
	})
}

function prepareGrid(leadData){			
console.log(leadData);
	
	destroyTable('myTable');
	var headArr = ['Id','Employee Details','Country','Title','Institution','Start Date','End Date','Duration','Batch','Remarks','Action' ];
	createHeader('theadForeignTraining',headArr);

		$('#tbodyForeignTraining').empty();
		if(checkValue(leadData) != '-'){
			leadData = JSON.parse(leadData);
			var count = 0;
			$.each(leadData, function( index, object ) {
				count++;
				var trTableBody = $('<tr class="text-center"></tr>').appendTo($('#tbodyForeignTraining'));
				$('<td data-sort="'+object['foreignTrainingPk']+'">'+count+'</td>').appendTo(trTableBody);
				$('<td>-</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['country'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['title'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['institution'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['startDate'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['endDate'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['duration'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['batch'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['remarks'])+'</td>').appendTo(trTableBody);
				$('<td><button class="btn btn-info edit-btn" type="button" module="foreigntraining" value="'+object['foreignTrainingPk']+'" onclick="fnEdit(this)" ><b><i class="fa fa-edit"></i></b></button> &nbsp;&nbsp;<button class="btn btn-danger del-btn" type="button" module="foreigntraining" value="'+object['foreignTrainingPk']+'" onclick="fnDelete(this)" ><b><i class="fa fa-trash-o"></i></b></button>').appendTo(trTableBody);
			
			});
		}
	dataTable = createTable('myTable');	
	hideLoading();
}

function customSearch(){
	var inputVal= $('#searchByEmplCode').val();
	dataTable.search( inputVal ).draw();	
}

function setupForm(response) {
	var json = JSON.parse(response);
	$('#country').val(json['country']);
	$('#title').val(json['title']);
	$('#institution').val(json['institution']);
	$('#startDate').val(json['startDate']);
	$('#endDate').val(json['endDate']);	
	$('#duration').val(json['duration']);
	$('#batch').val(json['batch']);
	$('#remarks').val(json['remarks']);
	$('#foreignTrainingPk').val(json['foreignTrainingPk'])
}

function resetForm(){
	$('#country').val('-1');
	$('#title').val('');
	$('#institution').val('');
	$('#startDate').val('');
	$('#endDate').val('');	
	$('#duration').val('');
	$('#batch').val('-1');
	$('#remarks').val('');
}

function checkMultiInstitute() {
	$('.institute').remove();
	if (checkValue($('#sellocalinstitution').val()) != '-'
			&& checkValue($('#txtlocalistitution').val()) != '-') {
		$('#txtlocalistitution')
				.parent()
				.append(
						"<label class='errtitle validationMsg clearfix institute'><span class='trn'>Please Select Either Institution OR Enter Institution name.</span></label>");
		return false;
	} else if (checkValue($('#sellocalinstitution').val()) != '-'
			|| checkValue($('#txtlocalistitution').val()) != '-') {
		return true;
	} else {

		$('#txtlocalistitution')
				.parent()
				.append(
						"<label class='errtitle validationMsg clearfix institute'>Please Select Institution OR Enter Institution name.</label>");
		return false;
	}
}