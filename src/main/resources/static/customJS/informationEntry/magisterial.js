var dataTable='';
function fnSaveMagisterial() {
	if(valOnSubmit()){
		startLoading();
		$.post({
			url : path + '/magisterial/save',
			data : $('form[name=magisterialform]').serialize(),
			success : function(response) {
				if(response == 'SuccessFullySaved'){
					showSuccess('<b>Success!</b><br> Magisterial record is sucessfully created');					
					afterSaveDataGet();
				}else{
					showError('<b>ERROR!</b><br> Magisterial record is not sucessfully created');	
					hideLoading();
				}				
			}
		});
	}
}

function afterSaveDataGet(){
	$.get({
		url : path + '/magisterial/fetchAll',
		success : function(response) {			
			if(response != ''){
				resetForm();
				hideShow();
				prepareGrid(response);
			}else{
				hideLoading();
				showError('<b>ERROR!</b><br> Magisterial records are not fetch sucessfully');	
			}				
		}
	})
}

function prepareGrid(leadData){			
console.log(leadData);
	
	destroyTable('myTable');
	var headArr = ['Id','Employee Details','Magistracy Power','From Date','To Date','Remarks','Action' ];
	createHeader('theadMagisterial',headArr);

		$('#tbodyMagisterial').empty();
		if(checkValue(leadData) != '-'){
			leadData = JSON.parse(leadData);
			var count = 0;
			$.each(leadData, function( index, object ) {
				count++;
				var trTableBody = $('<tr class="text-center"></tr>').appendTo($('#tbodyMagisterial'));
				$('<td data-sort="'+object['magisterialPk']+'">'+count+'</td>').appendTo(trTableBody);
				$('<td>-</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['magistracyPower'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['fromDate'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['toDate'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['remarks'])+'</td>').appendTo(trTableBody);
				$('<td><button class="btn btn-info" type="button" module="magisterial" value="'+object['magisterialPk']+'" onclick="fnEdit(this)" ><b><i class="fa fa-edit"></i></b></button> &nbsp;&nbsp;<button class="btn btn-danger" type="button" module="magisterial" value="'+object['magisterialPk']+'" onclick="fnDelete(this)" ><b><i class="fa fa-trash-o"></i></b></button>').appendTo(trTableBody);
			
			});
		}
	dataTable = createTable('myTable');	
	hideLoading();
}

function customSearch(){
	var inputVal= $('#searchByEmplCode').val();
	dataTable.search( inputVal ).draw();	
}

function setupForm(response) {
	var json = JSON.parse(response);
	$('#magistracyPower').val(json['magistracyPower']);
	$('#fromDate').val(json['fromDate']);
	$('#toDate').val(json['toDate']);
	$('#remarks').val(json['remarks']);
	$('#magisterialPk').val(json['magisterialPk'])
}

function resetForm(){
	$('#magistracyPower').val('-1');
	$('#fromDate').val('');
	$('#toDate').val('');	
	$('#remarks').val('');
}