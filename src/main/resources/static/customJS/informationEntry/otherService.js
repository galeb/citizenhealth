function fnSaveOtherService() {
	if(valOnSubmit()){
		startLoading();
		$.post({
			url : path + '/otherservice/save',
			data : $('form[name=otherServiceform]').serialize(),
			success : function(response) {
				if(response == 'SuccessFullySaved'){
					showSuccess('<b>Success!</b><br> OtherService record is sucessfully created');					
					afterSaveDataGet();
				}else{
					showError('<b>ERROR!</b><br> OtherService record is not sucessfully created');	
					hideLoading();
				}				
			}
		});
	}
}

function afterSaveDataGet(){
	$.get({
		url : path + '/otherservice/fetchAll',
		success : function(response) {			
			if(response != ''){
				resetForm();
				prepareGrid(response);
			}else{
				showError('<b>ERROR!</b><br>  records are not fetch sucessfully');	
			}				
		}
	})
}

function prepareGrid(leadData){			
console.log(leadData);
	
	destroyTable('myTable');
	var headArr = ['Id','Employee Details','Employer Name','Employer Address','Designation','Service Type','From Date','To Date','Remarks','Action' ];
	createHeader('theadOtherService',headArr);

		$('#tbodyOtherService').empty();

		if(checkValue(leadData) != '-'){
			leadData = JSON.parse(leadData);
			var count = 0;
			$.each(leadData, function( index, object ) {
				count++;
				var trTableBody = $('<tr class="text-center"></tr>').appendTo($('#tbodyOtherService'));
				$('<td data-sort="'+object['otherServicePk']+'">'+count+'</td>').appendTo(trTableBody);
				$('<td>-</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['employerName'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['employerAddress'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['designation'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['serviceType'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['fromDate'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['toDate'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['remarks'])+'</td>').appendTo(trTableBody);
				$('<td><button class="btn btn-info" type="button" module="otherservice" value="'+object['otherServicePk']+'" onclick="fnEdit(this)" ><b><i class="fa fa-edit"></i></b></button> &nbsp;&nbsp;<button class="btn btn-danger" type="button" module="otherservice" value="'+object['otherServicePk']+'" onclick="fnDelete(this)" ><b><i class="fa fa-trash-o"></i></b></button>').appendTo(trTableBody);
			
			});
		}

	createTable('myTable');	
	hideLoading();
}

function setupForm(response) {
	var json = JSON.parse(response);
	$('#employerName').val(json['employerName']);
	$('#employerAddress').val(json['employerAddress']);
	$('#designation').val(json['designation']);
	$('#serviceType').val(json['serviceType']);
	$('#fromDate').val(json['fromDate']);
	$('#toDate').val(json['toDate']);	
	$('#remarks').val(json['remarks']);
	$('#otherServicePk').val(json['otherServicePk'])
}

function resetForm(){
	$('#employerName').val('');
	$('#employerAddress').val('');
	$('#designation').val('');
	$('#serviceType').val('');
	$('#fromDate').val('');
	$('#toDate').val('');	
	$('#remarks').val('');
}