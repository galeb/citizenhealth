/**
 * 
 */
function fnSaveLocation() {
	if(valOnSubmit()){
	startLoading();
		$.post({
			url : path + '/location/save',
			data : $('form[name=locationform]').serialize(),
			success : function(response) {
				console.log(response);
				if(response == 'successFullySaved'){
					showSuccess('<b>Success!</b><br> Location record is sucessfully created');
					
					afterSaveDataGet();
				}else{
					showError('<b>ERROR!</b><br> Location record is not sucessfully created');	
					hideLoading();
				}				
			}
		});
	}
	}

function afterSaveDataGet(){
	window.location.href=path+'/location/view';
//	$.get({
//		url : path + '/location/fetchAll',
//		success : function(response) {			
//			if(response != ''){
//				resetForm();
//				prepareGrid(response);
//			}else{
//				showError('<b>ERROR!</b><br> Location records are not fetch sucessfully');	
//				hideLoading();
//			}				
//		}
//	})
}

function setupForm(response){
	var json = JSON.parse(response);
	$('#locationName').val(json['locationName']);
	$('#name_native').val(json['locationNativeName']);
	$('#type').val(json['locationType']);
	$('#isactive').val(json['isActive']);
	$('#location_id').val(json['locationPk']);
}

function prepareGrid(leadData){
	
	destroyTable('myTable');
	var headArr = ['Menu Name','Menu URL','Menu Icon','Parent','Action'];
	createHeader('theadMenu',headArr);

		$('#tbodyMenu').empty();
		if(checkValue(leadData) != '-'){			
			leadData = JSON.parse(leadData);			
			var count = 0;
			$.each(leadData, function( index, object ) {
				var trTableBody = $('<tr class="text-center"></tr>').appendTo($('#tbodyMenu'));				
				$('<td>'+checkValue(object['name'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['url'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['icon'])+'</td>').appendTo(trTableBody);
				$('<td>'+parentName(leadData,object)+'</td>').appendTo(trTableBody);
				$('<td><button class="btn btn-info" type="button" module="menu" value="'+object['id']+'" onclick="fnEdit(this)" ><b><i class="fa fa-edit"></i></b></button> &nbsp;&nbsp;<button class="btn btn-danger" type="button" module="menu" value="'+object['id']+'" onclick="fnDelete(this)" ><b><i class="fa fa-trash-o"></i></b></button>').appendTo(trTableBody);
			
			});
		}
	createTable('myTable');	
	hideLoading();
	
	
	if(checkValue(leadData) != '-'){
		leadData = JSON.parse(leadData);		
		$('#tbodyLocations').html('');
		var count = 0;
		$.each(leadData, function( index, object ) {
			count++;
			var trTableBody = $('<tr class="text-center"></tr>').appendTo($('#tbodyLocations'));
			$('<td data-sort="'+object['locationPk']+'">'+count+'</td>').appendTo(trTableBody);
			$('<td>'+checkValue(object['locationName'])+'</td>').appendTo(trTableBody);
			$('<td>'+checkValue(object['locationNativeName'])+'</td>').appendTo(trTableBody);
			$('<td>'+checkValue('-')+'</td>').appendTo(trTableBody);
			$('<td>'+checkValue('-')+'</td>').appendTo(trTableBody);
			$('<td>'+checkValue('-')+'</td>').appendTo(trTableBody);
			$('<td>'+checkValue('-')+'</td>').appendTo(trTableBody);
			
			$('<td><button class="btn btn-info edit-btn" type="button" module="location" value="'+object['locationPk']+'" onclick="fnEdit(this)" ><b><i class="fa fa-edit"></i></b></button> &nbsp;&nbsp;<button class="btn btn-danger del-btn" type="button" module="location" value="'+object['locationPk']+'" onclick="fnDelete(this)" ><b><i class="fa fa-trash-o"></i></b></button></td>').appendTo(trTableBody);
					  
		});			
		createTable('myTable');	
		hideLoading();
	}
}

function resetForm(){
	$('#locationName').val('');
	$('#name_native').val('');
	$('#type').val('');
	$('#isactive').val('-1');
}