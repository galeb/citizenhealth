function fnSavePostoffice() {
	if(valOnSubmit()){
		startLoading();
		$.post({
			url : path + '/postoffice/save',
			data : $('form[name=postofficeform]').serialize(),
			success : function(response) {
				if(response == 'SuccessFullySaved'){
					showSuccess('<b>Success!</b><br> PostOffice record is sucessfully created');					
					afterSaveDataGet();
				}else{
					showError('<b>ERROR!</b><br> PostOffice record is not sucessfully created');	
					hideLoading();
				}				
			}
		});
	}
}

function afterSaveDataGet(){
	$.get({
		url : path + '/postoffice/fetchAll',
		success : function(response) {			
			if(response != ''){
				resetForm();
				prepareGrid(response);
			}else{
				showError('<b>ERROR!</b><br> PostOffice records are not fetch sucessfully');	
			}				
		}
	})
}

function prepareGrid(leadData){			

	destroyTable('myTable');
	var headArr = ['Id','Name','Name Native','Locaion District Id','Action'];
	createHeader('theadPostOffice',headArr);

		$('#tbodyPostOffice').empty();

		if(checkValue(leadData) != '-'){
			leadData = JSON.parse(leadData);
			var count = 0;
			$.each(leadData, function( index, object ) {
				count++;
				var trTableBody = $('<tr class="text-center"></tr>').appendTo($('#tbodyPostOffice'));
				$('<td data-sort="'+object['postOfficePk']+'">'+count+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['postOfficeName'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['postOfficeNativeName'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['locationID'])+'</td>').appendTo(trTableBody);
				$('<td><button class="btn btn-info" type="button" module="postoffice" value="'+object['postOfficePk']+'" onclick="fnEdit(this)" ><b><i class="fa fa-edit"></i></b></button> &nbsp;&nbsp;<button class="btn btn-danger" type="button" module="postoffice" value="'+object['postOfficePk']+'" onclick="fnDelete(this)" ><b><i class="fa fa-trash-o"></i></b></button>').appendTo(trTableBody);
                                
			});
		}
	createTable('myTable');	
	hideLoading();
}

function setupForm(response) {
	var json = JSON.parse(response);
	$('#postOfficeName').val(json['postOfficeName']);
	$('#name_native').val(json['postOfficeNativeName']);
	$('#isactive').val(json['isActive']);
	$('#postOfficePk').val(json['postOfficePk'])
}

function resetForm(){
	$('#postOfficeName').val('');
	$('#name_native').val('');
	$('#isactive').val('-1');
}
