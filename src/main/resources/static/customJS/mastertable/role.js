function fnSaveRole() {
	if(valOnSubmit()){
		startLoading();
		$.post({
			url : path + '/role/save',
			data : $('form[name=Roleform]').serialize(),
			success : function(response) {
				if(response == 'SuccessFullySaved'){
					showSuccess('<b>Success!</b><br> Role record is sucessfully created');					
					afterSaveDataGet();
				}else{
					showError('<b>ERROR!</b><br> Role record is not sucessfully created');	
					hideLoading();
				}				
			}
		});
	}
}

function afterSaveDataGet(){
	$.get({
		url : path + '/role/fetchAll',
		success : function(response) {			
			if(response != ''){
				resetForm();
				prepareGrid(response);
			}else{
				showError('<b>ERROR!</b><br> Role records are not fetch sucessfully');	
			}				
		}
	})
}

function prepareGrid(leadData){			
	destroyTable('myTable');
	var headArr = ['Id','Role Name','Action'];
	createHeader('theadrole',headArr);
	
		$('#tbodyrole').empty();
		
		if(checkValue(leadData) != '-'){
			leadData = JSON.parse(leadData);
			var count = 0;
			$.each(leadData, function( index, object ) {
				count++;
				var trTableBody = $('<tr class="text-center"></tr>').appendTo($('#tbodyrole'));
				$('<td data-sort="'+object['id']+'">'+count+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['roleName'])+'</td>').appendTo(trTableBody);
				$('<td><button class="btn btn-info" type="button" module="role" value="'+object['id']+'" onclick="fnEdit(this)" ><b><i class="fa fa-edit"></i></b></button> &nbsp;&nbsp;<button class="btn btn-danger" type="button" module="role" value="'+object['id']+'" onclick="fnDelete(this)" ><b><i class="fa fa-trash-o"></i></b></button>').appendTo(trTableBody);
			});
		}
	createTable('myTable');	
	fetchDataFromSession();
	hideLoading();
}

function setupForm(response) {
	var json = JSON.parse(response);
	$('#roleName').val(json['tblRole']['roleName']);
	$('#id').val(json['tblRole']['id']);
	$('#lstResMenu').val(json['lstResMenu']);
	$('.js-example-basic-multiple').select2();
}

function resetForm(){
	$('#roleName').val('');
	$('#id').val('');
	$('#lstResMenu').val('-1');
	$('.js-example-basic-multiple').select2();
}
