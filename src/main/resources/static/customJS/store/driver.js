/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(function () {
    fetchAll();
});

function fetchAll() {
    $.get({
        url: path + "/driver/fetchAll",
        success: function (response) {
            destroyTable('myTable');
            var headArr = ['ক্রমিক নং', 'চালকের নাম', 'মোবাইল', 'গাড়ি নম্বর', 'অ্যাকশন'];
            createHeader('theadDriver', headArr);
            $('#tbodyDriver').empty();
            console.log(response);
            if (checkValue(response) != '-') {
                var count = 0;
                var json = JSON.parse(response);
                $.each(json, function (index, object) {
                    count++;
                    var trTableBody = $('<tr class="text-center"></tr>').appendTo($('#tbodyDriver'));
                    $('<td>' + checkValue(object['code']) + '</td>').appendTo(trTableBody);
                    $('<td>' + checkValue(object['driverName']) + '</td>').appendTo(trTableBody);
                    $('<td>' + checkValue(object['mobile']) + '</td>').appendTo(trTableBody);
                    $('<td>' + (checkValue(object['tblStrCarInfo']).carNo) + '</td>').appendTo(trTableBody);
                    $('<td><div class="row text-center" style="padding-left: 7px;"> <div class="col-xs-6"><button class="btn btn-info" type="button" module="driver" value="' + object['id'] + '" onclick="fnEdit(this);" ><b><i class="fa fa-edit"></i></b></button></div>&nbsp;<div class="col-xs-6"><button class="btn btn-info" type="button" module="driver" value="' + object['id'] + '" onclick="fnDelete(this);" ><b><i class="fa fa-trash-o"></i></b></button></div></div></td>').appendTo(trTableBody);
                });
                $("#myTable").DataTable({

                    "iCookieDuration": 60,
                    "bStateSave": false,

                    "bProcessing": true,
                    "bRetrieve": true,
                    "bJQueryUI": true,

                    "searchHighlight": true,
                    "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
                    "order": [[0, "asc"]],
                    "dom": 'Blfrtip',

                    "buttons": [
                        {
                            extend: 'excel',
                            text: 'Excel',
                            className: 'btn btn-primary',
                            exportOptions: {
                                columns: [0, 1, 2]
                            },
                            orientation: 'landscape',
                            title: 'Driver List'
                        },
                        {
                            extend: 'pdf',
                            text: 'PDF',
                            className: 'btn btn-primary',
                            exportOptions: {
                                columns: [0, 1, 2]
                            },
                            orientation: 'landscape',
                            title: 'Driver List'
                        },
                        {
                            extend: 'print',
                            text: 'Print',
                            className: 'btn btn-primary',
                            exportOptions: {
                                columns: [0, 1, 2]
                            },
                            orientation: 'landscape',
                            title: 'Driver List'
                        }
                    ]
                });
            }
            hideLoading();
        }
    });
}

function resetForm() {
    $('#id').val('');
    $('#code').val('');
    $('#driverName').val('');
    $('#mobile').val('');
    $('#tblStrCarInfo').val('');
}

function setupForm(response) {
    var json = JSON.parse(response);
    $('#id').val(json['id']);
    $('#code').val(json['code']);
    $('#driverName').val(json['driverName']);
    $('#mobile').val(json['mobile']);
    $('#tblStrCarInfo').val(json['tblStrCarInfo'].id);
}

function afterSaveDataGet() {
    fetchAll();
}


