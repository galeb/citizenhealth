/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(function () {
    fetchAll();
});

function fetchAll() {
    $.get({
        url: path + "/supplier/fetchAll",
        success: function (response) {
            destroyTable('myTable');
            var headArr = ['ক্রমিক নং', 'প্রতিষ্ঠানের নাম', 'মোবাইল', 'ঠিকানা', 'বিস্তারিত', 'অ্যাকশন'];
            createHeader('theadCategory', headArr);
            $('#tbodyCategory').empty();
            console.log(response);
            if (checkValue(response) != '-') {
                var count = 0;
                var json = JSON.parse(response);
                $.each(json, function (index, object) {
                    count++;
                    var trTableBody = $('<tr class="text-center"></tr>').appendTo($('#tbodyCategory'));
                    $('<td word-wrap:break-word>' + count + '</td>').appendTo(trTableBody);
                    $('<td word-wrap:break-word>' + checkValue(object['name']) + '</td>').appendTo(trTableBody);
                    $('<td word-wrap:break-word>' + checkValue(object['mobile']) + '</td>').appendTo(trTableBody);
                    $('<td word-wrap:break-word>' + checkValue(object['address']) + '</td>').appendTo(trTableBody);
                    $('<td word-wrap:break-word>' + checkValue(object['remarks']) + '</td>').appendTo(trTableBody);
                    $('<td word-wrap:break-word><div class="row text-center" style="padding-left: 7px;"> <div class="col-xs-6"><button class="btn btn-info" type="button" module="supplier" value="' + object['id'] + '" onclick="fnEdit(this);" ><b><i class="fa fa-edit"></i></b></button></div>&nbsp;<div class="col-xs-6"><button class="btn btn-info" type="button" module="supplier" value="' + object['id'] + '" onclick="fnDelete(this);" ><b><i class="fa fa-trash-o"></i></b></button></div></div></td>').appendTo(trTableBody);
                });
                $("#myTable").DataTable({

                    "iCookieDuration": 60,
                    "bStateSave": false,

                    "bProcessing": true,
                    "bRetrieve": true,
                    "bJQueryUI": true,

                    "searchHighlight": true,
                    "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
                    "order": [[0, "asc"]],
                    "dom": 'Blfrtip',

                    "buttons": [
                        {
                            extend: 'excel',
                            text: 'Excel',
                            className: 'btn btn-primary',
                            exportOptions: {

                                columns: [0, 1, 2]

                            },
                            orientation: 'landscape',
                            title: 'Supplier List'

                        },
                        {
                            extend: 'pdf',
                            text: 'PDF',
                            className: 'btn btn-primary',
                            exportOptions: {

                                columns: [0, 1, 2]
                            },
                            orientation: 'landscape',
                            title: 'Supplier List'

                        },
                        {
                            extend: 'print',
                            text: 'Print',
                            className: 'btn btn-primary',
                            exportOptions: {

                                columns: [0, 1, 2]

                            },
                            orientation: 'landscape',
                            title: 'Supplier List'
                        }
                    ]
                });
            }
            hideLoading();
        }
    });
}

function resetForm() {
    $('#id').val('');
    $('#name').val('');
    $('#mobile').val('');
    $('#address').val('');
    $('#remarks').val('');
}

function setupForm(response) {
    var json = JSON.parse(response);
    $('#id').val(json['id']);
    $('#name').val(json['name']);
    $('#mobile').val(json['mobile']);
    $('#address').val(json['address']);
    $('#remarks').val(json['remarks']);
}

function afterSaveDataGet() {
    fetchAll();
}


