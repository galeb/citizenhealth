/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(function () {
    fetchAll();
});

function fetchAll() {
    $.get({
        url: path + "/budget/fetchAll",
        success: function (response) {
            destroyTable('myTable');
            var headArr = ['ক্রমিক নং','্রাতিষ্ঠানিক কোড','অপারেশন কোড','অর্থনৈতিক গ্রূপ/কোড','বাজেট','সংশোধিত','ক্যাটাগরি','বাজেট ইয়ার' ,'অ্যাকশন'];
            createHeader('theadBudget', headArr);
            $('#tbodyBudget').empty();
            //console.log("ccccccccccc" + JSON.stringify(response));
            console.log(response);
           // console.log("dddddddddddd");
            //orgCode operationalCode economicCode budget  revisedBudget budgetYear category
            if (checkValue(response) != '-') {
                var count = 0;
                var json = JSON.parse(response);
                $.each(json, function (index, object) {
                    count++;

                    var trTableBody = $('<tr class="text-center"></tr>').appendTo($('#tbodyBudget'));
                    $('<td>' + checkValue(object['code']) + '</td>').appendTo(trTableBody);
                    $('<td>' + checkValue(object['orgCode']) + '</td>').appendTo(trTableBody);
                    $('<td>' + checkValue(object['operationalCode']) + '</td>').appendTo(trTableBody);
                    $('<td>' + checkValue(object['economicCode']) + '</td>').appendTo(trTableBody);
                    $('<td>' + checkValue(object['budget']) + '</td>').appendTo(trTableBody);
                    $('<td>' + checkValue(object['revisedBudget']) + '</td>').appendTo(trTableBody);
                    $('<td>' + checkValue(object['budgetYear']) + '</td>').appendTo(trTableBody);
                    $('<td>' + checkValue(object['category']) + '</td>').appendTo(trTableBody);
                    $('<td><div class="row text-center" style="padding-left: 7px;"> <div class="col-xs-6"><button class="btn btn-info" type="button" module="budget" value="' + object['id'] + '" onclick="fnEdit(this);" ><b><i class="fa fa-edit"></i></b></button></div>&nbsp;<div class="col-xs-6"><button class="btn btn-info" type="button" module="budget" value="' + object['id'] + '" onclick="fnDelete(this);" ><b><i class="fa fa-trash-o"></i></b></button></div></div></td>').appendTo(trTableBody);
                });
                $("#myTable").DataTable({

                    "iCookieDuration": 60,
                    "bStateSave": false,

                    "bProcessing": true,
                    "bRetrieve": true,
                    "bJQueryUI": true,

                    "searchHighlight": true,
                    "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
                    "order": [[0, "asc"]],
                    "dom": 'Blfrtip',

                    "buttons": [
                        {
                            extend: 'excel',
                            text: 'Excel',
                            className: 'btn btn-primary',
                            exportOptions: {
                                columns: [0, 1, 2]
                            },
                            orientation: 'landscape',
                            title: 'Budget List'
                        },
                        {
                            extend: 'pdf',
                            text: 'PDF',
                            className: 'btn btn-primary',
                            exportOptions: {
                                columns: [0, 1, 2]
                            },
                            orientation: 'landscape',
                            title: 'Budget List'

                        },
                        {
                            extend: 'print',
                            text: 'Print',
                            className: 'btn btn-primary',
                            exportOptions: {
                                columns: [0, 1, 2]
                            },
                            orientation: 'landscape',
                            title: 'Budget List'
                        }
                    ]
                });
            }
            console.log("kkkkkkkkkkk");
            hideLoading();
            console.log("ppppppppppp");
        }
    });
}
//orgCode operationalCode economicCode budget  revisedBudget budgetYear category
function resetForm() {
    $('#id').val('');
    $('#code').val('');
    $('#orgCode').val('');
    $('#operationalCode').val('');
    $('#economicCode').val('');
    $('#budget').val('');
    $('#revisedBudget').val('');
    $('#budgetYear').val('');
    $('#category').val('');
}

function setupForm(response) {
    var json = JSON.parse(response);
    $('#id').val(json['id']);
    $('#code').val(json['code']);
    $('#orgCode').val(json['carModel']);
    $('#operationalCode').val(json['carNo']);
    $('#economicCode').val(json['carNo']);
    $('#budget').val(json['carNo']);
    $('#revisedBudget').val(json['carNo']);
    $('#budgetYear').val(json['carNo']);
    $('#category').val(json['carNo']);
}

function afterSaveDataGet() {
    fetchAll();
}

