/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(function () {
    fetchAll();
});

function fetchAll() {
    $.get({
        url: path + "/car/fetchAll",
        success: function (response) {
            destroyTable('myTable');
            var headArr = ['ক্রমিক নং', 'গাড়ির মডেল', 'গাড়ি নম্বর', 'অ্যাকশন'];
            createHeader('theadCar', headArr);
            $('#tbodyCar').empty();
            //console.log("ccccccccccc" + JSON.stringify(response));
            console.log(response);
           // console.log("dddddddddddd");
            if (checkValue(response) != '-') {
                var count = 0;
                var json = JSON.parse(response);
                $.each(json, function (index, object) {
                    count++;

                    var trTableBody = $('<tr class="text-center"></tr>').appendTo($('#tbodyCar'));
                    $('<td>' + checkValue(object['code']) + '</td>').appendTo(trTableBody);
                    $('<td>' + checkValue(object['carModel']) + '</td>').appendTo(trTableBody);
                    $('<td>' + checkValue(object['carNo']) + '</td>').appendTo(trTableBody);
                    $('<td><div class="row text-center" style="padding-left: 7px;"> <div class="col-xs-6"><button class="btn btn-info" type="button" module="car" value="' + object['id'] + '" onclick="fnEdit(this);" ><b><i class="fa fa-edit"></i></b></button></div>&nbsp;<div class="col-xs-6"><button class="btn btn-info" type="button" module="car" value="' + object['id'] + '" onclick="fnDelete(this);" ><b><i class="fa fa-trash-o"></i></b></button></div></div></td>').appendTo(trTableBody);
                });
                $("#myTable").DataTable({

                    "iCookieDuration": 60,
                    "bStateSave": false,

                    "bProcessing": true,
                    "bRetrieve": true,
                    "bJQueryUI": true,

                    "searchHighlight": true,
                    "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
                    "order": [[0, "asc"]],
                    "dom": 'Blfrtip',

                    "buttons": [
                        {
                            extend: 'excel',
                            text: 'Excel',
                            className: 'btn btn-primary',
                            exportOptions: {
                                columns: [0, 1, 2]
                            },
                            orientation: 'landscape',
                            title: 'Car List'
                        },
                        {
                            extend: 'pdf',
                            text: 'PDF',
                            className: 'btn btn-primary',
                            exportOptions: {
                                columns: [0, 1, 2]
                            },
                            orientation: 'landscape',
                            title: 'Car List'

                        },
                        {
                            extend: 'print',
                            text: 'Print',
                            className: 'btn btn-primary',
                            exportOptions: {
                                columns: [0, 1, 2]
                            },
                            orientation: 'landscape',
                            title: 'Car List'
                        }
                    ]
                });
            }
            console.log("kkkkkkkkkkk");
            hideLoading();
            console.log("ppppppppppp");
        }
    });
}

function resetForm() {
    $('#id').val('');
    $('#code').val('');
    $('#carModel').val('');
    $('#carNo').val('');
}

function setupForm(response) {
    var json = JSON.parse(response);
    $('#id').val(json['id']);
    $('#code').val(json['code']);
    $('#carModel').val(json['carModel']);
    $('#carNo').val(json['carNo']);
}

function afterSaveDataGet() {
    fetchAll();
}

