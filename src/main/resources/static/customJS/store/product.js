/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(function(){
    fetchAll();
});

function fetchAll(){
    $.get({
       url : path+"/product/fetchAll",
       success:function(response){
           destroyTable('myTable');
            var headArr = ['ক্রমিক নং','আইটেম ধরণ','আইটেম নাম','অর্থনৈতিক গ্রূপ/কোড', 'আইটেম ডিটেলস','অ্যাকশন' ];
            createHeader('theadProduct',headArr);
            $('#tbodyProduct').empty();
            console.log(response);
            if(checkValue(response) != '-'){
                var count = 0;
                var json = JSON.parse(response);
                $.each(json,function(index,object){
                    count++;
                    var trTableBody = $('<tr class="text-center"></tr>').appendTo($('#tbodyProduct'));
                    $('<td>'+checkValue(object['code'])+'</td>').appendTo(trTableBody);
                    $('<td>'+checkValue(object['tblLookUpItemType']).lookUpName+'</td>').appendTo(trTableBody);
                    $('<td>'+checkValue(object['name'])+'</td>').appendTo(trTableBody);
                    $('<td>'+checkValue(object['economicCode'])+'</td>').appendTo(trTableBody);
                    $('<td>'+checkValue(object['remarks'])+'</td>').appendTo(trTableBody);
                    $('<td><div class="row text-center" style="padding-left: 7px;"> <div class="col-xs-6"><button class="btn btn-info" type="button" module="product" value="'+object['id']+'" onclick="fnEdit(this);" ><b><i class="fa fa-edit"></i></b></button></div>&nbsp;<div class="col-xs-6"><button class="btn btn-info" type="button" module="product" value="'+object['id']+'" onclick="fnDelete(this);" ><b><i class="fa fa-trash-o"></i></b></button></div></div></td>').appendTo(trTableBody);
                });
                $("#myTable").DataTable({

                "iCookieDuration": 60,
                "bStateSave": false,
                
                "bProcessing": true,
                "bRetrieve": true,
                "bJQueryUI": true,
                
                "searchHighlight": true,
                "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
                "order": [[0, "asc"]],
                "dom": 'Blfrtip',

                "buttons": [
                    {
                        extend: 'excel',
                        text: 'Excel',
                        className: 'btn btn-primary',
                        exportOptions: {

                            columns: [0, 1, 2]

                        },
                        orientation: 'landscape',
                        title: 'Product List'

                    },
                    {
                        extend: 'pdf',
                        text: 'PDF',
                        className: 'btn btn-primary',
                        exportOptions: {

                            columns: [0, 1, 2]
                        },
                        orientation: 'landscape',
                        title: 'Product List'

                    },
                    {
                        extend: 'print',
                        text: 'Print',
                        className: 'btn btn-primary',
                        exportOptions: {

                            columns: [0, 1, 2]

                        },
                        orientation: 'landscape',
                        title: 'Product List'
                    }
                ]
            });
            }
            hideLoading();
        }
    });
}

function resetForm(){
    $('#id').val('');
    $('#name').val('');
    $('#code').val('');
    $('#economicCode').val('');
    $('#tblLookUpItemType').val('');
    $('#remarks').val('');
}
function setupForm(response){
    var json = JSON.parse(response);
    $('#id').val(json['id']);
    $('#name').val(json['name']);
    $('#code').val(json['code']);
    $('#economicCode').val(json['economicCode']);
    $('#tblLookUpItemType').val(json['tblLookUpItemType'].lookUpPk);
    $('#remarks').val(json['remarks']);
}

function afterSaveDataGet(){
    fetchAll();
}


