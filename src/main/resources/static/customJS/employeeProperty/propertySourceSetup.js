function fnSavePropertySource() {
	if(valOnSubmit()){
		startLoading();
		$.post({
			url : path + '/poperty-source-setup/save',
			data : $('form[name=propertysourceform]').serialize(),
			success : function(response) {
				if(response == 'SuccessFullySaved'){
					showSuccess('<b>Success!</b><br> PropSource record is sucessfully created');
					afterSaveDataGet();
				}else{
					showError('<b>ERROR!</b><br> PropSource record is not sucessfully created');
					hideLoading();
				}				
			}
		});
	}
}

function afterSaveDataGet(){
	$.get({
		url : path + '/poperty-source-setup/fetchAll',
		success : function(response) {			
			if(response != ''){
				resetForm();
				prepareGrid(response);
			}else{
				showError('<b>ERROR!</b><br> PropSource records are not fetch sucessfully');
			}				
		}
	})
}

function prepareGrid(leadData){			
console.log(leadData);
	
	destroyTable('myTable');
	var headArr = ['Id','Name','Name Bangla','Property Type','Action' ];
	createHeader('theadPropSource',headArr);

		$('#tbodyPropSource').empty();

		if(checkValue(leadData) != '-'){
			leadData = JSON.parse(leadData);
			var count = 0;
			$.each(leadData, function( index, object ) {
				count++;
				var trTableBody = $('<tr class="text-center"></tr>').appendTo($('#tbodyPropSource'));
				$('<td data-sort="'+object['empPropSrcPk']+'">'+count+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['engSrcName'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['bngSrcName'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['propertyType'])+'</td>').appendTo(trTableBody);
				$('<td><button class="btn btn-info" type="button" module="poperty-source-setup" value="'+object['empPropSrcPk']+'" onclick="fnEdit(this)" ><b><i class="fa fa-edit"></i></b></button> &nbsp;&nbsp;<button class="btn btn-danger" type="button" module="poperty-source-setup" value="'+object['empPropSrcPk']+'" onclick="fnDelete(this)" ><b><i class="fa fa-trash-o"></i></b></button>').appendTo(trTableBody);
			});
		}

	createTable('myTable');	
	hideLoading();
}

function setupForm(response) {
	var json = JSON.parse(response);
	$('#engSrcName').val(json['engSrcName']);
	$('#bngSrcName').val(json['bngSrcName']);
	$('#propertyType').val(json['propertyType']);
	//$('#abbreviationNative').val(json['abbreviationNative']);
	//alert(json['pimsPayScaleId']['payScalePk']);
	//$('#pimsPayScaleId').val(json['pimsPayScaleId']['payScalePk']);
	$('#isactive').val(json['isActive']);	
	$('#empPropSrcPk').val(json['empPropSrcPk'])
}

function resetForm(){
	$('#engSrcName').val('');
	$('#bngSrcName').val('');
	$('#propertyType').val('');
	//$('#abbreviationNative').val('');
	//$('#pimsPayScaleId').val('-1');
	$('#isactive').val('-1');
}
