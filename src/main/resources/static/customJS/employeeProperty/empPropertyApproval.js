function fnSaveEmpPropertyApproval() {
    if($('#employeeFk').val()){
        if(valOnSubmit()){
            startLoading();
            $.post({
                url : path + '/emp-prop-approval/save',
                //enctype: 'multipart/form-data',
                data : $('form[name=empPropertyApprovalForm]').serialize(),
                success : function(response) {
                    if(response == 'SuccessFullySaved'){
                        showSuccess('<b>Success!</b><br> Asset record is sucessfully created');
                        afterSaveDataGet();
                    }else{
                        showError('<b>ERROR!</b><br> Asset record is not sucessfully created');
                        hideLoading();
                    }
                }
            });
        }
    }else{
        showError('<b>ERROR!</b><br> No employee selected!');

    }

}

function afterSaveDataGet(){
	$.get({
		url : path + '/poperty-source-setup/fetchAll',
		success : function(response) {			
			if(response != ''){
				resetForm();
				prepareGrid(response);
			}else{
				showError('<b>ERROR!</b><br> PropSource records are not fetch sucessfully');
			}				
		}
	})
}

function prepareGrid(leadData){			
console.log(leadData);
    destroyTable('WealthApprovalViewTable');
    var headArr = ['আবেদনের তারিখ','সম্পদের প্রকৃতি','তফসিল','মূল্য (টাকা)','বিক্রিত সম্পদ বিবরণী','Action'];
    createHeader('theadEmpPropApproval',headArr);

    $('#tbodyEmpPropApproval').empty();

    if(checkValue(leadData) != '-'){
        leadData = JSON.parse(leadData);
        var count = 0;
        $.each(leadData, function( index, object ) {
            count++;
            var trTableBody = $('<tr class="text-center"></tr>').appendTo($('#tbodyEmpPropApproval'));
            $('<td data-sort="'+object['applicationDate']+'">'+count+'</td>').appendTo(trTableBody);
            $('<td>'+checkValue(object['propSource']['bngSrcName'])+'</td>').appendTo(trTableBody);
            $('<td>'+checkValue(object['mouja'])+'</td>').appendTo(trTableBody);
            $('<td>'+checkValue(object['propValue'])+'</td>').appendTo(trTableBody);
            $('<td>'+checkValue(object['prevApprovalMemo'])+'</td>').appendTo(trTableBody);
            $('<td><button class="btn btn-info" type="button" module="emp-prop-approval" value="'+object['empPropApprovalPk']+'" onclick="fnEdit(this)" ><b><i class="fa fa-edit"></i></b></button> &nbsp;&nbsp;<button class="btn btn-danger" type="button" module="emp-prop-approval" value="'+object['empPropApprovalPk']+'" onclick="fnDelete(this)" ><b><i class="fa fa-trash-o"></i></b></button>').appendTo(trTableBody);
        });
    }

    $("#WealthApprovalViewTable").DataTable({
        "iCookieDuration": 60,
        "bStateSave": false,
        "bAutoWidth": true,
        "bScrollAutoCss": true,
        "bProcessing": true,
        "bRetrieve": true,
        "bJQueryUI": true,
        "sScrollX": "100%",
        "bScrollCollapse": true,
        "searchHighlight": true,
        //"lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
        "order": [[0, "asc"]],
        "dom": 'Blfrtip',

        "buttons": [
            {
                extend: 'excel',
                text: 'Excel',
                className: 'btn btn-primary',
                exportOptions: {

                    columns: [0, 1, 2, 3, 4]

                },
                orientation: 'landscape',
                title: 'Wealth Approval Report'

            },
            {
                extend: 'pdf',
                text: 'PDF',
                className: 'btn btn-primary',
                exportOptions: {

                    columns: [0, 1, 2, 3, 4]
                },
                orientation: 'landscape',
                title: 'Wealth Approval Report'

            },
            {
                extend: 'print',
                text: 'Print',
                className: 'btn btn-primary',
                exportOptions: {

                    columns: [0, 1, 2, 3, 4]

                },
                orientation: 'landscape',
                title: 'Wealth Approval Report'
            }
        ]
    });
    //createTable('WealthApprovalViewTable');

    hideLoading();
}

/*function setupForm(response) {
	var json = JSON.parse(response);
	$('#engSrcName').val(json['engSrcName']);
	$('#bngSrcName').val(json['bngSrcName']);
	$('#propertyType').val(json['propertyType']);
	//$('#abbreviationNative').val(json['abbreviationNative']);
	//alert(json['pimsPayScaleId']['payScalePk']);
	//$('#pimsPayScaleId').val(json['pimsPayScaleId']['payScalePk']);
	$('#isactive').val(json['isActive']);	
	$('#empPropSrcPk').val(json['empPropSrcPk'])
}*/

function resetForm(){
	$('#engSrcName').val('');
	$('#bngSrcName').val('');
	$('#propertyType').val('');
	//$('#abbreviationNative').val('');
	//$('#pimsPayScaleId').val('-1');
	$('#isactive').val('-1');
}


function fetchEmployeeInfo(){
    startLoading();
    var govId = $('#searchId').val();
    if(govId){
        $.get({
            url : path + '/employee-property/fetchEmployeeByGovId/'+govId,
            //data : "govId="+$('#searchId').val(),
            success : function(response) {
                hideLoading();
                //prepareGrid(null);
                if(response == 'emptyCode'){
                    var newWindow = window.open();
                    newWindow.document.write("<h1>Your Employee code is empty Please Enter</h1>");
                }else if(response == 'notFound'){
                    var newWindow = window.open();
                    newWindow.document.write("<h1>Your Employee code is incorrect Please Verify it</h1>");
                }else{
                    $('#empPropPk').val();
                    $('#employeeFk').val();
                    var json = JSON.parse(response);
                    $('#employeeFk').val(json.employeePk);
                    $('#empName').text(json.engName ? json.engName : '');
                    $('#empFatherName').text(json.engFatherName ? json.engFatherName : '');
                    $('#empDob').text(json.birthDate ? json.birthDate : '');
                    $('#empMotherName').text(json.engMotherName ? json.engMotherName : '');
                    /*$('#empRank').text(json.engName ? json.engName : '');
                    $('#empPosting').text(json.engName ? json.engName : '');*/
                    /*$('#empAppointment').text(json.engName ? json.engName : '');*/
                    /*$('#empOrganization').text(json.engName ? json.engName : '');*/
                    $('#empConfirmation').text(json.confirmationDate ? json.confirmationDate : '');
                    /*$('#empLocation').text(json.engName ? json.engName : '');*/
                    $('#empHomeDistrict').text(json.homeDistrict ? json.homeDistrict : '');
                    $('#empJoinDate').text(json.joinDate ? json.joinDate : '');

                    if(json.imagePath)
                        $.get({
                            url : path + '/employee-property/photo/'+govId,
                            success : function(res) {
                                document.getElementById("empImage").src = "data:image/png;base64," + res;
                            }});
                    if(json.employeePk)
                        $.get({
                            url : path + '/emp-prop-approval/fetchAllByEmpId/'+json.employeePk,
                            success : function(res) {
                                prepareGrid(res)
                            }});

                }


            }
        });
	}else {
        hideLoading();
        showError("<b>PIMS ID/NID is empty</b>");
    }

}
function fetchEmployeeInformation(){
    startLoading();
    var govId = $('#searchEmpId').val();
    if(govId){
        $.get({
            url : path + '/employee-property/fetchEmployeeByGovId/'+govId,
            success : function(response) {
                hideLoading();
                if(response == 'emptyCode'){
                    var newWindow = window.open();
                    newWindow.document.write("<h1>Your Employee code is empty Please Enter</h1>");
                }else if(response == 'notFound'){
                    var newWindow = window.open();
                    newWindow.document.write("<h1>Your Employee code is incorrect Please Verify it</h1>");
                }else{
                    $('#empPropPk').val();
                    $('#employeeFk').val();
                    var json = JSON.parse(response);
                    $('#employeeFk').val(json.employeePk);
                    $('#empInfoName').text(json.engName ? json.engName : '');
                    $('#empInfoFatherName').text(json.engFatherName ? json.engFatherName : '');
                    $('#empInfoDob').text(json.birthDate ? json.birthDate : '');
                    $('#empInfoMotherName').text(json.engMotherName ? json.engMotherName : '');
                    if(json.imagePath)
                        $.get({
                            url : path + '/employee-property/photo/'+govId,
                            success : function(res) {
                                document.getElementById("empInfoImage").src = "data:image/png;base64," + res;
                            }});
                }
            }
        });
	}else {
        hideLoading();
        showError("<b>PIMS ID/NID is empty</b>");
    }

}

function setupForm(response) {
    var json = JSON.parse(response);
    $('#engSrcName').val(json['engSrcName']);
    $('#bngSrcName').val(json['bngSrcName']);
    $('#propertyType').val(json['propertyType']);
    //$('#abbreviationNative').val(json['abbreviationNative']);
    //alert(json['pimsPayScaleId']['payScalePk']);
    //$('#pimsPayScaleId').val(json['pimsPayScaleId']['payScalePk']);
    $('#isactive').val(json['isActive']);
    $('#empPropSrcPk').val(json['empPropSrcPk'])
    $('#employeeForm :input, #employeeForm select').each(function() {
        if ($(this).attr('type') == 'date') {
            if (checkValue(json[$(this).attr('name')]) != '-') {
                $(this).val(formatDate(json[$(this).attr('name')]));
            }
        } else if($(this).attr('type') == 'file'){
            var path = json['imagePath'];
            editImage(path);
        }
        else {
            $(this).val(json[$(this).attr('name')]);
        }

    });
}

/*function editImage(path){
    var divImage = "<div class='imgcontainer' >";
    divImage = divImage
        + '<img class="img-responsive img-circle" id="productImg" src="' + path + '" alt="your image"/>';
    //  divImage = divImage + '<input class="delete" type="button" value="Delete" onclick="remove(this)" name="' + name + '"/></div></div></div>';
    $('#dvProductImgs').append(divImage);
    $("#dvImg").show();
}*/


var count = 0;
function fnAddMore() {
    count++;
    var tr = $('<tr></tr>');
    var td = $('<td><input type="text" name="attachmentList[' + count +'].attachmentName" placeholder="Document Title" class="form-control ng-untouched ng-pristine ng-invalid"/></td>').appendTo(tr);
    var td = $('<td><input type="file" name="attachmentList[' + count +'].attachment"/></td>').appendTo(tr);
    var td = $('<td><button class="btn btn-danger" type="button" onclick="fnDocDelete(this)"> <i class="fa fa-trash-o"></i></button></td>').appendTo(tr);
    $(tr).appendTo('#docAttachments');
}

function fnDocDelete(e) {
    $(e).parent().parent().remove();
    count--;

}


