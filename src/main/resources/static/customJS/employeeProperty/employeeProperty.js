function fnSaveEmployeeProperty() {
    if($('#employeeFk').val()){
        if(valOnSubmit()){
            startLoading();
            $.post({
                url : path + '/employee-property/save',
                data : $('form[name=employeePropertyForm]').serialize(),
                success : function(response) {
                    if(response === "SuccessFullySaved"){
                        showSuccess('<b>Success!</b><br> Asset record is sucessfully created');
                        afterSaveDataGet();
                    }else{
                        showError('<b>ERROR!</b><br> Asset record is not sucessfully created');
                        hideLoading();
                    }
                }
            });
        }
    }else{
        showError('<b>ERROR!</b><br> No employee selected!');

    }

}

function afterSaveDataGet(){
	$.get({
		url : path + '/poperty-source-setup/fetchAll',
		success : function(response) {			
			if(response != ''){
				resetForm();
				prepareGrid(response);
			}else{
				showError('<b>ERROR!</b><br> PropSource records are not fetch sucessfully');
			}				
		}
	})
}

function prepareGrid(leadData){			
console.log(leadData);
	
	/*destroyTable('myTable');
	var headArr = ['Id','Name','Name Bangla','Property Type','Action' ];
	createHeader('theadPropSource',headArr);

		$('#tbodyPropSource').empty();

		if(checkValue(leadData) != '-'){
			leadData = JSON.parse(leadData);
			var count = 0;
			$.each(leadData, function( index, object ) {
				count++;
				var trTableBody = $('<tr class="text-center"></tr>').appendTo($('#tbodyPropSource'));
				$('<td data-sort="'+object['empPropSrcPk']+'">'+count+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['engSrcName'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['bngSrcName'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['propertyType'])+'</td>').appendTo(trTableBody);
				$('<td><button class="btn btn-info" type="button" module="poperty-source-setup" value="'+object['empPropSrcPk']+'" onclick="fnEdit(this)" ><b><i class="fa fa-edit"></i></b></button> &nbsp;&nbsp;<button class="btn btn-danger" type="button" module="poperty-source-setup" value="'+object['empPropSrcPk']+'" onclick="fnDelete(this)" ><b><i class="fa fa-trash-o"></i></b></button>').appendTo(trTableBody);
			});
		}

	createTable('myTable');	*/
	hideLoading();
}

function setupForm(response) {
	var json = JSON.parse(response);
	$('#engSrcName').val(json['engSrcName']);
	$('#bngSrcName').val(json['bngSrcName']);
	$('#propertyType').val(json['propertyType']);
	//$('#abbreviationNative').val(json['abbreviationNative']);
	//alert(json['pimsPayScaleId']['payScalePk']);
	//$('#pimsPayScaleId').val(json['pimsPayScaleId']['payScalePk']);
	$('#isactive').val(json['isActive']);	
	$('#empPropSrcPk').val(json['empPropSrcPk'])
}

function resetForm(){
	$('#engSrcName').val('');
	$('#bngSrcName').val('');
	$('#propertyType').val('');
	//$('#abbreviationNative').val('');
	//$('#pimsPayScaleId').val('-1');
	$('#isactive').val('-1');
}


function fetchEmployeeInfo(){
    startLoading();
    var govId = $('#searchId').val();
    if(govId){
        $.get({
            url : path + '/employee-property/fetchEmployeeByGovId/'+govId,
            //data : "govId="+$('#searchId').val(),
            success : function(response) {
                hideLoading();
                if(response == 'emptyCode'){
                    var newWindow = window.open();
                    newWindow.document.write("<h1>Your Employee code is empty Please Enter</h1>");
                }else if(response == 'notFound'){
                    var newWindow = window.open();
                    newWindow.document.write("<h1>Your Employee code is incorrect Please Verify it</h1>");
                }else{
                    $('#empPropPk').val();
                    $('#employeeFk').val();
                    document.getElementById("empImage").src = "data:image/png;base64," + "";
                    var json = JSON.parse(response);
                    $('#employeeFk').val(json.employeePk);
                    $('#empName').text(json.engName ? json.engName : '');
                    $('#empFatherName').text(json.engFatherName ? json.engFatherName : '');
                    $('#empDob').text(json.birthDate ? json.birthDate : '');
                    $('#empMotherName').text(json.engMotherName ? json.engMotherName : '');
                    /*$('#empRank').text(json.engName ? json.engName : '');
                    $('#empPosting').text(json.engName ? json.engName : '');
                    $('#empAppointment').text(json.engName ? json.engName : '');
                    $('#empOrganization').text(json.engName ? json.engName : '');*/
                    $('#empConfirmation').text(json.confirmationDate ? json.confirmationDate : '');
                    /*$('#empLocation').text(json.engName ? json.engName : '');*/
                    $('#empHomeDistrict').text(json.homeDistrict ? json.homeDistrict : '');
                    $('#empJoinDate').text(json.joinDate ? json.joinDate : '');

                    if(json.imagePath)
                    $.get({
                        url : path + '/employee-property/photo/'+govId,
                        success : function(res) {
                            document.getElementById("empImage").src = "data:image/png;base64," + res;
                        }});
                    }
            }
        });
	}else {
        hideLoading();
        showError("<b>PIMS ID/NID is empty</b>");
    }


}
function fetchEmployeeInformation(){
    startLoading();
    var govId = $('#searchEmpId').val();
    if(govId){
        $.get({
            url : path + '/employee-property/fetchEmployeeByGovId/'+govId,
            success : function(response) {
                hideLoading();
                if(response == 'emptyCode'){
                    var newWindow = window.open();
                    newWindow.document.write("<h1>Your Employee code is empty Please Enter</h1>");
                }else if(response == 'notFound'){
                    var newWindow = window.open();
                    newWindow.document.write("<h1>Your Employee code is incorrect Please Verify it</h1>");
                }else{
                    $('#empPropPk').val();
                    $('#employeeFk').val();
                    var json = JSON.parse(response);
                    $('#employeeFk').val(json.employeePk);
                    $('#empInfoName').text(json.engName ? json.engName : '');
                    $('#empInfoFatherName').text(json.engFatherName ? json.engFatherName : '');
                    $('#empInfoDob').text(json.birthDate ? json.birthDate : '');
                    $('#empInfoMotherName').text(json.engMotherName ? json.engMotherName : '');
                    if(json.imagePath)
                        $.get({
                            url : path + '/employee-property/photo/'+govId,
                            success : function(res) {
                                document.getElementById("empInfoImage").src = "data:image/png;base64," + res;
                            }});
                }
            }
        });
    }else {
        hideLoading();
        showError("<b>PIMS ID/NID is empty</b>");
    }

}

function addMore(tableName) {
    // $(this).closest('tr').append($(this).closest('tr').clone());
    // var thisRow = $( this ).closest( 'tr' )[0];
    // $('#'+tableName+' tr:last').after( $( thisRow ).clone());
    var $tableBody = $('#'+tableName).find("tbody"),
        $trLast = $tableBody.find("tr:last"),
        $trNew = $trLast.clone();
    $trNew.find("input td:nth-child(3)").attr("name","jataaa");
    //$('#id td:nth-child(3)');
    $trLast.after($trNew);
}

$(document.body).on("click",".add-more", function() {
    var $tableBody = $(this).parents('table').find("tbody"),
        $trLast = $tableBody.find("tr:last"),
        $trNew = $trLast.clone();
    $trLast.after($trNew);
    console.log();
    if($(this).parents('table').attr("id") === "myTableMovable"){
        $(this).parents('table').find("tbody tr.tbl-row").each(function (ix, element) {
            $(element).find("select").eq(0).attr("id", "empPropSrcPkMvl" + ix);
            $(element).find("select").eq(0).attr("name", "tblEmpPropertyMovable[" + ix + "].propSource.empPropSrcPk");
            $(element).find("input").eq(0).attr("id", "achievementDate[" + ix + "]");
            $(element).find("input").eq(0).attr("name", "tblEmpPropertyMovable[" + ix + "].achievementDate");
            $(element).find("input").eq(1).attr("id", "propByName[" + ix + "]");
            $(element).find("input").eq(1).attr("name", "tblEmpPropertyMovable[" + ix + "].propByName");
            $(element).find("input").eq(2).attr("id", "propStyleDetail[" + ix + "]");
            $(element).find("input").eq(2).attr("name", "tblEmpPropertyMovable[" + ix + "].propStyleDetail");
            $(element).find("input").eq(3).attr("id", "achievementDescription[" + ix + "]");
            $(element).find("input").eq(3).attr("name", "tblEmpPropertyMovable[" + ix + "].achievementDescription");
            $(element).find("input").eq(4).attr("id", "sourceOfIncome[" + ix + "]");
            $(element).find("input").eq(4).attr("name", "tblEmpPropertyMovable[" + ix + "].sourceOfIncome");
            $(element).find("input").eq(5).attr("id", "soldTransferdProp[" + ix + "]");
            $(element).find("input").eq(5).attr("name", "tblEmpPropertyMovable[" + ix + "].soldTransferdProp");
            $(element).find("input").eq(6).attr("id", "remarks[" + ix + "]");
            $(element).find("input").eq(6).attr("name", "tblEmpPropertyMovable[" + ix + "].remarks");

        });
    }else if($(this).parents('table').attr("id") === "myTableImmovable"){
        $(this).parents('table').find("tbody tr.tbl-row").each(function (ix, element) {
            $(element).find("select").eq(0).attr("id", "empPropSrcPkMvl" + ix);
            $(element).find("select").eq(0).attr("name", "tblEmpPropertyDetail[" + ix + "].propSource.empPropSrcPk");
            $(element).find("input").eq(0).attr("id", "achievementDate[" + ix + "]");
            $(element).find("input").eq(0).attr("name", "tblEmpPropertyDetail[" + ix + "].achievementDate");
            $(element).find("input").eq(1).attr("id", "propByName[" + ix + "]");
            $(element).find("input").eq(1).attr("name", "tblEmpPropertyDetail[" + ix + "].propByName");
            $(element).find("input").eq(2).attr("id", "propStyleDetail[" + ix + "]");
            $(element).find("input").eq(2).attr("name", "tblEmpPropertyDetail[" + ix + "].propStyleDetail");
            $(element).find("input").eq(3).attr("id", "achievementDescription[" + ix + "]");
            $(element).find("input").eq(3).attr("name", "tblEmpPropertyDetail[" + ix + "].achievementDescription");
            $(element).find("input").eq(4).attr("id", "sourceOfIncome[" + ix + "]");
            $(element).find("input").eq(4).attr("name", "tblEmpPropertyDetail[" + ix + "].sourceOfIncome");

            $(element).find("input").eq(5).attr("id", "joiningAmountOfProp[" + ix + "]");
            $(element).find("input").eq(5).attr("name", "joiningAmountOfProp[" + ix + "].joiningAmountOfProp");
            $(element).find("input").eq(6).attr("id", "soldTransferdProp[" + ix + "]");
            $(element).find("input").eq(6).attr("name", "tblEmpPropertyDetail[" + ix + "].soldTransferdProp");
            $(element).find("input").eq(7).attr("id", "remarks[" + ix + "]");
            $(element).find("input").eq(7).attr("name", "tblEmpPropertyDetail[" + ix + "].remarks");

        });
    }

});


$(document.body).on("click",".remove-row", function(){
    if(confirm("Are you sure?")){
        // alert("total row: "+$(this).parents('table').find('tr').length);
        if($(this).parents('table').find('tr').length > 2){
            $(this).closest('tr').remove();
        }else{
            alert("Can't delete only single element!");
        }
    }
});