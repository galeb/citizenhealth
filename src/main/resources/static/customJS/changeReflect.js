$( document ).ready(function() {
	fetchDataFromSession();
	var dictionary = '';
});

function fetchDataFromSession(){	
	$.get({
		url : path + '/fetchSessionData',
		success : function(response) {		
			
			if(response != ''){
				var jsonResponse = JSON.parse(response);
				if(checkValue(jsonResponse['LSTMAINMENU'])){
					setMenu(JSON.parse(jsonResponse['LSTMAINMENU']));
					setLanguage(JSON.parse(jsonResponse['LANGUAGEMAPPER']));
				}
				
			}else{
				//showError('<b>ERROR!</b><br> Menu records are not fetch sucessfully');	
			}				
		}
	})
}

function setLanguage(LANGUAGEMAPPER){
	dictionary = LANGUAGEMAPPER;
	var flag = localStorage.getItem('languageFlag');
	if(flag== '' || flag == undefined || flag == 'undefined' || flag == null || flag == 'null'){
		flag ='en';
	}
	localStorage.setItem('languageFlag', flag);
	
	var translator = $('body').translate({
		lang : flag,
		t : dictionary
	}); 
	
}
function setMenu(lstMainMenu){
	$('#idAppend').empty();
	$.each(lstMainMenu, function(index, object) {
	//	console.log(object);
		var li = $('<li class="nav-item "></li>').appendTo($('#idAppend'));
		var parentA = $('<a class="nav-link href=""></a>').attr('href',path+object['parent']['url']).appendTo(li);
		$('<i class="'+object['parent']['icon']+'"></i><span class="trn">'+ object['parent']['name'] + '</span>').appendTo(parentA);
		if (object['child'] != 0) {
			$(li).addClass("nav-dropdown");
			$(parentA).addClass("nav-dropdown-toggle");
			var ul = $('<ul class="nav-dropdown-items"></ul>').appendTo(li);
			fnMakeChild(object['child'], ul);
		}
	});
}

function fnMakeChild(childObject, ul) {

	$.each(childObject, function(index, object) {

		var childLi = $('<li class="nav-item"></li>').appendTo(ul);
		var childA = $('<a class="nav-link href=""></a>').attr('href',path+object['url']).appendTo(childLi);
		$('<i class="' + object['icon'] + '"></i><span class="trn">'+ object['name'] + '</span>').appendTo(childA);

	});
}

function onClickChange(flag) {	
	localStorage.setItem('languageFlag', flag);
	var translator = $('body').translate({
		lang : flag,
		t : dictionary
	});
	translator.lang(flag);
}