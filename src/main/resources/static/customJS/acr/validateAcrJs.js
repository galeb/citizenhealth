/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(function () {
    fetchAllACR('onload');
});
function fetchAllACR(src) {
    $('#searchErrorMsg').html("");
    if (src == 'search') {
        if ($("#txtEmpCodeSearch").val() == '' && parseInt($('#optuseridSearch').val()) == -1 && parseInt($('#optrankidSearch').val()) == -1 && parseInt($('#optbatchidSearch').val()) == -1 && $("#txtFromDateSearch").val() == '' && $("#txtToDateSearch").val() == '') {
            $('#searchErrorMsg').html("Please Select any of the filter");
            return;
        } else {
            if ($("#txtFromDateSearch").val() != '' && $("#txtToDateSearch").val() == '') {
                $('#searchErrorMsg').html("Please Select To Date from filter");
                return;
            }
            if($("#txtFromDateSearch").val()=='' && $("#txtToDateSearch").val()!=''){
                $('#searchErrorMsg').html("Please Select From Date from filter");
                return;
            }
            if ($("#txtFromDateSearch").val() != '' && $("#txtToDateSearch").val() != '') {
            	var from = new Date(parseInt($("#txtFromDateSearch").val().split("/")[2]),parseInt($("#txtFromDateSearch").val().split("/")[1])-1,parseInt($("#txtFromDateSearch").val().split("/")[0]));
                var to = new Date(parseInt($("#txtToDateSearch").val().split("/")[2]),parseInt($("#txtToDateSearch").val().split("/")[1])-1,parseInt($("#txtToDateSearch").val().split("/")[0]));
               
                if (to.getTime() < from.getTime()) {
                    $('#searchErrorMsg').html("Invalid Date Range Please Select Correct From And To Date");
                    return;
                }
            }
        }
    }

    $('#searchErrorMsg').html("");
    startLoading();
    $.get({
        url: path + '/acr/fetchAll',
        data: {empCode: $("#txtEmpCodeSearch").val(), userid: $('#optuseridSearch').val(), rankid: $('#optrankidSearch').val(), batchid: $('#optbatchidSearch').val(), from: $("#txtFromDateSearch").val(), to: $("#txtToDateSearch").val()},
        success: function (response) {
            if (response != '') {
                prepareGrid(response);
            } else {
                showError('<b>ERROR!</b><br> ACR records are not fetch sucessfully');
                hideLoading();
            }
            hideLoading();
        }
    });
}
function fnSelectAll(){
    destroyTable('myTable');
    $('#myTable tr').each(function (i, row){
      $(this).find('input[type="checkbox"]').prop('checked',true);
    });
    setDatatable();
}
function fnDeSelectAll(){
    destroyTable('myTable');
    $('#myTable tr').each(function (i, row){
      $(this).find('input[type="checkbox"]').prop('checked',false);
    });
    setDatatable();
}

function prepareGrid(leadData) {
    destroyTable('myTable');
    var headArr = ['#','Select', 'Govt Id', 'Name, Designation, Organization, Location', 'RIO Number', 'CSO Number', 'Rank', 'Batch', 'Entry Date', 'ACR Type', 'Year(Duration)', 'Remarks', 'Action'];
    createHeader('theadAcr', headArr);
    $('#tbodyAcr').empty();
    if (checkValue(leadData) != '-') {
        leadData = JSON.parse(leadData);
        var count = 0;
        $.each(leadData, function( index, object ) {
             console.log(object);
            if(parseInt(checkValue(object['isApproved']))!=1){
                count++;
                var trTableBody = $('<tr class="text-center"></tr>').appendTo($('#tbodyAcr'));
                $('<td>'+count+'</td>').appendTo(trTableBody);
                $('<td><input type="checkbox" acrpk="'+object['acrPk']+'" /></td>').appendTo(trTableBody);
                $('<td>'+(valueCheck(object['pimsId'])!='' ? checkValue(resolveJSON(object,'pimsId.govId')) : '-') +'</td>').appendTo(trTableBody);
                $('<td>'+(valueCheck(object['pimsId'])!='' ? checkValue(resolveJSON(object,'pimsId.engName')) : '-') + ',' + checkValue(resolveJSON(object,'pimsId.designation'))+","+checkValue(resolveJSON(object,'pimsId.organisation.nameNative'))  + ',' + checkValue(resolveJSON(object,'pimsId.location.locationName'))+'</td>').appendTo(trTableBody);
                $('<td>'+checkValue(object['oruNo'])+'</td>').appendTo(trTableBody);
                $('<td>'+checkValue(object['csoNo'])+'</td>').appendTo(trTableBody);
                $('<td>'+(valueCheck(object['pimsId'])!='' ? checkValue(resolveJSON(object,'pimsId.rank.rankName')) : '-')+'</td>').appendTo(trTableBody);
                $('<td>'+(valueCheck(object['pimsId'])!='' ? checkValue(resolveJSON(object,'pimsId.batch')) : '-')+'</td>').appendTo(trTableBody);
                $('<td>'+formatDateAcr(checkValue(object['createdOn']))+'</td>').appendTo(trTableBody);
                $('<td>'+acrType(checkValue(object['acrType']))+'</td>').appendTo(trTableBody);
                $('<td>'+checkValue(object['acrYear'])+' ('+formatDateAcr(checkValue(object['from']))+'-'+formatDateAcr(checkValue(object['to']))+') '+' ('+dayCount(formatDateAcr(checkValue(object['from'])),formatDateAcr(checkValue(object['to'])))+'-(days))'+'</td>').appendTo(trTableBody);
                $('<td>'+checkValue(object['remark'])+'</td>').appendTo(trTableBody);
                $('<td><div class="row" style="padding-left: 7px;"> <div class="col-xs-6"><button class="btn btn-info" type="button" module="acr" value="'+object['acrPk']+'" onclick="fnValidateACR(this);" ><b><i class="fa fa-check"></i></b></button></div>&nbsp;<div class="col-xs-6"><button class="btn btn-info" type="button" module="acr" value="'+object['acrPk']+'" onclick="fnView(this)" ><b><i class="fa fa-eye"></i></b></button></div></div></td>').appendTo(trTableBody);
                // &nbsp;&nbsp;<button class="btn btn-danger" type="button" module="acr" value="'+object['acrPk']+'" onclick="fnDelete(this)" ><b><i class="fa fa-trash-o"></i></b></button>
            }
        });
    }
   setDatatable();
   hideLoading();
}
function setDatatable(){
     $('#myTable').DataTable({
                  "iCookieDuration": 60,
                "bStateSave": false,
                "bAutoWidth": true,
                "bScrollAutoCss": true,
                "bProcessing": true,
                "bRetrieve": true,
                "bJQueryUI": true,
                "sScrollX": "100%",
                "bScrollCollapse": true,
                "searchHighlight": true,
                    "lengthMenu" : [ [ 5, 10, 25, 50, -1 ], [ 5, 10, 25, 50, "All" ] ],
                    "order": [[ 0, "asc" ]],
                    dom: 'Blfrtip',
                    buttons: [
                        {
                            extend: 'excel',
                            text: 'Excel',
                            className: 'btn btn-primary',
                            exportOptions: {
                                columns: [0,2,3,4,5,6,7,8,9,10,11] 
                            },
                            orientation: 'landscape',
                            title: 'ACR Report'
                            
                        },
                        {
                            extend: 'pdf',
                            text: 'PDF',
                            className: 'btn btn-primary',
                            exportOptions: {
                                columns: [0,2,3,4,5,6,7,8,9,10,11] 
                            },
                            orientation: 'landscape',
                            title: 'ACR Report'
                        },
                        {
                            extend: 'print',
                            text: 'Print',
                            className: 'btn btn-primary',
                            exportOptions: {
                                columns: [0,2,3,4,5,6,7,8,9,10,11] 
                            },
                            orientation: 'landscape',
                            title: 'ACR Report'
                        }
                    ]
                });	
}
function dayCount(from, to) {
    if (from != '-' && to != '-') {
        debugger
        var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
        var formArr = from.split("/");
        var firstDate = new Date(parseInt(formArr[2]), parseInt(formArr[1]) - 1, parseInt(formArr[0]));
        var formArr = to.split("/");
        var secondDate = new Date(parseInt(formArr[2]), parseInt(formArr[1]) - 1, parseInt(formArr[0]));

        var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime()) / (oneDay)));
        return diffDays;
    }
    return 0;

}
function fnView(e) {
    startLoading();
    window.location.href = path + '/acr/view?validate=' + $(e).attr('value');

}
function fnValidateACR(e) {
    $('#validatedialog').dialog('open');
    $('#validateyes').attr('onclick', 'fnExecuteValidate("' + $(e).attr('module') + '",' + $(e).attr('value') + ')');
}
function fnValidateAll(){
    var count =0 ;
    destroyTable('myTable');
    $('#myTable tr').each(function (i, row){
        if($(this).find('input[type="checkbox"]').prop('checked')){
            count++;
        }
    });
    setDatatable();
    if(count>0){
        $('#validatedialog').dialog('open');
        $('#validateyes').attr('onclick', 'fnExecuteValidateAll()');
    }else{
        showError('<b>ERROR!</b><br>.No ACR is Seleceted');
    }
}
function fnExecuteValidateAll(){
    var ids = '';
    destroyTable('myTable');
    $('#myTable tr').each(function (i, row){
        if($(this).find('input[type="checkbox"]').prop('checked')){
           ids = ids + $(this).find('input[type="checkbox"]').attr('acrpk') + ",";
        }
    });
    setDatatable();
    closeValidatePopup();
    startLoading();
    $.get({
        url: path + "/acr/approveAll/",
        data : {ids : ids},
        success: function (response) {
            if (response == 'Successfully Approved') {
                showSuccess('<b>Success!</b><br> ACR successfully validated.');
            } else {
                showError('<b>ERROR!</b><br>.' + response);
            }
            fetchAllACR('onload');
        }
    });
}
function fnExecuteValidate(module, id) {

    closeValidatePopup();
    startLoading();
    $.get({
        url: path + '/' + module + "/approve/" + id,
        success: function (response) {
            if (response == 'Successfully Approved') {
                showSuccess('<b>Success!</b><br> ACR is successfully validated.');
            } else {
                showError('<b>ERROR!</b><br>.' + response);
            }
            fetchAllACR('onload');
        }
    });
}

function valueCheck(value) {
    if (value == undefined || value == '' || value.trim == '') {
        return "";
    }
    return value;
}
function reset() {
    $("#txtEmpCodeSearch").val('');
    $('#optuseridSearch').val('-1');
    $('#optrankidSearch').val('-1');
    $('#optbatchidSearch').val('-1');
    $("#txtFromDateSearch").val('');
    $("#txtToDateSearch").val('');
}