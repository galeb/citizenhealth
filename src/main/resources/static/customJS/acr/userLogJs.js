/**
 * 
 */
$(function(){
//	startLoading();
    fnFetchUserLog('onload');
});

function fnFetchUserLog(operation){
    //startLoading();
    if(operation=='search'){
            if(parseInt($('#optuseridSearch').val())==-1 && parseInt($('#optAcrYear').val())==-1 && $("#txtFromDateSearch").val()=='' && $("#txtToDateSearch").val()=='' ){
            $('#searchErrorMsg').html("Please Select any of the filter");
            return;
        }else{
            if($("#txtFromDateSearch").val()!='' && $("#txtToDateSearch").val()==''){
                $('#searchErrorMsg').html("Please Select To Date from filter");
                return;
            }
            if($("#txtFromDateSearch").val()=='' && $("#txtToDateSearch").val()!=''){
                $('#searchErrorMsg').html("Please Select From Date from filter");
                return;
            }
            if($("#txtFromDateSearch").val()!='' && $("#txtToDateSearch").val()!=''){
                var from = new Date(parseInt($("#txtFromDateSearch").val().split("/")[2]),parseInt($("#txtFromDateSearch").val().split("/")[1])-1,parseInt($("#txtFromDateSearch").val().split("/")[0]));
                var to = new Date(parseInt($("#txtToDateSearch").val().split("/")[2]),parseInt($("#txtToDateSearch").val().split("/")[1])-1,parseInt($("#txtToDateSearch").val().split("/")[0]));
                debugger;
                if(to.getTime() < from.getTime()){
                    $('#searchErrorMsg').html("Invalid Date Range Please Select Correct From And To Date");
                    return;
                }else{
                    $('#searchErrorMsg').html('');
                    startLoading();
                    $.get({
                        url : path +'/acr/fetchUserlog',
                        data : { userid : $('#optuseridSearch').val() , acryear : $('#optAcrYear').val() , from : $("#txtFromDateSearch").val() , to : $("#txtToDateSearch").val() },
                        success : function(response) {
                            prepareGrid(response);
                            hideLoading();
                        }
                    });
                }
            }
        }
    }else{
        console.log('without search');
        $.get({
            url : path +'/acr/fetchUserlog',
            data : { userid : $('#optuseridSearch').val() , acryear : $('#optAcrYear').val() , from : $("#txtFromDateSearch").val() , to : $("#txtToDateSearch").val() },
            success : function(response) {
                console.log(response);
                prepareGrid(response);
            }
        });
    }    
}

function prepareGrid(leadData){			
    destroyTable('myTable');

    var headArr = ['#','Username','ACR Year','Total(Days)','Date / Average'];
    createHeader('theadAcr',headArr);

    $('#tbodyAcr').empty();
    if(checkValue(leadData) != '-'){
        leadData = JSON.parse(leadData);
        var count = 0;
        $.each(leadData, function( index, object ) {
            count++;
            var trTableBody = $('<tr class="text-center"></tr>').appendTo($('#tbodyAcr'));
            console.log(object);
        
            $('<td>'+count+'</td>').appendTo(trTableBody);
            $('<td>'+checkValue(object['acrUser'])+'</td>').appendTo(trTableBody);
            $('<td>'+checkValue(object['acrYear'])+'</td>').appendTo(trTableBody);
            $('<td>'+checkValue(object['totalCount'])+'('+object['distinctDates'].length+')'+'</td>').appendTo(trTableBody);
            $('<td>'+(parseFloat(checkValue(object['totalCount']))/object['distinctDates'].length)+'</td>').appendTo(trTableBody);
            // &nbsp;&nbsp;<button class="btn btn-danger" type="button" module="acr" value="'+object['acrPk']+'" onclick="fnDelete(this)" ><b><i class="fa fa-trash-o"></i></b></button>
        });
    }
    hideLoading();
    $('#myTable').DataTable({
        "autoWidth":false, 
        "JQueryUI":true,
        "searchHighlight": true,
        "lengthMenu" : [ [ 5, 10, 25, 50, -1 ], [ 5, 10, 25, 50, "All" ] ],
        "order": [[ 0, "asc" ]],
        dom: 'Blfrtip',
        buttons: [
            {
                extend: 'excel',
                text: 'Excel',
                className: 'btn btn-primary',
                exportOptions: {
                    columns: [0,1,2,3,4] 
                },

                title: 'ACR Report'

            },
            {
                extend: 'pdf',
                text: 'PDF',
                className: 'btn btn-primary',
                exportOptions: {
                    columns: [0,1,2,3,4] 
                },

                title: 'ACR Report'
            },
            {
                extend: 'print',
                text: 'Print',
                className: 'btn btn-primary',
                exportOptions: {
                    columns: [0,1,2,3,4] 
                },

                title: 'ACR Report'
            }
        ]
    });	
    
 //   createTableWithButton('myTable');	
   
} 

function reset(){
    $('#searchErrorMsg').html('');
    $('#optuseridSearch').val('-1');
    $('#optAcrYear').val('-1');
    $("#txtFromDateSearch").val('');
    $("#txtToDateSearch").val(''); 
}


