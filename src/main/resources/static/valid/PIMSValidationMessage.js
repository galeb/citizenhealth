/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var VALIDATE_MSG_REQUIRED = 'Please Enter';
var VALIDATE_MSG_NO_SPACE = 'Space is not allowed';
var VALIDATE_MSG_INVALID_EMAIL = 'Please Enter valid email ID';
var VALIDATE_MSG_INVALID_EMAIL_MSGBOX = ' Only Min. 6 Max. 1000 alphanumeric and Special Characters(@,.,-,_) are allowed.';
var VALIDATE_MSG_EMAIL_INVALID = 'Only Min. 6 Max. 50 alphanumeric and Special Characters(@,.,-,_) are allowed.';
var VALIDATE_MSG_MINIMUM = 'Minimum';
    var VALIDATE_MSG_CHARS_ALLOWED = 'characters';
    var VALIDATE_MSG_MAXIMUM = 'Allows max.';
    var VALIDATE_MSG_CHANGESKEYWORD = 'Only alphabets, numbers and special characters (@,*, (,), -, +,/,.,&,Comma,Space) are allowed.';
    var VALIDATE_MSG_ONLY_NUMERIC = 'Only numerics are allowed';
    var VALIDATE_MSG_ONLY_POSITIVE = 'Only positive numbers without decimal are allowed.';
    var VALIDATE_MSG_ONLY_POSITIVE_1 = 'Only positive numbers are allowed.';
    var VALIDATE_MSG_ONLY_ALPHABETS = 'Only alphabets are allowed.';
    var VALIDATE_MSG_INVALID_CITY = 'Invalid city';
    var VALIDATE_MSG_INVALID_PHONE = 'Invalid phoneno';
    var VALIDATE_MSG_ONLY_ALPHA_NUM_SPECIAL = 'Please enter characters other than (=, &#39;, &quot; , $ , # , %)';
    var VALIDATE_MSG_ONLY_ALPHA_NUM_SPECIAL_COMMA = 'Please enter characters other than (=, &#39;, &quot; ,(comma))';
    var VALIDATE_MSG_ONLY_ALPHA_NUM_SPECIAL_CHAR = 'max. 5000 alphanumeric and special characters (*,@,/,+,-,Space,dot) are allowed.';
    var VALIDATE_MSG_NUMERICFEWALPHA = 'max. 50 alphabets, numbers and special characters (dot, -, /, comma, space, &, _) are allowed.';
    var VALIDATE_MSG_ONLY_ALPHA_NUM_SPECIAL_MARQUEE = 'Allows Max. 2000 characters, numbers, special characters (* - + / , .)';
    var VALIDATE_MSG_INVALID_PASSWORD_SPECIAL_CHAR = 'Password must comprise of at least one alphanumeric and special character (!,@,#,$,_,.,(,))';
    var VALIDATE_MSG_INVALID_FULL_NAME = 'Invalid fullname';
    var VALIDATE_MSG_INVALID_DOC_NAME = 'Invalid document name';
    var VALIDATE_MSG_INVALID_COMPANY_NAME = 'Invalid company name !';
    var VALIDATE_MSG_INVALID_KEYWORD = 'Max. 100 characters numbers and special characters (/ , - , ., ,, &) are allowed.';
    var VALIDATE_MSG_INVALID_WEBSITE = 'Invalid website';
    var VALIDATE_MSG_INVALID_ALPHA_NUM_SPACE = 'Only Alphabates , Numbers and Spaces are allowed.';
    var VALIDATE_MSG_INVALID_CLIENT_NAME = 'Invalid client name';
    var VALIDATE_MSG_NUM_DECIMAL = 'Invalid number it must contain decimal upto';
    var VALIDATE_MSG_INVALID_CONF_PASSWORD = 'Confirm password does not match with';
    var VALIDATE_MSG_SAME_PASSWORD_AS_LOGINID = 'Password cannot be same as email ID';
    var VALIDATE_MSG_SELECT = 'Please Select';
    var VALIDATE_MSG_COMMAINVALID='Max. 100 alphanumeric and Special Characters (), -, ,/,Space,& are allowed.';
    var VALIDATE_MSG_TXTAREA='characters numbers and special characters (/ , - , ., ,&#39;,&,(,), comma, space) are allowed.';
    var VALIDATE_MSG_ADDRESS='Characters numbers and special characters (/ , - , ., ,, space, &, :, ;) are allowed.';
    var VALIDATE_MSG_UPTO='max. {0} Digit(s) after decimal are allowed.'; 
    var VALIDATE_MSG_DECIMALPOINT='digits after decimal';
    var VALIDATE_MSG_COMMON_NUM_DECIMAL='Allows only numeric values and';
    var VALIDATE_MSG_UPTO_FIVE_DECIMAL='Allowed  only 1 to 5 decimal';
    var VALIDATE_MSG_ONLY_NUMERIC_WITH_COMMA = 'Only numerics are allowed including comma in it.';
    var VALIDATE_MSG_UPTO_NINE_NUMERIC='Only numerics 1 to 9 are allowed';
    var VALIDATE_MSG_ALLOW_MAX='Allows Max. ';
    var VALIDATE_MSG_BRIEF='characters,numbers and special characters ((,),_,:,&,+, /, -, ., Comma,  Space)';
    var VALIDATE_MSG_FORM_NAME='characters,numbers and special characters ( / , - , .)';
    var VALIDATE_MSG_TENDERBRIEF='characters,numbers and special characters ((,),_,:,&,+, /, -, .,&#39;,Comma,  Space)';
    var VALIDATE_MSG_SPECIALALPHANUMERIC='alphabets, numbers and special characters (@,*, (,), -, +,/,., Space)';
    var INVALID='Invalid';
    var COMMON_UPTO_FIVE_DECIMAL='and upto five decimal allowed';
    var VALIDATE_BETWEEN='must be between ';
    var VALIDATE_GREATER='must be greater than ';
    var VALIDATE_LESSAR='must be smaller than';
    var VALIDATE_GREATER_SYSDATE='must be greater than current Date and Time';
    var VALIDATE_PREPONE_DATE='You are not allowed to prepone';
    var VALIDATE_LESSAR_SYSDATE='must be smaller than current Date and Time';
    var VALIDATE_NUMBER_LENGTH='numbers';
    var VALIDATE_MSG_MAX='Max.';
    var VALIDATE_MSG_INCDECVAL_IN_PER='(%) should be from 0 to 100';
    var VALIDATE_MSG_REBATE_PERCENTAGE='Rebate cannot be less than 0% and greater than 100%';
    var VALIDATE_MSG_WEIGHTAGE_PERCENTAGE='Passing criteria should be less than or equal to scoring criteria';
    var VALIDATION_VALIDATIONMESSAGE='Allows Characters and Special Characters (Space, (.), (,))';
    var VALIDATE_TENDER_REFERENCE_NO='characters and special characters ((,), -, +,/,., Space)';
    var VALIDATE_TENDER_ADDRESS='Characters and Special Characters (,), -, /,., Space)';
    var VALIDATE_GE_CURRDATE='must be greater than or equals to current date and time.';
    var VALIDATE_GT_CURRDATE='must be greater than to current Date and Time.';
    var VALIDATE_GT_COMPDATE='must be greater than';
    var VALIDATE_GE_COMPDATE='must be greater than or equal to';
    var VALIDATE_LE_COMPDATE='must be less than or equal to';
    var VALIDATE_LT_COMPDATE='must be less than';
    var VALIDATE_LE_CURRDATE='must be less than or equal to current date.';
    var VALIDATE_REMARKS='Characters numbers and special characters ((,), -, +,/,., Space)';
    var VALIDATE_MSG_INVALID_ALPHANUMWITHNEWCHAR='Allows Max. 1000 alphabets, numbers and special characters ((,), -, +,/,., Space) ';
    var VALIDATE_MSG_INVALID_ALPHANUMWITHCHAR='Allows Max. 500 alphabets, numbers and Special Characters (/,-,.,, and space)';
    var VALIDATE_MSG_INVALID_ALPHANUMERICWITHSPECIAL='Allows Max. 1000 alphabets, numbers and special characters ((,),comma, -, +,/,., Space)';
    var VALIDATE_MSG_INVALID_ALPHAWITHSPECIAL='Alphabets and special characters (.,-,/,space with Max. allowed length 20 characters)';
    var VALIDATE_MSG_INVALID_SPECIALKEYWORD='alphabets, numbers and Special Characters (@,*, (,), -, +,/,., comma, Space)';
    var VALIDATE_MSG_INVALID_SPECIALKEYWORD_NUMBER='alphabets, numbers and special characters (@,*, (,), -, +,/,., Space)';
    var VALIDATE_MSG_INVALID_COMMON_KEYWORD='Allows Max. 1000 alphabets, numbers and special characters (@,*, (,), -, +,/,.,&,Comma,Space)';
    var VALIDATE_MSG_INVALID_SPECIAL_CHAR = "Allows Max. 50 Characters and Special character (',-,.)";
    var VALIDATE_MSG_INVALID_FINAL_NETVALUE_DEC="Max. 10 digits and 5 decimal points are allowed";
    var VALIDATE_MSG_INVALID_FINAL_NETVALUE_NUM="Only numeric values of max 10 digit value and 5 decimal points are allowed";
    var VALIDATE_TENDERKEYWORD_LENGTH='Allows Max. 1000 alphabets, numbers and special characters (@,*, (,), -, +,/,.,&,Comma,Space)';
    var VALIDATION_PERCENTAGE='Allows numeric value and special character (.) upto 5 decimal points';
    var VALIDATION_WEIGHTAGE_TEXPERCENTAGE_BEFORE='Allows numeric value and special character (.) upto ';
    var VALIDATION_WEIGHTAGE_TEXT_PERCENTAGE_AFTER='decimal points';
    var VALIDATION_SPLIT_PERCENTAGE_BEFORE='Allows numeric value and special character (.) upto';
    var VALIDATION_SPLIT_PERCENTAGE_AFTER='decimal points';
    var VALIDATE_MSG_SPLIT_ORDER_PERCENTAGE='Split order cannot be less than 0 and cannot be greater than or equal to 100';
    var VALIDATION_MSG_DATE_INVALID='Please enter valid date and time in DD/MM/YYYY and HH:MM format respectively';
    var VALIDATE_MSG_INVALID_CONF_ACCTNo='does not match with';
    var VALIDATE_MSG_PBG_DETAILS='Enter value from 0 to 100';
    var VALIDATE_NOZERO='Zero value is not allowed';
    var VALIDATE_NUMBER_NEG_POS="Allows numeric value and special character (.) upto 2 decimal points"





