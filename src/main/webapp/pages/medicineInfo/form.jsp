

<%@ include file="/pages/common.jsp" %>
<style>
    .main .container-fluid {
        padding: 0 10px !important;
        margin-top: -15px !important;
    }

    tr td {
        white-space: nowrap;
    }
</style>

<main class="main"> <!-- Breadcrumb-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="${pageContext.servletContext.contextPath}/index"><span class="trn">Home</span></a></li>
        <li class="breadcrumb-item"><a href="#"><span class="trn">Management</span></a></li>
        <li class="breadcrumb-item active"><span class="trn">Diagnosis Information</span></li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">

            <div class="card">
                <div class="card-header">
                    <strong><span class="trn">Diagnosis Information</span></strong>
                </div>

                <form:form role="form" name="product" action="${pageContext.servletContext.contextPath}/medicines/save" onsubmit="if(valOnSubmit()){startLoading();return true;}else{return false;}" method="post" modelAttribute="modelMedicine">
                    <div class="card-body">
                        <form:hidden path="id" name="id" id="id"/>

                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="name"><span class="trn">Name</span></label>
                                    <div class="col-md-8">
                                        <form:input path="name" class="form-control"
                                                    title="name"
                                                    formcontrolname="name" id="name" name="name"
                                                    placeholder="Enter Name" maxlength="100"/>
                                        <div class="ng-star-inserted">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>

                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="genericName"><span class="trn">Generic Name</span></label>
                                    <div class="col-md-8">
                                        <form:input path="genericName" class="form-control"
                                                    title="genericName"
                                                    formcontrolname="genericName" id="genericName" name="genericName"
                                                    placeholder="Enter Generic" maxlength="100"/>
                                        <div class="ng-star-inserted">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>

                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="manufactureName"><span class="trn">Manufacture Name</span></label>
                                    <div class="col-md-8">
                                        <form:input path="manufactureName" class="form-control"
                                                    title="manufactureName"
                                                    formcontrolname="manufactureName" id="manufactureName" name="manufactureName"
                                                    placeholder="Enter Manufacture Name" maxlength="100"/>
                                        <div class="ng-star-inserted">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>

                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="medicineType"><span class="trn">Medicine Type</span></label>
                                    <div class="col-md-8">
                                        <form:select id="medicineType" path="medicineType" class="form-control ng-untouched ng-pristine ng-invalid">
                                            <form:option value="Tablet">Tablet</form:option>
                                            <form:option value="Injection">Injection</form:option>
                                            <form:option value="Capsule">Capsule</form:option>
                                            <form:option value="Syrup">Syrup</form:option>
                                        </form:select>
                                    </div>

                                    <div class="ng-star-inserted">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>

                    </div>
                    <div class="card-footer" style="height: 52px;">
                        <form:button class="btn btn-sm btn-success float-right" type="submit">
                            <i class="fa fa-dot-circle-o"></i><span class="trn">Save</span>
                        </form:button>
                    </div>
                </form:form>
            </div>
        </div>
    </div>
</main>

<script type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/customJS/store/goodsReceipt.js"></script>
