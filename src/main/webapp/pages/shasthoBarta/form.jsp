<%@ include file="/pages/common.jsp" %>
<style>
    .main .container-fluid {
        padding: 0 10px !important;
        margin-top: -15px !important;
    }

    tr td {
        white-space: nowrap;
    }
</style>

<main class="main"> <!-- Breadcrumb-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="${pageContext.servletContext.contextPath}/index"><span class="trn">Home</span></a></li>
        <li class="breadcrumb-item"><a href="#"><span class="trn">Management</span></a></li>
        <li class="breadcrumb-item active"><span class="trn">Doctor Information</span></li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">

            <div class="card">
                <div class="card-header">
                    <strong><span class="trn">Shastho Barta</span></strong>
                </div>

                <form:form role="form" name="product" action="${pageContext.servletContext.contextPath}/shastho-barta/save" onsubmit="if(valOnSubmit()){startLoading();return true;}else{return false;}"
                           method="post" modelAttribute="modelShasthoBarta">
                    <div class="card-body">
                        <form:hidden path="id" name="id" id="id"/>

                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="title"><span class="trn">Title</span></label>
                                    <div class="col-md-8">
                                        <form:input path="title" class="form-control"
                                                    title="title"
                                                    formcontrolname="title" id="title" name="title"
                                                    placeholder="Enter title" maxlength="100"/>
                                        <div class="ng-star-inserted">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>

                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="barta"><span class="trn">Message/Barta</span></label>
                                    <div class="col-md-8">
                                        <form:textarea path="barta" class="form-control"
                                                    title="barta"
                                                    formcontrolname="barta" id="barta" name="barta"
                                                    placeholder="Enter barta" maxlength="1000"></form:textarea>
                                        <div class="ng-star-inserted">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>

                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="imagePath"><span class="trn">Image</span></label>
                                    <div class="col-md-8">
                                        <form:input path="imagePath" class="form-control"
                                                    title="imagePath"
                                                    formcontrolname="imagePath" id="imagePath" name="imagePath"
                                                    placeholder="Enter image path" ></form:input>
                                        <div class="ng-star-inserted">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>

                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="isActive"><span class="trn">Active</span></label>
                                    <div class="col-md-8">
                                        <form:select path="isActive" class="form-control" title="isActive" id="isActive" name="isActive">
                                            <form:option value="1">Yes</form:option>
                                            <form:option value="2">No</form:option>
                                        </form:select>
                                        <div class="ng-star-inserted">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>

                    </div>
                    <div class="card-footer" style="height: 52px;">
                        <form:button class="btn btn-sm btn-success float-right" type="submit">
                            <i class="fa fa-dot-circle-o"></i><span class="trn">Save</span>
                        </form:button>
                    </div>
                </form:form>
            </div>
        </div>
    </div>
</main>

<script>
    function setEconomyCode(e) {
        $(e).closest('tr').find('.economyCode').text($("#sp"+$(e).val()).text());
    }
    $("#pid").on('blur', function() {
        $.get({
            url : path + '/personal-info/fetchByPid',
            data : "pid="+$('#pid').val(),
            success : function(response) {
                if(response == 'emptyCode'){
                    showError('<b>ERROR!</b><br> Please Enter PID');
                }else if(response == 'notFound'){
                    showError('<b>ERROR!</b><br> No Record found for the Provided Personal ID');
                }else{
                    var json = JSON.parse(response);
                    console.log(response);
                    $('#pkid').val(json['id']);
                    $('#name').val(json['name']);
                    $('#email').val(json['email']);
                    $('#contactNo').val(json['contactNo']);
                }
            }
        });
    });
</script>
