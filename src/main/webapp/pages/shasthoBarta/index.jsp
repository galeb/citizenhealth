<%@ include file="/pages/common.jsp" %>
<title>Doctor Information</title>
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/resources/plugins/reflection/reflection-com.css">
<style>
    #myTable {
        table-layout: fixed;
        width: 100% !important;
    }
    #myTable td,
    #myTable th{
        width: auto !important;
        white-space: normal;
        text-overflow: ellipsis;
        overflow: hidden;
    }
</style>
<main class="main"> <!-- Breadcrumb-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="${pageContext.servletContext.contextPath}/index"><span class="trn">Home</span></a></li>
        <li class="breadcrumb-item"><a href="#"><span class="trn">Management</span></a></li>
        <li class="breadcrumb-item active"><span class="trn">Shastho Barta</span></li>
        <li class="float-right w-100">
            <div class="float-right" style="margin-top: -20px!important;">
                <a href="${pageContext.servletContext.contextPath}/shastho-barta/form" class="btn btn-success btn-sm"><i class="fa fa-edit c-white"></i> Create</a>
            </div>
        </li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="card">
                <div class="card-header">
                    <h4>Shastho Barta List</h4>
                </div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table id="myTable"  style="width:100%" class="table table-striped table-bordered dataTable">
                                <thead>
                                <tr>
                                    <th>Sl. No</th>
                                    <th>Title</th>
                                    <th>Barta</th>
                                    <th>Active</th><%--
                                    <th>Created By</th>
                                    <th>Created Date</th>--%>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <c:if test="${not empty bartaList}">
                                    <c:forEach items="${bartaList}" var="each" varStatus="loop">
                                        <tr>
                                            <td>${loop.index+1}</td>
                                            <td>${each.title}</td>
                                            <td>${each.barta}</td>
                                            <td>${each.isActive eq 1 ? 'Yes' : 'No'}</td>
                                            <%--<td>${each.createdBy.name}</td>
                                            <td>${each.createdOn}</td>--%>
                                            <td>
                                                <a href="${pageContext.servletContext.contextPath}/shastho-barta/show/${each.id}" target="_blank" class="c-white btn btn-sm btn-info"><i class="fa fa-eye c-white"></i> Show</a>
                                                <a href="${pageContext.servletContext.contextPath}/shastho-barta/edit/${each.id}" target="_blank" class="c-white btn btn-sm btn-warning"><i class="fa fa-edit c-white"></i> Edit</a>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                </c:if>
                                <c:if test="${bartaList == null}">
                                    <tr>
                                        <td class="bold center" colspan="7">No record Found...!</td>
                                    </tr>
                                </c:if>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</main>
<script type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/plugins/reflection/reflection-com.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script>
    $('#myTable').DataTable();

</script>