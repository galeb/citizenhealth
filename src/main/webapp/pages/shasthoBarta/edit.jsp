<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="t" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<title>Good Receipt</title>
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/resources/plugins/bootstrap-datepicker/datepicker3.css">
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/resources/plugins/reflection/reflection-com.css">
<main class="main"> <!-- Breadcrumb-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="${pageContext.servletContext.contextPath}/index"><span class="trn">Home</span></a></li>
        <li class="breadcrumb-item"><a href="#"><span class="trn">Shastho Barta</span></a></li>
        <li class="float-right w-100">
            <div class="float-right" style="margin-top: -20px!important;">
                <a href="${pageContext.servletContext.contextPath}/shastho-barta/index" class="btn btn-info btn-sm"><i class="fa fa-list c-white"></i> Index</a>
                <a href="${pageContext.servletContext.contextPath}/shastho-barta/create" class="btn btn-success btn-sm"><i class="fa fa-edit c-white"></i> Create</a>
            </div>
        </li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="card">
                <div class="card-header">
                    <strong><span class="trn"> </span></strong>
                </div>

                <form:form role="form" name="product"
                           action="${pageContext.servletContext.contextPath}/shastho-barta/save" onsubmit="if(valOnSubmit()){startLoading();return true;}else{return false;}"
                           method="post" modelAttribute="modelShasthoBarta">
                    <div class="card-body">
                        <form:hidden path="id" name="id" id="id"/>

                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="barta"><span class="trn">Barta</span></label>
                                    <div class="col-md-8">
                                        <form:input path="title" class="form-control"
                                                    title="title"
                                                    formcontrolname="title" id="title" name="title"
                                                    placeholder="Enter title" maxlength="100"/>
                                        <div class="ng-star-inserted">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>

                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="barta"><span class="trn">Message/Barta</span></label>
                                    <div class="col-md-8">
                                        <form:textarea path="barta" class="form-control"
                                                       title="barta"
                                                       formcontrolname="barta" id="barta" name="barta"
                                                       placeholder="Enter barta" maxlength="1000"></form:textarea>
                                        <div class="ng-star-inserted">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>

                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="imagePath"><span class="trn">Image</span></label>
                                    <div class="col-md-8">
                                        <form:input path="imagePath" class="form-control"
                                                    title="imagePath"
                                                    formcontrolname="imagePath" id="imagePath" name="imagePath"
                                                    placeholder="Enter image path" ></form:input>
                                        <div class="ng-star-inserted">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>



                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="isActive"><span class="trn">Active</span></label>
                                    <div class="col-md-8">
                                        <form:select id="isActive" path="isActive" class="form-control ng-untouched ng-pristine ng-invalid">
                                            <form:option value="1">Yes</form:option>
                                            <form:option value="0">No</form:option>
                                        </form:select>
                                        <div class="ng-star-inserted">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>
                    </div>
                    <div class="card-footer" style="height: 52px;">
                        <form:button class="btn btn-sm btn-success float-right" type="submit">
                            <i class="fa fa-dot-circle-o"></i><span class="trn">Save</span>
                        </form:button>
                    </div>
                </form:form>

            </div>
        </div>
    </div>
</main>
<script type="text/javascript" charset="utf8"
        src="${pageContext.servletContext.contextPath}/resources/customJS/common.js"></script>
<script type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/plugins/bootstrap-datepicker/bootstrap-datepicker.js"></script>
<script type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/plugins/reflection/reflection-com.js"></script>
<script>


</script>
