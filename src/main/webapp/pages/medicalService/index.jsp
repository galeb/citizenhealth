<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib prefix="f" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="t" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<title>Medical Service Information</title>
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/resources/plugins/reflection/reflection-com.css">
<main class="main"> <!-- Breadcrumb-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="${pageContext.servletContext.contextPath}/index"><span class="trn">Home</span></a></li>
        <li class="breadcrumb-item"><a href="#"><span class="trn">Management</span></a></li>
        <li class="breadcrumb-item active"><span class="trn">Medical Service Information</span></li>
        <li class="float-right w-100">
            <div class="float-right" style="margin-top: -20px!important;">
                <a href="${pageContext.servletContext.contextPath}/medical-services/form" class="btn btn-success btn-sm"><i class="fa fa-edit c-white"></i> Create</a>
            </div>
        </li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="card">
                <div class="card-header">
                    <h4>Medical Service Information List</h4>
                </div>

                <div class="card-body">
                    <table class="table table-striped table-bordered th-center th-colored">
                        <thead>
                        <tr>
                            <th>Sl. No</th>
                            <th>Name</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:if test="${not empty medicalServiceList}">
                            <c:forEach items="${medicalServiceList}" var="each" varStatus="loop">
                                <tr>
                                    <td class="center">${loop.index+1}</td>
                                    <td class="center">${each.serviceName}</td>
                                    <%--<td class="center">${each.tblStrApprovalStatus}--%>
                                    </td>
                                    <td class="center">
                                        <a href="${pageContext.servletContext.contextPath}/medical-services/show/${each.id}" target="_blank" class="c-white btn btn-sm btn-info"><i class="fa fa-eye c-white"></i> Show</a>
                                        <a href="${pageContext.servletContext.contextPath}/medical-services/edit/${each.id}" target="_blank" class="c-white btn btn-sm btn-warning"><i class="fa fa-edit c-white"></i> Edit</a>
                                    </td>
                                </tr>
                            </c:forEach>
                        </c:if>
                        <c:if test="${medicalServiceList == null}">
                            <tr>
                                <td class="bold center" colspan="7">No record Found...!</td>
                            </tr>
                        </c:if>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</main>
<script type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/plugins/reflection/reflection-com.js"></script>