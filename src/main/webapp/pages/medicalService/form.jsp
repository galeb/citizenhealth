<%@ include file="/pages/common.jsp" %>
<style>
    .main .container-fluid {
        padding: 0 10px !important;
        margin-top: -15px !important;
    }

    tr td {
        white-space: nowrap;
    }
</style>

<main class="main"> <!-- Breadcrumb-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="${pageContext.servletContext.contextPath}/index"><span
                class="trn">Home</span></a></li>
        <li class="breadcrumb-item"><a href="#"><span class="trn">Management</span></a></li>
        <li class="breadcrumb-item active"><span class="trn">Medical Service Information</span></li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">

            <div class="card">
                <div class="card-header">
                    <strong><span class="trn">Medical Service Information</span></strong>
                </div>

                <form:form role="form" name="product" action="${pageContext.servletContext.contextPath}/medical-services/save"
                           onsubmit="if(valOnSubmit()){startLoading();return true;}else{return false;}"
                           method="post" modelAttribute="modelMedicalService">
                    <div class="card-body">
                        <form:hidden path="id" name="id" id="id"/>

                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="serviceName"><span
                                            class="trn">Name</span></label>
                                    <div class="col-md-8">
                                        <form:input path="serviceName" class="form-control"
                                                    title="serviceName"
                                                    formcontrolname="name" id="serviceName" name="serviceName"
                                                    placeholder="Enter Service Name" maxlength="100"/>
                                        <div class="ng-star-inserted">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>

                    </div>
                    <div class="card-footer" style="height: 52px;">
                        <form:button class="btn btn-sm btn-success float-right" type="submit">
                            <i class="fa fa-dot-circle-o"></i><span class="trn">Save</span>
                        </form:button>
                    </div>
                </form:form>
            </div>
        </div>
    </div>
</main>

