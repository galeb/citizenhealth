<%-- 
    Document   : viewACR
    Created on : Apr 3, 2019, 5:11:57 PM
    Author     : ITMCS-1
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<script type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/customJS/common.js"></script>
<script>
   startLoading();
</script>
<style>
.button {
    background-color: #4CAF50; /* Green */
    border: none;
    color: white;
    padding: 11px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 0px 0px;
    cursor: pointer;
}

.button4 {
    border-radius: 18px;
}

.nav-tabs .nav-link.active, .nav-tabs .nav-item.show .nav-link {
    color: #5c6873;
    background-color: rgba(255, 255, 255, 0.9);
    border-color: #c8ced3 #c8ced3 rgba(255, 255, 255, 0.9);
}
</style>
<script type='text/javascript'>
   function hideShow()
   {
    	document.getElementById("aNew").classList.remove("active");
    	document.getElementById("aShow").classList.add("active");
    	document.getElementById("show").style.display = "block";
    	document.getElementById("create").style.display = "none";
   }
   function showHide()
   {
   	document.getElementById("aShow").classList.remove("active");
   	document.getElementById("aNew").classList.add("active");
   	document.getElementById("show").style.display = "none";
   	document.getElementById("create").style.display = "block";				
   }
</script>
<main class="main"> <!-- Breadcrumb-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="${pageContext.servletContext.contextPath}/index">Home</a></li>
        <li class="breadcrumb-item active">Annual Confidential Report</li>
    </ol>
    <div class="container-fluid">
        <div id="ui-view">
            <div>
                <div class="animated fadeIn">
                    <div class="row">
                        <div class="col-md-12 mb-12">
                            <ul class="nav nav-tabs" role="tablist">
                                <li class="nav-item"><a id="aShow" aria-controls="home" class="nav-link active" data-toggle="tab" role="tab" onclick='hideShow()'>Annual Confidential Report</a></li>
                                <li class="nav-item"><a id="aNew" aria-controls="profile" class="nav-link" data-toggle="tab" role="tab" onclick='showHide()'>Create New</a></li>
                            </ul>
                            <div id="show" class="tab-content">
                                <div class="tab-pane active" id="home" role="tabpanel">
                                    <div class="card-body">
                                        <div class="col-sm-12">
                                            <div class="card">
                                                <div class="card-header">
                                                    <strong>Annual Confidential Report</strong>
                                                </div>
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <form action="" class="form-horizontal ng-untouched ng-pristine ng-valid" method="post" novalidate="">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="form-group row">
                                                                            <label class="col-md-3 col-form-label" for="remarks">Search By Employee Code</label>
                                                                            <div class="col-md-9">
                                                                                <div class="input-group">
                                                                                    <input class="form-control" id="txtEmpCodeSearch" name="input2-group2" placeholder="Search By Employee Code" type="text">
                                                                                    <span class="input-group-append"><button class="btn btn-primary" type="button" onclick="fetchAllACR();startLoading();"> <i class="fa fa-search"></i> Search..</button></span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-12 table-responsive" id="divDatatable">
                                                    <table aria-describedby="DataTables_Table_0_info" class="table table-striped table-bordered datatable dataTable no-footer " id="myTable" role="grid" style="border-collapse: collapse !important">
                                                        <thead id="theadAcr"></thead>
                                                        <tbody id="tbodyAcr"></tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="create" class="tab-content" style="display: none">
                                <div class="tab-pane active" id="profile" role="tabpanel">
                                    <div>
                                        <form:form action="${pageContext.servletContext.contextPath}/acr/save" class="form-horizontal ng-untouched ng-pristine ng-invalid" modelAttribute="modelTblAcr"  onsubmit="if (valOnSubmit()){startLoading(); return true;}else{return false;}" method="post">
                                            <form:hidden path="acrPk" id="acrPk" name="acrPk" />
                                            <input type="hidden" id="docPk" name="docPk" >
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="card">
                                                        <div class="card-header">
                                                            <strong>ACR</strong>
                                                        </div>
                                                        <div class="card-body">
                                                            
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <div class="card">
                                                                        <div class="card-header">
                                                                            <strong>Personal Information</strong>
                                                                        </div>
                                                                        <div class="card-body">
                                                                            <div class="row">
                                                                                <div class="col-sm-12">
                                                                                    <table aria-describedby="DataTables_Table_0_info" class="table table-striped table-bordered datatable dataTable no-footer" id="DataTables_Table_0" role="grid" style="border-collapse: collapse !important">
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <th>Employee Code:</th>
                                                                                                <td><form:input path="pimsId.govId" title="Employee Code" id="empCode" aria-controls="DataTables_Table_0" class="form-control form-control-sm" placeholder="Search..." validarr="required@@" tovalid="true" onblur="validateTextComponent(this)" /></td>
                                                                                                <th>
                                                                                                    <center>
                                                                                                        <button type="button" class="button button4" onclick="fetchDetailsByEmpCode()">Search</button>
                                                                                                    </center>
                                                                                                </th>
                                                                                                <td rowspan="5" style="width: 150px;"><img id="profileImg" src="/resources/img/avatars/default.png" alt="Not Found" height="150px" width="150px"></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <th colspan="3">Personal Information</th>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <th>Name:</th>
                                                                                                <td id="empname"></td>
                                                                                                <th>Id: <span id="empid"></span></th>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <th>Designation:</th>
                                                                                                <td></td>
                                                                                                <th>Rank:</th>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <th>Office Location:</th>
                                                                                                <td></td>
                                                                                                <th><a onclick="previewHtml('html');" style="cursor:pointer"><u>PDS</u></a></th>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                                                                
                                                            <div class="row" style="margin-top: 10px;">
                                                                <div class="col-md-12">
                                                                    <div class=" card">
                                                                        <div class="card-header">
                                                                            <strong>General Section</strong>
                                                                        </div>
                                                                        <div class="card-body">

                                                                            <div class="row" style="margin-top: 10px;">
                                                                                <div class="col-sm-6">
                                                                                    <div class="form-group row">
                                                                                        <label class="col-md-3 col-form-label"
                                                                                               for="acrYear"> ACR Year</label>
                                                                                        <div class="col-md-9">
                                                                                            <form:input validarr="required@@year" tovalid="true" onblur="validateTextComponent(this)"
                                                                                                class="form-control ng-untouched ng-pristine ng-invalid"
                                                                                                formcontrolname="acrYear" id="acrYear"
                                                                                                path="acrYear" title="ACR Year" name="acrYear"
                                                                                                placeholder="Enter ACR Year" />
                                                                                            <!---->
                                                                                            <div class="ng-star-inserted">
                                                                                                <!---->
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-sm-6"></div>
                                                                            </div>
                                                                            <div class="row" style="margin-top: 10px;">

                                                                                <div class="col-sm-6">
                                                                                    <div class="form-group">
                                                                                        <div class="form-group row">
                                                                                            <label class="col-md-3 col-form-label"
                                                                                                   for="isAcrRequired"> ACR Required?</label>
                                                                                            <div class="col-md-9">
                                                                                                <form:select
                                                                                                    class="form-control ng-untouched ng-pristine ng-invalid"
                                                                                                    formcontrolname="isAcrRequired"
                                                                                                    path="isAcrRequired" id="isAcrRequired"  onblur="validateCombo(this);" isrequired="true" 
                                                                                                    title="ACR Required" name="isAcrRequired" onchange="reasonvalidation(this.value)">
                                                                                                    <form:option selected="selected" value="">Please Select</form:option>

                                                                                                    <c:forEach var="entry"
                                                                                                               items="${acrRequiredList}">

                                                                                                        <form:option value="${entry.lookUpName}" >${entry.lookUpName}</form:option>
                                                                                                    </c:forEach>
                                                                                                </form:select>
                                                                                                <div class="ng-star-inserted">
                                                                                                    <!---->
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-sm-6" id="reasonmaindiv">
                                                                                    <div class="form-group row">
                                                                                        <label class="col-md-3 col-form-label" for="reason">
                                                                                            Reason</label>
                                                                                        <div class="col-md-9">
                                                                                            <form:input disabled="true"
                                                                                                class="form-control ng-untouched ng-pristine ng-invalid"
                                                                                                formcontrolname="reason" id="reason"
                                                                                                name="reason" title="Reason"
                                                                                                placeholder="Enter Reason" path="reason" />
                                                                                            <!---->
                                                                                            <div class="ng-star-inserted">
                                                                                                <!---->
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row" style="margin-top: 10px;">

                                                                                <div class="col-sm-6">
                                                                                    <div class="form-group">
                                                                                        <div class="form-group row">
                                                                                            <label class="col-md-3 col-form-label"
                                                                                                   for="acrType">ACR Type</label>
                                                                                            <div class="col-md-9">
                                                                                                <form:select
                                                                                                    class="form-control ng-untouched ng-pristine ng-invalid"
                                                                                                    path="acrType" formcontrolname="acrType"  onblur="validateCombo(this);" isrequired="true" 
                                                                                                    id="acrType" title="ACR Type" name="acrType" onchange=" toorformfieldvelidation(this.value);">
                                                                                                    <form:option selected="selected" value="" >Please Select</form:option>
                                                                                                    <c:forEach var="entry" items="${acrTypeList }">
                                                                                                        <form:option value="${entry.lookUpName}" >${entry.lookUpName}</form:option>

                                                                                                    </c:forEach>

                                                                                                </form:select>
                                                                                                <div class="ng-star-inserted">
                                                                                                    <!---->
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row" style="margin-top: 10px;">

                                                                                <div class="col-sm-6">
                                                                                    <div class="form-group">
                                                                                        <div class="form-group row">
                                                                                            <label class="col-md-3 col-form-label"
                                                                                                   for="Period">Period</label>

                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row" style="margin-top: 10px;">

                                                                                <div class="col-sm-6">

                                                                                    <div class="form-group row">
                                                                                        <label class="col-md-3 col-form-label" for="from">From</label>
                                                                                        <div class="col-md-9">
                                                                                            <input
                                                                                                class="form-control ng-untouched ng-pristine ng-invalid"
                                                                                                formcontrolname="from" id="from" required="required"
                                                                                                title="Form Date" name="from" disabled="disabled"
                                                                                                placeholder="Enter From Date" type="Date">
                                                                                            <!---->
                                                                                            <div class="ng-star-inserted">
                                                                                                <!---->
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>

                                                                                </div>
                                                                                <div class="col-sm-6" id="maintodiv">
                                                                                    <div class="form-group row">
                                                                                        <label class="col-md-3 col-form-label" for="to">To</label>
                                                                                        <div class="col-md-9">
                                                                                            <input required="required"
                                                                                                class="form-control ng-untouched ng-pristine ng-invalid"
                                                                                                formcontrolname="to" id="to" name="to" disabled="disabled"
                                                                                                placeholder="Enter To Date" title="To Date"
                                                                                                type="Date">
                                                                                            <!---->
                                                                                            <div class="ng-star-inserted">
                                                                                                <!---->
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row" style="margin-top: 10px;">

                                                                                <div class="col-sm-6">
                                                                                    <div class="form-group">
                                                                                        <div class="form-group row">
                                                                                            <label class="col-md-3 col-form-label"
                                                                                                   for="healthReport">Health Report</label>
                                                                                            <div class="col-md-9">
                                                                                                <form:select
                                                                                                    class="form-control ng-untouched ng-pristine ng-invalid"
                                                                                                    title="Health Report"
                                                                                                    formcontrolname="healthReport"
                                                                                                    path="healthReport" id="healthReport"  onblur="validateCombo(this);" isrequired="true" 
                                                                                                    name="healthReport" onchange="healthdatevalidation(this.value)">
                                                                                                    <form:option selected="selected" value="">Please Select</form:option>
                                                                                                    <c:forEach var="entry"
                                                                                                               items="${healthReportList }">
                                                                                                        <form:option value="${entry.lookUpName}">${entry.lookUpName}</form:option>
                                                                                                    </c:forEach>
                                                                                                </form:select>
                                                                                                <div class="ng-star-inserted">
                                                                                                    <!---->
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-sm-6">

                                                                                    <div class="form-group row">
                                                                                        <label class="col-md-3 col-form-label"
                                                                                               for="healthReportDate">Health Report Date</label>
                                                                                        <div class="col-md-9">
                                                                                            <input
                                                                                                class="form-control ng-untouched ng-pristine ng-invalid"
                                                                                                formcontrolname="healthReportDate" disabled="disabled"
                                                                                                id="HealthReportDate" name="healthReportDate"
                                                                                                title="Health Report Date"
                                                                                                placeholder="Enter Health Report Date"
                                                                                                type="Date">
                                                                                            <!---->
                                                                                            <div class="ng-star-inserted">
                                                                                                <!---->
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>

                                                                                </div>

                                                                            </div>
                                                                            <div class="row" style="margin-top: 10px;">

                                                                                <div class="col-sm-6">

                                                                                    <div class="form-group row">
                                                                                        <label class="col-md-3 col-form-label"
                                                                                               for="oruSubDate">ORU(Officer reporting
                                                                                            Upon) Submission Date</label>
                                                                                        <div class="col-md-9">
                                                                                            <input validarr="required@@" tovalid="true" onblur="validateTextComponent(this)"
                                                                                                class="form-control ng-untouched ng-pristine ng-invalid"
                                                                                                formcontrolname="oruSubDate" id="oruSubDate"
                                                                                                name="oruSubDate"
                                                                                                title="ORU(Officer reporting Upon) Submission Date"
                                                                                                placeholder="Enter ORU(Officer reporting Upon) Submission Date"
                                                                                                type="Date">
                                                                                            <!---->
                                                                                            <div class="ng-star-inserted">
                                                                                                <!---->
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>

                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row" style="margin-top: 10px;">
                                                                <div class="col-md-6">
                                                                    <div class="card">
                                                                        <div class="card-header">
                                                                            <strong>RIO (Reporting Incharge Officer)</strong>
                                                                        </div>
                                                                        <div class="card-body">
                                                                            <div class="row" style="margin-top: 10px;">
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group row">
                                                                                        <label class="col-md-3 col-form-label" for="oruNo">No.
                                                                                            Given by RIO</label>
                                                                                        <div class="col-md-9">
                                                                                            <form:input path="oruNo" validarr="percentage:2@@required" tovalid="true" onblur="validateTextComponent(this)"
                                                                                                        class="form-control ng-untouched ng-pristine ng-invalid"
                                                                                                        formcontrolname="oruNo" id="oruNo" name="oruNo"
                                                                                                        title="RIO No" placeholder="Enter RIO No" />
                                                                                            <!---->
                                                                                            <div class="ng-star-inserted">
                                                                                                <!---->
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                            <div class="row" style="margin-top: 10px;">
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group row">
                                                                                        <label class="col-md-3 col-form-label"
                                                                                               for="oruRemark">Remarks given by RIO</label>
                                                                                        <div class="col-md-9">
                                                                                            <form:textarea validarr="required@@" tovalid="true" onblur="validateTextComponent(this)"
                                                                                                class="form-control ng-untouched ng-pristine ng-invalid" title="Remarks given by RIO"
                                                                                                formcontrolname="oruRemark" cols="5" rows="6"
                                                                                                id="oruRemark" path="oruRemark" name="oruRemark"
                                                                                                placeholder="Enter RIO Remarks"></form:textarea>
                                                                                                <div class="ng-star-inserted">
                                                                                                    <!---->
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>

                                                                                </div>
                                                                                <div class="row" style="margin-top: 10px;">
                                                                                    <div class="col-md-12">
                                                                                        <div class="form-group row">
                                                                                            <label class="col-md-3 col-form-label"
                                                                                                   for="ifOruNegRemark">If neg Remarks?</label>
                                                                                            <div class="col-md-9">
                                                                                            <form:select onblur="validateCombo(this);" isrequired="true" 
                                                                                                class="form-control ng-untouched ng-pristine ng-invalid"
                                                                                                formcontrolname="ifOruNegRemark"
                                                                                                id="ifOruNegRemark" name="ifOruNegRemark"
                                                                                                title="If neg Remarks" path="ifOruNegRemark" onchange="nevremarkvelidation(this.value)">
                                                                                                <form:option selected="selected" value="" >Please Select</form:option>
                                                                                                <c:forEach var="entry"
                                                                                                           items="${ifnegRemarksList }">
                                                                                                    <form:option value="${entry.lookUpName}">${entry.lookUpName}</form:option>
                                                                                                </c:forEach>

                                                                                            </form:select>
                                                                                            <div class="ng-star-inserted">
                                                                                                <!---->
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                            <div class="row" style="margin-top: 10px;">
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group row">
                                                                                        <label class="col-md-3 col-form-label"
                                                                                               for="negremarksByoru">Neg Remarks given By
                                                                                            RIO</label>
                                                                                        <div class="col-md-9">
                                                                                            <textarea
                                                                                                class="form-control ng-untouched ng-pristine ng-invalid"
                                                                                                formcontrolname="negremarksByoru" cols="5" disabled="disabled"
                                                                                                rows="6" id="negremarksByoru"
                                                                                                name="negremarksByoru"
                                                                                                title="Neg Remarks given By RIO"
                                                                                                path="negremarksByoru"
                                                                                                placeholder="Enter Neg Remarks given By RIO"></textarea>
                                                                                            <div class="ng-star-inserted">
                                                                                                <!---->
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                            <div class="row" style="margin-top: 10px;">
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group row">
                                                                                        <label class="col-md-3 col-form-label"
                                                                                               for="ifSugBYOru">Suggestion by RIO</label>
                                                                                        <div class="col-md-9">
                                                                                            <form:select onblur="validateCombo(this);" isrequired="true" 
                                                                                                class="form-control ng-untouched ng-pristine ng-invalid"
                                                                                                title="Suggestion by RIO"
                                                                                                formcontrolname="ifSugBYOru" id="ifSugBYOru"
                                                                                                name="ifSugBYOru" path="ifSugBYOru" onchange="sugdetailvelidation(this.value)">
                                                                                                <form:option selected="selected" value="">Please Select</form:option>
                                                                                                <c:forEach var="entry" items="${ifSugByOruList }">
                                                                                                    <form:option value="${entry.lookUpName}">${entry.lookUpName}</form:option>
                                                                                                </c:forEach>

                                                                                            </form:select>
                                                                                            <div class="ng-star-inserted">
                                                                                                <!---->
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                            <div class="row" style="margin-top: 10px;">
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group row">
                                                                                        <label class="col-md-3 col-form-label"
                                                                                               for="sugByOru">Suggestion details given by
                                                                                            RIO</label>
                                                                                        <div class="col-md-9">
                                                                                            <textarea disabled
                                                                                                class="form-control ng-untouched ng-pristine ng-invalid"
                                                                                                formcontrolname="sugByOru" cols="5" rows="6"
                                                                                                id="sugByOru" path="sugByOru" name="sugByOru"
                                                                                                title="Suggestion details given by RIO"
                                                                                                placeholder="Enter Suggestion details given by RIO"></textarea>
                                                                                                <div class="ng-star-inserted">
                                                                                                    <!---->
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>

                                                                                </div>
                                                                                <div class="row" style="margin-top: 10px;">
                                                                                    <div class="col-md-12">
                                                                                        <div class="form-group row">
                                                                                            <label class="col-md-3 col-form-label" for="oruId">RIO
                                                                                                ID</label>
                                                                                            <div class="col-md-9">
                                                                                            <form:input path="oruId" onblur="fetchdetailsforcsoandrio(this,'txtrioid')"
                                                                                                        class="form-control ng-untouched ng-pristine ng-invalid"
                                                                                                        formcontrolname="oruId" id="oruId" name="oruId"
                                                                                                        placeholder="Enter RIO ID" />
                                                                                            <!---->
                                                                                            <div class="ng-star-inserted">
                                                                                                <!---->
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                            <div class="row" style="margin-top: 10px;">
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group row">
                                                                                        <label class="col-md-3 col-form-label"
                                                                                               for="oruNameDesc">RIO Name and des</label>
                                                                                        <div class="col-md-9">
                                                                                            <label id="txtrioid">-</label>
                                                                                            <div class="ng-star-inserted">
                                                                                                <!---->
                                                                                               
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                            <div class="row" style="margin-top: 10px;">
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group row">
                                                                                        <label class="col-md-3 col-form-label"
                                                                                               for="oruOfficeLoc">RIO Office Location</label>
                                                                                        <div class="col-md-9">
                                                                                            <form:input 
                                                                                                class="form-control ng-untouched ng-pristine ng-invalid"
                                                                                                formcontrolname="oruOfficeLoc" id="oruOfficeLoc"
                                                                                                name="oruOfficeLoc" path="oruOfficeLoc" validarr="rioidrequired@@" tovalid="true" onblur="validateTextComponent(this)"
                                                                                                placeholder="Enter RIO Office Location" 
                                                                                                title="RIO Office Location" />
                                                                                            <!---->
                                                                                            <div class="ng-star-inserted">
                                                                                                <!---->
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                            <div class="row" style="margin-top: 10px;">
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group row">
                                                                                        <label class="col-md-3 col-form-label"
                                                                                               for="oruSignDate">RIO Signing Date</label>
                                                                                        <div class="col-md-9">
                                                                                            <input validarr="required@@" tovalid="true" onblur="validateTextComponent(this)"
                                                                                                class="form-control ng-untouched ng-pristine ng-invalid"
                                                                                                formcontrolname="oruSignDate" id="oruSignDate"
                                                                                                name="oruSignDate" path="oruSignDate"
                                                                                                placeholder="Enter RIO Signing Date" type="Date">
                                                                                            <!---->
                                                                                            <div class="ng-star-inserted">
                                                                                                <!---->
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                            <div class="row" style="margin-top: 10px;">
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group row">
                                                                                        <label class="col-md-3 col-form-label"
                                                                                               for="oruSubMissionDate">RIO Submission
                                                                                            date</label>
                                                                                        <div class="col-md-9">
                                                                                            <input validarr="required@@" tovalid="true" onblur="validateTextComponent(this)"
                                                                                                class="form-control ng-untouched ng-pristine ng-invalid"
                                                                                                formcontrolname="oruSubMissionDate"
                                                                                                id="oruSubMissionDate" name="oruSubMissionDate"
                                                                                                placeholder="Enter RIO Submission date"
                                                                                                type="Date">
                                                                                            <!---->
                                                                                            <div class="ng-star-inserted">
                                                                                                <!---->
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="card">
                                                                        <div class="card-header">
                                                                            <strong>CSO (Counter signing Officer)</strong>
                                                                        </div>
                                                                        <div class="card-body">
                                                                            <div class="row" style="margin-top: 10px;">
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group row">
                                                                                        <label class="col-md-3 col-form-label" for="csoNo">No.
                                                                                            Given by CSO</label>
                                                                                        <div class="col-md-9">
                                                                                            <form:input validarr="percentage:2@@required" tovalid="true" onblur="validateTextComponent(this)"
                                                                                                class="form-control ng-untouched ng-pristine ng-invalid"
                                                                                                formcontrolname="csoNo" id="csoNo" name="csoNo"
                                                                                                placeholder="Enter CSO No" title="CSO No"
                                                                                                path="csoNo" />
                                                                                            <!---->
                                                                                            <div class="ng-star-inserted">
                                                                                                <!---->
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                            <div class="row" style="margin-top: 10px;">
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group row">
                                                                                        <label class="col-md-3 col-form-label"
                                                                                               for="csoRemark">Remarks given by CSO</label>
                                                                                        <div class="col-md-9">
                                                                                            <form:textarea validarr="required@@" tovalid="true" onblur="validateTextComponent(this)"
                                                                                                class="form-control ng-untouched ng-pristine ng-invalid"
                                                                                                formcontrolname="csoRemark" cols="5" rows="6"
                                                                                                id="csoRemark" path="csoRemark" name="csoRemark"
                                                                                                title="Remarks given by CSO"
                                                                                                placeholder="Enter Remarks given by CSO"></form:textarea>
                                                                                                <div class="ng-star-inserted">
                                                                                                    <!---->
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>

                                                                                </div>
                                                                                <div class="row" style="margin-top: 10px;">
                                                                                    <div class="col-md-12">
                                                                                        <div class="form-group row">
                                                                                            <label class="col-md-3 col-form-label"
                                                                                                   for="ifCsoNegRemark">If neg Remarks?</label>
                                                                                            <div class="col-md-9">
                                                                                            <form:select  onblur="validateCombo(this);" isrequired="true"
                                                                                                class="form-control ng-untouched ng-pristine ng-invalid"
                                                                                                formcontrolname="ifCsoNegRemark"
                                                                                                id="ifCsoNegRemark" path="ifCsoNegRemark"
                                                                                                title="If neg Remarks" name="ifCsoNegRemark" onchange="nevremarkcsovelidation(this.value)">
                                                                                                <form:option selected="selected" value="">Please Select</form:option>
                                                                                                <c:forEach var="entry"
                                                                                                           items="${ifnegRemarksList }">
                                                                                                    <form:option value="${entry.lookUpName}">${entry.lookUpName}</form:option>
                                                                                                </c:forEach>
                                                                                            </form:select>
                                                                                            <div class="ng-star-inserted">
                                                                                                <!---->
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                            <div class="row" style="margin-top: 10px;">
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group row">
                                                                                        <label class="col-md-3 col-form-label"
                                                                                               for="negremarksByCSO">Neg Remarks given By
                                                                                            CSO</label>
                                                                                        <div class="col-md-9">
                                                                                            <textarea
                                                                                                class="form-control ng-untouched ng-pristine ng-invalid"
                                                                                                formcontrolname="negremarksByCSO" cols="5"
                                                                                                rows="6" path="negremarksByCSO" disabled
                                                                                                id="negremarksBycso" name="negremarksByCSO"
                                                                                                placeholder="Enter Neg Remarks given By CSO"></textarea>
                                                                                                <div class="ng-star-inserted">
                                                                                                    <!---->
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>

                                                                                </div>
                                                                                <div class="row" style="margin-top: 10px;">
                                                                                    <div class="col-md-12">
                                                                                        <div class="form-group row">
                                                                                            <label class="col-md-3 col-form-label"
                                                                                                   for="ifSugByCso">Sug by CSO</label>
                                                                                            <div class="col-md-9">
                                                                                            <form:select onblur="validateCombo(this);" isrequired="true"
                                                                                                class="form-control ng-untouched ng-pristine ng-invalid"
                                                                                                title="If Sug by CSO"
                                                                                                formcontrolname="ifSugByCso" id="ifSugByCso"
                                                                                                path="ifSugByCso" name="ifSugByCso" onchange="sugdetailcsovelidation(this.value)">
                                                                                                <form:option selected="selected" value="">Please Select</form:option>
                                                                                                <c:forEach var="entry" items="${ifSugByCSoList }">
                                                                                                    <form:option value="${entry.lookUpName}">${entry.lookUpName}</form:option>
                                                                                                </c:forEach>

                                                                                            </form:select>
                                                                                            <div class="ng-star-inserted">
                                                                                                <!---->
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                            <div class="row" style="margin-top: 10px;">
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group row">
                                                                                        <label class="col-md-3 col-form-label"
                                                                                               for="subByCso">Sug details given by CSO</label>
                                                                                        <div class="col-md-9">
                                                                                            <textarea disabled
                                                                                                class="form-control ng-untouched ng-pristine ng-invalid"
                                                                                                formcontrolname="subByCso" cols="5" rows="6"
                                                                                                id="subByCso" name="subByCso" path="subByCso"
                                                                                                placeholder="Enter Sug details given by CSO"></textarea>
                                                                                                <div class="ng-star-inserted">
                                                                                                    <!---->
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>

                                                                                </div>
                                                                                <div class="row" style="margin-top: 10px;">
                                                                                    <div class="col-md-12">
                                                                                        <div class="form-group row">
                                                                                            <label class="col-md-3 col-form-label" for="csoId">CSO
                                                                                                ID</label>
                                                                                            <div class="col-md-9">
                                                                                            <form:input onblur="fetchdetailsforcsoandrio(this,'txtcsoid')"
                                                                                                class="form-control ng-untouched ng-pristine ng-invalid"
                                                                                                formcontrolname="csoId" id="csoId" name="csoId"
                                                                                                path="csoId" placeholder="Enter CSO ID" />
                                                                                            <!---->
                                                                                            <div class="ng-star-inserted">
                                                                                                <!---->
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                            <div class="row" style="margin-top: 10px;">
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group row">
                                                                                        <label class="col-md-3 col-form-label"
                                                                                               for="acrYear">CSO Name and des</label>
                                                                                        <div class="col-md-9">
                                                                                            <label  id="txtcsoid">-</label>
                                                                                            <div class="ng-star-inserted">
                                                                                                <!---->
                                                                                               
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                            <div class="row" style="margin-top: 10px;">
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group row">
                                                                                        <label class="col-md-3 col-form-label"
                                                                                               for="csoOfficeLoc">CSO Office Location</label>
                                                                                        <div class="col-md-9">
                                                                                            <form:input validarr="csoidrequired@@" tovalid="true" onblur="validateTextComponent(this)"
                                                                                                class="form-control ng-untouched ng-pristine ng-invalid"
                                                                                                formcontrolname="csoOfficeLoc" id="csoOfficeLoc" title="CSO Office Location"
                                                                                                name="csoOfficeLoc" path="csoOfficeLoc"
                                                                                                placeholder="Enter CSO Office Location" />
                                                                                            <!---->
                                                                                            <div class="ng-star-inserted">
                                                                                                <!---->
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                            <div class="row" style="margin-top: 10px;">
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group row">
                                                                                        <label class="col-md-3 col-form-label"
                                                                                               for="csoSignDate">CSO Signing Date</label>
                                                                                        <div class="col-md-9">
                                                                                            <input
                                                                                                class="form-control ng-untouched ng-pristine ng-invalid"
                                                                                                formcontrolname="csoSignDate" id="csoSignDate"
                                                                                                title=" CSO Signing Date"
                                                                                                name="csoSignDate" validarr="required@@" tovalid="true" onblur="validateTextComponent(this)"
                                                                                                placeholder="Enter CSO Signing Date" type="Date">
                                                                                            <!---->
                                                                                            <div class="ng-star-inserted">
                                                                                                <!---->
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                            <div class="row" style="margin-top: 10px;">
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group row">
                                                                                        <label class="col-md-3 col-form-label"
                                                                                               for="csoSubMissionDate">CSO Submission
                                                                                            date</label>
                                                                                        <div class="col-md-9">
                                                                                            <input  validarr="required@@" tovalid="true" onblur="validateTextComponent(this)"
                                                                                                class="form-control ng-untouched ng-pristine ng-invalid"
                                                                                                formcontrolname="csoSubMissionDate" title=" CSO Submission date"
                                                                                                id="csoSubMissionDate" name="csoSubMissionDate"
                                                                                                placeholder="Enter CSO Submission date"
                                                                                                type="Date">
                                                                                            <!---->
                                                                                            <div class="ng-star-inserted">
                                                                                                <!---->
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row" style="margin-top: 10px;">
                                                                <div class="col-md-12">
                                                                    <div class=" card">
                                                                        <div class="card-header">
                                                                            <strong>Receiver Section</strong>
                                                                        </div>
                                                                        <div class="card-body">
                                                                            <div class="row" style="margin-top: 10px;">
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group row">
                                                                                        <label class="col-md-3 col-form-label"
                                                                                               for="receiverId">Receiver Id</label>
                                                                                        <div class="col-md-9">
                                                                                            <form:input validarr="required@@" tovalid="true" onblur="validateTextComponent(this)"
                                                                                                class="form-control ng-untouched ng-pristine ng-invalid"
                                                                                                formcontrolname="receiverId" id="receiverId"
                                                                                                name="receiverId" placeholder="Enter Receiver Id"
                                                                                                path="receiverId" title="Receiver Id" />
                                                                                            <!---->
                                                                                            <div class="ng-star-inserted">
                                                                                                <!---->
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                            <div class="row" style="margin-top: 10px;">
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group row">
                                                                                        <label class="col-md-3 col-form-label"
                                                                                               for="receiverDate">Receiving date</label>
                                                                                        <div class="col-md-9">
                                                                                            <input validarr="required@@" tovalid="true" onblur="validateTextComponent(this)"
                                                                                                class="form-control ng-untouched ng-pristine ng-invalid"
                                                                                                formcontrolname="receiverDate" id="receiverDate"
                                                                                                name="receiverDate"
                                                                                                placeholder="Enter Receiving date"
                                                                                                title="Receiving date" type="date">
                                                                                            <!---->
                                                                                            <div class="ng-star-inserted">
                                                                                                <!---->
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                            <div class="row" style="margin-top: 10px;">
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group row">
                                                                                        <label class="col-md-3 col-form-label"
                                                                                               for="decisionForNegRemark">Decision
                                                                                            against Neg Remarks</label>
                                                                                        <div class="col-md-9">
                                                                                            <form:select  onblur="validateCombo(this);" isrequired="true"
                                                                                                class="form-control ng-untouched ng-pristine ng-invalid"
                                                                                                formcontrolname="decisionForNegRemark"
                                                                                                title="Decision against Neg Remarks"
                                                                                                id="decisionForNegRemark"
                                                                                                name="decisionForNegRemark"
                                                                                                path="decisionForNegRemark" onchange="decisionagainstnevremarkvelidation(this.value)">
                                                                                                <form:option selected="selected" value="">Please Select</form:option>
                                                                                                <c:forEach var="entry"
                                                                                                           items="${decisionForNegRemarkList }">
                                                                                                    <form:option value="${entry.lookUpName}">${entry.lookUpName}</form:option>
                                                                                                </c:forEach>
                                                                                            </form:select>
                                                                                            <div class="ng-star-inserted">
                                                                                                <!---->
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                            <div class="row" style="margin-top: 10px;">
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group row">
                                                                                        <label class="col-md-3 col-form-label" for="memNo">Memo
                                                                                            No</label>
                                                                                        <div class="col-md-9">
                                                                                            <form:input  validarr="required@@" tovalid="true" onblur="validateTextComponent(this)"
                                                                                                class="form-control ng-untouched ng-pristine ng-invalid"
                                                                                                formcontrolname="memNo" id="memNo" name="memNo" title="Memo No"
                                                                                                path="memNo" placeholder="Enter Memo No" />
                                                                                            <!---->
                                                                                            <div class="ng-star-inserted">
                                                                                                <!---->
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                            <div class="row" style="margin-top: 10px;">
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group row">
                                                                                        <label class="col-md-3 col-form-label"
                                                                                               for="memDate">Memo Date</label>
                                                                                        <div class="col-md-9">
                                                                                            <input validarr="required@@" tovalid="true" onblur="validateTextComponent(this)"
                                                                                                class="form-control ng-untouched ng-pristine ng-invalid"
                                                                                                formcontrolname="memDate" id="memDate" title="Memo Date"
                                                                                                name="memDate" placeholder="Enter Memo Date"
                                                                                                type="date">
                                                                                            <!---->
                                                                                            <div class="ng-star-inserted">
                                                                                                <!---->
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                            <div class="row" style="margin-top: 10px;">
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group row">
                                                                                        <label class="col-md-3 col-form-label"
                                                                                               for="Duration">Duration</label>
                                                                                        <div class="col-md-9">
                                                                                            <div class="ng-star-inserted">
                                                                                                <!---->
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                            <div class="row" style="margin-top: 10px;">
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group row">
                                                                                        <label class="col-md-3 col-form-label" for="year">Year</label>
                                                                                        <div class="col-md-9">
                                                                                            <form:select disabled="true"
                                                                                                class="form-control ng-untouched ng-pristine ng-invalid"
                                                                                                title="Year" formcontrolname="year" id="year"
                                                                                                name="year" path="year" onchange="fnSetUntil()" >
                                                                                                <form:option selected="selected" value="">Please Select</form:option>
                                                                                                    <!---->
                                                                                                <form:option value="2019" class="ng-star-inserted">2019</form:option>
                                                                                                <form:option value="2018" class="ng-star-inserted">2018</form:option>
                                                                                                <form:option value="2017" class="ng-star-inserted">2017</form:option>
                                                                                                <form:option value="2016" class="ng-star-inserted">2016</form:option>
                                                                                                <form:option value="2015" class="ng-star-inserted">2015</form:option>
                                                                                                <form:option value="2014" class="ng-star-inserted">2014</form:option>
                                                                                            	<form:option value="2013" class="ng-star-inserted">2013</form:option>
                                                                                                <form:option value="2012" class="ng-star-inserted">2012</form:option>
                                                                                                <form:option value="2011" class="ng-star-inserted">2011</form:option>
                                                                                                <form:option value="2010" class="ng-star-inserted">2010</form:option>
                                                                                                <form:option value="2009" class="ng-star-inserted">2009</form:option>
                                                                                                <form:option value="2008" class="ng-star-inserted">2008</form:option>
                                                                                            </form:select>
                                                                                            <div class="ng-star-inserted">
                                                                                                <!---->
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                            <div class="row" style="margin-top: 10px;">
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group row">
                                                                                        <label class="col-md-3 col-form-label" for="until">Until</label>
                                                                                        <div class="col-md-9">
                                                                                            <input  disabled="true" readonly="readonly"
                                                                                                class="form-control ng-untouched ng-pristine ng-invalid"
                                                                                                formcontrolname="until" id="until" name="until"
                                                                                                placeholder="Enter Until" type="date">
                                                                                            <!---->
                                                                                            <div class="ng-star-inserted">
                                                                                                <!---->
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                            <div class="row" style="margin-top: 10px;">
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group row">
                                                                                        <label class="col-md-3 col-form-label"
                                                                                               for="description">Description</label>
                                                                                        <div class="col-md-9">
                                                                                            <form:textarea cols="10" rows="6"
                                                                                                           class="form-control ng-untouched ng-pristine ng-invalid"
                                                                                                           formcontrolname="description" id="description"
                                                                                                           name="description" path="description"
                                                                                                           placeholder="Enter Description"></form:textarea>
                                                                                                <div class="ng-star-inserted">
                                                                                                    <!---->
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>

                                                                                </div>
                                                                                <div class="row" style="margin-top: 10px;">
                                                                                    <div class="col-md-12">
                                                                                        <div class="form-group row">
                                                                                            <label class="col-md-3 col-form-label" for="remark">Remarks</label>
                                                                                            <div class="col-md-9">
                                                                                            <form:textarea cols="10" rows="6"
                                                                                                           class="form-control ng-untouched ng-pristine ng-invalid"
                                                                                                           formcontrolname="remark" id="remark"
                                                                                                           name="remark" path="remark"
                                                                                                           placeholder="Enter Remark"></form:textarea>
                                                                                                <div class="ng-star-inserted">
                                                                                                    <!---->
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>

                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class=" card">
                                                                            <div class="card-header">
                                                                                <strong>Common Section</strong>
                                                                            </div>
                                                                            <div class="card-body">
                                                                                <div class="row">
                                                                                    <div class="col-sm-12">
                                                                                        <table aria-describedby="DataTables_Table_0_info"
                                                                                               class="table table-striped table-bordered datatable dataTable no-footer"
                                                                                               id="DataTables_Table_0" role="grid"
                                                                                               style="border-collapse: collapse !important">
                                                                                            <thead>
                                                                                                <tr>
                                                                                                    <th>Documents</th>
                                                                                                    <th>Tag</th>
                                                                                                </tr>
                                                                                            </thead>
                                                                                            <tbody id="commonsectiontboby">
                                                                                                <c:forEach items="lstDocumnets" var="entry">
                                                                                                    
                                                                                                </c:forEach>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="card-footer">
                                                                <button class="btn btn-sm btn-success float-right"
                                                                        type="submit" >
                                                                    <i class="fa fa-dot-circle-o"></i> Save
                                                                </button>

                                                                <button style="margin: 0px 20px 0px 0px"
                                                                        class="btn btn-sm btn-success float-right" type="button"
                                                                        onclick="previewHtml('print');">
                                                                    <i class="fa fa-dot-circle-o"></i> Print
                                                                </button>
                                                            </div>
                                                        </div>
                                                </form:form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/customJS/acr/acrmanage.js"></script>
<script>
    var path='${pageContext.servletContext.contextPath}';
    var docJson = {};
    var docArr=[];
    $(function(){
        var json = JSON.parse('${lstDocumnets}');
        if('${successMsg}'!=''){
        	showSuccess('${successMsg}');
        }
        $.each(json,function(index,obj){
            if(obj['documentSub']!='-1'){
                if(!docJson.hasOwnProperty(obj['documentSub'])){
                    docArr=[]
                } else{
                   docArr= docJson[obj['documentSub']];
                }
                docArr.push(obj);
                docJson[obj['documentSub']]=docArr;
            }
        });

        $('#commonsectiontboby').html('');

        console.log(docJson);
        var tr = '';
        $.each(docJson,function(index,obj){
            tr = tr + '<tr>';
            tr = tr + '<td>'+index+'</td>';
            tr = tr + '<td><a style="cursor:pointer;" sub="'+index+'" onclick="fnOpenDocumentModel(this)"><i class="fa fa-eye"></i></a></td>';
            tr = tr + '/<tr>';
        });
        $('#commonsectiontboby').html(tr); 
    });
    function fnOpenDocumentModel(e){
        $('#dialog').dialog('open');
        destroyTable('mydocTable');
        var headArr = ['#','Document Date','Document Type','Document Sub type','Attachment','Remarks','Tag' ];
        createHeader('theaddoc',headArr);
        $('#tbodydoc').empty();
       
           var leadData = docJson[$(e).attr('sub')];
            var count = 0;
            $.each(leadData, function( index, object ) {
                count++;
                var trTableBody = $('<tr class="text-center"></tr>').appendTo($('#tbodydoc'));
                $('<td>'+count+'</td>').appendTo(trTableBody);
                $('<td data-sort="'+object['documentPk']+'">'+object['documentDate']+'</td>').appendTo(trTableBody);
                $('<td>'+object['documentType']+'</td>').appendTo(trTableBody);
                $('<td>'+checkValue(object['documentSub'])+'</td>').appendTo(trTableBody);
                $('<td>'+checkValue(object['attachment'])+'</td>').appendTo(trTableBody);
                $('<td>'+checkValue(object['remarks'])+'</td>').appendTo(trTableBody);
                $('<td><input type="checkbox" onchange="docCheck(this)" value="'+object['documentPk']+'"></td>').appendTo(trTableBody);
            });
       
        createTable('mydocTable');
    }
    function docCheck(ctrl){
        
        var str = $('#docPk').val();       
        if(str.indexOf($(ctrl).val()) != -1){
            str = str.replace($(ctrl).val(),'');
        }else{ 
            str += $(ctrl).val() + ","          
        }
        alert(str);
        $('#docPk').val(str);
       
    }
        
        
  
    function toorformfieldvelidation(acrtypevalue) {
        if(acrtypevalue == "Partial")
        {
        	 document.getElementById("from").disabled = false;
             document.getElementById("to").disabled = false;
            $('#from').attr('tovalid','true');
            $('#from').attr('validarr','required@@');
            $('#from').attr('onblur','validateTextComponent(this)');
            $('#to').attr('tovalid','true');
            $('#to').attr('validarr','required@@');
            $('#to').attr('onblur','validateTextComponent(this)');
        }
        else
        {
        	if(acrtypevalue == "Full"){
        		$('#from').val([$('#acrYear').val(), "01","01"].join('-'));
        		$('#to').val([$('#acrYear').val(), "12","31"].join('-'));
        	}
        	document.getElementById("from").disabled = true;
            document.getElementById("to").disabled = true;
            $('#from').removeAttr('tovalid');
            $('#from').removeAttr('validarr');
            $('#from').removeAttr('onblur');
            $('#to').removeAttr('tovalid');
            $('#to').removeAttr('validarr');
            $('#to').removeAttr('onblur');
        }
    }

    function healthdatevalidation(healthreportvalue)
    {
        if(healthreportvalue == "Applicable")
        {
        	document.getElementById("HealthReportDate").disabled = false;
        	$('#HealthReportDate').attr('tovalid','true');
            $('#HealthReportDate').attr('validarr','required@@');
            $('#HealthReportDate').attr('onblur','validateTextComponent(this)');
        }
        else
        {
        	document.getElementById("HealthReportDate").disabled = true;
        	 $('#HealthReportDate').removeAttr('tovalid');
             $('#HealthReportDate').removeAttr('validarr');
             $('#HealthReportDate').removeAttr('onblur');
        }
    }

    function reasonvalidation(acrrequiredvalue)
    {
        if(acrrequiredvalue == "No")
        {
        	document.getElementById("reason").disabled = false;
        	$('#reason').attr('tovalid','true');
            $('#reason').attr('validarr','required@@');
            $('#reason').attr('onblur','validateTextComponent(this)');
            
        } else
        {
        	document.getElementById("reason").disabled = true;
        	$('#reason').removeAttr('tovalid');
            $('#reason').removeAttr('validarr');
            $('#reason').removeAttr('onblur');
        }

    }

    function nevremarkvelidation(nevremarkvalue)
    {
        if (nevremarkvalue == "Yes")
        {
        	document.getElementById("negremarksByoru").disabled = false;
        	$('#negremarksByoru').attr('tovalid','true');
            $('#negremarksByoru').attr('validarr','required@@');
            $('#negremarksByoru').attr('onblur','validateTextComponent(this)');
            
        } else
        {
        	document.getElementById("negremarksByoru").disabled = true;
        	$('#negremarksByoru').removeAttr('tovalid');
            $('#negremarksByoru').removeAttr('validarr');
            $('#negremarksByoru').removeAttr('onblur');
        }
    }

    function nevremarkcsovelidation(nevremarkcsovalue)
    {
        if (nevremarkcsovalue == "Yes")
        {   
        	 document.getElementById("negremarksBycso").disabled = false;
        	 $('#negremarksBycso').attr('tovalid','true');
             $('#negremarksBycso').attr('validarr','required@@');
             $('#negremarksBycso').attr('onblur','validateTextComponent(this)');
           
        } else
        {
        	 document.getElementById("negremarksBycso").disabled = true;
        	 $('#negremarksBycso').removeAttr('tovalid');
             $('#negremarksBycso').removeAttr('validarr');
             $('#negremarksBycso').removeAttr('onblur');
        }
    }

    function sugdetailvelidation(sugbyriovalue)
    {
        if (sugbyriovalue == "Yes")
        {
        	document.getElementById("sugByOru").disabled = false;
        	$('#sugByOru').attr('tovalid','true');
            $('#sugByOru').attr('validarr','required@@');
            $('#sugByOru').attr('onblur','validateTextComponent(this)');
            
        } else
        {
        	document.getElementById("sugByOru").disabled = true;
        	$('#sugByOru').removeAttr('tovalid');
            $('#sugByOru').removeAttr('validarr');
            $('#sugByOru').removeAttr('onblur');
        }
    }

    function sugdetailcsovelidation(sugbycsovalue)
    {
        if (sugbycsovalue == "Yes")
        {
        	 document.getElementById("subByCso").disabled = false;
        	 $('#subByCso').attr('tovalid','true');
             $('#subByCso').attr('validarr','required@@');
             $('#subByCso').attr('onblur','validateTextComponent(this)');
            
        } else
        {
        	document.getElementById("subByCso").disabled = true;
        	$('#subByCso').removeAttr('tovalid');
            $('#subByCso').removeAttr('validarr');
            $('#subByCso').removeAttr('onblur');
        }
    }

    function decisionagainstnevremarkvelidation(value)
    {
        if (value == "Applicable")
        {
        	
        	document.getElementById("until").disabled = false;
            document.getElementById("year").disabled = false;
            $('#year').attr('isrequired','true');
            $('#year').attr('onblur','validateCombo(this)');
           
            
        } else
        {
        	document.getElementById("until").disabled = true;
            document.getElementById("year").disabled = true;
            $('#year').removeAttr('isrequired');
            $('#year').removeAttr('onblur');
        }
    }
</script>



