<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib prefix="f" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="t" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<title>Goods Receipt Details</title>
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/resources/plugins/reflection/reflection-com.css">
<main class="main"> <!-- Breadcrumb-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="${pageContext.servletContext.contextPath}/index"><span class="trn">Home</span></a></li>
        <li class="breadcrumb-item"><a href="#"><span class="trn">Store</span></a></li>
        <li class="breadcrumb-item active"><span class="trn">Good Receipt</span></li>
        <li class="float-right w-100">
            <div class="float-right" style="margin-top: -20px!important;">
                <a href="${pageContext.servletContext.contextPath}/strGoodsReceipt/index" class="btn btn-info btn-sm"><i class="fa fa-list c-white"></i> Index</a>
                <a href="${pageContext.servletContext.contextPath}/strGoodsReceipt/create" class="btn btn-success btn-sm"><i class="fa fa-edit c-white"></i> Create</a>
            </div>
        </li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="card">
                <div class="card-header"><strong><span class="trn">Show Goods Receipt</span></strong></div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="form-group">
                                <label>কোড</label>
                                <input type="text" class="form-control disabled" value="${tblStrGoodsReceiptInstance.code}"/>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="form-group">
                                <label>তারিখ </label>
                                <input type="text" class="form-control disabled" value="${tblStrGoodsReceiptInstance.transcDate}"/>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="form-group">
                                <label>সরবরাহকারী প্রতিষ্ঠানের নাম</label>
                                <input type="text" class="form-control disabled" value="${tblStrGoodsReceiptInstance.tblStrSupplier.name}"/>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="form-group">
                                <label>Remarks</label>
                                <input type="text" class="form-control disabled" value="${tblStrGoodsReceiptInstance.remarks}"/>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12 table-responsive">
                            <table class="table table-striped table-bordered no-footer ">
                                <thead>
                                <td>দ্রব্যের নাম/আইটেম নাম</td>
                                <td>অর্থনৈতিক গ্রুপ/কোড</td>
                                <td>পরিমাণ</td>
                                <td>একক দাম</td>
                                <td>মোট</td>
                                </thead>
                                <tbody>
                                <c:forEach var="each" items="${tblStrGoodsReceiptInstance.tblStrGrnDetails}">
                                    <tr class="tbl-row">
                                        <td>${each.tblStrProduct.name}</td>
                                        <td>${each.tblStrProduct.economicCode}</td>
                                        <td>${each.quantity}</td>
                                        <td>${each.unitPrice}</td>
                                        <td>${each.unitPrice * each.quantity}</td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>


                </div>

            </div>
        </div>
    </div>
</main>
<script type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/plugins/reflection/reflection-com.js"></script>
<script type="text/javascript">
    $(document).on("click", "#btn-submit", function () {
        takeAction("SUBMIT");
    });
    $(document).on("click", "#btn-approve", function () {
        takeAction("APPROVE");
    });
    $(document).on("click", "#btn-reject", function () {
        takeAction("REJECT");
    });
    $(document).on("change", "#tblStrDriverInfo", function () {
        var id = $(this).val();
        if (id) {
            startLoading();
            $.get({
                type: "GET",
                url: path + '/driver/getDriverDetails',
                dataType: "JSON",
                data: {
                    id: id
                },
                success: function (r) {
                    if (r.status == 'OK') {
                        $("#driverName").val(r.driverName);
                        $("#mobile").val(r.mobile);
                        $("#tblStrCarInfoId").val(r.tblStrCarInfoId);
                        $("#carModel").val(r.carModel);
                        $("#carNo").val(r.carNo);
                    }
                },
                error: function (e) {
                    alert("Sorry, failed to get driver details due to :" + e);
                }
            });
            hideLoading();
        }
        else{
            $("#driverName").val("");
            $("#mobile").val("");
            $("#tblStrCarInfoId").val("");
            $("#carModel").val("");
            $("#carNo").val("");
        }
    });

    function takeAction(t) {
        clearErrors();
        var id = $("#id").val();
        if (validateRequiredFields() && id && confirm("Are you sure, you want to proceed with action '" + t + "'...?")) {
            startLoading();
            $.get({
                type: "GET",
                url: path + '/strServiceRequisition/takeAction',
                dataType: "JSON",
                data: {
                    t: t,
                    i: $("#id").val(),
                    d: $("#tblStrDriverInfo").val() ? $("#tblStrDriverInfo").val() : '',
                    r: $("#remarks").val()?$("#remarks").val():''
                },
                success: function (r) {
                    if (r.status == 'OK') {
                        $(".btn").prop("disabled", true);
                        alert("Action taken successfully...");
                        location.reload();
                    }
                },
                error: function (e) {
                    alert("Sorry, failed to take action due to : " + JSON.stringify(e));
                }
            });
            hideLoading();
        }
    }
</script>
