<%@ include file="/pages/common.jsp" %>
<style>
    .main .container-fluid {
        padding: 0 10px !important;
        margin-top: -15px !important;
    }

    tr td {
        white-space: nowrap;
    }
</style>

<main class="main"> <!-- Breadcrumb-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="${pageContext.servletContext.contextPath}/index"><span
                class="trn">Home</span></a></li>
        <li class="breadcrumb-item"><a href="#"><span class="trn">Management</span></a></li>
        <li class="breadcrumb-item active"><span class="trn">Diagonosis Information</span></li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">

            <div class="card">
                <div class="card-header">
                    <strong><span class="trn">Diagonosis Information</span></strong>
                </div>

                <form:form role="form" name="product" action="${pageContext.servletContext.contextPath}/diagonosis/save"
                           onsubmit="if(valOnSubmit()){startLoading();return true;}else{return false;}"
                           method="post" modelAttribute="modelDiagonosis">
                    <div class="card-body">
                        <form:hidden path="id" name="id" id="id"/>

                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="name"><span
                                            class="trn">Name</span></label>
                                    <div class="col-md-8">
                                        <form:input path="name" class="form-control"
                                                    title="name"
                                                    formcontrolname="name" id="name" name="name"
                                                    placeholder="Enter Name" maxlength="100"/>
                                        <div class="ng-star-inserted">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="type"><span
                                            class="trn">Type</span></label>
                                    <div class="col-md-8">
                                        <form:select id="type" path="type"
                                                     class="form-control ng-untouched ng-pristine ng-invalid">
                                            <form:option value="X-RAY">X-RAY</form:option>
                                            <form:option value="Pathology">Pathology</form:option>
                                        </form:select>
                                    </div>

                                    <div class="ng-star-inserted">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>



                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="description"><span class="trn">Description</span></label>
                                    <div class="col-md-8">
                                        <form:input path="description" class="form-control"
                                                    title="description"
                                                    formcontrolname="description" id="description" name="description"
                                                    placeholder="Enter Description" maxlength="100"/>
                                        <div class="ng-star-inserted">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>

                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="minValue"><span class="trn">Minimum Value</span></label>
                                    <div class="col-md-8">
                                        <form:input type="number" path="minValue" class="form-control"
                                                    title="minValue"
                                                    formcontrolname="minValue" id="minValue" name="minValue"
                                                    placeholder="Enter Manufacture Name"/>
                                        <div class="ng-star-inserted">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>

                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="minValueMale"><span class="trn">Minimum Value for Male</span></label>
                                    <div class="col-md-8">
                                        <form:input type="number" path="minValueMale" class="form-control"
                                                    title="minValueMale"
                                                    formcontrolname="minValueMale" id="minValueMale" name="minValueMale"
                                                    placeholder="0"/>
                                        <div class="ng-star-inserted">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>

                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="minValueFemale"><span class="trn">Minimum Value for Female</span></label>
                                    <div class="col-md-8">
                                        <form:input type="number" path="minValueFemale" class="form-control"
                                                    title="minValueFemale"
                                                    formcontrolname="minValueMale" id="minValueFemale"
                                                    name="minValueFemale"
                                                    placeholder="0"/>
                                        <div class="ng-star-inserted">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>

                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="maxValue"><span class="trn">Maximum Value</span></label>
                                    <div class="col-md-8">
                                        <form:input type="number" path="maxValue" class="form-control"
                                                    title="maxValue"
                                                    formcontrolname="maxValue" id="maxValue" name="maxValue"
                                                    placeholder="Enter Manufacture Name"/>
                                        <div class="ng-star-inserted">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>

                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="maxValueMale"><span class="trn">Maximum Value for Male</span></label>
                                    <div class="col-md-8">
                                        <form:input type="number" path="maxValueMale" class="form-control"
                                                    title="maxValueMale"
                                                    formcontrolname="maxValueMale" id="maxValueMale" name="maxValueMale"
                                                    placeholder="0"/>
                                        <div class="ng-star-inserted">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>


                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="maxValueFemale"><span class="trn">Maximum Value for Female</span></label>
                                    <div class="col-md-8">
                                        <form:input type="number" path="maxValueFemale" class="form-control"
                                                    title="maxValueFemale"
                                                    formcontrolname="maxValueMale" id="maxValueFemale"
                                                    name="maxValueFemale"
                                                    placeholder="0"/>
                                        <div class="ng-star-inserted">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>

                            <div class="row">
                                                        <div class="col-sm-8">
                                                            <div class="form-group row">
                                                                <label class="col-md-4 col-form-label" for="remarks"><span
                                                                        class="trn">Remarks</span></label>
                                                                <div class="col-md-8">
                                                                    <form:textarea path="remarks"  class="form-control ng-untouched ng-pristine ng-invalid"
                                                                                   id="remarks" rows="2"/>

                                                                    <div class="ng-star-inserted">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4"></div>
                                                    </div>
                    </div>
                    <div class="card-footer" style="height: 52px;">
                        <form:button class="btn btn-sm btn-success float-right" type="submit">
                            <i class="fa fa-dot-circle-o"></i><span class="trn">Save</span>
                        </form:button>
                    </div>
                </form:form>
            </div>
        </div>
    </div>
</main>

