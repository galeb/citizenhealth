<%@ include file="/pages/common.jsp" %>
<style>
    .main .container-fluid {
        padding: 0 10px !important;
        margin-top: -15px !important;
    }

    tr td {
        white-space: nowrap;
    }
</style>

<main class="main"> <!-- Breadcrumb-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="${pageContext.servletContext.contextPath}/index"><span class="trn">Home</span></a></li>
        <li class="breadcrumb-item"><a href="#"><span class="trn">Management</span></a></li>
        <li class="breadcrumb-item active"><span class="trn">Doctor Information</span></li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">

            <div class="card">
                <div class="card-header">
                    <strong><span class="trn">Doctor Information</span></strong>
                </div>

                <form:form role="form" name="product" action="${pageContext.servletContext.contextPath}/doctor-info/save" onsubmit="if(valOnSubmit()){startLoading();return true;}else{return false;}" method="post" modelAttribute="modelDoctorInfo">
                    <div class="card-body">
                        <form:hidden path="id" name="id" id="id"/>

                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="name"><span class="trn">P-ID</span></label>
                                    <div class="col-md-8">
                                        <form:input path="" class="form-control"
                                                    title="pid"
                                                    formcontrolname="" id="pid"
                                                    placeholder="Enter Pid" maxlength="100"/>
                                        <form:hidden path="personalInfo.id" id="pkid"></form:hidden>
                                        <div class="ng-star-inserted">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>

                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="name"><span class="trn">Name</span></label>
                                    <div class="col-md-8">
                                        <form:input path="name" class="form-control"
                                                    title="name"
                                                    formcontrolname="name" id="name" name="name"
                                                    placeholder="Enter Name" maxlength="100"/>
                                        <div class="ng-star-inserted">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>

                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="contactNo"><span class="trn">Contact No</span></label>
                                    <div class="col-md-8">
                                        <form:input path="contactNo" class="form-control"
                                                    title="contactNo"
                                                    formcontrolname="contactNo" id="contactNo" name="contactNo"
                                                    placeholder="Enter contactNo" maxlength="100"/>
                                        <div class="ng-star-inserted">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>

                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="address"><span class="trn">Address</span></label>
                                    <div class="col-md-8">
                                        <form:input path="address" class="form-control"
                                                    title="address"
                                                    formcontrolname="address" id="address" name="address"
                                                    placeholder="Enter address" maxlength="100"/>
                                        <div class="ng-star-inserted">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>

                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="email"><span class="trn">Email</span></label>
                                    <div class="col-md-8">
                                        <form:input path="email" class="form-control"
                                                    title="email"
                                                    formcontrolname="email" id="email" name="email"
                                                    placeholder="Enter email" maxlength="100"/>
                                        <div class="ng-star-inserted">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>

                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="regInfo"><span class="trn">Reg. No.</span></label>
                                    <div class="col-md-8">
                                        <form:input path="regInfo" class="form-control"
                                                    title="regInfo"
                                                    formcontrolname="regInfo" id="regInfo" name="regInfo"
                                                    placeholder="Enter regInfo" maxlength="100"/>
                                        <div class="ng-star-inserted">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="regInfo"><span class="trn">Department</span></label>
                                    <div class="col-md-8">
                                        <form:select path="department" class="form-control" title="department" id="department" name="department">
                                            <form:option value="Breast Cancer Specialist">Breast Cancer Specialist</form:option>
                                            <form:option value="Cardiology">Cardiology</form:option>
                                            <form:option value="Chest Medicine">Chest Medicine</form:option>
                                            <form:option value="Sonologist">Sonologist</form:option>
                                            <form:option value="Skin / Dermatology">Skin / Dermatology</form:option>
                                            <form:option value="Diabetologist">Diabetologist</form:option>
                                            <form:option value="Dietician">Dietician</form:option>
                                            <form:option value="Endocrine Medicine">Endocrine Medicine</form:option>
                                            <form:option value="ENT, Head & Neck Surgery">ENT, Head & Neck Surgery</form:option>
                                            <form:option value="Eye / Opthalmology">Eye / Opthalmology</form:option>
                                            <form:option value="Gastroenterology">Gastroenterology</form:option>
                                            <form:option value="Neurosurgery">Neurosurgery</form:option>
                                            <form:option value="General Surgery">General Surgery</form:option>
                                            <form:option value="Haematology">Haematology</form:option>
                                            <form:option value="Liver Medicine/Hepatology">Liver Medicine/Hepatology</form:option>
                                            <form:option value="Liver, Biliary And Pancreatic Surgery">Liver, Biliary And Pancreatic Surgery</form:option>
                                            <form:option value="Medicine">Medicine</form:option>
                                            <form:option value="Neurology">Neurology</form:option>
                                            <form:option value="Gynaecology">Gynaecology</form:option>
                                            <form:option value="Orthopaedic Surgery">Orthopaedic Surgery</form:option>
                                            <form:option value="Pain Management">Pain Management</form:option>
                                            <form:option value="Child/Pediatric">Child/Pediatric</form:option>
                                            <form:option value="Pediatric Surgery">Pediatric Surgery</form:option>
                                            <form:option value="Physical Medicine And Rehabilitation">Physical Medicine And Rehabilitation</form:option>
                                            <form:option value="Physiotherapy Department">Physiotherapy Department</form:option>
                                            <form:option value="Psychiatry">Psychiatry</form:option>
                                            <form:option value="Rheumatology Medicine">Rheumatology Medicine</form:option>
                                            <form:option value="Skin / Dermatology">Skin / Dermatology</form:option>
                                            <form:option value="Urology Surgery">Urology Surgery</form:option>
                                            <form:option value="Nephrology / Kidney Medicine">Nephrology / Kidney Medicine</form:option>
                                            <form:option value="Child Neurology">Child Neurology</form:option>
                                            <form:option value="Nuclear Medicine">Nuclear Medicine</form:option>
                                            <form:option value="Colorectal Surgery">Colorectal Surgery</form:option>
                                            <form:option value="Paediatrics Surgery">Paediatrics Surgery</form:option>
                                            <form:option value="Plastic Surgery">Plastic Surgery</form:option>
                                            <form:option value="Vascular Surgery">Vascular Surgery</form:option>
                                            <form:option value="Oncology">Oncology</form:option>
                                            <form:option value="Breast, Colorectal & Laparoscopy Surgery">Breast, Colorectal & Laparoscopy Surgery</form:option>
                                            <form:option value="Lithotripsy Department">Lithotripsy Department</form:option>
                                        </form:select>
                                        <div class="ng-star-inserted">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>

                    </div>
                    <div class="card-footer" style="height: 52px;">
                        <form:button class="btn btn-sm btn-success float-right" type="submit">
                            <i class="fa fa-dot-circle-o"></i><span class="trn">Save</span>
                        </form:button>
                    </div>
                </form:form>
            </div>
        </div>
    </div>
</main>

<script>
    function setEconomyCode(e) {
        $(e).closest('tr').find('.economyCode').text($("#sp"+$(e).val()).text());
    }
    $("#pid").on('blur', function() {
        $.get({
            url : path + '/personal-info/fetchByPid',
            data : "pid="+$('#pid').val(),
            success : function(response) {
                if(response == 'emptyCode'){
                    showError('<b>ERROR!</b><br> Please Enter PID');
                }else if(response == 'notFound'){
                    showError('<b>ERROR!</b><br> No Record found for the Provided Personal ID');
                }else{
                    var json = JSON.parse(response);
                    console.log(response);
                    $('#pkid').val(json['id']);
                    $('#name').val(json['name']);
                    $('#email').val(json['email']);
                    $('#contactNo').val(json['contactNo']);
                }
            }
        });
    });
</script>
