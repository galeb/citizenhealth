<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>



<html lang="en">
  <head>
    <base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="description" content="CoreUI - Open Source Bootstrap Admin Template">
    <meta name="author" content="Łukasz Holeczek">
    <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,jQuery,CSS,HTML,RWD,Dashboard">
    <title>CoreUI Admin Panel</title>
    <link rel="icon" type="image/png" href="${pageContext.servletContext.contextPath}/resources/img/brand/favicon.png">
    <!-- Icons-->
    <link href="${pageContext.servletContext.contextPath}/resources/css/coreui-icons.min.css" rel="stylesheet">
    <link href="${pageContext.servletContext.contextPath}/resources/css/flag-icon.min.css" rel="stylesheet">
	<link href="${pageContext.servletContext.contextPath}/resources/css/font-awesome.min.css" rel="stylesheet">
	<link href="${pageContext.servletContext.contextPath}/resources/css/simple-line-icons.css" rel="stylesheet">
    <!-- Main styles for this application-->
    <link href="${pageContext.servletContext.contextPath}/resources/css/style.css" rel="stylesheet">
    <link href="${pageContext.servletContext.contextPath}/resources/vendors/pace-progress/css/pace.min.css" rel="stylesheet">
    <!-- Global site tag (gtag.js) - Google Analytics-->
    <script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-118965717-3"></script>
    <script>
      window.dataLayer = window.dataLayer || [];

      function gtag() {
        dataLayer.push(arguments);
      }
      gtag('js', new Date());
      // Shared ID
      gtag('config', 'UA-118965717-3');
      // Bootstrap ID
      gtag('config', 'UA-118965717-5');
    </script>
  </head>
  <body class="app flex-row align-items-center">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-md-5">
          <div class="card-group">
            <div class="card p-4">
              <div class="card-body">
                <h1>Signup</h1>
                <p class="text-muted">Create your account</p>
               <form:form role="form" action="${pageContext.servletContext.contextPath}/user/save" method="post" modelAttribute="userModel" >
                 <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="icon-user"></i>
                    </span>
                  </div>
                  <form:input class="form-control" path="name" placeholder="Name"/>
                </div>
                 <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text">@</span>

                  </div>
                  <form:input class="form-control" path="email" placeholder="eamil"/>
                </div>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="icon-user"></i>
                    </span>
                  </div>
                  <form:input class="form-control" path="userName" placeholder="Username"/>
                </div>
                <div class="input-group mb-4">
                  <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="icon-lock"></i>
                    </span>
                  </div>
                  <form:password class="form-control" path="password" placeholder="Password"/>
                </div>
                <div class="input-group mb-4">
                  <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="icon--asterisk"></i>
                    </span>
                  </div>
                  <form:select class="form-control" path="roleFK">
                    <form:option value="2">Citizen</form:option>
                    <form:option value="3">Medical Center</form:option>
                    <form:option value="4">Doctor</form:option>
                    <form:option value="5">Researcher</form:option>
                  </form:select>
                </div>
                <div class="row">
                  <div class="col-6">
                    <form:button class="btn btn-primary px-4" type="submit">Create Account</form:button>
                  </div>
                  <div class="col-6 text-right">
                    <button onclick="window.location.href='${pageContext.servletContext.contextPath}/login'" class="btn btn-link px-0" type="button"><u>Allready user?</u></button>
                  </div>                  
                </div>             
              </form:form>
              </div>
            </div>
            
          </div>
        </div>
      </div>
    </div>
    <!-- CoreUI and necessary plugins-->
    <script src="${pageContext.servletContext.contextPath}/resources/boot/jquery.min.js"></script>
    <script src="${pageContext.servletContext.contextPath}/resources/boot/popper.min.js"></script>
    <style src="${pageContext.servletContext.contextPath}/resources/css/bootstrap.min.css"></style>
	
    <script src="${pageContext.servletContext.contextPath}/resources/boot/pace.min.js"></script>
    <script src="${pageContext.servletContext.contextPath}/resources/boot/perfect-scrollbar.min.js"></script>
    <script src="${pageContext.servletContext.contextPath}/resources/boot/coreui.min.js"></script>
  </body>
</html>
