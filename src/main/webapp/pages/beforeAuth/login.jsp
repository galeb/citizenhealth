<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords"
          content="Creative , Creative Agency template, Creative, Agency, Parallax, parallax template, landing page, material design, Corporate, Multipurpose, Business">
    <link rel="icon" type="image/png" href="${pageContext.servletContext.contextPath}/resources/img/brand/favicon.png">
    <title>Health Information System</title>

    <!-- favicon -->
    <link rel="shortcut icon" type="image/x-icon"
          href="${pageContext.servletContext.contextPath}/resources/login/assets/img/favicon.ico">


    <!-- style css -->
    <link rel="stylesheet" href="${pageContext.servletContext.contextPath}/resources/login/style.css">
    <link rel="stylesheet" href="bootstrap.min.css">
    <link rel="stylesheet" href="responsiveslides.css">
    <!-- modernizr js -->
    <script src="${pageContext.servletContext.contextPath}/resources/login/assets/js/vendor/modernizr-2.8.3.min.js"></script>

    <style>
        #cssmenu ul ul {
            display: none;
        }

    </style>

</head>
<body>
<div class="wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="full-body">
                    <header class="header">
                        <div class="header_img">
                            <a href="#"><img width="100%" width="4%"
                                             src="${pageContext.servletContext.contextPath}/resources/login/assets/img/health_banner.jpeg"></a>
                        </div>
                        <form:form class="form-inline" role="form"
                                   action="${pageContext.servletContext.contextPath}/user/authenticate" method="post"
                                   modelAttribute="userModel">
                            <div class="form-group" style="padding-left: 50%;">
                                <label for="userName">User Name:</label>
                                <form:input id="userName" path="userName" name="userName" placeholder="Username"/>

                            </div>
                            <div class="form-group">
                                <label for="password">Password:</label>
                                <form:password id="password" path="password" name="password" placeholder="Password"/>

                            </div>
                            <div class="form-group">
                                <label class="errormsg" id="lblInvalidCredentials" style="color:#e61a1a"></label>
                                <input class="btn btn-success" type="submit" value="Login"/>
                            </div>
                            <%--<div class="header_img">
                        <a href="#"><img src="${pageContext.servletContext.contextPath}/resources/login/assets/img/captcha.png"></a>
                    </div>  --%>
                            <%--<div class="input-group" style="margin-left: 24px">
                                <div class="checkbox">
                                    <label>
                                      <input id="login-remember" type="checkbox" name="remember" value="1"> Remember me
                                    </label>
                                  </div>
                            </div>--%>

                            <!-- <div class="col-md-12"> -->
                            <%-- 															<span class='forgot-pass'><a href="${pageContext.servletContext.contextPath}/signup">Create new user?</a></span> --%>
                            <!-- </div> -->
                            <%--<div class="col-md-12">
                                <div class="log-btn">
                                    <input class="inlog" type="submit" value="Login"/>
                                </div>
                            </div>--%>

                        </form:form>
                        <a class="btn btn-sm btn-info" href="/signup">Register</a>
                    </header>
                    <div class="maincontent" style="padding: 20px;">
                        <div class="row">

                            <div class="col-md-12">
                                <c:forEach items="${bartaList}" var="each">
                                    <div class="panel">
                                        <div class="panel-body">
                                            <div class="row">
                                                <c:choose>
                                                    <c:when test="${each.imagePath ne null}">
                                                        <div class="col-md-6">
                                                            <img width="100%" height="100%"
                                                                 src="${pageContext.servletContext.contextPath}/resources/login/assets/img/${each.imagePath}">

                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="">
                                                                <h3>${each.title}</h3>
                                                                    <%--<h5>New research may explain why evolution made humans 'fat'</h5>--%>
                                                                <span>${each.barta}</span>
                                                            </div>
                                                        </div>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <div class="col-md-12">
                                                            <h3>${each.title}</h3>
                                                                <%--<h5>New research may explain why evolution made humans 'fat'</h5>--%>
                                                            <span>${each.barta}</span>
                                                        </div>
                                                    </c:otherwise>

                                                </c:choose>


                                                    <%--<div class="col-md-6">
                                                        <img width="100%" height="40%"
                                                             src="${pageContext.servletContext.contextPath}/resources/login/assets/img/citihealth_image.jpg">

                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="">
                                                            <h3>Shastho Barta</h3>
                                                            <h5>New research may explain why evolution made humans 'fat'</h5>
                                                            <br>
                                                            <p>
                                                                Scientists have compared fat samples from humans and other
                                                                primates and found that changes in DNA packaging affected how
                                                                the human body processes fat.
                                                            </p>
                                                        </div>
                                                    </div>--%>
                                            </div>

                                        </div>
                                    </div>
                                </c:forEach>

                            </div>
                        </div>
                    </div>
                    <div class="footer_area">
                        <div class="row">
                            <div class="col-md-12">
                                <p>Health Information System © 2019. All rights reserved.<br>
                                    Design & Developed By Abdullah Al Galeb, PMIT, Jahangirnagar University.</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="${pageContext.servletContext.contextPath}/resources/login/assets/js/bootstrap.min.js"></script>
<script>
    var resposneMessage = '${resposneMessage}';
    if (resposneMessage !== '') {
        $('#lblInvalidCredentials').html('<b>' + resposneMessage + '</b>');
    } else {
        $('#lblInvalidCredentials').html('');
    }
</script>
</body>

</html>
