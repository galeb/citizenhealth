<%@ include file="/pages/common.jsp" %>
<style>
    .main .container-fluid {
        padding: 0 10px !important;
        margin-top: -15px !important;
    }

    tr td {
        white-space: nowrap;
    }
</style>

<main class="main"> <!-- Breadcrumb-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="${pageContext.servletContext.contextPath}/index"><span class="trn">Home</span></a></li>
        <li class="breadcrumb-item"><a href="#"><span class="trn">Management</span></a></li>
        <li class="breadcrumb-item active"><span class="trn">Medical Center</span></li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">

            <div class="card">
                <div class="card-header">
                    <strong><span class="trn">Medical Center</span></strong>
                </div>

                <form:form role="form" name="product" action="${pageContext.servletContext.contextPath}/medical-centers/save" onsubmit="if(valOnSubmit()){startLoading();return true;}else{return false;}" method="post" modelAttribute="modelMedicalCenter">
                    <div class="card-body">
                        <form:hidden path="id" name="id" value="${medicalCenterInfo.id}" id="id"/>

                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="name"><span class="trn">Name</span></label>
                                    <div class="col-md-8">
                                        <form:input path="name" class="form-control"
                                                    title="name" value="${medicalCenterInfo.name}"
                                                    formcontrolname="name" id="name" name="name"
                                                    placeholder="Enter Name" maxlength="100"/>
                                        <div class="ng-star-inserted">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>

                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="contactNo"><span class="trn">Contact No</span></label>
                                    <div class="col-md-8">
                                        <form:input path="contactNo" class="form-control"
                                                    title="contactNo" value="${medicalCenterInfo.contactNo}"
                                                    formcontrolname="contactNo" id="contactNo" name="contactNo"
                                                    placeholder="Enter contactNo" maxlength="100"/>
                                        <div class="ng-star-inserted">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>

                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="address"><span class="trn">Address</span></label>
                                    <div class="col-md-8">
                                        <form:input path="address" class="form-control"
                                                    title="address"  value="${medicalCenterInfo.address}"
                                                    formcontrolname="address" id="address" name="address"
                                                    placeholder="Enter address" maxlength="100"/>
                                        <div class="ng-star-inserted">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>

                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="email"><span class="trn">Email</span></label>
                                    <div class="col-md-8">
                                        <form:input path="email" class="form-control"
                                                    title="email"  value="${medicalCenterInfo.email}"
                                                    formcontrolname="email" id="email" name="email"
                                                    placeholder="Enter email" maxlength="100"/>
                                        <div class="ng-star-inserted">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>

                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="regInfo"><span class="trn">Reg. No.</span></label>
                                    <div class="col-md-8">
                                        <form:input path="regInfo" class="form-control"
                                                    title="regInfo" value="${medicalCenterInfo.regInfo}"
                                                    formcontrolname="regInfo" id="regInfo" name="regInfo"
                                                    placeholder="Enter regInfo" maxlength="100"/>
                                        <div class="ng-star-inserted">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>

                       <%-- <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="locationType"><span class="trn">Location Type</span></label>
                                    <div class="col-md-8">
                                        <form:select id="locationType" path="locationType" class="form-control ng-untouched ng-pristine ng-invalid">
                                        <form:option value="Division">Division</form:option>
                                        <form:option value="District">District</form:option>
                                        <form:option value="Thana">Thana</form:option>
                                    </form:select>
                                    </div>

                                    <div class="ng-star-inserted">
                                    </div>
                                </div>
                            </div>
                        <div class="col-sm-4"></div>
                    </div>

                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="tblStrSupplier"><span class="trn">Parent Location</span></label>
                                    <div class="col-md-8">
                                        <form:select id="tblStrSupplier" path="parentLocation.id" class="form-control ng-untouched ng-pristine ng-invalid">
                                            <form:option value=""> --SELECT--</form:option>
                                            <form:options items="${locationList}" itemValue="id" itemLabel="name"></form:options>
                                        </form:select>


                                        <div class="ng-star-inserted">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>--%>

                    </div>
                    <div class="card-footer" style="height: 52px;">
                        <form:button class="btn btn-sm btn-success float-right" type="submit">
                            <i class="fa fa-dot-circle-o"></i><span class="trn">Save</span>
                        </form:button>
                    </div>
                </form:form>
            </div>
        </div>
    </div>
</main>

<script>
    function setEconomyCode(e) {
        $(e).closest('tr').find('.economyCode').text($("#sp"+$(e).val()).text());
    }
</script>
