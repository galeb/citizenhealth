<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="t" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<title>Good Receipt</title>
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/resources/plugins/bootstrap-datepicker/datepicker3.css">
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/resources/plugins/reflection/reflection-com.css">
<main class="main"> <!-- Breadcrumb-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="${pageContext.servletContext.contextPath}/index"><span class="trn">Home</span></a></li>
        <li class="breadcrumb-item"><a href="#"><span class="trn">Store</span></a></li>
        <li class="breadcrumb-item active"><span class="trn">Good Receipt</span></li>
        <li class="float-right w-100">
            <div class="float-right" style="margin-top: -20px!important;">
                <a href="${pageContext.servletContext.contextPath}/strGoodsReceipt/index" class="btn btn-info btn-sm"><i class="fa fa-list c-white"></i> Index</a>
                <a href="${pageContext.servletContext.contextPath}/strGoodsReceipt/create" class="btn btn-success btn-sm"><i class="fa fa-edit c-white"></i> Create</a>
            </div>
        </li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="card">
                <div class="card-header">
                    <strong><span class="trn"> </span></strong>
                </div>

                <form:form role="form" name="product" action="${pageContext.servletContext.contextPath}/strGoodsReceipt/save" onsubmit="if(valOnSubmit()){startLoading();return true;}else{return false;}" method="post" modelAttribute="tblStrGoodsReceiptInstance">
                    <div class="card-body">
                        <form:hidden path="id" name="id" id="id"/>

                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="code"><span class="trn">কোড</span></label>
                                    <div class="col-md-8">
                                        <form:input path="code" class="form-control"
                                                    title="code"
                                                    formcontrolname="name" id="code" name="code"
                                                    placeholder="Enter code" maxlength="10"/>
                                        <div class="ng-star-inserted">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="code"><span class="trn">তারিখ</span></label>
                                    <div class="col-md-8">
                                        <form:input type="input"
                                                    path="transcDate"
                                                    class="form-control datepicker" id="transcDate"
                                                    name="transcDate"
                                                    autocomplete="false"/>
                                        <div class="ng-star-inserted">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="tblStrSupplier"><span class="trn">সরবরাহকারী প্রতিষ্ঠানের নাম</span></label>
                                    <div class="col-md-8">
                                        <form:select id="tblStrSupplier" path="tblStrSupplier.id" class="form-control ng-untouched ng-pristine ng-invalid">
                                            <form:option value=""> --SELECT--</form:option>
                                            <c:forEach items="${tblSupplierList}" var="each">
                                                <option value="${each.id}" ${tblStrGoodsReceiptInstance.tblStrSupplier.id eq each.id ? 'selected':''}>${each.name}</option>
                                            </c:forEach>
                                        </form:select>


                                        <div class="ng-star-inserted">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="remarks"><span class="trn">Remarks</span></label>
                                    <div class="col-md-8">
                                        <form:textarea path="remarks" rows="5" cols="30" class="form-control ng-untouched ng-pristine ng-invalid"
                                                       title="remarks" onblur="validateTextComponent(this)"
                                                       formcontrolname="remarks" id="remarks" name="remarks"
                                                       placeholder="Enter remarks" maxlength="500"/>
                                        <div class="ng-star-inserted">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 table-responsive" id="requisitionDtl">
                                <table
                                        class="table table-striped table-bordered no-footer "
                                        id="requisitionDtlTbl" role="grid"
                                        style="border-collapse: collapse !important">
                                    <thead>
                                    <td>দ্রব্যের নাম/আইটেম নাম</td>
                                    <td>অর্থনৈতিক গ্রুপ/কোড</td>
                                    <td>পরিমাণ</td>
                                    <td>একক দাম</td>
                                    <td>মোট</td>
                                    <td><button type="button"
                                                class="form-control btn btn-success btn-sm pull-right add-more">
                                        <span class="trn">Add</span></button></td>
                                    </thead>
                                    <tbody>
                                    <c:forEach var="each" items="${tblStrGoodsReceiptInstance.tblStrGrnDetails}" varStatus="loop">

                                    <tr class="tbl-row">

                                        <td>
                                            <form:select id="productPk${loop.index}" onchange="setEconomyCode(this)" path="tblStrGrnDetails[${loop.index}].tblStrProduct" class="form-control ng-untouched ng-pristine ng-invalid">
                                                <form:option value=""> --SELECT--</form:option>
                                                <c:forEach items="${productList}" var="product">
                                                    <option value="${product.id}" ${each.tblStrProduct.id eq product.id ? 'selected':''}>${product.name}</option>
                                                </c:forEach>
                                            </form:select>

                                        </td>
                                        <td>
                                            <span class="economyCode">${each.tblStrProduct.economicCode}</span>
                                        </td>
                                        <td>
                                            <form:input  type="number" onchange="calculateTotal(this)" value="${each.quantity}"
                                                         path="tblStrGrnDetails[${loop.index}].quantity"
                                                         class="form-control quantity" id="quantity${loop.index}"
                                                         attr-field="quantity" name="quantity"/>
                                        </td>
                                        <td>
                                            <form:input type="number"  value="${each.unitPrice}" onchange="calculateTotal(this)"
                                                        path="tblStrGrnDetails[${loop.index}].unitPrice"
                                                        class="form-control unitPrice" id="unitPrice${loop.index}"
                                                        attr-field="unitPrice" name="unitPrice"/>
                                        </td>
                                        <td>
                                            <form:input type="number" readonly="true" value="${each.unitPrice * each.quantity}"
                                                        path="tblStrGrnDetails[${loop.index}].total"
                                                        class="form-control form" id="total${loop.index}"
                                                        name="total"/>
                                            <form:hidden value="${each.id}" path="tblStrGrnDetails[${loop.index}].id" id="id${loop.index}" name="id"/>

                                        </td>


                                        <td>
                                            <button type="button" attr-id="${each.id}"
                                                    class="form-control btn btn-danger btn-sm remove-row">
                                                <span class="trn">Remove</span></button>
                                        </td>
                                        </tr>
                                        </c:forEach>

                                        </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer" style="height: 52px;">
                        <form:button class="btn btn-sm btn-success float-right" type="submit">
                            <i class="fa fa-dot-circle-o"></i><span class="trn">Save</span>
                        </form:button>
                    </div>
                </form:form>
                <div style="display: none">
                    <c:forEach var="each" items="${productList}">
                        <span id="sp${each.id}">${each.economicCode}</span>
                    </c:forEach>
                </div>

            </div>
        </div>
    </div>
</main>
<script type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/customJS/store/goodsReceipt.js"></script>
<script type="text/javascript" charset="utf8"
        src="${pageContext.servletContext.contextPath}/resources/customJS/common.js"></script>
<script type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/plugins/bootstrap-datepicker/bootstrap-datepicker.js"></script>
<script type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/plugins/reflection/reflection-com.js"></script>
<script>
    $(document).ready(function (f) {
        validateFormElements();
    });

    function validateFormElements() {
        $(".required").prop("required", true);
        $(".disabled").prop("disabled", true);
        $(".disable-this").prop("disabled", true);
        $(".dtp-time").prop("placeholder", "HH:MM");
        generateDatePicker();
        validateServiceType();
    }

    $(document).on("click", "#btn-submit", function () {
        validateRequiredFields();
    });

    function validateServiceType() {
        hideAll();
        disableAll();
        var st = $("#tblStrServiceRequisitionType").val();
        if (!st || st == "CAR") {
            $(".div-car").show();
            enableAll("div-car");
        } else if (st == "ROOM") {
            $(".div-room").show();
            enableAll("div-room");
        } else if (st == "MAINTENANCE") {
            $(".div-maintenance").show();
            enableAll("div-maintenance");
        } else if (st == "LOUNGE") {
            $(".div-lounge").show();
            enableAll("div-lounge");
        }
    }

    function disableAll() {
        $(".div-car, .div-room, .div-maintenance, .div-lounge").each(function () {
            $(this).find(".disable-this").prop("disabled", true);
        });
    }

    function enableAll(selector) {
        $("." + selector).each(function () {
            $(this).find(".disable-this").prop("disabled", false);
        });
    }

    $(document).on("change", "#tblStrServiceRequisitionType", function () {
        validateServiceType();
    });

</script>
<%--
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
<script type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/customJS/store/car.js"></script>
--%>



