<%@ include file="/pages/common.jsp" %>
<title>Medical Center</title>
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/resources/plugins/reflection/reflection-com.css">
<main class="main"> <!-- Breadcrumb-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="${pageContext.servletContext.contextPath}/index"><span class="trn">Home</span></a></li>
        <li class="breadcrumb-item"><a href="#"><span class="trn">Management</span></a></li>
        <li class="breadcrumb-item active"><span class="trn">Medical Center</span></li>
        <li class="float-right w-100">
            <div class="float-right" style="margin-top: -20px!important;">
                <a href="${pageContext.servletContext.contextPath}/medical-centers/form" class="btn btn-success btn-sm"><i class="fa fa-edit c-white"></i> Create</a>
            </div>
        </li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="card">
                <div class="card-header">
                    <h4>Medical Center List</h4>
                </div>

                <div class="card-body">
                    <table class="table table-striped table-bordered th-center th-colored">
                        <thead>
                        <tr>
                            <th>Sl. No</th>
                            <th>Name</th>
                            <th>Contact No</th>
                            <th>Address</th>
                            <th>Reg. Info</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:if test="${not empty medicalCenterList}">
                            <c:forEach items="${medicalCenterList}" var="each" varStatus="loop">
                                <tr>
                                    <td class="center">${loop.index+1}</td>
                                    <td class="center">${each.name}</td>
                                    <td class="left">${each.contactNo}</td>
                                    <td class="center">${each.address}</td>
                                    <td class="center">${each.regInfo}
                                    </td>
                                    <td class="center">
                                        <a href="${pageContext.servletContext.contextPath}/medical-centers/show/${each.id}" target="_blank" class="c-white btn btn-sm btn-info"><i class="fa fa-eye c-white"></i> Show</a>
                                        <a href="${pageContext.servletContext.contextPath}/medical-centers/edit/${each.id}" target="_blank" class="c-white btn btn-sm btn-warning"><i class="fa fa-edit c-white"></i> Edit</a>
                                    </td>
                                </tr>
                            </c:forEach>
                        </c:if>
                        <c:if test="${medicalCenterList == null}">
                            <tr>
                                <td class="bold center" colspan="7">No record Found...!</td>
                            </tr>
                        </c:if>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</main>
<script type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/plugins/reflection/reflection-com.js"></script>