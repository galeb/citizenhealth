<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<script	type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/customJS/common.js"></script>

<script>
startLoading();
</script>



<main class="main">
<ol class="breadcrumb">
	<li class="breadcrumb-item"><a href="${pageContext.servletContext.contextPath}/index"><span class="trn">Home</span></a></li>
	<li class="breadcrumb-item active"><span class="trn">User Group Component</span></li>
</ol>

<div class="container-fluid">
	<div class="animated fadeIn">
			<div class="row">
				<div class="col-sm-12">
					<div class="card">
						<div class="card-header">
							<strong><span class="trn">User Group Role</span></strong>
						</div>
						<form:form role="form" name="Roleform" action="${pageContext.servletContext.contextPath}/role/save" onsubmit="return false;" method="post" modelAttribute="modelRoleMenuBean" >
						<div class="card-body">
							<form:hidden path="tblRole.id" name="tblRole.id"  id="id"/>
							<div class="row">
								<div class="col-sm-8">
									<div class="form-group row">
										<label class="col-md-3 col-form-label" for="name"><span class="trn">Role</span></label>
										<div class="col-md-9">
											<form:input path="tblRole.roleName" class="form-control ng-untouched ng-pristine ng-invalid"
											title="Role Name" validarr="required@@" tovalid="true"  onblur="validateTextComponent(this)"
												formcontrolname="roleName" id="roleName" name="roleName"
												placeholder="Enter Role" type="text"/>
											<!---->
											<div class="ng-star-inserted">
												<!---->
											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-4"></div>
							</div>							
							
						

						<div class="row">
							<div class="form-group col-md-8">
								<div class="form-group row">
									<label class="col-md-3 col-form-label" for="name"><span
										class="trn">Choose Display Menu</span></label>
										<div class="col-md-9">
											
											<form:select path="lstResMenu" id="lstResMenu" class="js-example-basic-multiple" style="width:80%" multiple="true">
												
												<c:forEach items="${lstMenu}" var="entry">
													<form:option value="${entry.id}">${entry.name}</form:option>
												</c:forEach>
											</form:select>
											<button class="btn btn-sm btn-primary" type="button" id="checkbox"><i class="fa fa-check-square-o"></i>
											&nbsp;<span class="trn" id="selCheckReset">Select All</span></button>
										</div>
									</div>
								<div class="col-sm-4"></div>
							</div>
						</div>
						
						</div>

						<div class="card-footer" style="height: 52px;">
							<form:button class="btn btn-sm btn-success float-right" type="button" onclick="fnSaveRole()"
								disabled="">
								<i class="fa fa-dot-circle-o"></i><span class="trn"> Save</span>
							</form:button>
						</div>
						</form:form>
						<div class="card-body">
							<div class="row"></div>
							<div class="row">
								<div class="col-sm-12 table-responsive" id="divDatatable">
									<table aria-describedby="DataTables_Table_0_info"
										class="table table-striped table-bordered datatable dataTable no-footer "
										id="myTable" role="grid"
										style="border-collapse: collapse !important">
										<thead id="theadrole"></thead>								
										<tbody id="tbodyrole"></tbody>
									</table>
								</div>
							</div>							
						</div>
					</div>
				</div>
			</div>
	</div>
</div>
</main>

 <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

<script	type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/customJS/mastertable/role.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script>
$(document).ready(function() {
    $('.js-example-basic-multiple').select2();
});
$("#checkbox").click(function(){	
	if($("#selCheckReset").html() == 'Select All'){
		$("#lstResMenu > option").prop("selected","selected");
        $("#lstResMenu").trigger("change");
	}
});
var leadData = '${lstRole}';
prepareGrid(leadData);
</script>