<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>


<main class="main">
<ol class="breadcrumb">
	<li class="breadcrumb-item trn">Home</li>
	<!-- <li class="breadcrumb-item">
            <a href="#">Admin</a>
          </li> -->
	<li class="trn breadcrumb-item active">Dashboard</li>
	
</ol>


<div class="container-fluid">
	<div class="animated fadeIn">
		<div class="card">
			<div class="card-body">
				<%--<div class="row">
					<div class="col-sm-5"></div>
					<div class="col-sm-7 d-none d-md-block"></div>



				</div>
				--%>
					<div class="row">

					<div class="col-md-12">
						<c:forEach items="${bartaList}" var="each">
							<div class="panel">
								<div class="panel-body">
									<div class="row">
										<c:choose>
											<c:when test="${each.imagePath ne null}">
												<div class="col-md-6">
													<img width="100%" height="100%"
														 src="${pageContext.servletContext.contextPath}/resources/login/assets/img/${each.imagePath}">

												</div>
												<div class="col-md-6">
													<div class="">
														<h3>${each.title}</h3>
															<%--<h5>New research may explain why evolution made humans 'fat'</h5>--%>
														<span>${each.barta}</span>
													</div>
												</div>
											</c:when>
											<c:otherwise>
												<div class="col-md-12">
													<h3>${each.title}</h3>
														<%--<h5>New research may explain why evolution made humans 'fat'</h5>--%>
													<span>${each.barta}</span>
												</div>
											</c:otherwise>

										</c:choose>


											<%--<div class="col-md-6">
                                                <img width="100%" height="40%"
                                                     src="${pageContext.servletContext.contextPath}/resources/login/assets/img/citihealth_image.jpg">

                                            </div>
                                            <div class="col-md-6">
                                                <div class="">
                                                    <h3>Shastho Barta</h3>
                                                    <h5>New research may explain why evolution made humans 'fat'</h5>
                                                    <br>
                                                    <p>
                                                        Scientists have compared fat samples from humans and other
                                                        primates and found that changes in DNA packaging affected how
                                                        the human body processes fat.
                                                    </p>
                                                </div>
                                            </div>--%>
									</div>
									<br>
								</div>
							</div>
						</c:forEach>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>

</main>
