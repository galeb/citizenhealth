<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib prefix="f" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="t" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<title>Patient Detail Report</title>
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/resources/plugins/reflection/reflection-com.css">
<main class="main"> <!-- Breadcrumb-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="${pageContext.servletContext.contextPath}/index"><span class="trn">Home</span></a></li>
        <li class="breadcrumb-item"><a href="#"><span class="trn">Health Info</span></a></li>
        <li class="breadcrumb-item active"><span class="trn">Doctor Detail Reports</span></li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="card">
                <div class="card-header"><strong><span class="trn">Reports</span></strong></div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-xs-3 col-sm-4 col-md-4 col-lg-4">
                            <div class="form-group row">
                                <label class="col-md-5">Registration No</label>
                                <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
                                    <input id="pid" type="text" class="form-control" placeholder="Enter Reg. No."/>
                                    <input id="pkid" type="hidden"/>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                            <div class="form-group row">
                                <label class="col-md-3">Name</label>
                                <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
                                    <label id="name" class="form-control"></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                            <div class="form-group row">
                                <label class="col-md-3">Father's Name</label>
                                <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
                                    <label id="fatherName" class="form-control"></label>

                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="row">
                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                            <div class="form-group row">
                                <label class="col-md-4">Mother's Name</label>
                                <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                    <label id="motherName" class="form-control"></label>

                                </div>
                            </div>
                        </div>
                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                            <div class="form-group row">
                                <label class="col-md-5">From Date</label>
                                <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
                                    <input id="fromDate" type="text" class="form-control datepicker"/>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                            <div class="form-group row">
                                <label class="col-md-5">To Date </label>
                                <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
                                    <input id="toDate" type="text" class="form-control datepicker" />
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                            <button id="searchBtn" class="btn btn-success btn-sm" disabled>Search</button>
                        </div>
                    </div>

                    <div class="row"  id="reportContent">

                    </div>


                </div>

            </div>
        </div>
    </div>
</main>
<script type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/plugins/reflection/reflection-com.js"></script>
<script type="text/javascript">
    $("#pid").on("blur",  function () {
        var pid = $("#pid").val();
        if(pid){
            $('#pkid').val("");
            $('#name').text("");
            $('#fatherName').text("");
            $('#motherName').text("");
            $.get({
                url : path + '/doctor-info/fetchByRegno',
                data : "regno="+pid,
                success : function(response) {
                    if(response == 'emptyCode'){
                        showError('<b>ERROR!</b><br> Please Enter Reg. No!');
                    }else if(response == 'notFound'){
                        showError('<b>ERROR!</b><br> No Record found for the Provided Registration No.');
                    }else{
                        var json = JSON.parse(response);
                        console.log(response);
                        $('#pkid').val(json['id']);
                        $('#name').text(json['personalInfo']['name']);
                        $('#fatherName').text(json['personalInfo']['fatherName']);
                        $('#motherName').text(json['personalInfo']['motherName']);
                        $("#searchBtn").attr("disabled", false);
                    }
                }
            });
        }else{
            alert("Please, provide registration no.");
            $("#searchBtn").attr("disabled", true);
        }
    });

    $(document).on("click", "#searchBtn", function () {
        var fromDate = $("#fromDate").val();
        var toDate = $("#toDate").val();
        var pkid =  $('#pkid').val();
        $("#reportContent").empty();
        if(pkid && fromDate && toDate){
            // alert(fromDate+":"+toDate+":"+reportType);
            $.get({
                type: "GET",
                url: path + '/reports/find-treatment-report-on-type',
                dataType: "JSON",
                data: {
                    fromDate: fromDate,
                    toDate: toDate,
                    pkid: pkid,
                    reportType: "doctor"
                },
                success: function (r) {
                    console.log(r);
                    $("#reportContent").append(makeGeneralReportContent(r));

                },
                error: function (e) {
                    alert("Sorry, failed to take action due to : " + JSON.stringify(e));
                }
            });
        }else{
            alert("Please, select date!")
        }

    });

    function makeGeneralReportContent(r){
        var treatmentBlock =  "";
        $.each(r, function (i) {
            var date = new Date(r[i].treatmentDate);
            treatmentBlock += "<div class='col-sm-12'>" +
                    "<div class='card'>" +
                    "<div class='card-header'>" +
                    "<strong>Date: "+ date.getDate() + "-" + date.getMonth() + "-" + date.getFullYear() +" </strong>" +
                    "</div>" +
                    "<div class='card-body'>" +
                    "<div class='col-sm-12'>" +
                    "<div class='card'>" +
                    "<div class='card-header'>" +
                    "<strong>Treatment Details</strong>" +
                    "</div>" +"<div class='card-body'>" +
                    "<div class='row'>"+
                    "<table class='table table-bordered'>" +
                    "<thead>" +
                    "<tr>" +
                    "<th>Treatment Date</th>" +
                    "<th>Medical Center</th>" +
                    "<th>Patient Details</th>" +
                    "<th>Symptom</th>" +
                    "<th>Disease</th>" +
                    "</tr>" +
                    "</thead>" +
                    "<tbody>";


            treatmentBlock += "<tr> " +
                    "<td>" + date.getDate() + "-" + date.getMonth() + "-" + date.getFullYear() + "</td>" +
                    "<td>" + r[i].medicalCenterInfo.name + "</td>" +
                    "<td>" +
                        "Name : "+r[i].patientInfo.name +
                        "<br>PID : "+r[i].patientInfo.pID +
                    "</td>" +
                    "<td>" + r[i].symptom + "</td>" +
                    "<td>" + r[i].disease + "</td>" +
                    "</tr>";

               // treatmentBlock += "<tr colspan='5'>";
            var medicineBlock =  "<tr colspan='5'><table class='table table-bordered'>" +
                    "<tr>" +
                    "<th>Medicine Name</th>" +
                    "<th>Doses</th>" +
                    "<th>Duration</th>" +
                    "</tr>";
            $.each(r[i].treatmentDtls, function (j) {
                medicineBlock += "<tr> " +
                        "<td>"+r[i].treatmentDtls[j].medicineInfo.name+"</td>" +
                        "<td>"+r[i].treatmentDtls[j].doses+"</td>" +
                        "<td>"+r[i].treatmentDtls[j].duration+"</td>" +
                        "</tr>";

            });
            medicineBlock +=  "</table></tr>";


            //treatmentBlock += "<tr colspan='5'>";
            var diagoBlock =  "<tr colspan='5'><table class='table table-bordered'>" +
                    "<tr>" +
                    "<th>Diagnosis Name</th>" +
                    "<th>Diagnosis Details</th>" +
                    "<th>Result</th>" +
                    "<th>Remarks</th>" +
                    "</tr>" ;
            $.each(r[i].diagoDtls, function (j) {
                diagoBlock += "<tr> " +
                        "<td>"+r[i].diagoDtls[j].diagonosis.name+"</td>" +
                        "<td>"+r[i].diagoDtls[j].testDetails+"</td>" +
                        "<td>"+r[i].diagoDtls[j].result+"</td>" +
                        "<td>"+r[i].diagoDtls[j].remarks+"</td>" +
                        "</tr>";

            });
            diagoBlock += " " +"</table></tr>";
            treatmentBlock += medicineBlock + diagoBlock;
            treatmentBlock += " </tbody></table>"+"</div>" +"</div>" +
                    "</div>" +
                    "</div>" +
                    "</div>" +
                    "</div>" +
                    "</div>";
            "</div> <hr style='border: 10px solid green;'/>";

        });

        console.log(treatmentBlock);
        return treatmentBlock;
    }

</script>

