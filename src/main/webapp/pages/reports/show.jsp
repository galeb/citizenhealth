<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib prefix="f" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="t" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<title>General Report</title>
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/resources/plugins/reflection/reflection-com.css">
<main class="main"> <!-- Breadcrumb-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="${pageContext.servletContext.contextPath}/index"><span class="trn">Home</span></a></li>
        <li class="breadcrumb-item"><a href="#"><span class="trn">Health Info</span></a></li>
        <li class="breadcrumb-item active"><span class="trn">Reports</span></li>
        <%--<li class="float-right w-100">
            <div class="float-right" style="margin-top: -20px!important;">
                <a href="${pageContext.servletContext.contextPath}/strGoodsReceipt/index" class="btn btn-info btn-sm"><i class="fa fa-list c-white"></i> Index</a>
                <a href="${pageContext.servletContext.contextPath}/strGoodsReceipt/create" class="btn btn-success btn-sm"><i class="fa fa-edit c-white"></i> Create</a>
            </div>
        </li>--%>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="card">
                <div class="card-header"><strong><span class="trn">Reports</span></strong></div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                            <div class="form-group row">
                                <label class="col-md-5">From Date</label>
                                <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
                                <input id="fromDate" type="text" class="form-control datepicker"/>
                                    </div>
                            </div>
                        </div>

                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                            <div class="form-group row">
                                <label class="col-md-5">To Date </label>
                                <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
                                <input id="toDate" type="text" class="form-control datepicker" />
                                    </div>
                            </div>
                        </div>
                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                            <div class="form-group row">
                                <label class="col-md-4">Report Type</label>
                                <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                    <select class="form-control" id="reportType">
                                        <option value="general">General</option>
                                        <option value="division_wise_patient">Division Wise Patient</option>
                                        <option value="district_wise_patient">District Wise Patient</option>
                                    </select>

                                </div>
                            </div>
                        </div>
                        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                            <button id="searchBtn" class="btn btn-success btn-sm">Search</button>
                        </div>
                    </div>

                    <div class="row"  id="reportContent">

                    </div>


                </div>

            </div>
        </div>
    </div>
</main>
<script type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/plugins/reflection/reflection-com.js"></script>
<script type="text/javascript">
    $(document).on("click", "#btn-submit", function () {
        takeAction("SUBMIT");
    });
    $(document).on("click", "#btn-approve", function () {
        takeAction("APPROVE");
    });
    $(document).on("click", "#btn-reject", function () {
        takeAction("REJECT");
    });
    $(document).on("click", "#searchBtn", function () {
        var fromDate = $("#fromDate").val();
        var toDate = $("#toDate").val();
        var reportType = $("#reportType").val();
        $("#reportContent").empty();
        if(fromDate && toDate && reportType){
           // alert(fromDate+":"+toDate+":"+reportType);
            $.get({
                type: "GET",
                url: path + '/reports/find-by-value',
                dataType: "JSON",
                data: {
                    fromDate: fromDate,
                    toDate: toDate,
                    reportType: reportType
                },
                success: function (r) {
                    if (reportType === "general") {
                        $("#reportContent").append(makeGeneralReportContent(r));
                    }else if (reportType === "division_wise_patient") {
                        $("#reportContent").append(makeDivisionWiseReportContent(r,"Division"));
                    }else if (reportType === "district_wise_patient") {
                        $("#reportContent").append(makeDivisionWiseReportContent(r,"District"));
                    }else if (reportType === "thana_wise_patient") {
                        $("#reportContent").append(makeDivisionWiseReportContent(r,"Thana"));
                    }
                },
                error: function (e) {
                    alert("Sorry, failed to take action due to : " + JSON.stringify(e));
                }
            });
        }else{
            alert("Please, select date!")
        }

    });

    function makeGeneralReportContent(r){
        var content = "<div class='col-sm-12'>" +
                        "<div class='card'>" +
                            "<div class='card-header'>" +
                            "<strong>Report Details</strong>" +
                            "</div>" +
                                "<div class='card-body'>" +
                                    "<div class='row'>" +
                                        "<label class='col-md-4 col-lg-4 text-right'><strong>Total Patient :</strong></label> "+
                                        "<label class='col-md-2 col-lg-2'>"+ eval(r.totalPatient ? r.totalPatient:0) +"</label> "+
                                        "<label class='col-md-4 col-lg-4 text-right'><strong>Total Male Patient :</strong></label> "+
                                        "<label class='col-md-2 col-lg-2'>"+ eval(r.totalMalePatient ? r.totalMalePatient : 0)+"</label> "+
                                        "<label class='col-md-4 col-lg-4 text-right'><strong>Total Female Patient : </strong></label> "+
                                        "<label class='col-md-2 col-lg-2'>"+ eval(r.totalFemalePatient ? r.totalFemalePatient :0)+"</label> "+

                                        "<label class='col-md-4 col-lg-4 text-right'><strong>Total Doctor : </strong></label> "+
                                        "<label class='col-md-2 col-lg-2'> "+ eval(r.totalDoctor ? r.totalDoctor:0)+"</label> "+
                                        "<label class='col-md-4 col-lg-4 text-right'><strong>Total Male Doctor : </strong></label> "+
                                        "<label class='col-md-2 col-lg-2'>"+ eval(r.totalMaleDoctor?r.totalMaleDoctor:0)+"</label> "+
                                        "<label class='col-md-4 col-lg-4 text-right'><strong>Total Female Doctor :</strong></label> "+
                                        "<label class='col-md-2 col-lg-2'>"+ eval(r.totalFemaleDoctor? r.totalFemaleDoctor:0)+"</label> "+
                                        "<label class='col-md-4 col-lg-4 text-right'><strong>Average Patient per Doctor :</strong></label> "+
                                        "<label class='col-md-2 col-lg-2'>"+ eval(r.totalPatient/r.totalDoctor)+"</label> "+
                                    "</div>" +
                                "</div>" +
                            "</div>" +
                        "</div>";
        return content;
    }
    function makeDivisionWiseReportContent(r,columnName){
        var trs = "";
        for(var i=0;i< r.objectList.length;i++){
            trs+="<tr><td>"+r.objectList[i].propertyName+"</td><td>"+r.objectList[i].propertyValue+"</td></tr>";
        };

        var content = "<div class='col-sm-12'>" +
                        "<div class='card'>" +
                            "<div class='card-header'>" +
                            "<strong>Report Details</strong>" +
                            "</div>" +
                                "<div class='card-body'>" +
                                    "<div class='col-sm-12'><div class='col-sm-12'>" +
                                        "<table class='table table-striped table-bordered'>" +
                                        "<thead><tr>" +
                                        "<th>"+columnName+" Name</th>" +
                                        "<th>Total Patient</th>" +
                                        "</tr></thead>"
                                        +"<tbody>"+trs+"</tbody>"+
                                        "</table>" +
                                "</div>" +
                                "</div>" +

                                "</div>" +
                            "</div>" +
                        "</div>";
        return content;
    }
    $(document).on("change", "#tblStrDriverInfo", function () {
        var id = $(this).val();
        if (id) {
            startLoading();
            $.get({
                type: "GET",
                url: path + '/driver/getDriverDetails',
                dataType: "JSON",
                data: {
                    id: id
                },
                success: function (r) {
                    if (r.status == 'OK') {
                        $("#driverName").val(r.driverName);
                        $("#mobile").val(r.mobile);
                        $("#tblStrCarInfoId").val(r.tblStrCarInfoId);
                        $("#carModel").val(r.carModel);
                        $("#carNo").val(r.carNo);
                    }
                },
                error: function (e) {
                    alert("Sorry, failed to get driver details due to :" + e);
                }
            });
            hideLoading();
        }
        else{
            $("#driverName").val("");
            $("#mobile").val("");
            $("#tblStrCarInfoId").val("");
            $("#carModel").val("");
            $("#carNo").val("");
        }
    });

    function takeAction(t) {
        clearErrors();
        var id = $("#id").val();
        if (validateRequiredFields() && id && confirm("Are you sure, you want to proceed with action '" + t + "'...?")) {
            startLoading();
            $.get({
                type: "GET",
                url: path + '/strServiceRequisition/takeAction',
                dataType: "JSON",
                data: {
                    t: t,
                    i: $("#id").val(),
                    d: $("#tblStrDriverInfo").val() ? $("#tblStrDriverInfo").val() : '',
                    r: $("#remarks").val()?$("#remarks").val():''
                },
                success: function (r) {
                    if (r.status == 'OK') {
                        $(".btn").prop("disabled", true);
                        alert("Action taken successfully...");
                        location.reload();
                    }
                },
                error: function (e) {
                    alert("Sorry, failed to take action due to : " + JSON.stringify(e));
                }
            });
            hideLoading();
        }
    }
</script>
