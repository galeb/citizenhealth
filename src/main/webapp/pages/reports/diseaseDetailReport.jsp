<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib prefix="f" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="t" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<title>Disease Detail Report</title>
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/resources/plugins/reflection/reflection-com.css">
<main class="main"> <!-- Breadcrumb-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="${pageContext.servletContext.contextPath}/index"><span class="trn">Home</span></a></li>
        <li class="breadcrumb-item"><a href="#"><span class="trn">Health Info</span></a></li>
        <li class="breadcrumb-item active"><span class="trn">Disease Detail Reports</span></li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="card">
                <div class="card-header"><strong><span class="trn">Reports</span></strong></div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-xs-3 col-sm-4 col-md-4 col-lg-4">
                            <div class="form-group row">
                                <label class="col-md-5">Disease</label>
                                <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
                                   <select class="form-control" id="disease">
                                       <c:forEach items="${diseaseList}" var="each">
                                           <option value="${each}">${each}</option>
                                       </c:forEach>
                                   </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                            <div class="form-group row">
                                <label class="col-md-5">From Date</label>
                                <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
                                    <input id="fromDate" type="text" class="form-control datepicker"/>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                            <div class="form-group row">
                                <label class="col-md-5">To Date </label>
                                <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
                                    <input id="toDate" type="text" class="form-control datepicker" />
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                            <button id="searchBtn" class="btn btn-success btn-sm">Search</button>
                        </div>
                    </div>

                    <div class="row"  id="reportContent">

                    </div>


                </div>

            </div>
        </div>
    </div>
</main>
<script type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/plugins/reflection/reflection-com.js"></script>
<script type="text/javascript">

    $(document).on("click", "#searchBtn", function () {
        var fromDate = $("#fromDate").val();
        var toDate = $("#toDate").val();
        var disease =  $('#disease').val();
        $("#reportContent").empty();
        if(disease && fromDate && toDate){
            // alert(fromDate+":"+toDate+":"+reportType);
            $.get({
                type: "GET",
                url: path + '/reports/find-treatment-by-disease',
                dataType: "JSON",
                data: {
                    fromDate: fromDate,
                    toDate: toDate,
                    disease: disease
                },
                success: function (r) {
                    console.log(r);
                    $("#reportContent").append(makeGeneralReportContent(r));

                },
                error: function (e) {
                    alert("Sorry, failed to take action due to : " + JSON.stringify(e));
                }
            });
        }else{
            alert("Please, select date!")
        }

    });

    function makeGeneralReportContent(r){
        var treatmentBlock =  "";
        treatmentBlock += "<div class='col-sm-12'>" +
                "<div class='card'>" +
                "<div class='card-header'>" +
                "<strong>Total patient : " + r.length+" </strong>" +
                "</div>" +
                "<div class='card-body'>" +
                "<div class='col-sm-12'>" +
                "<div class='card'>" +
                "<div class='card-header'>" +
                "<strong>Treatment Details</strong>" +
                "</div>" +"<div class='card-body'>" +
                "<div class='row'>"+
                "<table class='table table-bordered'>" +
                "<thead>" +
                "<tr>" +
                "<th>Treatment Date</th>" +
                "<th>Medical Center</th>" +
                "<th>Patient Details</th>" +
                "<th>Symptom</th>" +
                "</tr>" +
                "</thead>" +
                "<tbody>";
        $.each(r, function (i) {
            var date = new Date(r[i].treatmentDate);


            var medicalCenterName = r[i].medicalCenterInfo?r[i].medicalCenterInfo.name : "";
            var patientName = r[i].patientInfo?r[i].patientInfo.name : "";
            var pid = r[i].patientInfo?r[i].patientInfo.pID : "";
            var symptom = r[i].symptom? r[i].symptom : "";
            var disease = r[i].disease? r[i].disease : "";
            treatmentBlock += "<tr> " +
                    "<td>" + date.getDate() + "-" + date.getMonth() + "-" + date.getFullYear() + "</td>" +
                    "<td>" + medicalCenterName + "</td>" +
                    "<td>" +
                        "Name : "+patientName +
                        "<br>PID : "+pid +
                    "</td>" +
                    "<td>" + symptom + "</td>" +
                    "</tr>";

               // treatmentBlock += "<tr colspan='5'>";
           /* var medicineBlock =  "<tr colspan='5'><table class='table table-bordered'>" +
                    "<tr>" +
                    "<th>Medicine Name</th>" +
                    "<th>Doses</th>" +
                    "<th>Duration</th>" +
                    "</tr>";*/
            /*$.each(r[i].treatmentDtls, function (j) {
                medicineBlock += "<tr> " +
                        "<td>"+r[i].treatmentDtls[j].medicineInfo.name+"</td>" +
                        "<td>"+r[i].treatmentDtls[j].doses+"</td>" +
                        "<td>"+r[i].treatmentDtls[j].duration+"</td>" +
                        "</tr>";

            });*/
           // medicineBlock +=  "</table></tr>";


            //treatmentBlock += "<tr colspan='5'>";
           /* var diagoBlock =  "<tr colspan='5'><table class='table table-bordered'>" +
                    "<tr>" +
                    "<th>Diagnosis Name</th>" +
                    "<th>Diagnosis Details</th>" +
                    "<th>Result</th>" +
                    "<th>Remarks</th>" +
                    "</tr>" ;*/
            /*$.each(r[i].diagoDtls, function (j) {
                diagoBlock += "<tr> " +
                        "<td>"+r[i].diagoDtls[j].diagonosis.name+"</td>" +
                        "<td>"+r[i].diagoDtls[j].testDetails+"</td>" +
                        "<td>"+r[i].diagoDtls[j].result+"</td>" +
                        "<td>"+r[i].diagoDtls[j].remarks+"</td>" +
                        "</tr>";

            });
            diagoBlock += " " +"</table></tr>";*/
            //treatmentBlock += medicineBlock + diagoBlock;


        });
        treatmentBlock += " </tbody></table>"+"</div>" +"</div>" +
                "</div>" +
                "</div>" +
                "</div>" +
                "</div>" +
                "</div>";
        "</div> <hr style='border: 10px solid green;'/>";
        console.log(treatmentBlock);
        return treatmentBlock;
    }

</script>

