

<header class="app-header navbar">
	<button class="navbar-toggler sidebar-toggler d-lg-none mr-auto"
		type="button" data-toggle="sidebar-show">
		<span class="navbar-toggler-icon"></span>
	</button>
	<a class="navbar-brand mrg-10" href="${pageContext.servletContext.contextPath}/index" style="margin: -10px">
		Health Info System
		<%--<img
		class="navbar-brand-full"
		src="${pageContext.servletContext.contextPath}/resources/img/brand/logoPims.svg"
		width="89" height="25" alt="CoreUI Logo">
		<img
		class="navbar-brand-minimized"
		src="${pageContext.servletContext.contextPath}/resources/img/brand/sygnet.svg"
		width="30" height="30" alt="CoreUI Logo">--%>
	</a>
	<button class="navbar-toggler sidebar-toggler d-md-down-none"
		type="button" data-toggle="sidebar-lg-show">
		<span class="navbar-toggler-icon"></span>
	</button>

	<ul class="nav navbar-nav ml-auto">
		
		<li class="nav-item dropdown" dropdown placement="bottom right">
			<a aria-haspopup="true" class="nav-link" data-toggle="dropdown"
			role="button" aria-expanded="false"> <img class="img-avatar"
				src="${pageContext.servletContext.contextPath}/resources/img/avatars/6.jpg"
				alt="admin@bootstrapmaster.com"  onclick="addClass()">
		</a>
			<%--<sec:authentication property="principal.username" />--%>
			<div id="myDiv" class="dropdown-menu dropdown-menu-right">
				<div class="dropdown-header text-center">
					<strong><span class="trn">Language</span></strong>
				</div>

				<a class="dropdown-item" href="#" onclick="onClickChange('bg')"> <i class="fa fa-flag"></i>
					Bangladesh<span class="badge badge-info"></span>
				</a>
				<a class="dropdown-item" href="#" onclick="onClickChange('en')"> <i class="fa fa-flag"></i>
					English<span class="badge badge-info"></span>
				</a>
				<div class="dropdown-divider"></div>
				<a class="dropdown-item" href="${pageContext.servletContext.contextPath}/logout"> <i class="fa fa-lock"></i>
					<span class="trn">Logout</span>
				</a>
			</div>
		</li>

	</ul>

</header>
<script>	
	function addClass() {
		var element = document.getElementById("myDiv");
		if(element.classList.contains("show"))
			element.classList.remove("show");
		else
			element.classList.add("show");
	}
	
</script>
