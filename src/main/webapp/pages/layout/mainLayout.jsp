<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
	<meta name="description" content="CoreUI - Open Source Bootstrap Admin Template">
	<meta name="author" content="Łukasz Holeczek">
	<meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,jQuery,CSS,HTML,RWD,Dashboard">
	<link rel="icon" type="image/png" href="${pageContext.servletContext.contextPath}/resources/img/brand/logotitle.png">
	<title class="trn">Health Information System</title>

	<!-- Icons-->
	<link href="${pageContext.servletContext.contextPath}/resources/css/coreui-icons.min.css" rel="stylesheet">
	<link href="${pageContext.servletContext.contextPath}/resources/css/flag-icon.min.css" rel="stylesheet">
	<link href="${pageContext.servletContext.contextPath}/resources/css/font-awesome.min.css" rel="stylesheet">
	<link href="${pageContext.servletContext.contextPath}/resources/css/simple-line-icons.css" rel="stylesheet">
	<!-- Main styles for this application-->
	<link href="${pageContext.servletContext.contextPath}/resources/css/style.css" rel="stylesheet">
	<link href="${pageContext.servletContext.contextPath}/resources/vendors/pace-progress/css/pace.min.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/resources/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/resources/localcss/jquery.dataTables.css">
	<link rel="stylesheet" href="${pageContext.servletContext.contextPath}/resources/localcss/jquery-ui.css" />
	<link rel="stylesheet" href="${pageContext.servletContext.contextPath}/resources/customCss/custom.css" />
	<link href="${pageContext.servletContext.contextPath}/resources/localcss/buttons.dataTables.min.css" rel="stylesheet">
	<script src="${pageContext.servletContext.contextPath}/resources/boot/jquery.min.js"></script>
	<script src="${pageContext.servletContext.contextPath}/resources/translate/jquery.translate.js"></script>
	<script>
        var path='${pageContext.servletContext.contextPath}';
        var PIMS_Upload ='/home/PIMSUploads/';
        var userRights = '${LSTRIGHT}';
        var userRoleNames = '${USERROLENAMES}';
	</script>
	<script type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/customJS/common.js"></script>
	<script type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/customJS/changeReflect.js"></script>
	<script type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/customJS/acr/jquery.dataTables.js"></script>
	<script type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/localjs/dataTables.fixedHeader.min.js"></script>
	<script type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/customJS/acr/dataTables.buttons.min.js"></script>
	<script type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/customJS/acr/buttons.flash.min.js"></script>
	<script type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/customJS/acr/jszip.min.js"></script>
	<script type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/customJS/acr/pdfmake.min.js"></script>
	<script type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/customJS/acr/vfs_fonts.js"></script>
	<script type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/customJS/acr/buttons.html5.min.js"></script>
	<script type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/customJS/acr/buttons.print.min.js"></script>
	<style>
		body, html {
			height: 100%;
			width: 100%;
			min-height: 100%;
			padding: 0;
			margin: 0;
		}

		mrg-10 {
			margin: -10px !important;
		}

		.error {
			width: auto;
			height: 20px;
			height: auto;
			position: absolute;
			left: 50%;
			margin-left: -100px;
			bottom: 10px;
			background-color: #e63e3cf0;
			color: #F0F0F0;
			font-family: Calibri;
			font-size: 20px;
			padding: 10px;
			text-align: center;
			border-radius: 7px;
			-webkit-box-shadow: 0px 0px 24px -1px rgba(56, 56, 56, 1);
			-moz-box-shadow: 0px 0px 24px -1px rgba(56, 56, 56, 1);
			box-shadow: 0px 0px 24px -1px rgba(56, 56, 56, 1);
		}

		.success {
			width: auto;
			height: 20px;
			height: auto;
			position: absolute;
			border-radius: 7px;
			left: 50%;
			margin-left: -100px;
			bottom: 10px;
			background-color: #0c7230e0;
			color: #F0F0F0;
			font-family: Calibri;
			font-size: 20px;
			padding: 10px;
			text-align: center;
			-webkit-box-shadow: 0px 0px 24px -1px rgba(56, 56, 56, 1);
			-moz-box-shadow: 0px 0px 24px -1px rgba(56, 56, 56, 1);
			box-shadow: 0px 0px 24px -1px rgba(56, 56, 56, 1);
		}

		.validationMsg {
			color: red;
		}

		.div-border {
			font-size: 15px;
			border-bottom: 1px solid gray;
		}

		.btn-danger-no {
			color: #fff;
			background-color: #f86c6b;
			border-color: #f86c6b
		}

		#loading {
			width: 100%;
			height: 100%;
			top: 0;
			left: 0;
			position: fixed;
			display: block;
			opacity: 0.7;
			background-color: #fff;
			z-index: 99;
			text-align: center;
		}

		#loading-image {
			position: absolute;
			top: 200px;
			z-index: 100;
		}

		/*.ui-widget-overlay {
            background-color: rgba(0, 0, 0, .4);
            opacity: 1;
        }*/

		.ui-widget-overlay {
			background: #aaaaaa !important;
			opacity: 0.85 !important;
			/*  filter: Alpha(Opacity=.3);*/
		}

		.ui-dialog-mask {
			position: fixed;
			width: 100%;
			height: 100%;
		}

		tr td {
			white-space: nowrap;
		}

		tr th {
			white-space: nowrap;
		}
		/*.main{
            margin-left: 260px !important;
        }*/
		html:not([dir="rtl"]) .sidebar {
			margin-left: -260px;
		}


		li.open .nav-item .nav-icon{
			color: #000;
		}
		li.open .nav-item .nav-icon:hover{
			color: #000;
		}
		li.open .nav-item.child .nav-link{
			color: #000 !important;
			background: #f2f2f2 !important;
		}
		li.open .nav-item.child a:hover{
			color: #000 !important;
			background: #a6c8ec !important;
		}
		.nav-link:hover{
			color: #000 !important;
			background: #a6c8ec !important;
		}
		.nav-icon:hover{
			color: #000;
		}
		li.open .nav-item.child .nav-link.active {
			color: #000 !important;
			background: #d7d7d7 !important;
		}
		.sidebar .nav-link.active .nav-icon {
			color: #000 !important;
		}
	</style>
	<script>
        window.dataLayer = window.dataLayer || [];
        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());
        // Shared ID
        gtag('config', 'UA-118965717-3');
        // Bootstrap ID
        gtag('config', 'UA-118965717-5');
        function dateChange(ev) {
            if(checkValue($(this).attr('validarr'))!='-'){
                validateTextComponent(this);
            }
        }
        $(function () {
            $(".datepicker").datepicker({
                dateFormat: 'dd/mm/yy'
            }).change(dateChange).on('changeDate', dateChange);

            $("#dialog").dialog({
                modal: true,
                autoOpen: false,
                title: "Confirmation",
                width: 400,
                height: 170,
                hideCloseButton: false,
                show: {
                    effect: "fade",
                    duration: 300
                },
                hide: {
                    effect: "fade"
                },
                open: function(event, ui) {
                    $('body').addClass('ui-widget-overlay ui-dialog-mask');
                    return true;
                },
                close: function(event, ui) {
                    $('body').removeClass('ui-widget-overlay ui-dialog-mask');
                    return true;
                }
            });


            $("#ValidatePimsdialog").dialog({
                modal: true,
                autoOpen: false,
                title: "Confirmation",
                width: 400,
                height: 170,
                hideCloseButton: false,
                show: {
                    effect: "fade",
                    duration: 300
                },
                hide: {
                    effect: "fade"
                },
                open: function(event, ui) {
                    $('body').addClass('ui-widget-overlay ui-dialog-mask');
                    return true;
                },
                close: function(event, ui) {
                    $('body').removeClass('ui-widget-overlay ui-dialog-mask');
                    return true;
                }
            });


            $("#UpdateDialog").dialog({
                modal: true,
                autoOpen: false,
                title: "Confirmation",
                width: 400,
                height: 170,
                hideCloseButton: false,
                show: {
                    effect: "fade",
                    duration: 300
                },
                hide: {
                    effect: "fade"
                },
                open: function(event, ui) {
                    $('body').addClass('ui-widget-overlay ui-dialog-mask');
                    return true;
                },
                close: function(event, ui) {
                    $('body').removeClass('ui-widget-overlay ui-dialog-mask');
                    return true;
                }
            });



            $("#docdialog").dialog({
                modal: true,
                autoOpen: false,
                title: "Documents",
                width: 900,
                height: 500,
                show: {
                    effect: "fade",
                    duration: 300
                },
                hide: {
                    effect: "fade"
                },
                open: function(event, ui) {
                    $('body').addClass('ui-widget-overlay ui-dialog-mask');
                    return true;
                },
                close: function(event, ui) {
                    $('body').removeClass('ui-widget-overlay ui-dialog-mask');
                    return true;
                }
            });
            $("#validatedialog").dialog({
                modal: true,
                autoOpen: false,
                title: "Validate ACR",
                width: 400,
                height: 170,
                closeOnEscape: false,
                show: {
                    effect: "fade",
                    duration: 300
                },
                hide: {
                    effect: "fade"
                },
                open: function(event, ui) {
                    $('body').addClass('ui-widget-overlay ui-dialog-mask');
                    return true;
                },
                close: function(event, ui) {
                    $('body').removeClass('ui-widget-overlay ui-dialog-mask');
                    return true;
                }
            });
            $("#rejectPopup").dialog({
                modal: true,
                autoOpen: false,
                title: "Reject Records",
                width: 400,
                height: 280,
                show: {
                    effect: "fade",
                    duration: 300
                },
                hide: {
                    effect: "fade"
                },
                open: function(event, ui) {
                    $('body').addClass('ui-widget-overlay ui-dialog-mask');
                    return true;
                },
                close: function(event, ui) {
                    $('body').removeClass('ui-widget-overlay ui-dialog-mask');
                    return true;
                }
            });

            $("#GOdialog").dialog({
                modal: true,
                autoOpen: false,
                title: "Generate GO",
                width: 400,
                height: 180,
                show: {
                    effect: "fade",
                    duration: 300
                },
                hide: {
                    effect: "fade"
                },
                open: function(event, ui) {
                    $('body').addClass('ui-widget-overlay ui-dialog-mask');
                    return true;
                },
                close: function(event, ui) {
                    $('body').removeClass('ui-widget-overlay ui-dialog-mask');
                    return true;
                }
            });
            $("#myaccModal").dialog({
                modal: true,
                autoOpen: false,
                title: "Approve Accommodation Request",
                width: 450,
                height: 180,
                show: {
                    effect: "fade",
                    duration: 300
                },
                hide: {
                    effect: "fade"
                },
                open: function (event, ui) {
                    $('body').addClass('ui-widget-overlay ui-dialog-mask');
                    return true;
                },
                close: function (event, ui) {
                    $('body').removeClass('ui-widget-overlay ui-dialog-mask');
                    return true;
                }
            });

            $("#myrecheckModal").dialog({
                modal: true,
                autoOpen: false,
                title: "Recheck Accommodation Request",
                width: 450,
                height: 250,
                show: {
                    effect: "fade",
                    duration: 300
                },
                hide: {
                    effect: "fade"
                },
                open: function (event, ui) {
                    $('body').addClass('ui-widget-overlay ui-dialog-mask');
                    return true;
                },
                close: function (event, ui) {
                    $('body').removeClass('ui-widget-overlay ui-dialog-mask');
                    return true;
                }
            });

            $("#mydeskaccModal").dialog({
                modal: true,
                autoOpen: false,
                title: "Approve Accommodation Request",
                width: 450,
                height: 180,
                show: {
                    effect: "fade",
                    duration: 300
                },
                hide: {
                    effect: "fade"
                },
                open: function (event, ui) {
                    $('body').addClass('ui-widget-overlay ui-dialog-mask');
                    return true;
                },
                close: function (event, ui) {
                    $('body').removeClass('ui-widget-overlay ui-dialog-mask');
                    return true;
                }
            });

            $("#mydeskrecheckModal").dialog({
                modal: true,
                autoOpen: false,
                title: "Reject Accommodation Request",
                width: 450,
                height: 250,
                show: {
                    effect: "fade",
                    duration: 300
                },
                hide: {
                    effect: "fade"
                },
                open: function (event, ui) {
                    $('body').addClass('ui-widget-overlay ui-dialog-mask');
                    return true;
                },
                close: function (event, ui) {
                    $('body').removeClass('ui-widget-overlay ui-dialog-mask');
                    return true;
                }
            });
            $( "#allotbox" ).dialog({
                modal: true,
                autoOpen: false,
                title: "Allotment Information",
                width: 750,
                height: 350,
                show: {
                    effect: "fade",
                    duration: 300
                },
                hide: {
                    effect: "fade"
                },
                open: function(event, ui) {
                    $('body').addClass('ui-widget-overlay ui-dialog-mask');
                    return true;
                },
                close: function(event, ui) {
                    $('body').removeClass('ui-widget-overlay ui-dialog-mask');
                    return true;
                }
            });
            $("#savedialog").dialog({
                modal: true,
                autoOpen: false,
                title: "Confirmation",
                width: 400,
                height: 170,
                hideCloseButton: false,
                show: {
                    effect: "fade",
                    duration: 300
                },
                hide: {
                    effect: "fade"
                },
                open: function(event, ui) {
                    $('body').addClass('ui-widget-overlay ui-dialog-mask');
                    return true;
                },
                close: function(event, ui) {
                    $('body').removeClass('ui-widget-overlay ui-dialog-mask');
                    return true;
                }
            });
            $("#editdialog").dialog({
                modal: true,
                autoOpen: false,
                title: "Confirmation",
                width: 400,
                height: 170,
                hideCloseButton: false,
                show: {
                    effect: "fade",
                    duration: 300
                },
                hide: {
                    effect: "fade"
                },
                open: function(event, ui) {
                    $('body').addClass('ui-widget-overlay ui-dialog-mask');
                    return true;
                },
                close: function(event, ui) {
                    $('body').removeClass('ui-widget-overlay ui-dialog-mask');
                    return true;
                }
            });
        });


        function closeaccommodationValidatePopup(){
            $('#myaccModal').dialog('close');
        }
        function closeaccommodationrecheckPopup(){
            $('#myrecheckModal').dialog('close');
        }
        function closedeskaccommodationValidatePopup(){
            $('#mydeskaccModal').dialog('close');
        }
        function closedeskaccommodationrecheckPopup(){
            $('#mydeskrecheckModal').dialog('close');
        }

        function closePopup() {
            $('#confirmPopMessage').val('');
            $('#dialog').dialog('close');
        }

        function closePimsValidatePopup() {
            $('#ValidatePimsdialog').dialog('close');
        }



        function closePimsUpdatePopup() {
            $('#UpdateDialog').dialog('close');
        }


        function closeSavePopup() {
            $('#confirmsavePopMessage').val('');
            $('#savedialog').dialog('close');
        }

        function closeEditPopup() {
            $('#confirmEditPopMessage').val('');
            $('#editdialog').dialog('close');
        }

        function closeRejectPopup() {
            $('#rejectMessage').val('');
            $('#rejectBlank').val('');
            $('#mainPath').val('');
            $('#mainPath').attr('module','');
            $('#rejectRemarks').val('');
            $('#rejectPopup').dialog('close');
        }
        function closeDocPopup() {
            $('#docdialog').dialog('close');
        }
        function closeValidatePopup() {
            $('#validatedialog').dialog('close');
        }
        function closeGOPopup() {
            $('#GOdialog').dialog('close');
        }

        function addClass() {
            var element = document.getElementById("myDiv");
            if (element.classList.contains("show"))
                element.classList.remove("show");
            else
                element.classList.add("show");
        }

        function closeallotPopup() {
            $('#allotbox').dialog('close');
        }
	</script>
</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">

<tiles:insertAttribute name="header" />


<div id="allotbox" title="Basic dialog" style="display: none">
	<!-- <div class="div-border"> -->
	<!-- <pre></pre> -->
	<table>
		<tr>
			<td><label><span class="trn">Position Taken
							Date:</span></label></td>
			<td><input id="positiontakenDate" name="token"
					   placeholder="Enter Position Taken Date" type="text"
					   class="form-control datepicker" /></td>
			<td><label><span class="trn">Handover Date: </span></label></td>
			<td><input id="handoverDate" name="handover"
					   class="form-control datepicker" placeholder="Enter Handover Date"
					   type="text" /></td>
		</tr>
		<tr>
			<td><label><span class="trn">PRL Date:</span></label></td>
			<td><input id="prldate" name="token"
					   class="form-control datepicker" placeholder="Enter PRL Date"
					   type="text" /></td>
			<td><label><span class="trn">Status: </span></label></td>
			<td><select id="statusFk" class="form-control">
				<option value="-1">Select option</option>
			</select>
			<td>
		</tr>
		<tr>
			<td><label><span class="trn">Remark:</span></label></td>
			<td><textarea id="allremarks" name="token"
						  placeholder="Enter Remark" class="form-control" cols="40" rows="5"></textarea>
			</td>

		</tr>
	</table>
	<!-- <pre></pre> -->
	<!-- </div> -->
	<div>
		<pre></pre>
		<button class="btn btn-success" id="allotyes">
			<span class="trn">Save</span>
		</button>
		<button class="btn btn-danger-no" onclick="closeallotPopup()">
			<span class="trn">No</span>
		</button>
	</div>
</div>

<div id="validatedialog" style="display: none" class="modal"
	 align="center">
	<div class="div-border">
		<pre></pre>
		<span class="trn"><b>Are you sure that you want to
					validate this ACR?</b></span>
		<pre></pre>

	</div>

	<div>
		<pre></pre>
		<button class="btn btn-success" id="validateyes">
			<span class="trn">Yes</span>
		</button>
		<button class="btn btn-danger-no" onclick="closeValidatePopup()">
			<span class="trn">No</span>
		</button>
	</div>
</div>

<div id="GOdialog" style="display: none" class="modal" align="center">
	<div class="div-border">
		<pre></pre>
		<span class="trn"><b>Are you sure that you want to
					generate GO Letter ?</b></span>
		<pre></pre>
	</div>
	<div>
		<pre></pre>
		<button class="btn btn-success" id="goyes">
			<span class="trn">Yes</span>
		</button>
		<button class="btn btn-danger-no" onclick="closeGOPopup()">
			<span class="trn">No</span>
		</button>
	</div>
</div>
<div class="modal" id="myaccModal" style="display: none" align="center">
	<div class="div-border">
		<pre></pre>
		<span class="trn"><b>Are you sure that you want to approve
					this request ?</b></span>
		<pre></pre>

	</div>
	<div>
		<pre></pre>
		<button class="btn btn-success" id="accommodationvalidateyes">
			<span class="trn">Yes</span>
		</button>
		<button class="btn btn-danger-no"
				onclick="closeaccommodationValidatePopup()">
			<span class="trn">No</span>
		</button>
	</div>
</div>

<div class="modal" id="myrecheckModal" role="dialog">
	<div class="div-border">
		<pre></pre>
		<table>
			<tr>
				<td>Remarks</td>
				<td><textarea cols="40" rows="5" id="txtrecheckremarks"></textarea></td>
			</tr>
		</table>
		<pre></pre>
	</div>
	<div>
		<pre></pre>
		<button class="btn btn-success" id="recheckyes">
			<span class="trn">Yes</span>
		</button>
		<button class="btn btn-danger-no"
				onclick="closeaccommodationrecheckPopup()">
			<span class="trn">No</span>
		</button>
	</div>
</div>

<div class="modal" id="mydeskaccModal" style="display: none"
	 align="center">
	<div class="div-border">
		<pre></pre>
		<span class="trn"><b>Are you sure that you want to approve
					this request ?</b></span>
		<pre></pre>

	</div>
	<div>
		<pre></pre>
		<button class="btn btn-success" id="deskaccommodationvalidateyes">
			<span class="trn">Yes</span>
		</button>
		<button class="btn btn-danger-no"
				onclick="closedeskaccommodationValidatePopup()">
			<span class="trn">No</span>
		</button>
	</div>
</div>

<div class="modal" id="mydeskrecheckModal" role="dialog">
	<div class="div-border">
		<pre></pre>
		<table>
			<tr>
				<td>Remarks</td>
				<td><textarea cols="40" rows="5" id="txtdeskrecheckremarks"></textarea></td>
			</tr>
		</table>
		<pre></pre>
	</div>
	<div>
		<pre></pre>
		<button class="btn btn-success" id="deskrecheckyes">
			<span class="trn">Yes</span>
		</button>
		<button class="btn btn-danger-no"
				onclick="closedeskaccommodationrecheckPopup()">
			<span class="trn">No</span>
		</button>
	</div>
</div>


<div id="rejectPopup" style="display: none" class="modal"
	 align="center">
	<div class="div-border" style="height: 171px;">
		<pre></pre>
		<b><span class="trn">Are you sure that you want to reject
					this Record?</b></span>
		<pre></pre>
		<textarea title="Reject Remarks" class="form-control"
				  id="rejectRemarks" name="remarks"
				  placeholder="Please Enter Reject Remarks*"></textarea>
		<span id="rejectBlank" class="trn" style="color: red;"></span> <input
			type="hidden" module="" id="mainPath" value="" />
	</div>

	<div>
		<pre></pre>
		<button class="btn btn-success" onclick="rejectFunction()">
			<span class="trn">Yes</span>
		</button>
		<button class="btn btn-danger-no" onclick="closeRejectPopup()">
			<span class="trn">No</span>
		</button>
	</div>
</div>


<div id="ValidatePimsdialog" style="display: none" class="modal" align="center">
	<div class="div-border">
		<pre></pre>
		<span class="trn"><b>Are you sure that you want to <span style="color:green;">Validate</span>
					this record?</b></span>
		<pre></pre>
		<!-- <input type="hidden" id="confirmPopMessage" value=""> -->
	</div>

	<div>
		<pre></pre>
		<button class="btn btn-success" id="pimsyes" onclick="fnApprove(this)">
			<span class="trn">Yes</span>
		</button>
		<button class="btn btn-danger-no" onclick="closePimsValidatePopup()">
			<span class="trn">No</span>
		</button>
	</div>
</div>


<div id="UpdateDialog" style="display: none" class="modal" align="center">
	<div class="div-border">
		<pre></pre>
		<span class="trn"><b>Are you sure that you want to <span style="color:green;">Update</span>
					this record?</b></span>
		<pre></pre>
	</div>

	<div>
		<pre></pre>
		<button class="btn btn-success" id="pimsEdu" onclick="fnUpdateEducationPoint(this)">
			<span class="trn">Yes</span>
		</button>
		<button class="btn btn-danger-no" onclick="closePimsValidatePopup()">
			<span class="trn">No</span>
		</button>
	</div>
</div>



<div id="dialog" style="display: none" class="modal" align="center">
	<div class="div-border">
		<pre></pre>
		<span class="trn"><b>Are you sure that you want to delete
					this record?</b></span>
		<pre></pre>
		<input type="hidden" id="confirmPopMessage" value="">
	</div>

	<div>
		<pre></pre>
		<button class="btn btn-success" id="yes">
			<span class="trn">Yes</span>
		</button>
		<button class="btn btn-danger-no" onclick="closePopup()">
			<span class="trn">No</span>
		</button>
	</div>
</div>

<div id="savedialog" style="display: none" class="modal" align="center">
	<div class="div-border">
		<pre></pre>
		<span class="trn"><b>Are you sure that you want to <font  id ="inputval"color="green"></font>
					this record?</b></span>
		<pre></pre>
		<input type="hidden" id="confirmsavePopMessage" value="">
	</div>

	<div>
		<pre></pre>
		<button class="btn btn-success" id="saveyes">
			<span class="trn">Yes</span>
		</button>
		<button class="btn btn-danger-no" onclick="closeSavePopup()">
			<span class="trn">No</span>
		</button>
	</div>
</div>

<div id="editdialog" style="display: none" class="modal" align="center">
	<div class="div-border">
		<pre></pre>
		<span class="trn"><b>Are you sure that you want to <font color="#20a8d8">Edit</font>
					this record?</b></span>
		<pre></pre>
		<input type="hidden" id="confirmEditPopMessage" value="">
	</div>

	<div>
		<pre></pre>
		<button class="btn btn-success" id="edityes" urlpk="" onclick="fnEditMain(this);">
			<span class="trn">Yes</span>
		</button>
		<button class="btn btn-danger-no" onclick="closeEditPopup()">
			<span class="trn">No</span>
		</button>
	</div>
</div>

<div id="docdialog" style="display: none" class="modal" align="center">
	<div class="div-border">
		<pre></pre>
		<table aria-describedby="DataTables_Table_0_info"
			   class="table table-striped table-bordered datatable dataTable no-footer "
			   id="mydocTable" role="grid"
			   style="border-collapse: collapse !important">
			<thead id="theaddoc"></thead>
			<tbody id="tbodydoc"></tbody>
		</table>
		<pre></pre>

	</div>
	<div>
		<pre></pre>
		<button class="btn btn-danger-no" onclick="closeDocPopup()">
			<span class="trn">Close</span>
		</button>
	</div>
</div>
<div class="app-body" id="app-body">
	<tiles:insertAttribute name="menu" />
	<tiles:insertAttribute name="body" />
</div>
<div id="footerDiv">
	<tiles:insertAttribute name="footer" />
</div>

<div class='error' style='z-index: 159; display: none; width: 30%;'>
	<lable id="errorMsg"></lable>
</div>

<div class='success' style='z-index: 159; display: none; width: 30%;'>
	<lable id="successMsg"></lable>
</div>
<script
		src="${pageContext.servletContext.contextPath}/resources/boot/popper.min.js"></script>
<script
		src="${pageContext.servletContext.contextPath}/resources/boot/pace.min.js"></script>
<script
		src="${pageContext.servletContext.contextPath}/resources/boot/perfect-scrollbar.min.js"></script>
<script
		src="${pageContext.servletContext.contextPath}/resources/boot/coreui.min.js"></script>
<!-- Plugins and scripts required by this view-->
<!-- <script src="${pageContext.servletContext.contextPath}/resources/boot/Chart.min.js"></script> -->

<script
		src="${pageContext.servletContext.contextPath}/resources/boot/custom-tooltips.min.js"></script>
<script
		src="${pageContext.servletContext.contextPath}/resources/js/main.js"></script>
<script
		src="${pageContext.servletContext.contextPath}/resources/valid/PIMSCommonValidations.js"></script>
<script
		src="${pageContext.servletContext.contextPath}/resources/valid/PIMSValidationMessage.js"></script>
<script
		src="${pageContext.servletContext.contextPath}/resources/localjs/bootstrap.min.js"></script>
<script
		src="${pageContext.servletContext.contextPath}/resources/localjs/jquery-ui.js"></script>
<script
		src="${pageContext.servletContext.contextPath}/resources/localjs/jquery.modal.min.js"></script>
<link rel="stylesheet"
	  href="${pageContext.servletContext.contextPath}/resources/localcss/jquery.modal.min.css" />
<script type="text/javascript">
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
        $(".tip-top").tooltip({
            placement : 'top'
        });
        $(".tip-right").tooltip({
            placement : 'right'
        });
        $(".tip-bottom").tooltip({
            placement : 'bottom'
        });
        $(".tip-left").tooltip({
            placement : 'left'
        });
    });
</script>

<!-- <script type="text/javascript">

	var userRights = '${LSTRIGHT}';
	console.log(userRights);
</script> -->

</body>
</html>