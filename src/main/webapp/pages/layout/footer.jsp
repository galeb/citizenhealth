
<footer class="app-footer">
	<div>
		<a href="${pageContext.servletContext.contextPath}/index"><span class="trn">Health Information System</span> </a> <span class="trn">&copy; 2019</span>
	</div>
	<div class="ml-auto">
		<span class="trn">Powered by</span> <a href="${pageContext.servletContext.contextPath}/index">
			<span class="trn">Abdullah Al Galeb, PMIT, Jahangirnagar University.</span></a>
	</div>
</footer>
