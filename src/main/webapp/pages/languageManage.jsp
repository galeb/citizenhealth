
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<script	type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/customJS/common.js"></script>
<script>
//startLoading();
</script>
<main class="main"> 
<ol class="breadcrumb">
	<li class="breadcrumb-item"><a href="${pageContext.servletContext.contextPath}/index"><span class="trn">Home</span></a></li>
	<li class="breadcrumb-item active"><span class="trn">Setup Form</span></li>
</ol>
<div class="container-fluid">
	<div class="animated fadeIn">
			<div class="row">
				<div class="col-sm-12">
					<div class="card">
						<div class="card-header">
							<strong><span class="trn">Language Element</span></strong>
						</div>
						<form:form role="form" name="languageform" action="${pageContext.servletContext.contextPath}/language/save" onsubmit="return false;" method="post" modelAttribute="modelLanguage" >
						<div class="card-body">
							<form:hidden path="lstLanguage[0].languagePk" name="lstLanguage[0].languagePk"  id="lstLanguage0languagePk"/>
							<div class="row">
								<div class="col-sm-8">
									<div class="form-group row">
										<div class="col-md-5">
											<b><span class="trn">Key</span></b>
											<div class="ng-star-inserted">
											</div>
										</div>
										<div class="col-md-5">
											<b><span class="trn">Value</span></b>
											<div class="ng-star-inserted">
											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-4">
									<button class="btn btn-sm btn-primary" onclick="addMultipleKeyandValuePair()"><i class="fa fa-plus"></i><span class="trn">Add</span></button>
									&nbsp;&nbsp;<button class="btn btn-sm btn-danger" id="btnMultiKeyPair" style="display:none;" onclick="deleteMultiKeypair()"><i class="fa fa-trash"></i> <span class="trn">Delete</span></button>
								</div>
							</div>
							<div id="parentDivKeyValue">
								<div class="row" id="divKeyValue0">
									<div class="col-sm-8">
										<div class="form-group row">
											<div class="col-md-5">
											<form:input path="lstLanguage[0].englishName" class="form-control ng-untouched ng-pristine ng-invalid"
												title="English Name" validarr="required@@" tovalid="true"  onblur="validateTextComponent(this);checkValSession(this)"
													formcontrolname="englishName" id="lstLanguage0englishName" name="lstLanguage[0].englishName"
													placeholder="Enter English Name" />											
												<div class="ng-star-inserted">
												</div>
											</div>
											<b>:</b>
											<div class="col-md-5">
												<form:input path="lstLanguage[0].bengaliName" class="form-control ng-untouched ng-pristine ng-invalid"
												title="Bengali Name" validarr="required@@" tovalid="true"  onblur="validateTextComponent(this);checkValSession(this)"
													formcontrolname="bengaliName" id="lstLanguage0bengaliName" name="lstLanguage[0].bengaliName"
													placeholder="Enter Bengali Name" />
												<div class="ng-star-inserted">
												</div>
											</div>
										</div>
									</div>
									<div class="col-sm-4"></div>
								</div>
							</div>
						</div>
						
						<div class="card-footer" style="height: 52px;">
							<form:button class="btn btn-sm btn-success float-right" type="button" onclick="fnSaveLanguage()"
								disabled="">
								<i class="fa fa-dot-circle-o"></i><span class="trn">Save</span>
							</form:button>
						</div>
						</form:form>
						<div class="card-body">
							<div class="row"></div>
							<div class="row">
								<div class="col-sm-12 table-responsive" id="divDatatable">
									<table aria-describedby="DataTables_Table_0_info"
										class="table table-striped table-bordered datatable dataTable no-footer "
										id="myTable" role="grid"
										style="border-collapse: collapse !important">
										<thead id="theadLanguage"></thead>								
										<tbody id="tbodyLanguage"></tbody>
									</table>
								</div>
							</div>							
						</div>
					</div>
				</div>
			</div>
	</div>
</div>
</main>
<script	type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/customJS/language.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script>
var leadData = '${lstLanguage}';
prepareGrid(leadData);
</script>
