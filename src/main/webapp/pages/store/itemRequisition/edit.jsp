<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<link href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css" rel="stylesheet">
<style>
    .main .container-fluid {
        padding: 0 10px !important;
        margin-top: -15px !important;
    }

    tr td {
        white-space: nowrap;
    }
</style>

<main class="main"> <!-- Breadcrumb-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="${pageContext.servletContext.contextPath}/index"><span class="trn">Home</span></a></li>
        <li class="breadcrumb-item"><a href="#"><span class="trn">Store</span></a></li>
        <li class="breadcrumb-item active"><span class="trn">রিকুইজিশন ফর্ম</span></li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">

            <div class="card">
                <div class="card-header">
                    <strong><span class="trn">রিকুইজিশন ফর্ম</span></strong>
                </div>

                <%--name--%>
                <%--mobile--%>
                <%--address--%>
                <%--remarks--%>

                <form:form role="form" name="product" action="${pageContext.servletContext.contextPath}/itemRequisition/save" onsubmit="if(valOnSubmit()){startLoading();return true;}else{return false;}" method="post" modelAttribute="modelItemRequisition">
                    <div class="card-body">
                        <form:hidden path="id" name="id" id="id"/>

                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="code"><span class="trn">কোড</span></label>
                                    <div class="col-md-8">
                                        <form:input path="code" class="form-control"
                                                    title="code" readonly="true"
                                                    formcontrolname="name" id="code" name="code"
                                                    placeholder="Enter code" maxlength="10"/>
                                        <div class="ng-star-inserted">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="code"><span class="trn">তারিখ</span></label>
                                    <div class="col-md-8">
                                        <form:input type="input"
                                                    path="transcDate"
                                                    class="form-control datepicker" id="transcDate"
                                                    name="transcDate"
                                                    autocomplete="false"/>
                                        <div class="ng-star-inserted">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="branch"><span class="trn">শাখা/দপ্তর</span></label>
                                    <div class="col-md-8">
                                        <form:input path="branch" class="form-control"
                                                    title="branch"
                                                    formcontrolname="branch" id="branch" name="branch"
                                                    placeholder="Enter branch" maxlength="100"/>



                                        <div class="ng-star-inserted">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="remarks"><span class="trn">Remarks</span></label>
                                    <div class="col-md-8">
                                        <form:textarea path="remarks" rows="5" cols="30" class="form-control ng-untouched ng-pristine ng-invalid"
                                                       title="remarks" onblur="validateTextComponent(this)"
                                                       formcontrolname="remarks" id="remarks" name="remarks"
                                                       placeholder="Enter remarks" maxlength="500"/>
                                        <div class="ng-star-inserted">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 table-responsive" id="requisitionDtl">
                                <table
                                        class="table table-striped table-bordered no-footer "
                                        id="requisitionDtlTbl" role="grid"
                                        style="border-collapse: collapse !important">
                                    <thead>
                                    <td>দ্রব্যের নাম/আইটেম নাম</td>
                                    <td>পরিমাণ</td>
                                    <td>দ্রব্যের বিবরণ</td>
                                    <td><button type="button"
                                                class="form-control btn btn-success btn-sm pull-right add-more">
                                        <span class="trn">Add</span></button></td>
                                    </thead>
                                    <c:forEach var="each" items="${modelItemRequisition.tblStrItemRequisitionDetails}" varStatus="loop">
                                            <tr class="tbl-row">
                                                <td>
                                                    <form:select id="productPk${loop.index}" path="tblStrItemRequisitionDetails[${loop.index}].tblStrProduct" class="form-control ng-untouched ng-pristine ng-invalid">
                                                        <c:forEach items="${productList}" var="product">
                                                            <option value="${product.id}" ${each.tblStrProduct.id eq product.id ? 'selected':''}>${product.name}</option>
                                                        </c:forEach>
                                                    </form:select>
                                                    <%--<select id="productPk0" class="form-control ng-untouched ng-pristine ng-invalid">
                                                        <c:forEach items="${productList}" var="product">
                                                            <option value="${product.id}" ${each.tblStrProduct.id eq product.id ? 'selected':''}>${product.name}</option>
                                                        </c:forEach>
                                                    </select>--%>

                                                </td>
                                                <td>
                                                    <form:input type="number"
                                                                path="tblStrItemRequisitionDetails[${loop.index}].quantity"
                                                                class="form-control" id="quantity${loop.index}" value="${each.quantity}"
                                                                name="quantity"/>
                                                    <%--<input type="number" value="${each.quantity}"
                                                                class="form-control" id="quantity0"
                                                                name="quantity"/>--%>
                                                </td>
                                                <td>
                                                    <form:input
                                                            path="tblStrItemRequisitionDetails[${loop.index}].remarks"
                                                            class="form-control" id="remarks${loop.index}" value="${each.remarks}"
                                                            name="remarks"/>
                                                    <%--<input  value="${each.remarks}"
                                                            class="form-control" id="remarks0"
                                                            name="remarks"/>--%>
                                                    <form:hidden value="${each.id}" path="tblStrItemRequisitionDetails[${loop.index}].id" id="id0" name="id"/>
                                                </td>

                                                <td>
                                                    <button type="button" attr-id="${each.id}"
                                                            class="form-control btn btn-danger btn-sm remove-row">
                                                        <span class="trn">Remove</span></button>
                                                </td>
                                            </tr>
                                        </c:forEach>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer" style="height: 52px;">
                        <form:button class="btn btn-sm btn-success float-right" type="submit">
                            <i class="fa fa-dot-circle-o"></i><span class="trn">Save</span>
                        </form:button>
                    </div>
                </form:form>

            </div>
        </div>
    </div>
</main>

<script type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/customJS/store/itemRequisition.js"></script>

