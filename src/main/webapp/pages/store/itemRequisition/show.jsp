<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib prefix="f" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="t" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<title>Item Requisition</title>
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/resources/plugins/reflection/reflection-com.css">
<link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.12.0/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

<main class="main"> <!-- Breadcrumb-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="${pageContext.servletContext.contextPath}/index"><span class="trn">Home</span></a></li>
        <li class="breadcrumb-item"><a href="#"><span class="trn">Store</span></a></li>
        <li class="breadcrumb-item active"><span class="trn">Item Requisition</span></li>
        <li class="float-right w-100">
            <div class="float-right" style="margin-top: -20px!important;">
                <a href="${pageContext.servletContext.contextPath}/itemRequisition/index" class="btn btn-info btn-sm"><i class="fa fa-list c-white"></i> Index</a>
                <a href="${pageContext.servletContext.contextPath}/itemRequisition/create" class="btn btn-success btn-sm"><i class="fa fa-edit c-white"></i> Create</a>
            </div>
        </li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="card">
                <div class="card-header"><strong><span class="trn">Show/Approve Requisition</span></strong></div>

                <div class="card-body">
                    <div class="row">


                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                            <div class="form-group">
                                <label>রিকুইজিশন নম্বর </label>
                                <input type="text" class="form-control disabled" value="${itemRequisition.code}"/>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                            <div class="form-group">
                                <label>রিকুইজিশন  তারিখ </label>
                                <input type="text" class="form-control disabled" value="${itemRequisition.transcDate}"/>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                            <div class="form-group">
                                <label>শাখা/দপ্তর</label>
                                <input type="text" class="form-control disabled" value="${itemRequisition.branch}"/>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                            <div class="form-group">
                                <label>Remarks</label>
                                <input type="text" class="form-control disabled" value="${itemRequisition.remarks}"/>
                            </div>
                        </div>
                    </div>


                        <div class="row">
                            <div class="col-sm-12 table-responsive">
                                <table class="table table-striped table-bordered no-footer ">
                                    <thead>
                                    <td>দ্রব্যের নাম/আইটেম নাম</td>
                                    <td>পরিমাণ</td>
                                    <td>দ্রব্যের বিবরণ</td>
                                    </thead>
                                    <tbody>
                                    <c:forEach var="each" items="${itemRequisition.tblStrItemRequisitionDetails}">
                                        <tr class="tbl-row">
                                            <td>${each.tblStrProduct.name}</td>
                                            <td>${each.quantity}</td>
                                            <td>${each.remarks}</td>
                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                        </div>


                    <div class="row">

                        <c:if test="${itemRequisition.tblStrApprovalStatus != 'APPROVED' && itemRequisition.tblStrApprovalStatus != 'REJECTED'}">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="form-group">
                                    <label class="required-indicator">অ্যাকশন  মন্তব্য</label>
                                    <input type="text" class="form-control required" id="remarks" value="${itemRequisition.actionRemarks}"/>
                                </div>
                            </div>
                        </c:if>
                    </div>
                </div>
                <div class="card-footer" style="height: 52px;">
                    <input type="hidden" name="id" id="id" value="${itemRequisition.id}"/>
                    <c:if test="${itemRequisition.tblStrApprovalStatus == 'SUBMITTED'}">
                        <div class="float-left">
                            <button class="btn btn-sm btn-success btn" id="btn-approve" type="button">
                                <i class="fa fa-check"></i> Approve
                            </button>
                        </div>
                        <div class="float-right">
                            <button class="btn btn-sm btn-danger btn" id="btn-reject" type="button">
                                <i class="fa fa-close"></i> Reject
                            </button>
                        </div>
                    </c:if>
                    <c:if test="${itemRequisition.tblStrApprovalStatus == 'ENTRY'}">
                        <div class="float-left">
                            <button class="btn btn-sm btn-success btn" id="btn-submit" type="button">
                                <i class="fa fa-forward"></i> Submit
                            </button>
                        </div>
                        <div class="float-right">
                            <a href="${pageContext.servletContext.contextPath}/itemRequisition/delete/${itemRequisition.id}" target="_blank" class="c-white btn btn-sm btn-danger" onclick="return confirm('Are you sure, you want to delete this record...?')"><i class="fa fa-trash c-white"></i> Delete</a>
                        </div>
                    </c:if>
                </div>
            </div>
        </div>
    </div>
</main>

<script type="text/javascript">
    $(document).on("click", "#btn-submit", function () {
        takeAction("SUBMIT");
    });
    $(document).on("click", "#btn-approve", function () {
        takeAction("APPROVE");
    });
    $(document).on("click", "#btn-reject", function () {
        takeAction("REJECT");
    });
    function clearErrors() {
        $(".has-field-error").each(function () {
            $(this).removeClass("has-field-error");
        });
        $(".has-error").each(function () {
            $(this).removeClass("has-error");
        });
    }

    function takeAction(t) {
        var id = $("#id").val();
        if (id && confirm("Are you sure, you want to proceed with action '" + t + "'...?")) {
            startLoading();
            $.get({
                type: "GET",
                url: path + '/itemRequisition/takeAction',
                dataType: "JSON",
                data: {
                    t: t,
                    i: $("#id").val(),
                    d: $("#tblStrDriverInfo").val() ? $("#tblStrDriverInfo").val() : '',
                    r: $("#remarks").val()?$("#remarks").val():''
                },
                success: function (r) {
                    if (r.status == 'OK') {
                        $(".btn").prop("disabled", true);
                        alert("Action taken successfully...");
                        location.reload();
                    }
                },
                error: function (e) {
                    alert("Sorry, failed to take action due to : " + JSON.stringify(e));
                }
            });
            hideLoading();
        }
    }



</script>
