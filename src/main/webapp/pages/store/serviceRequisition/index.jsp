<%--
 @Created By : Reflection
 @Date       : 2019.05.01 11:28:55 AM

 @Author     : Md. Hoshen Mahmud Khan
 @Email      : saif_hmk@live.com
 @Phone      : +88 01672 036757
--%>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib prefix="f" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="t" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<title>Service Requisition</title>
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/resources/plugins/reflection/reflection-com.css">
<main class="main"> <!-- Breadcrumb-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="${pageContext.servletContext.contextPath}/index"><span class="trn">Home</span></a></li>
        <li class="breadcrumb-item"><a href="#"><span class="trn">Store</span></a></li>
        <li class="breadcrumb-item active"><span class="trn">Service Requisition</span></li>
        <li class="float-right w-100">
            <div class="float-right" style="margin-top: -20px!important;">
                <a href="${pageContext.servletContext.contextPath}/strServiceRequisition/create" class="btn btn-success btn-sm"><i class="fa fa-edit c-white"></i> Create</a>
            </div>
        </li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="card hidden">
                <div class="card-header">
                    <h4>Search</h4>
                </div>
                <f:form name="strServiceRequisition" role="form" action="${pageContext.servletContext.contextPath}/strServiceRequisition/index" method="get" modelAttribute="tblStrServiceRequisitionInstance">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                <div class="form-group">
                                    <label>রিকুইজিশন ধরণ</label>
                                    <select name="tblStrServiceRequisitionType" class="form-control">
                                        <option value="">Select One</option>
                                        <c:forEach items="${tblStrServiceRequisitionTypeList}" var="tblStrServiceRequisitionType">
                                            <option value="${tblStrServiceRequisitionType.key}">${tblStrServiceRequisitionType.value}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                <div class="form-group">
                                    <label>রিকুইজিশন নম্বর </label>
                                    <input type="text" class="form-control" value="${tblStrServiceRequisitionInstance.code}"/>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                <div class="form-group">
                                    <label>রিকুইজিশন তারিখ</label>
                                    <input type="text" class="form-control dtp-date" value="${tblStrServiceRequisitionInstance.transcDate}"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer" style="height: 52px;">
                        <div class="row float-right">
                            <button class="btn btn-sm btn-info" type="submit">
                                <i class="fa fa-search"></i>Search
                            </button>
                        </div>
                    </div>
                </f:form>
            </div>

            <div class="card">
                <div class="card-header">
                    <h4>রিকুইজিশন তালিকা </h4>
                </div>

                <div class="card-body">
                    <table class="table table-striped table-bordered th-center th-colored">
                        <thead>
                        <tr>
                            <th>ক্রমিকনম্বর  </th>
                            <th>তারিখ</th>
                            <th>আবেদনকারী</th>
                            <th>উপাধি</th>
                            <th>রিকুইজিশন ধরণ </th>
                            <th>স্টেটাস </th>
                            <th>অ্যাকশন </th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:if test="${not empty strServiceRequisitionInstanceList}">
                            <c:set var="j" value="${1}"></c:set>
                            <c:forEach items="${strServiceRequisitionInstanceList}" var="tblStrServiceRequisition">
                                <tr>
                                    <td class="center">${tblStrServiceRequisition.code}</td>
                                    <td class="center">${tblStrServiceRequisition.transcDate}</td>
                                    <td class="left">${tblStrServiceRequisition.tblEmployee.engName}</td>
                                    <td class="left">${tblStrServiceRequisition.tblEmployee.designation}</td>
                                    <td class="center">${tblStrServiceRequisition.tblStrServiceRequisitionType}</td>
                                    <td class="center">${tblStrServiceRequisition.tblStrApprovalStatus}
                                    </td>
                                    <td class="center">
                                        <a href="${pageContext.servletContext.contextPath}/strServiceRequisition/show/${tblStrServiceRequisition.id}" target="_blank" class="c-white btn btn-sm btn-info"><i class="fa fa-eye c-white"></i> Show</a>
                                        <c:if test="${tblStrServiceRequisition.tblStrApprovalStatus == 'ENTRY'}">
                                            <a href="${pageContext.servletContext.contextPath}/strServiceRequisition/edit/${tblStrServiceRequisition.id}" target="_blank" class="c-white btn btn-sm btn-warning"><i class="fa fa-edit c-white"></i> Edit</a>
                                        </c:if>
                                    </td>
                                </tr>
                            </c:forEach>
                        </c:if>
                        <c:if test="${strServiceRequisitionInstanceList == null}">
                            <tr>
                                <td class="bold center" colspan="7">No record Found...!</td>
                            </tr>
                        </c:if>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</main>
<script type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/plugins/reflection/reflection-com.js"></script>