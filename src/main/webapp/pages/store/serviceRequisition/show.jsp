<%--
 @Created By : Reflection
 @Date       : 2019.05.01 11:28:55 AM

 @Author     : Md. Hoshen Mahmud Khan
 @Email      : saif_hmk@live.com
 @Phone      : +88 01672 036757
--%>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib prefix="f" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="t" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<title>Service Requisition</title>
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/resources/plugins/reflection/reflection-com.css">
<main class="main"> <!-- Breadcrumb-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="${pageContext.servletContext.contextPath}/index"><span class="trn">Home</span></a></li>
        <li class="breadcrumb-item"><a href="#"><span class="trn">Store</span></a></li>
        <li class="breadcrumb-item active"><span class="trn">Service Requisition</span></li>
        <li class="float-right w-100">
            <div class="float-right" style="margin-top: -20px!important;">
                <a href="${pageContext.servletContext.contextPath}/strServiceRequisition/index" class="btn btn-info btn-sm"><i class="fa fa-list c-white"></i> Index</a>
                <a href="${pageContext.servletContext.contextPath}/strServiceRequisition/create" class="btn btn-success btn-sm"><i class="fa fa-edit c-white"></i> Create</a>
            </div>
        </li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="card">
                <div class="card-header"><strong><span class="trn">Show/Approve Requisition</span></strong></div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                            <div class="form-group">
                                <label>রিকুইজিশন  ধরণ</label>
                                <input type="text" class="form-control disabled" value="${tblStrServiceRequisitionInstance.tblStrServiceRequisitionType}"/>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                            <div class="form-group">
                                <label>রিকুইজিশন নম্বর </label>
                                <input type="text" class="form-control disabled" value="${tblStrServiceRequisitionInstance.code}"/>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                            <div class="form-group">
                                <label>রিকুইজিশন  তারিখ </label>
                                <input type="text" class="form-control disabled" value="${tblStrServiceRequisitionInstance.transcDate}"/>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <c:if test="${tblStrServiceRequisitionInstance.tblStrServiceRequisitionType == 'CAR'}">
                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                <div class="form-group">
                                    <label>সরকারি?</label>
                                    <input type="text" class="form-control disabled" value="${tblStrServiceRequisitionInstance.govtPurpose}"/>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                <div class="form-group">
                                    <label>রিপোর্ট  স্থান</label>
                                    <input type="text" class="form-control disabled" value="${tblStrServiceRequisitionInstance.reportingPlace}"/>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                <div class="form-group">
                                    <label>রিপোর্ট  তারিখ</label>
                                    <input type="text" class="form-control disabled" value="${tblStrServiceRequisitionInstance.reportingDate}"/>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                <div class="form-group">
                                    <label>রিপোর্ট সময়</label>
                                    <input type="text" class="form-control disabled" value="${tblStrServiceRequisitionInstance.reportingTime}"/>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                <div class="form-group">
                                    <label>ফেরতের সময় (সম্ভাব্য)</label>
                                    <input type="text" class="form-control disabled" value="${tblStrServiceRequisitionInstance.returningTime}"/>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                <div class="form-group">
                                    <label>ভ্রমণ তারিখ</label>
                                    <input type="text" class="form-control disabled" value="${tblStrServiceRequisitionInstance.travelDate}"/>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                <div class="form-group">
                                    <label>ভ্রমণ সময় </label>
                                    <input type="text" class="form-control disabled" value="${tblStrServiceRequisitionInstance.travelTime}"/>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                <div class="form-group">
                                    <label>ভ্রমণ স্থান </label>
                                    <input type="text" class="form-control disabled" value="${tblStrServiceRequisitionInstance.travelPlace}"/>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                <div class="form-group">
                                    <label>ভ্রমণ গন্তব্যস্থল</label>
                                    <input type="text" class="form-control disabled" value="${tblStrServiceRequisitionInstance.travelDestination}"/>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                <div class="form-group">
                                    <label>ভ্রমণ দূরত্ব</label>
                                    <input type="text" class="form-control disabled" value="${tblStrServiceRequisitionInstance.travelDistance}"/>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="form-group">
                                    <label>বিবরণ</label>
                                    <input type="text" class="form-control disabled" value="${tblStrServiceRequisitionInstance.description}"/>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="form-group">
                                    <label>মন্তব্য</label>
                                    <input type="text" class="form-control disabled" value="${tblStrServiceRequisitionInstance.remarks}"/>
                                </div>
                            </div>
                        </c:if>
                        <c:if test="${tblStrServiceRequisitionInstance.tblStrServiceRequisitionType == 'ROOM'}">
                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 ">
                                <div class="form-group">
                                    <label>ব্যক্তির সংখ্যা</label>
                                    <input type="text" class="form-control disabled" value="${tblStrServiceRequisitionInstance.noOfPerson}"/>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 ">
                                <div class="form-group">
                                    <label>রুম সংখ্যা</label>
                                    <input type="text" class="form-control disabled" value="${tblStrServiceRequisitionInstance.noOfRoom}"/>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                <div class="form-group">
                                    <label>সরকারি ?</label>
                                    <input type="text" class="form-control disabled" value="${tblStrServiceRequisitionInstance.govtPurpose}"/>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="form-group">
                                    <label>মন্তব্য</label>
                                    <input type="text" class="form-control disabled" value="${tblStrServiceRequisitionInstance.remarks}"/>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
                                <div class="form-group">
                                    <label>বিবরণ</label>
                                    <input type="text" class="form-control disabled" value="${tblStrServiceRequisitionInstance.description}"/>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 ">
                                <div class="form-group">
                                    <label>তারিখ থেকে</label>
                                    <input type="text" class="form-control disabled" value="${tblStrServiceRequisitionInstance.fromDate}"/>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 ">
                                <div class="form-group">
                                    <label>তারিখ পর্যন্ত</label>
                                    <input type="text" class="form-control disabled" value="${tblStrServiceRequisitionInstance.toDate}"/>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 ">
                                <div class="form-group">
                                    <label>সময় হইতে</label>
                                    <input type="text" class="form-control disabled" value="${tblStrServiceRequisitionInstance.fromTime}"/>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 ">
                                <div class="form-group">
                                    <label>সময় থেকে</label>
                                    <input type="text" class="form-control disabled" value="${tblStrServiceRequisitionInstance.toTime}"/>
                                </div>
                            </div>

                        </c:if>
                        <c:if test="${tblStrServiceRequisitionInstance.tblStrServiceRequisitionType == 'MAINTENANCE'}">
                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                <div class="form-group">
                                    <label>ভ্রমণ তারিখ</label>
                                    <input type="text" class="form-control disabled" value="${tblStrServiceRequisitionInstance.travelDate}"/>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="form-group">
                                    <label>যন্ত্রপাতি বিবরণ</label>
                                    <input type="text" class="form-control disabled" value="${tblStrServiceRequisitionInstance.equipmentDescription}"/>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="form-group">
                                    <label>বিবরণ</label>
                                    <input type="text" class="form-control disabled" value="${tblStrServiceRequisitionInstance.description}"/>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
                                <div class="form-group">
                                    <label>মন্তব্য</label>
                                    <input type="text" class="form-control disabled" value="${tblStrServiceRequisitionInstance.remarks}"/>
                                </div>
                            </div>
                        </c:if>
                        <c:if test="${tblStrServiceRequisitionInstance.tblStrServiceRequisitionType == 'LOUNGE'}">
                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                <div class="form-group">
                                    <label>ভ্রমনের তারিখ</label>
                                    <input type="text" class="form-control disabled" value="${tblStrServiceRequisitionInstance.travelDate}"/>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                <div class="form-group">
                                    <label>যাত্রীর নাম</label>
                                    <input type="text" class="form-control disabled" value="${tblStrServiceRequisitionInstance.passengerName}"/>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                <div class="form-group">
                                    <label>ভ্রমণ স্থান</label>
                                    <input type="text" class="form-control disabled" value="${tblStrServiceRequisitionInstance.travelPlace}"/>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                <div class="form-group">
                                    <label>ভ্রমণ গন্তব্য</label>
                                    <input type="text" class="form-control disabled" value="${tblStrServiceRequisitionInstance.travelDestination}"/>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                <div class="form-group">
                                    <label>কর্মকর্তার নাম</label>
                                    <input type="text" class="form-control disabled" value="${tblStrServiceRequisitionInstance.tblEmployee.engName}"/>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                <div class="form-group">
                                    <label>প্রাধিকার প্রাপ্ত ব্যাক্তির সাথে যাত্রীর সম্পর্ক</label>
                                    <input type="text" class="form-control disabled" value="${tblStrServiceRequisitionInstance.passengerRelationship}"/>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                <div class="form-group">
                                    <label>এয়ারলাইন্স এর নাম ও ফ্লাইট নম্বর</label>
                                    <input type="text" class="form-control disabled" value="${tblStrServiceRequisitionInstance.airlinesFlightNo}"/>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 ">
                                <div class="form-group">
                                    <label>আগমনের তারিখ</label>
                                    <input type="text" class="form-control disabled" value="${tblStrServiceRequisitionInstance.fromDate}"/>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 ">
                                <div class="form-group">
                                    <label>আগমনের সময়</label>
                                    <input type="text" class="form-control disabled" value="${tblStrServiceRequisitionInstance.fromTime}"/>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 ">
                                <div class="form-group">
                                    <label>প্রস্থানের তারিখ</label>
                                    <input type="text" class="form-control disabled" value="${tblStrServiceRequisitionInstance.toDate}"/>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 ">
                                <div class="form-group">
                                    <label>প্রস্থানের সময়</label>
                                    <input type="text" class="form-control disabled" value="${tblStrServiceRequisitionInstance.toTime}"/>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                <div class="form-group">
                                    <label>Branch</label>
                                    <input type="text" class="form-control disabled" value="${tblStrServiceRequisitionInstance.branch}"/>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="form-group">
                                    <label>মন্তব্য</label>
                                    <input type="text" class="form-control disabled" value="${tblStrServiceRequisitionInstance.remarks}"/>
                                </div>
                            </div>
                        </c:if>
                        <c:if test="${tblStrServiceRequisitionInstance.tblStrServiceRequisitionType == 'CAR' && tblStrServiceRequisitionInstance.tblStrApprovalStatus == 'SUBMITTED'}">
                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                <div class="form-group">
                                    <label class="required-indicator">চালকের নাম</label>
                                    <select name="tblStrDriverInfo" id="tblStrDriverInfo" class="form-control required">
                                        <option value="">Select One</option>
                                        <c:forEach items="${tblStrDriverInfoList}" var="tblStrDriverInfo">
                                            <option value="${tblStrDriverInfo.id}" <c:if test="${tblStrServiceRequisitionInstance.tblStrDriverInfo.id == tblStrDriverInfo.id}">selected</c:if> >${tblStrDriverInfo.driverName}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                <div class="form-group">
                                    <label>গাড়ি</label>
                                    <input type="text" class="form-control disabled" id="carNo" value="${tblStrServiceRequisitionInstance.tblStrDriverInfo.tblStrCarInfo.carNo}"/>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                <div class="form-group">
                                    <label>মোবাইল</label>
                                    <input type="text" class="form-control disabled" id="mobile" value="${tblStrServiceRequisitionInstance.tblStrDriverInfo.mobile}"/>
                                </div>
                            </div>
                        </c:if>
                        <c:if test="${tblStrServiceRequisitionInstance.tblStrServiceRequisitionType == 'CAR' && tblStrServiceRequisitionInstance.tblStrApprovalStatus == 'APPROVED'}">
                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                <div class="form-group">
                                    <label>গাড়ি</label>
                                    <select name="tblStrDriverInfo" class="form-control disabled">
                                        <option value="">Select One</option>
                                        <c:forEach items="${tblStrDriverInfoList}" var="tblStrDriverInfo">
                                            <option value="${tblStrDriverInfo.id}" <c:if test="${tblStrServiceRequisitionInstance.tblStrDriverInfo.id == tblStrDriverInfo.id}">selected</c:if> >${tblStrDriverInfo.driverName}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                <div class="form-group">
                                    <label>গাড়ি</label>
                                    <input type="text" class="form-control disabled" value="${tblStrServiceRequisitionInstance.tblStrDriverInfo.tblStrCarInfo.carNo}"/>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                <div class="form-group">
                                    <label>মোবাইল</label>
                                    <input type="text" class="form-control disabled" value="${tblStrServiceRequisitionInstance.tblStrDriverInfo.mobile}"/>
                                </div>
                            </div>
                        </c:if>
                        <c:if test="${tblStrServiceRequisitionInstance.tblStrApprovalStatus != 'APPROVED' && tblStrServiceRequisitionInstance.tblStrApprovalStatus != 'REJECTED'}">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="form-group">
                                    <label class="required-indicator">অ্যাকশন  মন্তব্য</label>
                                    <input type="text" class="form-control required" id="remarks" value="${tblStrServiceRequisitionInstance.actionRemarks}"/>
                                </div>
                            </div>
                        </c:if>
                    </div>
                </div>
                <div class="card-footer" style="height: 52px;">
                    <input type="hidden" name="id" id="id" value="${tblStrServiceRequisitionInstance.id}"/>
                    <c:if test="${tblStrServiceRequisitionInstance.tblStrApprovalStatus == 'SUBMITTED'}">
                        <div class="float-left">
                            <button class="btn btn-sm btn-success btn" id="btn-approve" type="button">
                                <i class="fa fa-check"></i> Approve
                            </button>
                        </div>
                        <div class="float-right">
                            <button class="btn btn-sm btn-danger btn" id="btn-reject" type="button">
                                <i class="fa fa-close"></i> Reject
                            </button>
                        </div>
                    </c:if>
                    <c:if test="${tblStrServiceRequisitionInstance.tblStrApprovalStatus == 'ENTRY'}">
                        <div class="float-left">
                            <button class="btn btn-sm btn-success btn" id="btn-submit" type="button">
                                <i class="fa fa-forward"></i> Submit
                            </button>
                        </div>
                        <div class="float-right">
                            <a href="${pageContext.servletContext.contextPath}/strServiceRequisition/delete/${tblStrServiceRequisitionInstance.id}" target="_blank" class="c-white btn btn-sm btn-danger" onclick="return confirm('Are you sure, you want to delete this record...?')"><i class="fa fa-trash c-white"></i> Delete</a>
                        </div>
                    </c:if>
                </div>
            </div>
        </div>
    </div>
</main>
<script type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/plugins/reflection/reflection-com.js"></script>
<script type="text/javascript">
    $(document).on("click", "#btn-submit", function () {
        takeAction("SUBMIT");
    });
    $(document).on("click", "#btn-approve", function () {
        takeAction("APPROVE");
    });
    $(document).on("click", "#btn-reject", function () {
        takeAction("REJECT");
    });
    $(document).on("change", "#tblStrDriverInfo", function () {
        var id = $(this).val();
        if (id) {
            startLoading();
            $.get({
                type: "GET",
                url: path + '/driver/getDriverDetails',
                dataType: "JSON",
                data: {
                    id: id
                },
                success: function (r) {
                    if (r.status == 'OK') {
                        $("#driverName").val(r.driverName);
                        $("#mobile").val(r.mobile);
                        $("#tblStrCarInfoId").val(r.tblStrCarInfoId);
                        $("#carModel").val(r.carModel);
                        $("#carNo").val(r.carNo);
                    }
                },
                error: function (e) {
                    alert("Sorry, failed to get driver details due to :" + e);
                }
            });
            hideLoading();
        }
        else{
            $("#driverName").val("");
            $("#mobile").val("");
            $("#tblStrCarInfoId").val("");
            $("#carModel").val("");
            $("#carNo").val("");
        }
    });

    function takeAction(t) {
        clearErrors();
        var id = $("#id").val();
        if (validateRequiredFields() && id && confirm("Are you sure, you want to proceed with action '" + t + "'...?")) {
            startLoading();
            $.get({
                type: "GET",
                url: path + '/strServiceRequisition/takeAction',
                dataType: "JSON",
                data: {
                    t: t,
                    i: $("#id").val(),
                    d: $("#tblStrDriverInfo").val() ? $("#tblStrDriverInfo").val() : '',
                    r: $("#remarks").val()?$("#remarks").val():''
                },
                success: function (r) {
                    if (r.status == 'OK') {
                        $(".btn").prop("disabled", true);
                        alert("Action taken successfully...");
                        location.reload();
                    }
                },
                error: function (e) {
                    alert("Sorry, failed to take action due to : " + JSON.stringify(e));
                }
            });
            hideLoading();
        }
    }
</script>
