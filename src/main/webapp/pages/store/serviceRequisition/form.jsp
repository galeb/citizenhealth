<%--
 @Created By : Reflection
 @Date       : 2019.05.01 11:28:55 AM

 @Author     : Md. Hoshen Mahmud Khan
 @Email      : saif_hmk@live.com
 @Phone      : +88 01672 036757
--%>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib prefix="f" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="t" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix= "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<title>Service Requisition</title>
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/resources/plugins/bootstrap-datepicker/datepicker3.css">
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/resources/plugins/reflection/reflection-com.css">
<main class="main"> <!-- Breadcrumb-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="${pageContext.servletContext.contextPath}/index"><span class="trn">Home</span></a></li>
        <li class="breadcrumb-item"><a href="#"><span class="trn">Store</span></a></li>
        <li class="breadcrumb-item active"><span class="trn">Service Requisition</span></li>
        <li class="float-right w-100">
            <div class="float-right" style="margin-top: -20px!important;">
                <a href="${pageContext.servletContext.contextPath}/strServiceRequisition/index" class="btn btn-info btn-sm"><i class="fa fa-list c-white"></i> Index</a>
                <a href="${pageContext.servletContext.contextPath}/strServiceRequisition/create" class="btn btn-success btn-sm"><i class="fa fa-edit c-white"></i> Create</a>
            </div>
        </li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="card">
                <div class="card-header">
                    <strong><span class="trn">রিকুইজিশন ফর্ম </span></strong>
                </div>

                <f:form name="strServiceRequisition" role="form" action="${pageContext.servletContext.contextPath}/strServiceRequisition/save" method="post" modelAttribute="tblStrServiceRequisitionInstance">
                    <div class="card-body">
                        <input type="hidden" name="id" id="id" value="${tblStrServiceRequisitionInstance.id}"/>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                <div class="form-group">
                                    <label>রিকুইজিশন ধরণ </label>
                                    <c:choose>
                                        <c:when test="${tblStrServiceRequisitionInstance.id == null}">
                                            <f:select path="tblStrServiceRequisitionType" class="form-control" id="tblStrServiceRequisitionType" name="tblStrServiceRequisitionType">
                                                <f:options items="${serviceRequisitionTypeList}"/>
                                            </f:select>
                                        </c:when>
                                        <c:when test="${tblStrServiceRequisitionInstance.id != null}">
                                            <%--<input type="hidden" name="tblStrServiceRequisitionType" value="${tblStrServiceRequisitionInstance.tblStrServiceRequisitionType}"/>--%>
                                            <input type="text" class="form-control tblStrServiceRequisitionType disabled" name="tblStrServiceRequisitionType" value="${tblStrServiceRequisitionInstance.tblStrServiceRequisitionType}"/>
                                        </c:when>
                                    </c:choose>
                                </div>
                            </div>

                            <c:if test="${tblStrServiceRequisitionInstance.code != null}">
                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <label>রিকুইজিশন নম্বর </label>
                                        <input type="text" class="form-control code disabled" name="code" value="${tblStrServiceRequisitionInstance.code}"/>
                                    </div>
                                </div>
                            </c:if>

                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                <div class="form-group">
                                    <label class="required-indicator">রিকুইজিশন তারিখ</label>
                                    <input type="text" class="form-control transcDate required dtp-date" name="transcDate" value="<fmt:formatDate pattern = "dd/MM/yyyy" value = "${tblStrServiceRequisitionInstance.transcDate}"/>"/>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="hidden div-car">
                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <label class="required-indicator">সরকারি?</label>
                                        <div class="">
                                            Yes <input type="radio" class="disable-this" name="govtPurpose" value="true" checked>
                                            No <input type="radio" class="disable-this" name="govtPurpose" value="false">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <label>রিপোর্ট স্থান</label>
                                        <f:input type="text" class="form-control reportingPlace disable-this" name="reportingPlace" path="reportingPlace"/>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <label>রিপোর্ট তারিখ </label>
                                        <f:input type="text" class="form-control dtp-date reportingDate disable-this" name="reportingDate" path="reportingDate"/>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <label>রিপোর্ট সময়</label>
                                        <f:input type="text" class="form-control dtp-time reportingTime disable-this" name="reportingTime" path="reportingTime"/>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <label>ফেরতের সময় (সম্ভাব্য)</label>
                                        <f:input type="text" class="form-control dtp-time returningTime disable-this" name="returningTime" path="returningTime"/>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <label>ভ্রমণ তারিখ</label>
                                        <f:input type="text" class="form-control dtp-date travelDate disable-this" name="travelDate" path="travelDate"/>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <label>ভ্রমণ সময় </label>
                                        <f:input type="text" class="form-control dtp-time travelTime disable-this" name="travelTime" path="travelTime"/>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <label>ভ্রমণ স্থান </label>
                                        <f:input type="text" class="form-control travelPlace disable-this" name="travelPlace" path="travelPlace"/>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <label>ভ্রমণ গন্তব্যস্থল</label>
                                        <f:input type="text" class="form-control travelDestination disable-this" name="travelDestination" path="travelDestination"/>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <label>ভ্রমণ দূরত্ব</label>
                                        <f:input type="text" class="form-control travelDistance disable-this" name="travelDistance" path="travelDistance"/>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="form-group">
                                        <label>বিবরণ</label>
                                        <f:textarea class="form-control description disable-this" name="description" path="description"/>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="form-group">
                                        <label>মন্তব্য </label>
                                        <f:textarea class="form-control remarks disable-this" name="remarks" path="remarks"/>
                                    </div>
                                </div>

                            </div>
                            <div class="hidden div-room">

                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 ">
                                    <div class="form-group">
                                        <label class="required-indicator">ব্যক্তির সংখ্যা</label>
                                        <input type="number" path="noOfPerson" class="form-control required noOfPerson disable-this" name="noOfPerson"/>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 ">
                                    <div class="form-group">
                                        <label class="required-indicator">রুম সংখ্যা</label>
                                        <input type="number" path="noOfRoom" class="form-control required noOfRoom disable-this" name="noOfRoom"/>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <label class="required-indicator">সরকারি ?</label>
                                        <input type="number" path="govtPurpose" class="form-control required govtPurpose disable-this"/>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="form-group">
                                        <label>মন্তব্য</label>
                                        <f:textarea class="form-control remarks disable-this" name="remarks" path="remarks"/>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
                                    <div class="form-group">
                                        <label>বিবরণ</label>
                                        <f:textarea class="form-control disable-this" name="description" path="description"/>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 ">
                                    <div class="form-group">
                                        <label>তারিখ থেকে</label>
                                        <input type="text" path="fromDate" class="form-control dtp-date fromDate disable-this" id="fromDate" name="fromDate"/>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 ">
                                    <div class="form-group">
                                        <label>তারিখ পর্যন্ত </label>
                                        <input type="text" path="toDate" class="form-control dtp-date toDate disable-this" id="toDate" name="toDate"/>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 ">
                                    <div class="form-group">
                                        <label>সময় হইতে </label>
                                        <input type="text" path="fromTime" class="form-control dtp-time fromTime disable-this" id="fromTime" name="fromTime"/>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 ">
                                    <div class="form-group">
                                        <label>সময় থেকে</label>
                                        <input type="text" path="toTime" class="form-control dtp-time toTime disable-this" id="toTime" name="toTime"/>
                                    </div>
                                </div>

                            </div>
                            <div class="hidden div-maintenance">
                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <label>ভ্রমণ তারিখ</label>
                                        <input type="text" class="form-control dtp-date travelDate disable-this" name="travelDate"/>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="form-group">
                                        <label>যন্ত্রপাতি বিবরণ</label>
                                        <f:textarea class="form-control equipmentDescription disable-this" name="equipmentDescription" path="equipmentDescription"/>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="form-group">
                                        <label>বিবরণ</label>
                                        <f:textarea class="form-control disable-this" name="description" path="description"/>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
                                    <div class="form-group">
                                        <label>মন্তব্য</label>
                                        <f:textarea class="form-control remarks disable-this" name="remarks" path="remarks"/>
                                    </div>
                                </div>
                            </div>
                            <div class="hidden div-lounge">
                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <label>ভ্রমনের তারিখ</label>
                                        <input type="text" class="form-control dtp-date travelDate disable-this" name="travelDate"/>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <label>যাত্রীর নাম</label>
                                        <input type="text" class="form-control passengerName disable-this" name="passengerName"/>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <label>ভ্রমণ স্থান</label>
                                        <input type="text" class="form-control travelPlace disable-this" name="travelPlace"/>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <label>ভ্রমণ গন্তব্য</label>
                                        <input type="text" class="form-control travelDestination disable-this" name="travelDestination"/>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <label>কর্মকর্তার নাম</label>
                                        <f:select path="tblEmployee" class="form-control disable-this" id="tblEmployee" name="tblEmployee">
                                            <f:options items="${tblEmployeeList}"/>
                                        </f:select>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <label>প্রাধিকার প্রাপ্ত ব্যাক্তির সাথে যাত্রীর সম্পর্ক</label>
                                        <input type="text" class="form-control passengerRelationship disable-this" name="passengerRelationship"/>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <label>এয়ারলাইন্স এর নাম ও ফ্লাইট নম্বর </label>
                                        <input type="text" class="form-control airlinesFlightNo disable-this" name="airlinesFlightNo"/>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 ">
                                    <div class="form-group">
                                        <label>আগমনের তারিখ</label>
                                        <input type="text" class="form-control dtp-date fromDate disable-this" name="fromDate"/>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 ">
                                    <div class="form-group">
                                        <label>আগমনের সময়</label>
                                        <input type="text" class="form-control dtp-time fromTime disable-this" name="fromTime"/>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 ">
                                    <div class="form-group">
                                        <label>প্রস্থানের তারিখ</label>
                                        <input type="text" class="form-control dtp-date toDate disable-this" name="toDate"/>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 ">
                                    <div class="form-group">
                                        <label>প্রস্থানের সময়</label>
                                        <input type="text" class="form-control dtp-time toTime disable-this" name="toTime"/>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <label>Branch</label>
                                        <input type="text" class="form-control branch disable-this" name="branch"/>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="form-group">
                                        <label>মন্তব্য</label>
                                        <f:textarea class="form-control remarks disable-this" name="remarks" path="remarks"/>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="card-footer" style="height: 52px;">
                        <div class="row float-right">
                            <c:choose>
                                <c:when test="${tblStrServiceRequisitionInstance.id == null}">
                                    <button class="btn btn-sm btn-success" id="btn-submit" type="submit">
                                        <i class="fa fa-save"></i><span class="trn"> Save</span>
                                    </button>
                                </c:when>
                                <c:when test="${tblStrServiceRequisitionInstance.id != null && tblStrServiceRequisitionInstance.tblStrApprovalStatus == 'ENTRY' }">
                                    <button class="btn btn-sm btn-warning" id="btn-update" type="submit">
                                        <i class="fa fa-edit"></i><span class="trn"> Update</span>
                                    </button>
                                </c:when>
                            </c:choose>
                        </div>
                    </div>
                </f:form>
            </div>
        </div>
    </div>
</main>
<script type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/plugins/bootstrap-datepicker/bootstrap-datepicker.js"></script>
<script type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/plugins/reflection/reflection-com.js"></script>
<script>
    $(document).ready(function (f) {
        validateFormElements();
    });

    function validateFormElements() {
        $(".required").prop("required", true);
        $(".disabled").prop("disabled", true);
        $(".disable-this").prop("disabled", true);
        $(".dtp-time").prop("placeholder", "HH:MM");
        generateDatePicker();
        validateServiceType();
    }

    $(document).on("click", "#btn-submit", function () {
        validateRequiredFields();
    });

    function validateServiceType() {
        hideAll();
        disableAll();
        var st = $("#tblStrServiceRequisitionType").val();
        if (!st || st == "CAR") {
            $(".div-car").show();
            enableAll("div-car");
        } else if (st == "ROOM") {
            $(".div-room").show();
            enableAll("div-room");
        } else if (st == "MAINTENANCE") {
            $(".div-maintenance").show();
            enableAll("div-maintenance");
        } else if (st == "LOUNGE") {
            $(".div-lounge").show();
            enableAll("div-lounge");
        }
    }

    function disableAll() {
        $(".div-car, .div-room, .div-maintenance, .div-lounge").each(function () {
            $(this).find(".disable-this").prop("disabled", true);
        });
    }

    function enableAll(selector) {
        $("." + selector).each(function () {
            $(this).find(".disable-this").prop("disabled", false);
        });
    }

    $(document).on("change", "#tblStrServiceRequisitionType", function () {
        validateServiceType();
    });

</script>
<%--
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
<script type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/customJS/store/car.js"></script>
--%>



