
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<link href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css" rel="stylesheet">
<style>
    .main .container-fluid {
        padding: 0 10px !important;
        margin-top: -15px !important;
    }

    tr td {
        white-space: nowrap;
    }
</style>

<main class="main"> <!-- Breadcrumb-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="${pageContext.servletContext.contextPath}/index"><span class="trn">Home</span></a></li>
        <li class="breadcrumb-item"><a href="#"><span class="trn">Store</span></a></li>
        <li class="breadcrumb-item active"><span class="trn">Goods Receive</span></li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">

            <div class="card">
                <div class="card-header">
                    <strong><span class="trn">Goods Receive</span></strong>
                </div>

                <%--name--%>
                <%--mobile--%>
                <%--address--%>
                <%--remarks--%>

                <form:form role="form" name="product" action="${pageContext.servletContext.contextPath}/strGoodsReceipt/save" onsubmit="if(valOnSubmit()){startLoading();return true;}else{return false;}" method="post" modelAttribute="tblStrGoodsReceiptInstance">
                    <div class="card-body">
                        <form:hidden path="id" name="id" id="id"/>

                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="code"><span class="trn">কোড</span></label>
                                    <div class="col-md-8">
                                        <form:input path="code" class="form-control"
                                                    title="code"
                                                    formcontrolname="name" id="code" name="code"
                                                    placeholder="Enter code" maxlength="10"/>
                                        <div class="ng-star-inserted">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="code"><span class="trn">তারিখ</span></label>
                                    <div class="col-md-8">
                                        <form:input type="input"
                                                    path="transcDate"
                                                    class="form-control datepicker" id="transcDate"
                                                    name="transcDate"
                                                    autocomplete="false"/>
                                        <div class="ng-star-inserted">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="tblStrSupplier"><span class="trn">সরবরাহকারী প্রতিষ্ঠানের নাম</span></label>
                                    <div class="col-md-8">
                                        <form:select id="tblStrSupplier" path="tblStrSupplier.id" class="form-control ng-untouched ng-pristine ng-invalid">
                                            <form:option value=""> --SELECT--</form:option>
                                            <form:options items="${tblSupplierList}" itemValue="id" itemLabel="name"></form:options>
                                        </form:select>


                                        <div class="ng-star-inserted">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="remarks"><span class="trn">Remarks</span></label>
                                    <div class="col-md-8">
                                        <form:textarea path="remarks" rows="5" cols="30" class="form-control ng-untouched ng-pristine ng-invalid"
                                                       title="remarks" onblur="validateTextComponent(this)"
                                                       formcontrolname="remarks" id="remarks" name="remarks"
                                                       placeholder="Enter remarks" maxlength="500"/>
                                        <div class="ng-star-inserted">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 table-responsive" id="requisitionDtl">
                                <table
                                        class="table table-striped table-bordered no-footer "
                                        id="requisitionDtlTbl" role="grid"
                                        style="border-collapse: collapse !important">
                                    <thead>
                                    <td>দ্রব্যের নাম/আইটেম নাম</td>
                                    <td>অর্থনৈতিক গ্রুপ/কোড</td>
                                    <td>পরিমাণ</td>
                                    <td>একক দাম</td>
                                    <td>মোট</td>
                                    <td><button type="button"
                                                class="form-control btn btn-success btn-sm pull-right add-more">
                                        <span class="trn">Add</span></button></td>
                                    </thead>
                                    <tbody>
                                    <tr class="tbl-row">

                                        <td>
                                            <form:select id="productPk0" onchange="setEconomyCode(this)" path="tblStrGrnDetails[0].tblStrProduct" class="form-control ng-untouched ng-pristine ng-invalid">
                                                <form:option value=""> --SELECT--</form:option>
                                                <form:options items="${productList}" itemValue="id" itemLabel="name"></form:options>
                                            </form:select>

                                        </td>
                                        <td>
                                            <span class="economyCode"></span>
                                        </td>
                                        <td>
                                            <form:input  type="number" value="0" onchange="calculateTotal(this)"
                                                        path="tblStrGrnDetails[0].quantity"
                                                        class="form-control quantity" id="quantity0"
                                                         attr-field="quantity" name="quantity"/>
                                        </td>
                                        <td>
                                            <form:input type="number"  value="0" onchange="calculateTotal(this)"
                                                        path="tblStrGrnDetails[0].unitPrice"
                                                        class="form-control unitPrice" id="unitPrice0"
                                                        attr-field="unitPrice" name="unitPrice"/>
                                        </td>
                                        <td>
                                            <form:input type="number" readonly="true" value="0"
                                                        path="tblStrGrnDetails[0].total"
                                                        class="form-control form" id="total0"
                                                        name="total"/>
                                        </td>

                                        <td>
                                            <button type="button"
                                                    class="form-control btn btn-danger btn-sm remove-row">
                                                <span class="trn">Remove</span></button>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer" style="height: 52px;">
                        <form:button class="btn btn-sm btn-success float-right" type="submit">
                            <i class="fa fa-dot-circle-o"></i><span class="trn">Save</span>
                        </form:button>
                    </div>
                </form:form>
                <div style="display: none">
                    <c:forEach var="each" items="${productList}">
                        <span id="sp${each.id}">${each.economicCode}</span>
                    </c:forEach>
                </div>
                <div class="card-body">
                    <table aria-describedby="DataTables_Table_0_info"
                           class="table table-striped table-bordered datatable dataTable no-footer"
                           id="myTable" role="grid"
                           style="border-collapse: collapse !important">
                        <thead id="theadProduct"></thead>
                        <tbody id="tbodyProduct"></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</main>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.5/datepicker.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
<script type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/customJS/store/goodsReceipt.js"></script>
<script type="text/javascript" charset="utf8"
        src="${pageContext.servletContext.contextPath}/resources/customJS/common.js"></script>


<script>
    function setEconomyCode(e) {
        $(e).closest('tr').find('.economyCode').text($("#sp"+$(e).val()).text());
    }
</script>
