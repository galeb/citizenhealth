<%--
    Document   : ViewBudget
    Created on : Apr 25, 2019, 6:37:05 PM
    Author     : ITMCS-1
--%>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<link href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css" rel="stylesheet">
<style>
    .main .container-fluid {
        padding: 0 10px !important;
        margin-top: -15px !important;
    }

    tr td {
        white-space: nowrap;
    }
</style>
<script>
    startLoading();
</script>
<main class="main"> <!-- Breadcrumb-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="${pageContext.servletContext.contextPath}/index"><span
                class="trn">Home</span></a></li>
        <li class="breadcrumb-item"><a href="#"><span class="trn">Store</span></a></li>
        <li class="breadcrumb-item active"><span class="trn">বাৎসরিক বরাদ্দ</span></li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">

            <div class="card">
                <div class="card-header">
                    <strong><span class="trn">বাৎসরিক বরাদ্দ</span></strong>
                </div>

                <form:form role="form" name="budget" action="${pageContext.servletContext.contextPath}/storebudget/save"
                           onsubmit="if(valOnSubmit()){startLoading();return true;}else{return false;}" method="post"
                           modelAttribute="modelBudget">
                    <div class="card-body">
                        <form:hidden path="id" name="id" id="id"/>

                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="code"><span class="trn">ক্রমিক নং</span></label>
                                    <div class="col-md-8">
                                        <form:input readonly="true" path="code"
                                                    class="form-control"
                                                    title="code" formcontrolname="code" id="code" name="code"
                                                    maxlength="10"/>
                                        <div class="ng-star-inserted">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>

                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="orgCode"><span
                                            class="trn">প্রাতিষ্ঠানিক কোড </span></label>
                                    <div class="col-md-8">
                                        <form:input path="orgCode"
                                                    class="form-control ng-untouched ng-pristine ng-invalid"
                                                    title="orgCode" validarr="required@@" tovalid="true"
                                                    onblur="validateTextComponent(this)"
                                                    formcontrolname="orgCode" id="orgCode" name="orgCode"
                                                    placeholder="Enter name" maxlength="50"/>
                                        <div class="ng-star-inserted">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="operationalCode"><span
                                            class="trn">অপারেশন কোড </span></label>
                                    <div class="col-md-8">
                                        <form:input path="operationalCode"
                                                    class="form-control ng-untouched ng-pristine ng-invalid"
                                                    title="operationalCode" validarr="required@@" tovalid="true"
                                                    onblur="validateTextComponent(this)"
                                                    formcontrolname="operationalCode" id="operationalCode" name="operationalCode"
                                                    placeholder="Enter carNo" maxlength="15"/>
                                        <div class="ng-star-inserted">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>

                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="economicCode"><span
                                            class="trn">অর্থনৈতিক গ্রূপ/কোড  </span></label>
                                    <div class="col-md-8">
                                        <form:input path="economicCode"
                                                    class="form-control ng-untouched ng-pristine ng-invalid"
                                                    title="economicCode" validarr="required@@" tovalid="true"
                                                    onblur="validateTextComponent(this)"
                                                    formcontrolname="economicCode" id="economicCode" name="economicCode"
                                                    placeholder="Enter economicCode" maxlength="15"/>
                                        <div class="ng-star-inserted">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>

                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="budget"><span
                                            class="trn">বাজেট </span></label>
                                    <div class="col-md-8">
                                        <form:input path="budget"
                                                    class="form-control ng-untouched ng-pristine ng-invalid"
                                                    title="budget" validarr="required@@" tovalid="true"
                                                    onblur="validateTextComponent(this)"
                                                    formcontrolname="budget" id="budget" name="budget"
                                                    placeholder="Enter budget" maxlength="15"/>
                                        <div class="ng-star-inserted">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>

                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="revisedBudget"><span
                                            class="trn">সংশোধিত বাজেট </span></label>
                                    <div class="col-md-8">
                                        <form:input path="revisedBudget"
                                                    class="form-control ng-untouched ng-pristine ng-invalid"
                                                    title="revisedBudget" validarr="required@@" tovalid="true"
                                                    onblur="validateTextComponent(this)"
                                                    formcontrolname="revisedBudget" id="revisedBudget" name="revisedBudget"
                                                    placeholder="Enter revisedBudget" maxlength="15"/>
                                        <div class="ng-star-inserted">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>

                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="budgetYear"><span
                                            class="trn">বাজেট ইয়ার  </span></label>
                                    <div class="col-md-8">
                                        <form:input path="budgetYear"
                                                    class="form-control ng-untouched ng-pristine ng-invalid"
                                                    title="budgetYear" validarr="required@@" tovalid="true"
                                                    onblur="validateTextComponent(this)"
                                                    formcontrolname="budgetYear" id="budgetYear" name="budgetYear"
                                                    placeholder="Enter budgetYear" maxlength="15"/>
                                        <div class="ng-star-inserted">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>

                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="category"><span
                                            class="trn">বাজেট ক্যাটাগরি </span></label>
                                    <div class="col-md-8">
                                        <form:input path="category"
                                                    class="form-control ng-untouched ng-pristine ng-invalid"
                                                    title="category" validarr="required@@" tovalid="true"
                                                    onblur="validateTextComponent(this)"
                                                    formcontrolname="category" id="category" name="category"
                                                    placeholder="Enter category" maxlength="15"/>
                                        <div class="ng-star-inserted">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>

                    </div>
                    <div class="card-footer" style="height: 52px;">
                        <form:button class="btn btn-sm btn-success float-right" type="submit">
                            <i class="fa fa-dot-circle-o"></i><span class="trn">Save</span>
                        </form:button>
                    </div>
                </form:form>
                <div class="card-body">
                    <table aria-describedby="DataTables_Table_0_info"
                           class="table table-striped table-bordered datatable dataTable no-footer"
                           id="myTable" role="grid"
                           style="border-collapse: collapse !important">
                        <thead id="theadBudget"></thead>
                        <tbody id="tbodyBudget"></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</main>

<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf8"
        src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" charset="utf8"
        src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script type="text/javascript" charset="utf8"
        src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" charset="utf8"
        src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script type="text/javascript" charset="utf8"
        src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script type="text/javascript" charset="utf8"
        src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script type="text/javascript" charset="utf8"
        src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
<script type="text/javascript" charset="utf8"
        src="${pageContext.servletContext.contextPath}/resources/customJS/store/budget.js"></script>



