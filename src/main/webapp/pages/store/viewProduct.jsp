<%-- 
    Document   : ViewProduct
    Created on : Apr 25, 2019, 6:37:05 PM
    Author     : ITMCS-1
--%>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<link href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css" rel="stylesheet">
<style>
    .main .container-fluid {
        padding: 0 10px !important;
        margin-top: -15px !important;
    }

    tr td {
        white-space: nowrap;
    }
</style>
<script>
    startLoading();
</script>
<main class="main"> <!-- Breadcrumb-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="${pageContext.servletContext.contextPath}/index"><span class="trn">Home</span></a></li>
        <li class="breadcrumb-item"><a href="#"><span class="trn">Store</span></a></li>
        <li class="breadcrumb-item active"><span class="trn">আইটেম এন্ট্রি</span></li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">

            <div class="card">
                <div class="card-header">
                    <strong><span class="trn">আইটেম এন্ট্রি</span></strong>
                </div>

                <%--name--%>
                <%--mobile--%>
                <%--address--%>
                <%--remarks--%>

                <form:form role="form" name="product" action="${pageContext.servletContext.contextPath}/product/save" onsubmit="if(valOnSubmit()){startLoading();return true;}else{return false;}" method="post" modelAttribute="modelProduct">
                    <div class="card-body">
                        <form:hidden path="id" name="id" id="id"/>
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="code"><span class="trn">ক্রমিক নং</span></label>
                                    <div class="col-md-8">
                                        <form:input path="code" class="form-control"
                                                    title="code" readonly="true"
                                                    formcontrolname="name" id="code" name="code"
                                                    placeholder="Enter code" maxlength="10"/>
                                        <div class="ng-star-inserted">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>


                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="name"><span class="trn">আইটেম ধরণ</span></label>
                                    <div class="col-md-8">
                                        <form:select id="tblLookUpItemType" path="tblLookUpItemType.lookUpPk" class="form-control ng-untouched ng-pristine ng-invalid">
                                            <form:option value=""> --SELECT--</form:option>
                                            <form:options items="${itemTypes}" itemValue="lookUpPk" itemLabel="lookUpName"></form:options>
                                        </form:select>

                                            <%--<form:input path="tblLookUpItemType" class="form-control ng-untouched ng-pristine ng-invalid"--%>
                                            <%--title="name" validarr="required@@" tovalid="true"  onblur="validateTextComponent(this)"--%>
                                            <%--formcontrolname="tblLookUpItemType" id="tblLookUpItemType" name="tblLookUpItemType"--%>
                                            <%--placeholder="Enter name" maxlength="50"/>--%>
                                        <div class="ng-star-inserted">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="name"><span class="trn">আইটেম নাম</span></label>
                                    <div class="col-md-8">
                                        <form:input path="name" class="form-control ng-untouched ng-pristine ng-invalid"
                                                    title="name" validarr="required@@" tovalid="true" onblur="validateTextComponent(this)"
                                                    formcontrolname="name" id="name" name="name"
                                                    placeholder="Enter name" maxlength="50"/>
                                        <div class="ng-star-inserted">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="name"><span class="trn">অর্থনৈতিক গ্রূপ/কোড</span></label>
                                    <div class="col-md-8">
                                        <form:input path="economicCode" class="form-control ng-untouched ng-pristine ng-invalid"
                                                    title="mobile" validarr="required@@" tovalid="true" onblur="validateTextComponent(this)"
                                                    formcontrolname="economicCode" id="economicCode" name="economicCode"
                                                    placeholder="Enter mobile" maxlength="30"/>
                                        <div class="ng-star-inserted">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>

                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="name"><span class="trn">আইটেম ডিটেলস</span></label>
                                    <div class="col-md-8">
                                        <form:textarea path="remarks" rows="5" cols="30" class="form-control ng-untouched ng-pristine ng-invalid"
                                                       title="remarks" onblur="validateTextComponent(this)"
                                                       formcontrolname="remarks" id="remarks" name="remarks"
                                                       placeholder="Enter remarks" maxlength="500"/>
                                        <div class="ng-star-inserted">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>


                    </div>
                    <div class="card-footer" style="height: 52px;">
                        <form:button class="btn btn-sm btn-success float-right" type="submit">
                            <i class="fa fa-dot-circle-o"></i><span class="trn">Save</span>
                        </form:button>
                    </div>
                </form:form>
                <div class="card-body">
                    <table aria-describedby="DataTables_Table_0_info"
                           class="table table-striped table-bordered datatable dataTable no-footer"
                           id="myTable" role="grid"
                           style="border-collapse: collapse !important">
                        <thead id="theadProduct"></thead>
                        <tbody id="tbodyProduct"></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</main>

<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
<script type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/customJS/store/product.js"></script>



