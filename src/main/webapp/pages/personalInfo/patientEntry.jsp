<%-- 
    Document   : viewACR
    Created on : Apr 3, 2019, 5:11:57 PM
    Author     : ITMCS-1
--%>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<script type="text/javascript" charset="utf8"
        src="${pageContext.servletContext.contextPath}/resources/customJS/common.js"></script>
<script>
    // startLoading();
</script>
<style>
    .button {
        background-color: #4CAF50; /* Green */
        border: none;
        color: white;
        padding: 11px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 16px;
        margin: 0px 0px;
        cursor: pointer;
    }

    .button4 {
        border-radius: 18px;
    }

    .nav-tabs .nav-link.active, .nav-tabs .nav-item.show .nav-link {
        color: #5c6873;
        background-color: rgba(255, 255, 255, 0.9);
        border-color: #c8ced3 #c8ced3 rgba(255, 255, 255, 0.9);
    }
</style>

<main class="main"> <!-- Breadcrumb-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="${pageContext.servletContext.contextPath}/index">Home</a></li>
        <li class="breadcrumb-item active">Patient Details</li>
    </ol>
    <div class="container-fluid">
        <div id="ui-view">
            <div>
                <div class="animated fadeIn">
                    <div class="row">
                        <div class="col-md-12 mb-12">
                            <div class="tab-content">
                                <div class="tab-pane active" id="profile" role="tabpanel">
                                    <div>
                                        <form:form
                                                action="${pageContext.servletContext.contextPath}/personal-info/patient-treatment-save"
                                                class="form-horizontal ng-untouched ng-pristine ng-invalid"
                                                modelAttribute="modelTreatmentDetailDTO" name="patientInfoEntryForm"
                                                onsubmit="if (valOnSubmit()){startLoading(); return true;}else{return false;}"
                                                method="post">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="card">
                                                        <div class="card-header">
                                                            <strong>Patient Info</strong>
                                                        </div>
                                                        <div class="card-body">

                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <table aria-describedby="DataTables_Table_0_info"
                                                                           class="table table-striped table-bordered datatable dataTable no-footer"
                                                                           id="DataTables_Table_0" role="grid"
                                                                           style="border-collapse: collapse !important">
                                                                        <tbody>
                                                                        <tr>
                                                                            <th>Personal ID:</th>
                                                                            <td><form:input path="personalInfo.pID"
                                                                                            title="Personal ID"
                                                                                            id="pID"
                                                                                            class="form-control form-control-sm"
                                                                                            placeholder="Search..."/></td>
                                                                            <th>
                                                                                <button type="button"
                                                                                        class="button button4"
                                                                                        onclick="fetchDetailsByPID()">
                                                                                    Search
                                                                                </button>
                                                                            </th>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <table aria-describedby="DataTables_Table_0_info"
                                                                           class="table table-striped table-bordered datatable dataTable no-footer"
                                                                           id="DataTables_Table_1" role="grid"
                                                                           style="border-collapse: collapse !important">
                                                                        <tbody>
                                                                        <tr>
                                                                            <th>Name:</th>
                                                                            <td><form:hidden path="personalInfo.id"
                                                                                             id="pkid" name="id"/>

                                                                                <form:input type="text" value=""
                                                                                            path="personalInfo.name"
                                                                                            class="form-control"
                                                                                            id="name"
                                                                                            attr-field="name"
                                                                                            name="name"/>

                                                                            </td>
                                                                            <th>Date of Birth:</th>
                                                                            <td>
                                                                                <form:input type="input"
                                                                                            path="personalInfo.dob"
                                                                                            class="form-control datepicker datePicker"
                                                                                            id="dob"
                                                                                            name="dob"
                                                                                            autocomplete="false"/>

                                                                            </td>
                                                                            <th>Father's Name:</th>
                                                                            <td>
                                                                                <form:input type="text" value=""
                                                                                            path="personalInfo.fatherName"
                                                                                            class="form-control"
                                                                                            id="fatherName"
                                                                                            attr-field="name"
                                                                                            name="fatherName"/>

                                                                            </td>
                                                                            <th>Mother's Name:</th>
                                                                            <td>
                                                                                <form:input type="text"
                                                                                            path="personalInfo.motherName"
                                                                                            class="form-control "
                                                                                            id="motherName"
                                                                                            name="motherName" value=""/>

                                                                            </td>
                                                                        </tr>

                                                                        <tr>
                                                                            <th>NID:</th>
                                                                            <td>
                                                                                <form:input type="text" value=""
                                                                                            path="personalInfo.nid"
                                                                                            class="form-control"
                                                                                            id="nid"
                                                                                            attr-field="name"
                                                                                            name="nid"/>

                                                                            </td>
                                                                            <th>Birth Id:</th>
                                                                            <td>
                                                                                <form:input type="text"
                                                                                            path="personalInfo.bid"
                                                                                            class="form-control "
                                                                                            id="bid"
                                                                                            name="bid" value=""/>

                                                                            </td>
                                                                            <th>Email:</th>
                                                                            <td>
                                                                                <form:input path="personalInfo.email"
                                                                                            id="email" name="email"
                                                                                            class="form-control ng-untouched ng-pristine ng-invalid"/>

                                                                            </td>
                                                                            <th>Gender:</th>
                                                                            <td>
                                                                                <select id="gender"
                                                                                        name="personalInfo.gender"
                                                                                        class="form-control ng-untouched ng-pristine ng-invalid">
                                                                                    <option value="Male">MALE</option>

                                                                                </select>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th colspan="4">Address:</th>
                                                                        </tr>
                                                                        <tr>
                                                                            <th>Division:</th>
                                                                            <td>
                                                                                <select id="productPk" name=" "
                                                                                        class="form-control ng-untouched ng-pristine ng-invalid">
                                                                                    <c:forEach items="${locationList}"
                                                                                               var="product">
                                                                                        <option value="${product.id}" ${each.id eq 1 ? 'selected':''}>${product.name}</option>
                                                                                    </c:forEach>
                                                                                </select>
                                                                            </td>

                                                                            <th>District:</th>
                                                                            <td>
                                                                                <form:select id="productPk"
                                                                                             path="personalInfo.addressInfo.location.id"
                                                                                             class="form-control ng-untouched ng-pristine ng-invalid">
                                                                                    <c:forEach items="${locationList}"
                                                                                               var="product">
                                                                                        <option value="${product.id}" ${each.id eq 1 ? 'selected':''}>${product.name}</option>
                                                                                    </c:forEach>
                                                                                </form:select>
                                                                            </td>
                                                                            <th>House No./Village:</th>
                                                                            <td>
                                                                                <form:textarea
                                                                                        path="personalInfo.addressInfo.houseVillage"
                                                                                        id="houseVillage"
                                                                                        name="houseVillage"
                                                                                        class="form-control ng-untouched ng-pristine ng-invalid"/>
                                                                            </td>

                                                                            <th>Post Office:</th>
                                                                            <td>
                                                                                <form:textarea
                                                                                        path="personalInfo.addressInfo.postOffice"
                                                                                        id="postOffice"
                                                                                        name="postOffice"
                                                                                        class="form-control ng-untouched ng-pristine ng-invalid"/>
                                                                            </td>

                                                                        </tr>

                                                                        </tbody>
                                                                    </table>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="card">
                                                        <div class="card-header">
                                                            <strong>Doctor Information</strong>
                                                        </div>
                                                        <div class="card-body">

                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <table aria-describedby="DataTables_Table_0_info"
                                                                           class="table table-striped table-bordered datatable dataTable no-footer"
                                                                           id="DataTables_Table_6" role="grid"
                                                                           style="border-collapse: collapse !important">
                                                                        <tbody>
                                                                        <tr>
                                                                            <th>Reg. No.:</th>
                                                                            <td><form:input path="doctorInfo.regInfo"
                                                                                            title="regInfo"
                                                                                            id="regInfo"
                                                                                            class="form-control form-control-sm"
                                                                                            placeholder="Search..."/></td>
                                                                            <th>
                                                                                <button type="button"
                                                                                        class="button button4"
                                                                                        onclick="fetchDoctorDetailsByRegId()">
                                                                                    Search
                                                                                </button>
                                                                            </th>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <table aria-describedby="DataTables_Table_0_info"
                                                                           class="table table-striped table-bordered datatable dataTable no-footer"
                                                                           id="DataTables_Table_7" role="grid"
                                                                           style="border-collapse: collapse !important">
                                                                        <tbody>
                                                                        <tr>
                                                                            <th>Name:</th>
                                                                            <td><form:hidden path="doctorInfo.id"
                                                                                             id="doctorPkId" name="id"/>

                                                                                <form:input type="text" value=""
                                                                                            path="doctorInfo.name"
                                                                                            class="form-control"
                                                                                            id="doctorName"
                                                                                            attr-field="name"
                                                                                            name="name"/>

                                                                            </td>
                                                                            <th>Speciality:</th>
                                                                            <td>
                                                                                <form:input type="input"
                                                                                            path="doctorInfo.specility"
                                                                                            class="form-control "
                                                                                            id="specility"
                                                                                            name="specility"/>

                                                                            </td>
                                                                            <th>Department:</th>
                                                                            <td>
                                                                                <form:input type="text" value=""
                                                                                            path="doctorInfo.department"
                                                                                            class="form-control"
                                                                                            id="department"
                                                                                            attr-field="department"
                                                                                            name="department"/>

                                                                            </td>
                                                                            <th>Contact No:</th>
                                                                            <td>
                                                                                <form:input type="text"
                                                                                            path="doctorInfo.contactNo"
                                                                                            class="form-control "
                                                                                            id="doctorContactNo"
                                                                                            name="contactNo" value=""/>

                                                                            </td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="card">
                                                        <div class="card-header">
                                                            <strong>Medical Center Information</strong>
                                                        </div>
                                                        <div class="card-body">

                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <table aria-describedby="DataTables_Table_0_info"
                                                                           class="table table-striped table-bordered datatable dataTable no-footer"
                                                                           id="DataTables_Table_3" role="grid"
                                                                           style="border-collapse: collapse !important">
                                                                        <tbody>
                                                                        <tr>
                                                                            <th>Registration ID:</th>
                                                                            <td><form:input
                                                                                    path="medicalCenterInfo.regInfo"
                                                                                    class="form-control"
                                                                                    title="regInfo"
                                                                                    formcontrolname="regInfo"
                                                                                    id="medicalCenterRegInfo"
                                                                                    name="regInfo"
                                                                                    placeholder="Enter regInfo"
                                                                                    maxlength="100"/>

                                                                            <th>
                                                                                <button type="button"
                                                                                        class="button button4"
                                                                                        onclick="fetchMedicalCentryByRegInfo()">
                                                                                    Search
                                                                                </button>
                                                                            </th>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <table aria-describedby="DataTables_Table_0_info"
                                                                           class="table table-striped table-bordered datatable dataTable no-footer"
                                                                           id="DataTables_Table_4" role="grid"
                                                                           style="border-collapse: collapse !important">
                                                                        <tbody>
                                                                        <tr>
                                                                            <th>Center Name:</th>
                                                                            <td><form:hidden path="medicalCenterInfo.id"
                                                                                             id="medicalCenterInfoPkId"
                                                                                             name="id"/>

                                                                                <form:input
                                                                                        path="medicalCenterInfo.name"
                                                                                        class="form-control"
                                                                                        title="name"
                                                                                        formcontrolname="name"
                                                                                        id="medicalCenterName"
                                                                                        name="medicalCenter.name"
                                                                                        placeholder="Enter Name"
                                                                                        maxlength="100"/>


                                                                            </td>
                                                                            <th>Contact No:</th>
                                                                            <td>
                                                                                <form:input
                                                                                        path="medicalCenterInfo.contactNo"
                                                                                        class="form-control"
                                                                                        title="contactNo"
                                                                                        formcontrolname="contactNo"
                                                                                        id="medicalCentercontactNo"
                                                                                        name="medicalCenter.contactNo"
                                                                                        placeholder="Enter contactNo"
                                                                                        maxlength="100"/>

                                                                            </td>
                                                                            <th>Address:</th>
                                                                            <td>
                                                                                <form:input
                                                                                        path="medicalCenterInfo.address"
                                                                                        class="form-control"
                                                                                        title="address"
                                                                                        formcontrolname="address"
                                                                                        id="medicalCenteraddress"
                                                                                        name="medicalCenter.address"
                                                                                        placeholder="Enter address"
                                                                                        maxlength="100"/>

                                                                            </td>
                                                                            <th>Email:</th>
                                                                            <td>
                                                                                <form:input
                                                                                        path="medicalCenterInfo.email"
                                                                                        class="form-control"
                                                                                        title="email"
                                                                                        formcontrolname="email"
                                                                                        id="medicalCenteremail"
                                                                                        name="medicalCenter.email"
                                                                                        placeholder="Enter email"
                                                                                        maxlength="100"/>


                                                                            </td>
                                                                        </tr>

                                                                        </tbody>
                                                                    </table>

                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="card">
                                                        <div class="card-header">
                                                            <strong>Treatment Information</strong>
                                                        </div>
                                                        <div class="card-body">
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <table aria-describedby="DataTables_Table_0_info"
                                                                           class="table table-striped table-bordered datatable dataTable no-footer"
                                                                           id="DataTables_Table_21" role="grid"
                                                                           style="border-collapse: collapse !important">
                                                                        <tbody>
                                                                        <tr>
                                                                            <th>Treatment Date:</th>
                                                                            <td>
                                                                                <form:input
                                                                                        path="treatmentInfo.treatmentDate"
                                                                                        class="form-control datepicker datePicker"
                                                                                        title="treatmentDate"
                                                                                        formcontrolname="treatmentDate"
                                                                                        id="medicalCentertreatmentDate"
                                                                                        name="medicalCenter.treatmentDate"
                                                                                        placeholder="Enter treatmentDate"
                                                                                        maxlength="100"/>

                                                                            </td>

                                                                            <th>Symptom:</th>
                                                                            <td>
                                                                                <form:input path="treatmentInfo.symptom"
                                                                                            class="form-control"
                                                                                            title="name"
                                                                                            formcontrolname="name"
                                                                                            id="medicalCenterName"
                                                                                            name="medicalCenter.symptom"
                                                                                            placeholder="Enter Name"
                                                                                            maxlength="100"/>


                                                                            </td>
                                                                            <th>Disease:</th>
                                                                            <td>
                                                                                <form:input path="treatmentInfo.disease"
                                                                                            class="form-control"
                                                                                            title="disease"
                                                                                            formcontrolname="disease"
                                                                                            id="disease"
                                                                                            name="medicalCenter.disease"
                                                                                            placeholder="Enter disease"
                                                                                            maxlength="100"/>


                                                                            </td>
                                                                        </tr>

                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <table id="myTableMedicine"
                                                                           class="table table-striped table-bordered datatable dataTable no-footer">
                                                                        <tr>
                                                                            <th>Sl. No</th>
                                                                            <th>Medicine</th>
                                                                            <th>Doses</th>
                                                                            <th>Duration</th>
                                                                            <th>Remarks</th>
                                                                            <th>Action</th>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><span>1</span></td>
                                                                            <td>
                                                                                <%--<form:select path="treatmentDtlList[0].medicine.id"  id="treatmentDtlList[0].medicinePk"
                                                                                             name="treatmentDtlList[0].medicine.id"
                                                                                             class="form-control ng-untouched ng-pristine ng-invalid">
                                                                                    <form:options items="${medicineList}" itemValue="id" itemLabel="name"/>
                                                                                </form:select>--%>
                                                                                <select id="treatmentDtlList[0].medicineInfo.id"
                                                                                        name="treatmentDtlList[0].medicineInfo.id"
                                                                                        class="form-control ng-untouched ng-pristine ng-invalid">
                                                                                    <c:forEach items="${medicineList}"
                                                                                               var="each">
                                                                                        <option value="${each.id}">${each.name}</option>
                                                                                    </c:forEach>
                                                                                </select>
                                                                            </td>
                                                                            <td>
                                                                                <form:input
                                                                                        path="treatmentDtlList[0].doses"
                                                                                        class="form-control"
                                                                                        title="doses"
                                                                                        formcontrolname="doses"
                                                                                        id="treatmentDtlList[0].doses"
                                                                                        name="treatmentDtlList[0].doses"
                                                                                        placeholder="Enter doses"
                                                                                        maxlength="100"/>

                                                                            </td>
                                                                            <td>
                                                                                <form:input
                                                                                        path="treatmentDtlList[0].duration"
                                                                                        class="form-control"
                                                                                        title="duration"
                                                                                        formcontrolname="duration"
                                                                                        id="treatmentDtlList[0].duration"
                                                                                        name="treatmentDtlList[0].duration"
                                                                                        placeholder="Enter duration"
                                                                                        maxlength="100"/>

                                                                            </td>
                                                                            <td>
                                                                                <form:input
                                                                                        path="treatmentDtlList[0].remarks"
                                                                                        class="form-control"
                                                                                        title="remarks"
                                                                                        formcontrolname="remarks"
                                                                                        id="treatmentDtlList[0].remarks"
                                                                                        name="treatmentDtlList[0].remarks"
                                                                                        placeholder="Enter remarks"
                                                                                        maxlength="100"/>

                                                                            </td>
                                                                            <td>
                                                                                <button type="button"
                                                                                        class="form-control btn btn-danger btn-sm remove-row">
                                                                                    <span class="trn">Remove</span>
                                                                                </button>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="5"></td>
                                                                            <td>
                                                                                <button type="button"
                                                                                        class="form-control btn btn-success btn-sm pull-right add-more">
                                                                                    Add More
                                                                                </button>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>

                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="card">
                                                        <div class="card-header">
                                                            <strong>Diagnosis Information</strong>
                                                        </div>
                                                        <div class="card">
                                                            <div class="card-body">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <table id="myTableDiago"
                                                                               class="table table-striped table-bordered datatable dataTable no-footer">
                                                                            <tr>
                                                                                <th>Sl. No</th>
                                                                                <th>Test</th>
                                                                                <th>Test details</th>
                                                                                <th>Result</th>
                                                                                <th>Remarks</th>
                                                                                <th>Action</th>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <span>1</span>
                                                                                </td>
                                                                                <td>
                                                                                    <select id="diagoDtlList[0].diagonosis.id "
                                                                                            name="diagoDtlList[0].diagonosis.id"
                                                                                            class="form-control ng-untouched ng-pristine ng-invalid">
                                                                                        <c:forEach
                                                                                                items="${diagonisisList}"
                                                                                                var="each">
                                                                                            <option value="${each.id}">${each.name}</option>
                                                                                        </c:forEach>
                                                                                    </select>
                                                                                </td>
                                                                                <td>
                                                                                    <form:input
                                                                                            path="diagoDtlList[0].testDetails"
                                                                                            class="form-control"
                                                                                            title="testDetails"
                                                                                            formcontrolname="testDetails"
                                                                                            id="diagoDtlList[0].testDetails"
                                                                                            name="diagoDtlList[0].testDetails"
                                                                                            placeholder="Enter testDetails"
                                                                                            maxlength="100"/>

                                                                                </td>
                                                                                <td>
                                                                                    <form:input
                                                                                            path="diagoDtlList[0].result"
                                                                                            class="form-control"
                                                                                            title="result"
                                                                                            formcontrolname="result"
                                                                                            id="diagoDtlList[0].result"
                                                                                            name="diagoDtlList[0].result"
                                                                                            placeholder="Enter result"
                                                                                            maxlength="100"/>

                                                                                </td>
                                                                                <td>
                                                                                    <form:input
                                                                                            path="diagoDtlList[0].remarks"
                                                                                            class="form-control"
                                                                                            title="remarks"
                                                                                            formcontrolname="remarks"
                                                                                            id="diagoDtlList[0].remarks"
                                                                                            name="diagoDtlList[0].remarks"
                                                                                            placeholder="Enter remarks"
                                                                                            maxlength="100"/>

                                                                                </td>
                                                                                <td>
                                                                                    <button type="button"
                                                                                            class="form-control btn btn-danger btn-sm remove-row">
                                                                                        <span class="trn">Remove</span>
                                                                                    </button>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="5"></td>
                                                                                <td>
                                                                                    <button type="button"
                                                                                            class="form-control btn btn-success btn-sm pull-right add-more">
                                                                                        Add More
                                                                                    </button>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </div>

                                                                </div>
                                                            </div>


                                                        </div>
                                                        <div class="card-footer">
                                                            <form:button data-toggle="tooltip"
                                                                         title="Click here to Save"
                                                                         class="btn btn-sm btn-success float-right"
                                                                         type="button" disabled=""
                                                                         onclick="fnSaveTreatmentDetails()"
                                                                         id="savebutton">
                                                                <i class="fa fa-dot-circle-o"></i>
                                                                <span class="trn">Save</span>
                                                            </form:button>

                                                        </div>
                                                    </div>

                                                </div>

                                            </div>
                                        </form:form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<script type="text/javascript" charset="utf8"
        src="${pageContext.servletContext.contextPath}/resources/customJS/personalInfo/personalInfo.js"></script>
<script>
    function fnSaveTreatmentDetails() {
        //if($('#employeeFk').val()){
        console.log("comes to fnSaveTreatmentDetails()");
        console.log(valOnSubmit());
        if (valOnSubmit()) {
            //startLoading();
            $.post({
                url: path + '/personal-info/patient-treatment-save',
                data: $('form[name=patientInfoEntryForm]').serialize(),
                success: function (response) {
                    console.log(response);
                    if (response === "SUCCESS") {
                        //console.log("Data saved successfully");
                        showSuccess('<b>Success!</b><br> Information successfully added');
                        location.reload();
                    } else {
                        showError('<b>ERROR!</b><br> Information not added!');
                        // hideLoading();
                    }
                }
            });
        }
        /* }else{
         showError('<b>ERROR!</b><br> No employee selected!');

         }*/

    }


    $(document.body).on("click", ".add-more", function () {
        var $tableBody = $(this).parents('table').find("tbody"),
                $trLast = $tableBody.find("tr:last").prev(),
                $trNew = $trLast.clone();
        $trLast.after($trNew);
//        $trNew.find("select").eq(0).val("");
        $trNew.find("input").eq(1).val("");
        $trNew.find("input").eq(2).val("");
        $trNew.find("input").eq(3).val("");
        $trNew.find("input").eq(4).val("");
        console.log($(this).parents('table').attr("id"));
        console.log("comes to medicine");
        if ($(this).parents('table').attr("id") === "myTableMedicine") {
            $(this).parents('table').find("tbody tr").each(function (ix, element) {
                console.log("comes to medicine");
                $(element).find("span").eq(0).text(ix);
                $(element).find("select").eq(0).attr("id", "treatmentDtlListmedicinePk["+ ix +"]");
                $(element).find("select").eq(0).attr("name", "treatmentDtlList[" + ix + "].medicineInfo.id");
                $(element).find("input").eq(0).attr("id", "treatmentDtlListdoses[" + ix + "]");
                $(element).find("input").eq(0).attr("name", "treatmentDtlList[" + ix + "].doses");
                $(element).find("input").eq(1).attr("id", "treatmentDtlListduration[" + ix + "]");
                $(element).find("input").eq(1).attr("name", "treatmentDtlList[" + ix + "].duration");
                $(element).find("input").eq(2).attr("id", "treatmentDtlListremarks[" + ix + "]");
                $(element).find("input").eq(2).attr("name", "treatmentDtlList[" + ix + "].remarks");
            });
        } else if ($(this).parents('table').attr("id") === "myTableDiago") {
            $(this).parents('table').find("tbody tr").each(function (ix, element) {
                console.log("comes to diago table");
                $(element).find("span").eq(0).text(ix);
                $(element).find("select").eq(0).attr("id", "diagoDtlListId" + ix);
                $(element).find("select").eq(0).attr("name", "diagoDtlList[" + ix + "].diagonosis.id");
                $(element).find("input").eq(0).attr("id", "diagoDtlListtestDetails[" + ix + "]");
                $(element).find("input").eq(0).attr("name", "diagoDtlListId[" + ix + "].testDetails");
                $(element).find("input").eq(1).attr("id", "diagoDtlListresult[" + ix + "]");
                $(element).find("input").eq(1).attr("name", "diagoDtlListId[" + ix + "].result");
                $(element).find("input").eq(2).attr("id", "diagoDtlListremarks[" + ix + "]");
                $(element).find("input").eq(2).attr("name", "diagoDtlListId[" + ix + "].remarks");
            });
        }
        /*$('.hasDatepicker').each(function () {
            $(this).removeClass('hasDatepicker').datepicker({
                dateFormat: 'dd-mm-yy'
            })
        });*/
    });


    $(document.body).on("click", ".remove-row", function () {
        if (confirm("Are you sure?")) {
            // alert("total row: "+$(this).parents('table').find('tr').length);
            if ($(this).parents('table').find('tr').length > 3) {
                $(this).closest('tr').remove();
            } else {
                alert("Can't delete only single element!");
            }
        }
    });
</script>



