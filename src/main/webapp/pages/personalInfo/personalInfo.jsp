<%-- 
    Document   : viewACR
    Created on : Apr 3, 2019, 5:11:57 PM
    Author     : ITMCS-1
--%>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<script type="text/javascript" charset="utf8"
        src="${pageContext.servletContext.contextPath}/resources/customJS/common.js"></script>
<script>
    // startLoading();
</script>
<style>
    .button {
        background-color: #4CAF50; /* Green */
        border: none;
        color: white;
        padding: 11px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 16px;
        margin: 0px 0px;
        cursor: pointer;
    }

    .button4 {
        border-radius: 18px;
    }

    .nav-tabs .nav-link.active, .nav-tabs .nav-item.show .nav-link {
        color: #5c6873;
        background-color: rgba(255, 255, 255, 0.9);
        border-color: #c8ced3 #c8ced3 rgba(255, 255, 255, 0.9);
    }
</style>
<script type='text/javascript'>
    function hideShow() {
        document.getElementById("aNew").classList.remove("active");
        document.getElementById("aShow").classList.add("active");
        document.getElementById("show").style.display = "block";
        document.getElementById("create").style.display = "none";
    }

    function showHide() {
        document.getElementById("aShow").classList.remove("active");
        document.getElementById("aNew").classList.add("active");
        document.getElementById("show").style.display = "none";
        document.getElementById("create").style.display = "block";
    }
</script>
<main class="main"> <!-- Breadcrumb-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="${pageContext.servletContext.contextPath}/index">Home</a></li>
        <li class="breadcrumb-item active">Personal Information</li>
    </ol>
    <div class="container-fluid">
        <div id="ui-view">
            <div>
                <div class="animated fadeIn">
                    <div class="row">
                        <div class="col-md-12 mb-12">
                            <ul class="nav nav-tabs" role="tablist">
                                <li class="nav-item"><a id="aShow" aria-controls="home" class="nav-link active"
                                                        data-toggle="tab" role="tab" onclick='hideShow()'>List</a></li>
                                <li class="nav-item"><a id="aNew" aria-controls="profile" class="nav-link"
                                                        data-toggle="tab" role="tab" onclick='showHide()'>New/Edit</a>
                                </li>
                            </ul>
                            <div id="show" class="tab-content">
                                <div class="tab-pane active" id="home" role="tabpanel">
                                    <div class="card-body">
                                        <div class="col-sm-12">
                                            <div class="card">
                                                <div class="card-header">
                                                    <strong>Personal Information</strong>
                                                </div>
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <form action=""
                                                                  class="form-horizontal ng-untouched ng-pristine ng-valid"
                                                                  method="post" novalidate="">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="form-group row">
                                                                            <label class="col-md-3 col-form-label"
                                                                                   for="remarks">Search By P-ID</label>
                                                                            <div class="col-md-9">
                                                                                <div class="input-group">
                                                                                    <input class="form-control"
                                                                                           id="txtPIDSearch"
                                                                                           name="input2-group2"
                                                                                           placeholder="Search By P-ID"
                                                                                           type="text">
                                                                                    <span class="input-group-append"><button
                                                                                            class="btn btn-primary"
                                                                                            type="button"
                                                                                            onclick="fetchDetailsByPID();"> <i
                                                                                            class="fa fa-search"></i> Search..</button></span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-12 table-responsive" id="divDatatable">
                                                    <table aria-describedby="DataTables_Table_0_info"
                                                           class="table table-striped table-bordered datatable dataTable no-footer "
                                                           id="myTable" role="grid"
                                                           style="border-collapse: collapse !important">
                                                        <thead id="theadAcr">
                                                        <th>Name</th>
                                                        <th>PID</th>
                                                        <th>Gender</th>
                                                        <th>Contact No</th>
                                                        <th>Action</th>

                                                        </thead>
                                                        <tbody id="tbodyAcr">
                                                        <c:forEach var="each" items="${personalInfoList}">
                                                            <tr>
                                                                <td>${each.name}</td>
                                                                <td>${each.pID}</td>
                                                                <td>${each.gender}</td>
                                                                <td>${each.contactNo}</td>
                                                                <td><button type="button" class="btn btn-success">Detail</button> </td>
                                                            </tr>
                                                        </c:forEach>


                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="create" class="tab-content" style="display: none">
                                <div class="tab-pane active" id="profile" role="tabpanel">
                                    <div>
                                        <form:form action="${pageContext.servletContext.contextPath}/personal-info/save"
                                                   class="form-horizontal ng-untouched ng-pristine ng-invalid"
                                                   modelAttribute="modelPersonalInfo"
                                                   onsubmit="if (valOnSubmit()){startLoading(); return true;}else{return false;}"
                                                   method="post">
                                        <form:hidden path="id" id="pkid" name="id"/>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="card">
                                                    <div class="card-header">
                                                        <strong>Personal Info</strong>
                                                    </div>
                                                    <div class="card-body">

                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <table aria-describedby="DataTables_Table_0_info"
                                                                       class="table table-striped table-bordered datatable dataTable no-footer"
                                                                       id="DataTables_Table_0" role="grid"
                                                                       style="border-collapse: collapse !important">
                                                                    <tbody>
                                                                    <tr>
                                                                        <th>Personal ID:</th>
                                                                        <td><form:input path="pID" title="Personal ID"
                                                                                        id="pID"
                                                                                        class="form-control form-control-sm"
                                                                                        placeholder="Search..."/></td>
                                                                        <th>
                                                                            <button type="button" class="button button4"
                                                                                    onclick="fetchDetailsByPID()">
                                                                                Search
                                                                            </button>
                                                                        </th>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <table aria-describedby="DataTables_Table_0_info"
                                                                       class="table table-striped table-bordered datatable dataTable no-footer"
                                                                       id="DataTables_Table_1" role="grid"
                                                                       style="border-collapse: collapse !important">
                                                                    <tbody>
                                                                    <tr>
                                                                        <th colspan="3">Personal Information</th>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Name:</th>
                                                                        <td>
                                                                            <form:input type="text" value=""
                                                                                        path="name"
                                                                                        class="form-control" id="name"
                                                                                        attr-field="name" name="name"/>

                                                                        </td>
                                                                        <th>Date of Birth:</th>
                                                                        <td>
                                                                            <form:input type="input" path="dob"
                                                                                        class="form-control datepicker datePicker" id="dob"
                                                                                        name="dob" autocomplete="false"/>

                                                                        </td>
                                                                    </tr>

                                                                    <tr>
                                                                        <th>Father's Name:</th>
                                                                        <td>
                                                                            <form:input type="text" value=""
                                                                                        path="fatherName"
                                                                                        class="form-control" id="fatherName"
                                                                                        attr-field="name" name="fatherName"/>

                                                                        </td>
                                                                        <th>Mother's Name:</th>
                                                                        <td>
                                                                            <form:input type="text" path="motherName"
                                                                                        class="form-control " id="motherName"
                                                                                        name="motherName" value=""/>

                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>NID:</th>
                                                                        <td>
                                                                            <form:input type="text" value=""
                                                                                        path="nid"
                                                                                        class="form-control" id="nid"
                                                                                        attr-field="name" name="nid"/>

                                                                        </td>
                                                                        <th>Birth Id:</th>
                                                                        <td>
                                                                            <form:input type="text" path="bid"
                                                                                        class="form-control " id="bid"
                                                                                        name="bid" value=""/>

                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Email:</th>
                                                                        <td>
                                                                            <form:input path="email" id="email" name="email"
                                                                                    class="form-control ng-untouched ng-pristine ng-invalid"/>

                                                                        </td>
                                                                        <th>Gender:</th>
                                                                        <td>
                                                                            <select id="gender" name="gender"
                                                                                    class="form-control ng-untouched ng-pristine ng-invalid">
                                                                                <option value="Male">Male</option>
                                                                                <option value="Female">Female</option>
                                                                                <option value="Other">Other</option>

                                                                            </select>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th colspan="4">Address: </th>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Division:</th>
                                                                        <td>
                                                                            <select id="divisionPk" name="division.id" onchange="fetchDistrictsByDivisionId()"
                                                                                         class="form-control ng-untouched ng-pristine ng-invalid">
                                                                                <option value="">Select</option>
                                                                                <c:forEach items="${divisionList}"
                                                                                           var="each">
                                                                                    <option value="${each.id}" ${each.id}>${each.name}</option>
                                                                                </c:forEach>
                                                                            </select>
                                                                        </td>

                                                                        <th>District:</th>
                                                                        <td>
                                                                            <form:select id="districtPk" path="district.id" onchange="fetchThanasByDistrictId()"
                                                                                         class="form-control ng-untouched ng-pristine ng-invalid">
                                                                                <option value="">Select</option>
                                                                            </form:select>
                                                                        </td>

                                                                    </tr>

                                                                    <tr>
                                                                        <th>Thana/Upazilla:</th>
                                                                        <td>
                                                                            <form:select id="thanaPk" path="thana.id"
                                                                                         class="form-control ng-untouched ng-pristine ng-invalid">
                                                                                <option value="">Select</option>
                                                                            </form:select>
                                                                        </td>
                                                                        <th>House No./Village:</th>
                                                                        <td>
                                                                            <form:textarea path="addressInfo.houseVillage" id="houseVillage" name="houseVillage"
                                                                                           class="form-control ng-untouched ng-pristine ng-invalid"/>
                                                                        </td>

                                                                    </tr>

                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="card-footer">
                                                        <button class="btn btn-sm btn-success float-right"
                                                                type="submit">
                                                            <i class="fa fa-dot-circle-o"></i> Save
                                                        </button>

                                                    </div>
                                                </div>
                                                </form:form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<script type="text/javascript" charset="utf8"
        src="${pageContext.servletContext.contextPath}/resources/customJS/personalInfo/personalInfo.js"></script>
<script>
    var path = '${pageContext.servletContext.contextPath}';
</script>






