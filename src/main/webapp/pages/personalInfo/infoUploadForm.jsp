<%@ include file="/pages/common.jsp" %>
<style>
    .main .container-fluid {
        padding: 0 10px !important;
        margin-top: -15px !important;
    }

    tr td {
        white-space: nowrap;
    }
</style>

<main class="main"> <!-- Breadcrumb-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="${pageContext.servletContext.contextPath}/index"><span class="trn">Home</span></a></li>
        <li class="breadcrumb-item"><a href="#"><span class="trn">Management</span></a></li>
        <li class="breadcrumb-item active"><span class="trn">Treatment Information</span></li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">


                <div class="card">
                <div class="card-header">
                    <strong><span class="trn">Treatment Information Upload</span></strong>
                </div>

                <form method="POST" role="form" name="product" action="${pageContext.servletContext.contextPath}/personal-info/upload" enctype="multipart/form-data">
                    <div class="card-body">
                        <c:if test="${message ne null}">
                        <div class="card">
                            <div class="card-header">
                                <strong style="color: red">${message}</strong>
                            </div>
                        </div>
                        </c:if>

                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="file"><span class="trn">Treatment General Information</span></label>
                                    <div class="col-md-8">
                                        <input type="file" name="file" id="file" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" />
                                        <div class="ng-star-inserted">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>

                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="file1"><span class="trn">Treatment Detail Information</span></label>
                                    <div class="col-md-8">
                                        <input type="file" name="file1" id="file1" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"  />
                                        <div class="ng-star-inserted">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>


                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label" for="file2"><span class="trn">Diagnosis Detail Information</span></label>
                                    <div class="col-md-8">
                                        <input type="file" name="file2" id="file2" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"  />
                                        <div class="ng-star-inserted">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4"></div>
                        </div>


                    </div>
                    <div class="card-footer" style="height: 52px;">
                        <button class="btn btn-sm btn-success float-right" type="submit">
                            <i class="fa fa-dot-circle-o"></i><span class="trn">Save</span>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</main>

<script>
    function setEconomyCode(e) {
        $(e).closest('tr').find('.economyCode').text($("#sp"+$(e).val()).text());
    }
</script>
