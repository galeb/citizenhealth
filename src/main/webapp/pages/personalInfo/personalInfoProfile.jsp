<%-- 
    Document   : viewACR
    Created on : Apr 3, 2019, 5:11:57 PM
    Author     : ITMCS-1
--%>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<script type="text/javascript" charset="utf8"
        src="${pageContext.servletContext.contextPath}/resources/customJS/common.js"></script>
<script>
    // startLoading();
</script>
<style>
    .button {
        background-color: #4CAF50; /* Green */
        border: none;
        color: white;
        padding: 11px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 16px;
        margin: 0px 0px;
        cursor: pointer;
    }

    .button4 {
        border-radius: 18px;
    }

    .nav-tabs .nav-link.active, .nav-tabs .nav-item.show .nav-link {
        color: #5c6873;
        background-color: rgba(255, 255, 255, 0.9);
        border-color: #c8ced3 #c8ced3 rgba(255, 255, 255, 0.9);
    }
</style>
<script type='text/javascript'>

</script>
<main class="main"> <!-- Breadcrumb-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="${pageContext.servletContext.contextPath}/index">Home</a></li>
        <li class="breadcrumb-item active">Personal Information</li>
    </ol>
    <div class="container-fluid">
        <div id="ui-view">
            <div>
                <div class="animated fadeIn">
                    <div class="row">
                        <div class="col-md-12 mb-12">
                            <div id="create" class="tab-content">
                                <div class="tab-pane active" id="profile" role="tabpanel">
                                    <div>
                                        <form:form action="${pageContext.servletContext.contextPath}/personal-info/save-profile"
                                                   class="form-horizontal ng-untouched ng-pristine ng-invalid"
                                                   modelAttribute="personalInfo"
                                                   onsubmit="if (valOnSubmit()){startLoading(); return true;}else{return false;}"
                                                   method="post">
                                        <form:hidden path="id" id="pkid" name="id" value="${personalInfo.id}"/>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="card">
                                                    <div class="card-header">
                                                        <strong>Personal Info</strong>
                                                    </div>
                                                    <div class="card-body">

                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <table aria-describedby="DataTables_Table_0_info"
                                                                       class="table table-striped table-bordered datatable dataTable no-footer"
                                                                       id="DataTables_Table_0" role="grid"
                                                                       style="border-collapse: collapse !important">
                                                                    <tbody>
                                                                    <tr>
                                                                        <th>Personal ID:</th>
                                                                        <td><form:input path="pID" title="Personal ID"
                                                                                        id="pID"
                                                                                        class="form-control form-control-sm" readonly="true"/></td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <table aria-describedby="DataTables_Table_0_info"
                                                                       class="table table-striped table-bordered datatable dataTable no-footer"
                                                                       id="DataTables_Table_1" role="grid"
                                                                       style="border-collapse: collapse !important">
                                                                    <tbody>
                                                                    <tr>
                                                                        <th colspan="3">Personal Information</th>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Name:</th>
                                                                        <td>
                                                                            <form:input type="text"
                                                                                        path="name"
                                                                                        class="form-control" id="name"
                                                                                        attr-field="name" name="name"/>

                                                                        </td>
                                                                        <th>Date of Birth:</th>
                                                                        <td>
                                                                            <fmt:formatDate value="${personalInfo.dob}"
                                                                                            type="date"
                                                                                            pattern="dd/MM/yyyy"
                                                                                            var="theFormattedDate" />
                                                                            <form:input type="input" path="dob"  value="${theFormattedDate}"
                                                                                        class="form-control datepicker datePicker" id="dob"
                                                                                        name="dob" autocomplete="false"/>

                                                                        </td>
                                                                    </tr>

                                                                    <tr>
                                                                        <th>Father's Name:</th>
                                                                        <td>
                                                                            <form:input type="text"
                                                                                        path="fatherName"
                                                                                        class="form-control" id="fatherName"
                                                                                        attr-field="name" name="fatherName"/>

                                                                        </td>
                                                                        <th>Mother's Name:</th>
                                                                        <td>
                                                                            <form:input type="text" path="motherName"
                                                                                        class="form-control " id="motherName"
                                                                                        name="motherName" />

                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>NID:</th>
                                                                        <td>
                                                                            <form:input type="text"
                                                                                        path="nid"
                                                                                        class="form-control" id="nid"
                                                                                        attr-field="name" name="nid"/>

                                                                        </td>
                                                                        <th>Birth Id:</th>
                                                                        <td>
                                                                            <form:input type="text" path="bid"
                                                                                        class="form-control " id="bid"
                                                                                        name="bid"/>

                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Email:</th>
                                                                        <td>
                                                                            <form:input path="email" id="email" name="email"
                                                                                    class="form-control ng-untouched ng-pristine ng-invalid"/>

                                                                        </td>
                                                                        <th>Gender:</th>
                                                                        <td>
                                                                            <select id="gender" name="gender"
                                                                                    class="form-control ng-untouched ng-pristine ng-invalid">
                                                                                <option value="Male">Male</option>
                                                                                <option value="Female">Female</option>
                                                                                <option value="Other">Other</option>

                                                                            </select>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th colspan="4">Address: </th>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Division:</th>
                                                                        <td>
                                                                            <select id="divisionPk" name="division.id" onchange="fetchDistrictsByDivisionId()"
                                                                                         class="form-control ng-untouched ng-pristine ng-invalid">
                                                                                <option value="">Select</option>
                                                                                <c:forEach items="${divisionList}"
                                                                                           var="each">
                                                                                    <option value="${each.id}" ${each.id eq personalInfo.division.id ? 'selected' : ''}>${each.name}</option>
                                                                                </c:forEach>
                                                                            </select>
                                                                        </td>

                                                                        <th>District:</th>
                                                                        <td>
                                                                            <form:select id="districtPk" path="district.id" onchange="fetchThanasByDistrictId()"
                                                                                         class="form-control ng-untouched ng-pristine ng-invalid">
                                                                                <option value="${personalInfo.district.id}">${personalInfo != null? personalInfo.district.name : 'Select'}</option>
                                                                            </form:select>
                                                                        </td>

                                                                    </tr>

                                                                    <tr>
                                                                        <th>Thana/Upazilla:</th>
                                                                        <td>
                                                                            <form:select id="thanaPk" path="thana.id"
                                                                                         class="form-control ng-untouched ng-pristine ng-invalid">
                                                                                <option value="${personalInfo.thana.id}">${personalInfo != null? personalInfo.thana.name : 'Select'}</option>
                                                                            </form:select>
                                                                        </td>
                                                                        <th>House No./Village:</th>
                                                                        <td>
                                                                            <form:textarea path="houseVillage" id="houseVillage" name="houseVillage"
                                                                                           class="form-control ng-untouched ng-pristine ng-invalid"/>
                                                                        </td>

                                                                    </tr>

                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="card-footer">
                                                        <button class="btn btn-sm btn-success float-right"
                                                                type="submit">
                                                            <i class="fa fa-dot-circle-o"></i> Save
                                                        </button>

                                                    </div>
                                                </div>
                                                </form:form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<script type="text/javascript" charset="utf8"
        src="${pageContext.servletContext.contextPath}/resources/customJS/personalInfo/personalInfo.js"></script>
<script>
    var path = '${pageContext.servletContext.contextPath}';
</script>






