/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.galeb.healthInfoSystem.acr;


import com.galeb.healthInfoSystem.bean.SessionBean;
import com.galeb.healthInfoSystem.common.customenum.LookUpEnum;
import com.galeb.healthInfoSystem.common.utility.service.CommonUtility;
import com.galeb.healthInfoSystem.data.TblDocument;
import com.galeb.healthInfoSystem.data.TblLookUp;
import com.galeb.healthInfoSystem.data.TblUser;
import com.galeb.healthInfoSystem.management.document.DocumentService;
import com.galeb.healthInfoSystem.mastertable.lookup.LookUpService;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;


import com.galeb.healthInfoSystem.data.TblACR;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author ITMCS-1
 */
@Controller
@RequestMapping("/acr")
public class ACRController {

	
    /*@Autowired
    ACRService aCRService; 

    @Autowired
    DocumentService documentService;

    @Autowired
    LookUpService lookupService;
    
  


    @RequestMapping(value="/view", method = RequestMethod.GET)
    public String view(Model model,RedirectAttributes redirectAttributes){
        model.addAttribute("modelTblAcr",new TblACR());
        List<String> keyWords = new ArrayList<>();
        keyWords.add(LookUpEnum.ACRREQUIRED.toString());
        keyWords.add(LookUpEnum.ACRTYPE.toString());
        keyWords.add(LookUpEnum.HEALTHREPORT.toString());
        keyWords.add(LookUpEnum.NEGREMARK.toString());
        keyWords.add(LookUpEnum.IFSUGBYORU.toString());
        keyWords.add(LookUpEnum.IFSUGBYCSO.toString());
        keyWords.add(LookUpEnum.DESCISIONFORNEGREMARK.toString());
        List<TblLookUp> lookUps = lookupService.fetchLookUpByKeyword(keyWords);
        
        model.addAttribute("acrRequiredList", fetchLookUps(LookUpEnum.ACRREQUIRED.toString(),lookUps));
        model.addAttribute("acrTypeList", fetchLookUps(LookUpEnum.ACRTYPE.toString(),lookUps));
        model.addAttribute("healthReportList", fetchLookUps(LookUpEnum.HEALTHREPORT.toString(),lookUps));
        model.addAttribute("ifnegRemarksList", fetchLookUps(LookUpEnum.NEGREMARK.toString(),lookUps));
        model.addAttribute("ifSugByOruList", fetchLookUps(LookUpEnum.IFSUGBYORU.toString(),lookUps));
        model.addAttribute("ifSugByCSoList", fetchLookUps(LookUpEnum.IFSUGBYCSO.toString(),lookUps));
        model.addAttribute("decisionForNegRemarkList", fetchLookUps(LookUpEnum.DESCISIONFORNEGREMARK.toString(),lookUps));
        model.addAttribute("lstDocumnets", CommonUtility.convertToJsonString(documentService.fetchAllDocument()));
        return "com.pims.acr";
    }
    
    private List<TblLookUp> fetchLookUps(String name,List<TblLookUp> lookUps){
        List<TblLookUp> list = new ArrayList();
        for(int i = 0 ; i < lookUps.size() ; i++){
            if(lookUps.get(i).getKeyword().equalsIgnoreCase(name)){
                list.add(lookUps.get(i));
            }
        }
        return list;
    }


    @RequestMapping(value = "/save" , method = RequestMethod.POST)
    public ModelAndView Acrsavedata(@ModelAttribute("modelTblAcr") TblACR tblAcr ,HttpServletRequest req,RedirectAttributes redirectAttributes)
    {

            ModelAndView mv=new ModelAndView();

            Date date=new Date();
            SessionBean sessionBean=(SessionBean)req.getSession(false).getAttribute("AUTHSESSION");
            TblUser tblUser =sessionBean.getUser();
            tblAcr.setCreatedBy(tblUser);
            tblAcr.setUpdatedBy(tblUser);
            tblAcr.setCreatedOn(date);

            tblAcr = aCRService.Acrsavedataservice(tblAcr);
            aCRService.saveAcrDocMap(tblAcr,req);

            redirectAttributes.addFlashAttribute("successMsg","ACR Successfully saved");

            mv.setViewName("redirect:/acr/view");
            return mv;
    }
	
    @ResponseBody
    @RequestMapping(value = "/fetchAll", method= RequestMethod.GET)
    public String FetchalldataofAcr(HttpServletRequest req) 
    {
            SessionBean sessionBean=(SessionBean)req.getSession(false).getAttribute("AUTHSESSION");
            TblUser tblUser =sessionBean.getUser();
            String code = req.getParameter("empCode");
            List<TblACR> lstacrbyuserid=aCRService.FetchAllDataByUserIdservice(tblUser,code);
            return CommonUtility.convertToJsonString(lstacrbyuserid);
    }
        
    @ResponseBody
    @RequestMapping(value="/fetchByEmpCode",method=RequestMethod.GET)
    public String fetchByEmpCode(HttpServletRequest request){
        String code = request.getParameter("empCode");
        if(code.trim().isEmpty()){
            return "emptyCode";
        }else{
           *//* TblEmployee tblEmployee = employeeService.getemployeedatabygovmentidservice(code);
            return tblEmployee!=null ? CommonUtility.convertToJsonString(employeeService.getemployeedatabygovmentidservice(code)) : "notFound";
        *//* return null;
        }
    }
        
    @ResponseBody
    @RequestMapping(value="/fetchDocumentByEmpCode" , method=RequestMethod.GET)
    public String fetchdataofdocumentbyempcode(HttpServletRequest req)
    {
        String code = req.getParameter("empCode");
        if(code.trim().isEmpty()){
            return "emptyCode";
        }else{
            List<TblDocument>  tblDocument= documentService.fetchdataofdocumentbygovmentidservice(code);
            return tblDocument.size() > 0 ? CommonUtility.convertToJsonString(documentService.fetchdataofdocumentbygovmentidservice(code)) : "notFound";
        }
    }
        
    @ResponseBody
    @RequestMapping(value="/delete/{acrId}",method=RequestMethod.GET)
    public String deleteAcr(@PathVariable("acrId")BigInteger acrId){
        aCRService.deleteById(acrId);
        return "DELETEDSUCCSSFULLY";
    }
    
    @ResponseBody
    @RequestMapping(value="/edit/{acrId}",method=RequestMethod.GET)
    public String editAcr(@PathVariable("acrId")BigInteger acrId){
        
        TblACR tblAcr = aCRService.fetchById(acrId);
        return CommonUtility.convertToJsonString(tblAcr)+"#&#"+CommonUtility.convertToJsonString(aCRService.findDocByAcr(tblAcr));
    }
    
   @ResponseBody
   @RequestMapping(value="/fetchacrbyemployeepk/{empPK}" , method=RequestMethod.GET)
   public String fetchacrbyemployeepk(@PathVariable("empPK") BigInteger employeepk)
   {
	   List<TblACR>  lstdataofacrtablebyemppk=aCRService.fetchacrbyemployeepkservice(employeepk);
	   return CommonUtility.convertToJsonString(lstdataofacrtablebyemppk);
   }

    */
    
}
