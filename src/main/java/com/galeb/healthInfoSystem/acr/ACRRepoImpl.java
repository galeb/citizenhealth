/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.galeb.healthInfoSystem.acr;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.galeb.healthInfoSystem.common.dal.service.CommonDAO;
import com.galeb.healthInfoSystem.common.dal.service.OperationTypeEnum;
import com.galeb.healthInfoSystem.data.TblACR;
import com.galeb.healthInfoSystem.data.TblUser;

import java.math.BigInteger;

/**
 *
 * @author ITMCS-PC
 */
@Repository
public class ACRRepoImpl implements ACRRepo{

    /*@Autowired
    CommonDAO commonDAO;

    
    
    @Override
    public TblACR Acrsavedatarepo(TblACR tblACR) {
        commonDAO.saveOrUpdate(tblACR);
        return tblACR;
    }

    @Override
    public List<TblACR> FetchAllDataByUserIdrepo(TblUser tblUser, String code) {
        *//*if(code!=null && !code.trim().isEmpty()){
            TblEmployee tblEmployee = empRepo.getemployeedatabygovmentidrepo(code);
            return commonDAO.findEntity(TblACR.class,"createdBy.id",OperationTypeEnum.EQ,tblUser.getUserPk(),"pimsId.employeePk",OperationTypeEnum.EQ,tblEmployee.getEmployeePk());
        }else{
            return commonDAO.findEntity(TblACR.class,"createdBy.id",OperationTypeEnum.EQ,tblUser.getUserPk());
        }*//*
        return null;
    }

    @Override
    public void deleteById(BigInteger id) {
        TblACR tblAcr = new TblACR();
        tblAcr.setId(id);
        commonDAO.delete(tblAcr);
    }

    @Override
    public TblACR fetchById(BigInteger id) {
        List<TblACR> lstAcr = commonDAO.findEntity(TblACR.class,"acrPk",OperationTypeEnum.EQ,id);
        if(lstAcr!=null && !lstAcr.isEmpty()){
            return lstAcr.get(0);
        }
        return null;
    }

    @Override
    public List<TblACR> fetchacrbyemployeepkrepo(BigInteger employeepk) {		
        return commonDAO.findEntity(TblACR.class, "pimsId.employeePk",OperationTypeEnum.EQ,employeepk);
    }

    *//*@Override
    public void saveAcrDocMap(List<TblACRDocumentMap> lstACRDocumentMap) {
       commonDAO.saveOrUpdateAll(lstACRDocumentMap);
    }

    @Override
    public void deleteDocbyAcr(TblACR tblAcr) {
        List<TblACRDocumentMap> list = commonDAO.findEntity(TblACRDocumentMap.class, "acrPk.acrPk",OperationTypeEnum.EQ,tblAcr.getAcrPk());
        for(TblACRDocumentMap tblAcrmap : list){
            commonDAO.delete(tblAcrmap);
        }
    }

    @Override
    public List<TblACRDocumentMap> fetchDocByAcr(TblACR tblAcr) {
        return commonDAO.findEntity(TblACRDocumentMap.class, "acrPk.acrPk",OperationTypeEnum.EQ,tblAcr.getAcrPk());
    }*//*
    */
}
