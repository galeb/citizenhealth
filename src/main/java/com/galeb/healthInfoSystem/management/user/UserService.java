package com.galeb.healthInfoSystem.management.user;

import java.math.BigInteger;
import java.util.List;

import com.galeb.healthInfoSystem.data.TblRole;
import org.springframework.stereotype.Service;

import com.galeb.healthInfoSystem.data.TblUser;

@Service
public interface UserService {

	String addUser(TblUser user);
	
	TblUser authenticateUser(TblUser user);
	
	TblUser fetchUserByUsername(String username);

	List<TblUser> fetchAllUser();

	String deleteUserbyId(Long userId);

	TblUser fetchUserbyId(Long userId);
	
	List<TblRole> fetchAllRole();
}
