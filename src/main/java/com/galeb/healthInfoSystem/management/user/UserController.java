package com.galeb.healthInfoSystem.management.user;

import java.math.BigInteger;

import javax.servlet.http.HttpServletRequest;

import com.galeb.healthInfoSystem.bean.SessionBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.galeb.healthInfoSystem.common.utility.service.CommonUtility;
import com.galeb.healthInfoSystem.data.TblUser;
import com.galeb.healthInfoSystem.management.language.LanguageService;

@Controller
@RequestMapping("/user")
public class UserController {
	
	@Autowired
	UserService userService;
	
	@Autowired
	LanguageService languageService;
	
	@RequestMapping(value = "/view", method = RequestMethod.GET)
	public String addPostoffice(HttpServletRequest request, Model model) {
		try {
			model.addAttribute("modelUser", new TblUser());
			model.addAttribute("allRole",userService.fetchAllRole());
			model.addAttribute("lstUsers", CommonUtility.convertToJsonString(userService.fetchAllUser()));
						
			return "com.healthInfo.user";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@ResponseBody
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String saveUser(@ModelAttribute("userModel") TblUser user, HttpServletRequest request, Model model) {
		try {
			String resposne = userService.addUser(user);
			return resposne;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@RequestMapping(value = "/authenticate", method = RequestMethod.POST)
	public String authenticateUser(@ModelAttribute("userModel") TblUser user, HttpServletRequest request, Model model) {
		try {
			model.addAttribute("userModel", user);			
			TblUser authenticatedUser = userService.authenticateUser(user);
			if (null != authenticatedUser) {
				model.addAttribute("userModel", authenticatedUser);
				SessionBean sessionBean = new SessionBean();
				sessionBean.setUser(authenticatedUser);
				CommonUtility.setToSession(request, "AUTHSESSION", sessionBean);
				CommonUtility.setToSession(request, "LANGUAGEMAPPER",
						CommonUtility.buildLanguageValJSON(languageService.fetchAllLanguageElements()));
				return "redirect:/index";
			} else {
				model.addAttribute("resposneMessage", "Invalid username or password!");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "beforeAuth/login";
	}
	
	
	@ResponseBody
	@RequestMapping(value = "/edit/{userId}", method = RequestMethod.GET)
	public String edit(@PathVariable("userId")Long userId,HttpServletRequest request, Model model) {
		try {
			TblUser user =userService.fetchUserbyId(userId);
			return CommonUtility.convertToJsonString(user);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;		
	}
	
	@ResponseBody
	@RequestMapping(value = "/fetchAll", method = RequestMethod.GET)
	public String fetchAllUser(HttpServletRequest request, Model model) {
		try {
			String lstUsers = CommonUtility.convertToJsonString(userService.fetchAllUser());
			return lstUsers;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;		
	}
	
	@ResponseBody
	@RequestMapping(value = "/delete/{userId}", method = RequestMethod.GET)
	public String delete(@PathVariable("userId")Long userId,HttpServletRequest request, Model model) {
		try {
			String resposneString =userService.deleteUserbyId(userId);
			return resposneString;			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;		
	}
}
