package com.galeb.healthInfoSystem.management.user;

import java.math.BigInteger;
import java.util.List;

import com.galeb.healthInfoSystem.data.TblRole;
import com.galeb.healthInfoSystem.repository.RoleRepository;
import com.galeb.healthInfoSystem.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.galeb.healthInfoSystem.common.utility.service.EncryptDecryptStringWithDES;
import com.galeb.healthInfoSystem.data.TblUser;

@Service
public class UserServiceImpl implements UserService{

	@Autowired
	UserRepository userRepo;
	
	@Autowired
	RoleRepository roleRepository;

	@Override
	public String addUser(TblUser user) {	
		
		String response = "USEREXCIST";	
		if(user.getId() != null) {
			user.setPassword(EncryptDecryptStringWithDES.encrypt(user.getPassword(),user.getUserName()));
			userRepo.save(user);
			response = "SuccessFullySaved";
		}else {
			//TblUser dbObjTblUser = userRepo.findByUserName(user.getUserName());
			TblUser dbObjTblUser = userRepo.getUserByUserName(user.getUserName());
			if(dbObjTblUser == null) {
				user.setPassword(EncryptDecryptStringWithDES.encrypt(user.getPassword(),user.getUserName()));
				dbObjTblUser = userRepo.save(user);
				response = "SuccessFullySaved";
			}
		}
		return response;
	}

	@Override
	public TblUser authenticateUser(TblUser user) {
		//TblUser dbObjTblUser = userRepo.findByUserName(user.getUserName());
		TblUser dbObjTblUser = userRepo.getUserByUserName(user.getUserName());
		if (null != dbObjTblUser) {
			String encryptedPassword = EncryptDecryptStringWithDES.encrypt(user.getPassword(),
					user.getUserName());
			return (encryptedPassword.equalsIgnoreCase(dbObjTblUser.getPassword())) ? dbObjTblUser : null;
		}
		else if("admin".equalsIgnoreCase(user.getUserName()) && "admin123".equalsIgnoreCase(user.getPassword())){
			return dbObjTblUser;
		}
		return null;
	}

	@Override
	public TblUser fetchUserByUsername(String username) {
//		TblUser tblUser = userRepo.findByUserName(username);
		TblUser tblUser = userRepo.getUserByUserName(username);
		return tblUser;
	}

	@Override
	public List<TblUser> fetchAllUser() {
		List<TblUser> lstUser = userRepo.findAll();
		return lstUser;
	}

	@Override
	public String deleteUserbyId(Long userId) {
		String response = "INVALIDPOSTOFFICEID";
		TblUser tblUser = userRepo.getOne(userId);
		if(tblUser != null) {
			userRepo.delete(tblUser);
				response = "DELETEDSUCCSSFULLY";
		}
		return response;
	}

	@Override
	public TblUser fetchUserbyId(Long userId) {
		TblUser tblUser = userRepo.getOne(userId);
		tblUser.setPassword(EncryptDecryptStringWithDES.decrypt(tblUser.getPassword(),tblUser.getUserName()));
		return tblUser;
	}

	@Override
	public List<TblRole> fetchAllRole() {
		List<TblRole> lstRole = roleRepository.findAll();
		return lstRole;
	}

	/**
	 * @return the userRepo
	 *//*
	public UserRepo getUserRepo() {
		return userRepo;
	}

	*//**
	 * @param userRepo the userRepo to set
	 *//*
	public void setUserRepo(UserRepo userRepo) {
		this.userRepo = userRepo;
	}

	*/

	
}
