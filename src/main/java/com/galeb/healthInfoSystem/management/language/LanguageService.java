package com.galeb.healthInfoSystem.management.language;

import java.math.BigInteger;
import java.util.List;

import com.galeb.healthInfoSystem.data.TblLanguage;
import org.springframework.stereotype.Service;

@Service
public interface LanguageService {

	List<TblLanguage> fetchAllLanguageElements();
	
	String addLanguageElement(List<TblLanguage> tblLanguageElement);
	
	TblLanguage fetchById(Long id);
	
	String deleteLanguageElement(Long id);
}
