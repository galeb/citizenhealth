package com.galeb.healthInfoSystem.management.language;


import com.galeb.healthInfoSystem.data.TblLanguage;

public class LanguageBean {
	
	TblLanguage[] lstLanguage;

	public TblLanguage[] getLstLanguage() {
		return lstLanguage;
	}

	public void setLstLanguage(TblLanguage[] lstLanguage) {
		this.lstLanguage = lstLanguage;
	}

}
