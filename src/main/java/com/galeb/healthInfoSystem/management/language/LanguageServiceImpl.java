package com.galeb.healthInfoSystem.management.language;

import java.math.BigInteger;
import java.util.List;

import com.galeb.healthInfoSystem.data.TblLanguage;
import com.galeb.healthInfoSystem.repository.LanguageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LanguageServiceImpl implements LanguageService{

	@Autowired
    LanguageRepository languageRepository;
	
	@Override
	public List<TblLanguage> fetchAllLanguageElements() {
		return languageRepository.findAll();
	}

	@Override
	public String addLanguageElement(List<TblLanguage> tblLanguageElement) {
        for (TblLanguage each: tblLanguageElement
             ) {
            languageRepository.save(each);
        }
        return "success";
	}

	@Override
	public TblLanguage fetchById(Long id) {
		return languageRepository.getOne(id);
	}

	@Override
	public String deleteLanguageElement(Long id) {
		String response = "INVALIDLANGUAGEELEID";
		TblLanguage tblLanguage = languageRepository.getOne(id);
		if(tblLanguage != null) {
				response = "DELETEDSUCCSSFULLY";
            languageRepository.delete(tblLanguage);
		}
		return response;
		
	}
	
	

	

}
