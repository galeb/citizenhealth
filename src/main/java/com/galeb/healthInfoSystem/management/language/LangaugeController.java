package com.galeb.healthInfoSystem.management.language;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.galeb.healthInfoSystem.common.utility.service.CommonUtility;
import com.galeb.healthInfoSystem.data.TblLanguage;
import com.galeb.healthInfoSystem.management.user.UserService;



@Controller
@RequestMapping("/language")
public class LangaugeController {
	
	@Autowired
	LanguageService languageService;
	
	@Autowired
	UserService userService;
	
	@RequestMapping(value="/view",method=RequestMethod.GET)
	public String viewLocation(Model model,HttpServletRequest request) {
		try {
			model.addAttribute("modelLanguage", new LanguageBean());
			model.addAttribute("lstLanguage",CommonUtility.convertToJsonString(languageService.fetchAllLanguageElements()));		
			
			return "com.healthInfo.language";
		}catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@ResponseBody
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String savePostOffice(@ModelAttribute("modelLanguage") LanguageBean lstLanguageBean, HttpServletRequest request, Model model) {
		try {
			if(lstLanguageBean!=null) {						
				String resposne = languageService.addLanguageElement(Arrays.asList(lstLanguageBean.getLstLanguage()));
				List<TblLanguage> lstLanguageElement = languageService.fetchAllLanguageElements();
				CommonUtility.setToSession(request,"LANGUAGEMAPPER",CommonUtility.buildLanguageValJSON(lstLanguageElement));
				return resposne;
			}else {
				return "EmptyForm";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;		
	}
	
	@ResponseBody
	@RequestMapping(value = "/edit/{languageId}", method = RequestMethod.GET)
	public String edit(@PathVariable("languageId")Long languageId,HttpServletRequest request, Model model) {
		try {
			TblLanguage tblLanguage =languageService.fetchById(languageId);
			return CommonUtility.convertToJsonString(tblLanguage);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;		
	}
	
	@ResponseBody
	@RequestMapping(value = "/delete/{languageId}", method = RequestMethod.GET)
	public String delete(@PathVariable("languageId")Long languageId,HttpServletRequest request, Model model) {
		try {
			String resposneString =languageService.deleteLanguageElement(languageId);
			List<TblLanguage> lstLanguageElement = languageService.fetchAllLanguageElements();
			CommonUtility.setToSession(request,"LANGUAGEMAPPER",CommonUtility.buildLanguageValJSON(lstLanguageElement));
			return resposneString;			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;		
	}
	
	
	@ResponseBody
	@RequestMapping(value = "/fetchAll", method = RequestMethod.GET)
	public String fetchAllLanguageEle(HttpServletRequest request, Model model) {
		try {
			String lstLanguageEle = CommonUtility.convertToJsonString(languageService.fetchAllLanguageElements());
			return lstLanguageEle;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;		
	}
}
