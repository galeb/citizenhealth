package com.galeb.healthInfoSystem.management.menu;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import com.galeb.healthInfoSystem.data.TblMenu;
import com.galeb.healthInfoSystem.repository.MenuRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MenuServiceImpl implements MenuService{

	@Autowired
    MenuRepository menuRepo;
	
	@Override
	public TblMenu addMenu(TblMenu tblMenu) {
		tblMenu.setCreatedOn(new Date());
		tblMenu.setUpdatedOn(new Date());
		return menuRepo.save(tblMenu);
	}

	@Override
	public List<TblMenu> fetchAllMenu() {
		return menuRepo.findAll();
	}
	
	@Override
	public List<TblMenu> fetchAllChildMenu() {

    //return menuRepo.fetchAllChildMenu();
        return menuRepo.findAll();

	}
	/*public List<TblMenu> fetchAllChildMenu() {
		return menuRepo.findAll();
	}*/

	
	@Override
	public String deleteMenu(Long MenuId) {
		String response = "INVALIDMENUID";
		TblMenu tblMenu = menuRepo.getOne(MenuId);
		if(tblMenu != null) {
			menuRepo.delete(tblMenu);
				response = "DELETEDSUCCSSFULLY";
		}
		return response;
	}

	@Override
	public TblMenu fetchMenubyId(Long MenuId) {
		return menuRepo.getOne(MenuId);
	}
}
