package com.galeb.healthInfoSystem.management.menu;

import java.math.BigInteger;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.galeb.healthInfoSystem.bean.SessionBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.galeb.healthInfoSystem.common.utility.service.CommonUtility;
import com.galeb.healthInfoSystem.data.TblMenu;
import com.galeb.healthInfoSystem.management.user.UserService;

@Controller
@RequestMapping("menu")
public class MenuController {
	
	@Autowired
	MenuService menuService;
	
	@Autowired
	UserService userService;
	
	
	
	@RequestMapping(value = "/view", method = RequestMethod.GET)
	public String addPostoffice(HttpServletRequest request, Model model) {
		try {
			model.addAttribute("modelMenu", new TblMenu());
			List<TblMenu> lstMenu = menuService.fetchAllMenu();
			model.addAttribute("lstMenu", CommonUtility.convertToJsonString(lstMenu));
			model.addAttribute("lstMainMenu", lstMenu);
			return "com.healthInfo.menu";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@ResponseBody
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String saveMenu(@ModelAttribute("modelMenu") TblMenu tblMenu, HttpServletRequest request, Model model) {
		try {
			if(tblMenu!=null) {
				HttpSession httpSession = request.getSession();
				SessionBean sessionBean = (SessionBean) httpSession.getAttribute("AUTHSESSION");
				tblMenu.setCreatedBy(sessionBean.getUser());				
				menuService.addMenu(tblMenu);		
				return "SuccessFullySaved";
			}else {
				return "EmptyForm";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;		
	}
	
	@ResponseBody
	@RequestMapping(value = "/fetchAll", method = RequestMethod.GET)
	public String saveMenu(HttpServletRequest request, Model model) {
		try {
			List<TblMenu> lstMenu = menuService.fetchAllMenu();
			String strJsonLstMenu = CommonUtility.convertToJsonString(lstMenu);
			
			return strJsonLstMenu;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;		
	}
	
	@ResponseBody
	@RequestMapping(value = "/delete/{MenuId}", method = RequestMethod.GET)
	public String deleteMenu(@PathVariable("MenuId")Long MenuId,HttpServletRequest request, Model model) {
		try {
			String resposneString =menuService.deleteMenu(MenuId);			
			return resposneString;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;		
	}
	
	@ResponseBody
	@RequestMapping(value = "/edit/{MenuId}", method = RequestMethod.GET)
	public String edit(@PathVariable("MenuId")Long MenuId,HttpServletRequest request, Model model) {
		try {
			TblMenu tblMenu =menuService.fetchMenubyId(MenuId);			
			return CommonUtility.convertToJsonString(tblMenu);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;		
	}

}
