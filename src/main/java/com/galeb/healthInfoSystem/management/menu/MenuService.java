package com.galeb.healthInfoSystem.management.menu;

import java.math.BigInteger;
import java.util.List;

import com.galeb.healthInfoSystem.data.TblMenu;
import org.springframework.stereotype.Service;

@Service
public interface MenuService {

	TblMenu addMenu(TblMenu tblMenu);
	
	List<TblMenu> fetchAllMenu();
	
	String deleteMenu(Long menuId);
	
	TblMenu fetchMenubyId(Long menuId);
	
	List<TblMenu> fetchAllChildMenu();
}
