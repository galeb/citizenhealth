package com.galeb.healthInfoSystem.management.document;

import java.math.BigInteger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.galeb.healthInfoSystem.data.TblDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.galeb.healthInfoSystem.bean.SessionBean;
import com.galeb.healthInfoSystem.common.utility.service.CommonUtility;
import com.galeb.healthInfoSystem.data.TblUser;
import com.galeb.healthInfoSystem.management.user.UserService;

@Controller
@RequestMapping("document")
public class DocumentController {

	/*@Autowired
	DocumentService documentService;
	
	@Autowired
	UserService userService;
	
	@RequestMapping(value = "/view", method = RequestMethod.GET)
	public String addDocument(HttpServletRequest request, Model model) {
		try {
			model.addAttribute("modelDocument", new TblDocument());
			model.addAttribute("lstDocument", CommonUtility.convertToJsonString(documentService.fetchAllDocument()));		
			
			return "com.pims.document";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String saveDocument(@ModelAttribute("modelDocument") TblDocument tblDocument, HttpServletRequest request,
			Model model) {
		try {
			if (tblDocument != null) {

				HttpSession httpSession = request.getSession();
				SessionBean sessionBean = (SessionBean) httpSession.getAttribute("AUTHSESSION");
				TblUser tblUser = sessionBean.getUser();
				
				String fileUploadMsg = CommonUtility.uploadFile(tblDocument.getFileDoc().get(0));
				if (!"FILEEMPTY".equalsIgnoreCase(fileUploadMsg)) {
					tblDocument.setAttachment(fileUploadMsg);
				}else{
					if(tblDocument.getId() != null && "FILEEMPTY".equalsIgnoreCase(fileUploadMsg)) {
						tblDocument.setAttachment(tblDocument.getAttachment());
					}
				}
				tblDocument.setCreatedBy(tblUser);
				documentService.addDocument(tblDocument);
				model.addAttribute("response","SUCCESSFULLYSAVE");
				return "redirect:/document/view";
			} else {
				return "EmptyForm";
			}
		} catch (Exception e) {
			e.printStackTrace();
			return e.getMessage();
		}
	}
	
	@ResponseBody
	@RequestMapping(value = "/fetchAll", method = RequestMethod.GET)
	public String saveDocument(HttpServletRequest request, Model model) {
		try {
			String lstDocument = CommonUtility.convertToJsonString(documentService.fetchAllDocument());
			return lstDocument;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;		
	}
	
	@ResponseBody
	@RequestMapping(value = "/delete/{documentId}", method = RequestMethod.GET)
	public String deleteDocument(@PathVariable("documentId")BigInteger documentId,HttpServletRequest request, Model model) {
		try {
			String resposneString =documentService.deleteDocument(documentId);
			return resposneString;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;		
	}
	
	@ResponseBody
	@RequestMapping(value = "/edit/{documentId}", method = RequestMethod.GET)
	public String edit(@PathVariable("documentId")BigInteger documentId,HttpServletRequest request, Model model) {
		try {
			TblDocument tblDocument =documentService.fetchDocumentbyId(documentId);
			return CommonUtility.convertToJsonString(tblDocument);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;		
	}
	
	*/
	
}
