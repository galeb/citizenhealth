/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.galeb.healthInfoSystem.service;


import com.galeb.healthInfoSystem.data.PersonalInfo;
import com.galeb.healthInfoSystem.repository.PersonalInfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Date;
import java.util.List;


@Service
public class PersonalInfoServiceImpl implements PersonalInfoService {

    @Autowired
    PersonalInfoRepository personalInfoRepository;

    @Override
    public PersonalInfo save(PersonalInfo personalInfo) {
        //get 9 digit Personal ID
        String operation = "";
        Date date = new Date();
        /*if (personalInfo.getId() != null) {

            //   tblProduct.setUpdatedOn(date);
            //operation = OperationLogTypeEnum.UPDATE.toString();
        } else {
            personalInfo.setpID(setPersonalId());
            //   tblProduct.setCreatedOn(date);
            //operation = OperationLogTypeEnum.INSERT.toString();
        }*/
        return personalInfoRepository.save(personalInfo);
    }

    private String setPersonalId(){
        if(personalInfoRepository.getMaxPid() == null && personalInfoRepository.getMaxPid().isEmpty()){
            return "100000001";
        }else {
            Long pid = Long.valueOf(personalInfoRepository.getMaxPid());
            return String.valueOf(pid+1);
        }

    }


}
