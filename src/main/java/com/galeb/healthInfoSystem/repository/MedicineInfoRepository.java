package com.galeb.healthInfoSystem.repository;

import com.galeb.healthInfoSystem.data.MedicineInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the MedicineInfo entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MedicineInfoRepository extends JpaRepository<MedicineInfo, Long> {

}
