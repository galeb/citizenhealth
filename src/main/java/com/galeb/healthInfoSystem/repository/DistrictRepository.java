package com.galeb.healthInfoSystem.repository;

import com.galeb.healthInfoSystem.data.District;
import com.galeb.healthInfoSystem.data.Division;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data  repository for the Diagonosis entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DistrictRepository extends JpaRepository<District, Long> {
   // Optional<PersonalInfo> findOneByRegInfo(String regInfo);
    List<District> findAllByDivisionId(Long id);
    /*@Query("SELECT u FROM PersonalInfo u WHERE u.userInfo.login = :login")
    Optional<PersonalInfo> findByLogin(String login);
*/
}
