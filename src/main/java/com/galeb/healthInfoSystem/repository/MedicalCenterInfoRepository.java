package com.galeb.healthInfoSystem.repository;

import com.galeb.healthInfoSystem.data.MedicalCenterInfo;
import com.galeb.healthInfoSystem.data.TblUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data  repository for the MedicalCenterInfo entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MedicalCenterInfoRepository extends JpaRepository<MedicalCenterInfo, Long> {
    MedicalCenterInfo findOneByRegInfo(String regInfo);

    List<MedicalCenterInfo> findAllByUsers(TblUser user);

    MedicalCenterInfo findOneByUsers(TblUser user);

}
