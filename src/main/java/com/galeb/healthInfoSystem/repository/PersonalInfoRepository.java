package com.galeb.healthInfoSystem.repository;

import com.galeb.healthInfoSystem.data.PersonalInfo;
import com.galeb.healthInfoSystem.data.TblUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;


/**
 * Spring Data  repository for the PersonalInfo entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PersonalInfoRepository extends JpaRepository<PersonalInfo, Long> {

    PersonalInfo findOneByNid(String NID);

    PersonalInfo findOneByPID(String pid);

    PersonalInfo findOneByBid(String bid);

    PersonalInfo findOneByUserInfo(TblUser user);

    @Query("SELECT u FROM PersonalInfo u WHERE u.userInfo.userName = :login")
    Optional<PersonalInfo> findByLogin(String login);

    @Query("SELECT max (u.pID) FROM PersonalInfo u ")
    String getMaxPid();



}
