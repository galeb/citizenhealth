package com.galeb.healthInfoSystem.repository;

import com.galeb.healthInfoSystem.data.TreatmentDtl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;


/**
 * Spring Data  repository for the TreatmentDtl entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TreatmentDtlRepository extends JpaRepository<TreatmentDtl, Long> {

    @Query("SELECT u FROM TreatmentDtl u WHERE u.treatmentInfo.id = :id")
    List<TreatmentDtl> findByPatientId(Long id);

   // @Query("SELECT u FROM TreatmentInfo u WHERE u.patientInfo.id = :id and u.treatmentInfo.treatmentDate between :start and :end")

    @Query("SELECT u FROM TreatmentDtl u WHERE u.treatmentInfo.patientInfo.id = :id and  u.treatmentInfo.treatmentDate between :fDate and :tDate")
    List<TreatmentDtl> findByPatientIdAndDate(@Param("id")Long id, @Param("fDate")Date fDate, @Param("tDate")Date tDate);

    @Query("select new map (count(u) , u.medicineInfo.name) from TreatmentDtl u where u.treatmentInfo.treatmentDate between :fromDate and :toDate group by u.medicineInfo")
    List<?> findTotalGroupByMedicineByDate(Date fromDate, Date toDate);

}
