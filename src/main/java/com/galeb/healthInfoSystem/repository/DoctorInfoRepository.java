package com.galeb.healthInfoSystem.repository;

import com.galeb.healthInfoSystem.data.DoctorInfo;
import com.galeb.healthInfoSystem.data.PersonalInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


/**
 * Spring Data  repository for the DoctorInfo entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DoctorInfoRepository extends JpaRepository<DoctorInfo, Long> {
    DoctorInfo findOneByRegInfo(String regInfo);

    DoctorInfo findOneByPersonalInfo(PersonalInfo personalInfo);

    /*@Query("SELECT u FROM PersonalInfo u WHERE u.userInfo.login = :login")
    Optional<PersonalInfo> findByLogin(String login);
*/

}
