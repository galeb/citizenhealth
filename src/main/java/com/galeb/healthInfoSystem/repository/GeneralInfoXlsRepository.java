package com.galeb.healthInfoSystem.repository;

import com.galeb.healthInfoSystem.data.AddressInfo;
import com.galeb.healthInfoSystem.data.GeneralInfoXls;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data  repository for the Diagonosis entity.
 */
@SuppressWarnings("unused")
@Repository
public interface GeneralInfoXlsRepository extends JpaRepository<GeneralInfoXls, Long> {
   // Optional<PersonalInfo> findOneByRegInfo(String regInfo);

    //@Query("SELECT u FROM PersonalInfo u WHERE u.userInfo.login = :login")
    List<GeneralInfoXls> findAllByStatus(Boolean status);
}
