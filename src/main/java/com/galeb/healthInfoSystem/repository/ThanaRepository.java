package com.galeb.healthInfoSystem.repository;

import com.galeb.healthInfoSystem.data.District;
import com.galeb.healthInfoSystem.data.Thana;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data  repository for the Diagonosis entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ThanaRepository extends JpaRepository<Thana, Long> {
    List<Thana> findAllByDistrictId(Long id);
    /*@Query("SELECT u FROM PersonalInfo u WHERE u.userInfo.login = :login")
    Optional<PersonalInfo> findByLogin(String login);
*/
}
