package com.galeb.healthInfoSystem.repository;

import com.galeb.healthInfoSystem.data.AddressInfo;
import com.galeb.healthInfoSystem.data.TblUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data  repository for the Diagonosis entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UserRepository extends JpaRepository<TblUser, Long> {
   TblUser findByUserName(@Param("userName") String userName);
    // Optional<PersonalInfo> findOneByRegInfo(String regInfo);
    @Query("select u from TblUser u where u.userName = :userName")
    TblUser getUserByUserName(@Param("userName") String userName);
    /*@Query("SELECT u FROM PersonalInfo u WHERE u.userInfo.login = :login")
    Optional<PersonalInfo> findByLogin(String login);
*/
}
