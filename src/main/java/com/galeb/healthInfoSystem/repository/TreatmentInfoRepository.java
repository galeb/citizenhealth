package com.galeb.healthInfoSystem.repository;

import com.galeb.healthInfoSystem.data.TreatmentInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;


/**
 * Spring Data  repository for the TreatmentInfo entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TreatmentInfoRepository extends JpaRepository<TreatmentInfo, Long> {

    @Query("SELECT u FROM TreatmentInfo u WHERE u.patientInfo.id = :id")
    List<TreatmentInfo> findByPatientId(@Param("id") Long id);

    @Query("SELECT u FROM TreatmentInfo u WHERE u.patientInfo.id = :id and u.treatmentDate between :fDate and :tDate")
    List<TreatmentInfo> findByPatientIdAndDate(@Param("id") Long id, @Param("fDate") Date fDate, @Param("tDate") Date tDate);


    @Query("SELECT u FROM TreatmentInfo u WHERE u.doctorInfo.id = :id and u.treatmentDate between :fDate and :tDate")
    List<TreatmentInfo> findByDoctorIdAndDate(@Param("id") Long id, @Param("fDate") Date fDate, @Param("tDate") Date tDate);

     @Query("SELECT u FROM TreatmentInfo u WHERE upper(u.disease) like  :disease and u.treatmentDate between :fDate and :tDate")
    List<TreatmentInfo> findByDiseaseAndDate(@Param("disease") String disease, @Param("fDate") Date fDate, @Param("tDate") Date tDate);

    @Query("SELECT u FROM TreatmentInfo u WHERE u.medicalCenterInfo.id = :id and u.treatmentDate between :fDate and :tDate")
    List<TreatmentInfo> findByMedicalCenterIdAndDate(@Param("id") Long id, @Param("fDate") Date fDate, @Param("tDate") Date tDate);

    @Query("SELECT u FROM TreatmentInfo u WHERE u.treatmentDate between :fromDate and :toDate")
    TreatmentInfo findByTwoDate(Date fromDate, Date toDate);

    @Query("SELECT count(u) FROM TreatmentInfo u WHERE u.treatmentDate between :fromDate and :toDate")
    Integer findTotalPatientByTwoDate(@Param("fromDate") Date fromDate, @Param("toDate") Date toDate);

    @Query("SELECT count(u.id) FROM TreatmentInfo u WHERE u.treatmentDate between :fromDate and :toDate and u.patientInfo.gender = :gender")
    Integer findTotalPatientByTwoDateAndGender(@Param("fromDate") Date fromDate,@Param("toDate") Date toDate,@Param("gender") String gender);

    @Query("select new map(u.patientInfo.gender as gender,count(u) as total ) from TreatmentInfo u where u.treatmentDate between :fromDate and :toDate  group by u.patientInfo.gender")
    List<?> findTotalGroupByGender(@Param("fromDate") Date fromDate,@Param("toDate")  Date toDate);

    @Query("SELECT count(distinct u.doctorInfo) FROM TreatmentInfo u WHERE u.treatmentDate between :fromDate and :toDate")
    Integer findTotalDoctorPatientByTwoDate(@Param("fromDate") Date fromDate, @Param("toDate") Date toDate);

    @Query("select new map(u.doctorInfo.personalInfo.gender as gender,count(u) as total ) from TreatmentInfo u where u.treatmentDate between :fromDate and :toDate  group by u.doctorInfo.personalInfo.gender")
    List<?> findTotalDoctorGroupByGender(@Param("fromDate") Date fromDate,@Param("toDate")  Date toDate);

    @Query("select new map(u.patientInfo.division.name as division, count(u) as total ) from TreatmentInfo u where u.treatmentDate between :fromDate and :toDate  group by u.patientInfo.division.name")
    List<?> findPatientGroupByDivision(@Param("fromDate") Date fromDate,@Param("toDate")  Date toDate);

    @Query("select new map(u.patientInfo.district.name as district,count(u) as total ) from TreatmentInfo u where u.treatmentDate between :fromDate and :toDate  group by u.patientInfo.district.name")
    List<?> findPatientGroupByDistrict(@Param("fromDate") Date fromDate,@Param("toDate")  Date toDate);

    @Query("select new map(u.patientInfo.thana.name as thana,count(u) as total ) from TreatmentInfo u where u.treatmentDate between :fromDate and :toDate  group by u.patientInfo.thana.name")
    List<?> findPatientGroupByThana(@Param("fromDate") Date fromDate,@Param("toDate")  Date toDate);

    @Query("select new map(u.medicalCenterInfo.name,count(u) ) from TreatmentInfo u where u.treatmentDate between :fromDate and :toDate  group by u.medicalCenterInfo")
    List<?> findTotalGroupByMedicalCenterByDate(Date fromDate, Date toDate);

    @Query("select new map (count(u) , u.medicalCenterInfo.name) from TreatmentInfo u where u.treatmentDate between :fromDate and :toDate  group by u.patientInfo.address")
    List<?> findGroupByAddressByDate(Date fromDate, Date toDate);

    @Query("select distinct u.disease from TreatmentInfo u order by u.disease")
    List<String> findDiseaseList();
}
