package com.galeb.healthInfoSystem.repository;

import com.galeb.healthInfoSystem.data.AddressInfo;
import com.galeb.healthInfoSystem.data.TblRoleMenuMapping;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data  repository for the Diagonosis entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RoleMenuRepository extends JpaRepository<TblRoleMenuMapping, Long> {
   // Optional<PersonalInfo> findOneByRegInfo(String regInfo);

    @Query("SELECT u FROM TblRoleMenuMapping u WHERE u.rolePk = :rolePk")
    List<TblRoleMenuMapping> findAllByRolePk(@Param("rolePk") Long rolePk);
}
