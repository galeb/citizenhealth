package com.galeb.healthInfoSystem.repository;

import com.galeb.healthInfoSystem.data.Diagonosis;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Diagonosis entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DiagonosisRepository extends JpaRepository<Diagonosis, Long> {
   // Optional<PersonalInfo> findOneByRegInfo(String regInfo);

    /*@Query("SELECT u FROM PersonalInfo u WHERE u.userInfo.login = :login")
    Optional<PersonalInfo> findByLogin(String login);
*/
}
