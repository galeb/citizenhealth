package com.galeb.healthInfoSystem.repository;

import com.galeb.healthInfoSystem.data.AddressInfo;
import com.galeb.healthInfoSystem.data.ShasthoBarta;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data  repository for the Diagonosis entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ShasthoBartaRepository extends JpaRepository<ShasthoBarta, Long> {
   // Optional<PersonalInfo> findOneByRegInfo(String regInfo);

    List<ShasthoBarta> findAllByIsActive(int val);
    /*@Query("SELECT u FROM PersonalInfo u WHERE u.userInfo.login = :login")
    Optional<PersonalInfo> findByLogin(String login);
*/
}
