package com.galeb.healthInfoSystem.repository;

import com.galeb.healthInfoSystem.data.TreatmentDiagoDtl;
import com.galeb.healthInfoSystem.data.TreatmentDtl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;


/**
 * Spring Data  repository for the TreatmentDiagoDtl entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TreatmentDiagoDtlRepository extends JpaRepository<TreatmentDiagoDtl, Long> {
    @Query("SELECT u FROM TreatmentDiagoDtl u WHERE u.patientInfo.id = :id")
    List<TreatmentDiagoDtl> findByPatientId(Long id);

    @Query("SELECT u FROM TreatmentDiagoDtl u WHERE u.treatmentInfo.patientInfo.id = :id and  u.treatmentInfo.treatmentDate between :fDate and :tDate")
    List<TreatmentDiagoDtl> findByPatientIdAndDate(@Param("id") Long id, @Param("fDate") Date fDate, @Param("tDate") Date tDate);


    @Query("select new map(count(u) , u.diagonosis.name) from TreatmentDiagoDtl u where u.testDate between :fromDate and :toDate group by u.doctorInfo")
    List<?> findTotalGroupByTestCenterByDate(Date fromDate, Date toDate);

}
