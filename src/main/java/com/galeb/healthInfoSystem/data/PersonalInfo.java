package com.galeb.healthInfoSystem.data;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.JoinColumnOrFormula;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * A PersonalInfo.
 */
@Entity
@Table(name = "personal_info")
//@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class PersonalInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "general_info_sequence")
    @SequenceGenerator(name = "general_info_sequence", sequenceName = "general_info_sequence", allocationSize = 1, initialValue = 1)
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "father_name")
    private String fatherName;


    @Column(name = "mother_name")
    private String motherName;

    @Column(name = "nid")
    private String nid;

    @Column(name = "bid")
    private String bid;

    @Column(name = "PID")
    private String pID;

    @Column(name = "contact_no")
    private String contactNo;

    @Column(name = "address")
    private String address;

    @Column(name = "email")
    private String email;

    @Column(name = "height")
    private String height;

    @Column(name = "weight")
    private String weight;

    @Column(name = "dob")
    @DateTimeFormat(pattern = "dd/MM/yyyy")
   // @Temporal(TemporalType.DATE)
    @Temporal(TemporalType.TIMESTAMP)
    private  Date dob;

    @Column(name = "gender")
    private String gender;

    @Column(name = "houseVillage")
    private String houseVillage;

    @Column(name = "active_status")
    private Boolean activeStatus;

    @Column(name = "create_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;

    @Column(name = "create_by")
    private Long createBy;

    @Column(name = "update_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;

    @Column(name = "update_by")
    private Long updateBy;

    @ManyToOne
    @JsonIgnoreProperties("personalInfos")
    private TblUser userInfo;


    @ManyToOne
    @JoinColumn(name = "division_id")
    private Division division;


    @ManyToOne
    @JoinColumn(name = "district_id")
    private District district;


    @ManyToOne
    @JoinColumn(name = "thana_id")
    private Thana thana;

    @Transient
    private AddressInfo addressInfo;
    //@OneToMany(mappedBy = "personalInfo", cascade = CascadeType.ALL)
    @Transient
    private List<TreatmentInfo> treatmentInfos = new ArrayList();


    //@OneToMany(mappedBy = "personalInfo", cascade = CascadeType.ALL)
    @Transient
    private List<AddressInfo> addressInfos = new ArrayList();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public PersonalInfo name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNid() {
        return nid;
    }

    public PersonalInfo nid(String nid) {
        this.nid = nid;
        return this;
    }

    public void setNid(String nid) {
        this.nid = nid;
    }

    public String getBid() {
        return bid;
    }

    public PersonalInfo bid(String bid) {
        this.bid = bid;
        return this;
    }

    public void setBid(String bid) {
        this.bid = bid;
    }

    public String getpID() {
        return pID;
    }

    public void setpID(String pID) {
        this.pID = pID;
    }

    public String getContactNo() {
        return contactNo;
    }

    public PersonalInfo contactNo(String contactNo) {
        this.contactNo = contactNo;
        return this;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getAddress() {
        return address;
    }

    public PersonalInfo address(String address) {
        this.address = address;
        return this;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public String getMotherName() {
        return motherName;
    }

    public void setMotherName(String motherName) {
        this.motherName = motherName;
    }

    public String getEmail() {
        return email;
    }

    public PersonalInfo email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getHeight() {
        return height;
    }

    public PersonalInfo height(String height) {
        this.height = height;
        return this;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getWeight() {
        return weight;
    }

    public PersonalInfo weight(String weight) {
        this.weight = weight;
        return this;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getGender() {
        return gender;
    }

    public PersonalInfo gender(String gender) {
        this.gender = gender;
        return this;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Boolean isActiveStatus() {
        return activeStatus;
    }

    public PersonalInfo activeStatus(Boolean activeStatus) {
        this.activeStatus = activeStatus;
        return this;
    }

    public void setActiveStatus(Boolean activeStatus) {
        this.activeStatus = activeStatus;
    }


    public Long getCreateBy() {
        return createBy;
    }

    public PersonalInfo createBy(Long createBy) {
        this.createBy = createBy;
        return this;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public PersonalInfo updateBy(Long updateBy) {
        this.updateBy = updateBy;
        return this;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public TblUser getUserInfo() {
        return userInfo;
    }

    public PersonalInfo userInfo(TblUser User) {
        this.userInfo = User;
        return this;
    }

    public Boolean getActiveStatus() {
        return activeStatus;
    }

    public List<TreatmentInfo> getTreatmentInfos() {
        return treatmentInfos;
    }

    public void setTreatmentInfos(List<TreatmentInfo> treatmentInfos) {
        this.treatmentInfos = treatmentInfos;
    }

    public List<AddressInfo> getAddressInfos() {
        return addressInfos;
    }

    public void setAddressInfos(List<AddressInfo> addressInfos) {
        this.addressInfos = addressInfos;
    }

    public void setUserInfo(TblUser User) {
        this.userInfo = User;
    }

    public AddressInfo getAddressInfo() {
        return addressInfo;
    }

    public void setAddressInfo(AddressInfo addressInfo) {
        this.addressInfo = addressInfo;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Division getDivision() {
        return division;
    }

    public void setDivision(Division division) {
        this.division = division;
    }

    public District getDistrict() {
        return district;
    }

    public void setDistrict(District district) {
        this.district = district;
    }

    public Thana getThana() {
        return thana;
    }

    public void setThana(Thana thana) {
        this.thana = thana;
    }

    public String getHouseVillage() {
        return houseVillage;
    }

    public void setHouseVillage(String houseVillage) {
        this.houseVillage = houseVillage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PersonalInfo personalInfo = (PersonalInfo) o;
        if (personalInfo.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), personalInfo.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PersonalInfo{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", nid='" + getNid() + "'" +
            ", bid='" + getBid() + "'" +
            ", contactNo='" + getContactNo() + "'" +
            ", address='" + getAddress() + "'" +
            ", email='" + getEmail() + "'" +
            ", height='" + getHeight() + "'" +
            ", weight='" + getWeight() + "'" +
            ", dob='" + getDob() + "'" +
            ", gender='" + getGender() + "'" +
            ", activeStatus='" + isActiveStatus() + "'" +
            ", createDate='" + getCreateDate() + "'" +
            ", createBy=" + getCreateBy() +
            ", updateDate='" + getUpdateDate() + "'" +
            ", updateBy=" + getUpdateBy() +
            "}";
    }

}
