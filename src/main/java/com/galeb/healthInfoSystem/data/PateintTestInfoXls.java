package com.galeb.healthInfoSystem.data;


import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * A PersonalInfo.
 */
@Entity
@Table(name = "xls_patient_test")
//@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class PateintTestInfoXls implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "xls_patient_sequence")
    @SequenceGenerator(name = "xls_patient_sequence", sequenceName = "xls_patient_sequence", allocationSize = 1, initialValue = 1)
    private Long id;

    
    @Column(name = "p_name")
    private String patientName;

    @Column(name = "p_father_name")
    private String patientFatherName;


    @Column(name = "p_mother_name")
    private String patientMotherName;

    @Column(name = "p_nid")
    private String patientNid;

    @Column(name = "p_bid")
    private String patientBid;

    @Column(name = "P_ID")
    private String patientID;

    @Column(name = "p_contact_no")
    private String patientContactNo;

    @Column(name = "p_address")
    private String patientAddress;

    @Column(name = "p_email")
    private String patientEmail;

    @Column(name = "height")
    private String height;

    @Column(name = "weight")
    private String weight;

    @Column(name = "p_dob")
    private String pateintDob;

    @NotNull
    @Column(name = "p_gender")
    private String patientGender;

    @Column(name = "create_date")
    @Temporal(TemporalType.TIMESTAMP)    private Date createDate;

    @Column(name = "create_by")
    private Long createBy;

    @Column(name = "update_date")
    @Temporal(TemporalType.TIMESTAMP)    private Date updateDate;

    @Column(name = "update_by")
    private Long updateBy;

    @NotNull
    @Column(name = "d_name")
    private String doctorName;

    @Column(name = "d_contact_no")
    private String doctorContactNo;

    @Column(name = "d_address")
    private String doctorAddress;

    @Column(name = "d_email")
    private String doctoremail;

    @Column(name = "d_specility")
    private String doctorSpecility;

    @Column(name = "d_department")
    private String doctorDepartment;

    @Column(name = "d_reg_info")
    private String doctorRegInfo;

    @Column(name = "test_details")
    private String testDetails;

    @Column(name = "result")
    private String result;

    @Column(name = "remarks")
    private String remarks;

    @Column(name = "diago_date")
    private String diagoDate;

    @Column(name = "diago_result_date")
    private String diagoResultDate;

    @Column(name = "mc_reg_info")
    private String medicalRegInfo;

    @Column(name = "status")
    private Boolean status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getPatientFatherName() {
        return patientFatherName;
    }

    public void setPatientFatherName(String patientFatherName) {
        this.patientFatherName = patientFatherName;
    }

    public String getPatientMotherName() {
        return patientMotherName;
    }

    public void setPatientMotherName(String patientMotherName) {
        this.patientMotherName = patientMotherName;
    }

    public String getPatientNid() {
        return patientNid;
    }

    public void setPatientNid(String patientNid) {
        this.patientNid = patientNid;
    }

    public String getPatientBid() {
        return patientBid;
    }

    public void setPatientBid(String patientBid) {
        this.patientBid = patientBid;
    }

    public String getPatientID() {
        return patientID;
    }

    public void setPatientID(String patientID) {
        this.patientID = patientID;
    }

    public String getPatientContactNo() {
        return patientContactNo;
    }

    public void setPatientContactNo(String patientContactNo) {
        this.patientContactNo = patientContactNo;
    }

    public String getPatientAddress() {
        return patientAddress;
    }

    public void setPatientAddress(String patientAddress) {
        this.patientAddress = patientAddress;
    }

    public String getPatientEmail() {
        return patientEmail;
    }

    public void setPatientEmail(String patientEmail) {
        this.patientEmail = patientEmail;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getPateintDob() {
        return pateintDob;
    }

    public void setPateintDob(String pateintDob) {
        this.pateintDob = pateintDob;
    }

    public String getPatientGender() {
        return patientGender;
    }

    public void setPatientGender(String patientGender) {
        this.patientGender = patientGender;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public String getDoctorContactNo() {
        return doctorContactNo;
    }

    public void setDoctorContactNo(String doctorContactNo) {
        this.doctorContactNo = doctorContactNo;
    }

    public String getDoctorAddress() {
        return doctorAddress;
    }

    public void setDoctorAddress(String doctorAddress) {
        this.doctorAddress = doctorAddress;
    }

    public String getDoctoremail() {
        return doctoremail;
    }

    public void setDoctoremail(String doctoremail) {
        this.doctoremail = doctoremail;
    }

    public String getDoctorSpecility() {
        return doctorSpecility;
    }

    public void setDoctorSpecility(String doctorSpecility) {
        this.doctorSpecility = doctorSpecility;
    }

    public String getDoctorDepartment() {
        return doctorDepartment;
    }

    public void setDoctorDepartment(String doctorDepartment) {
        this.doctorDepartment = doctorDepartment;
    }

    public String getDoctorRegInfo() {
        return doctorRegInfo;
    }

    public void setDoctorRegInfo(String doctorRegInfo) {
        this.doctorRegInfo = doctorRegInfo;
    }

    public String getTestDetails() {
        return testDetails;
    }

    public void setTestDetails(String testDetails) {
        this.testDetails = testDetails;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getDiagoDate() {
        return diagoDate;
    }

    public void setDiagoDate(String diagoDate) {
        this.diagoDate = diagoDate;
    }

    public String getDiagoResultDate() {
        return diagoResultDate;
    }

    public void setDiagoResultDate(String diagoResultDate) {
        this.diagoResultDate = diagoResultDate;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getMedicalRegInfo() {
        return medicalRegInfo;
    }

    public void setMedicalRegInfo(String medicalRegInfo) {
        this.medicalRegInfo = medicalRegInfo;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }
}
