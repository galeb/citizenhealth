package com.galeb.healthInfoSystem.data;

import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="TBL_ROLEMENUMAP")
public class TblRoleMenuMapping {

	 @Id
	 @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "role_menu_sequence")
	 @SequenceGenerator(name = "role_menu_sequence", sequenceName = "role_menu_sequence", allocationSize = 1, initialValue = 1)
	 private Long id;
	
	
	 @Column(name = "ROLE_PK")
	 private Long rolePk;
	 
	 
	 @Column(name = "MENU_PK")
	 private Long menuPk;
	 
	 @Column(name = "IS_ACTIVE")
	 private int isActive;
	 
	 @Column(name = "CREATED_ON")
	 private Date createdOn;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getRolePk() {
		return rolePk;
	}

	public void setRolePk(Long rolePk) {
		this.rolePk = rolePk;
	}

	public Long getMenuPk() {
		return menuPk;
	}

	public void setMenuPk(Long menuPk) {
		this.menuPk = menuPk;
	}

	public int getIsActive() {
		return isActive;
	}

	public void setIsActive(int isActive) {
		this.isActive = isActive;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
}
