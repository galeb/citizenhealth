package com.galeb.healthInfoSystem.data;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A TreatmentInfo.
 */
@Entity
@Table(name = "treatment_info")
//@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class TreatmentInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "treatment_info_sequence")
    @SequenceGenerator(name = "treatment_info_sequence", sequenceName = "treatment_info_sequence", allocationSize = 1, initialValue = 1)
    private Long id;

    @NotNull
    @Column(name = "symptom", nullable = false)
    private String symptom;

    @Column(name = "contact_no")
    private String contactNo;

    @Column(name = "disease")
    private String disease;

    @Column(name = "remarks")
    private String remarks;

    @Lob
    @Column(name = "attachment")
    private byte[] attachmentContentBlob;

    @Column(name = "attachment_type")
    private String attachmentContentBlobContentType;

    @Column(name = "active_status")
    private Boolean activeStatus;

    @Column(name = "treatment_date")
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    @Temporal(TemporalType.TIMESTAMP)
    private Date treatmentDate;


    @Column(name = "create_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;

    @Column(name = "create_by")
    private Long createBy;

    @Column(name = "update_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;

    @Column(name = "update_by")
    private Long updateBy;

    @ManyToOne
    @JsonIgnoreProperties("treatmentInfos")
    private PersonalInfo patientInfo;

    @ManyToOne
    @JsonIgnoreProperties("treatmentInfos")
    private DoctorInfo doctorInfo;

    @ManyToOne
    @JsonIgnoreProperties("treatmentInfos")
    private MedicalCenterInfo medicalCenterInfo;

    @OneToMany(cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            mappedBy = "treatmentInfo")
    @JsonManagedReference
    private Set<TreatmentDtl> treatmentDtls = new HashSet<>();

    @OneToMany(cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            mappedBy = "treatmentInfo")
    @JsonManagedReference
    private Set<TreatmentDiagoDtl> diagoDtls = new HashSet<>();


    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSymptom() {
        return symptom;
    }

    public TreatmentInfo symptom(String symptom) {
        this.symptom = symptom;
        return this;
    }

    public void setSymptom(String symptom) {
        this.symptom = symptom;
    }

    public String getContactNo() {
        return contactNo;
    }

    public TreatmentInfo contactNo(String contactNo) {
        this.contactNo = contactNo;
        return this;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getDisease() {
        return disease;
    }

    public TreatmentInfo disease(String disease) {
        this.disease = disease;
        return this;
    }

    public void setDisease(String disease) {
        this.disease = disease;
    }

    public String getRemarks() {
        return remarks;
    }

    public TreatmentInfo remarks(String remarks) {
        this.remarks = remarks;
        return this;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public byte[] getAttachmentContentBlob() {
        return attachmentContentBlob;
    }

    public TreatmentInfo attachmentContentBlob(byte[] attachmentContentBlob) {
        this.attachmentContentBlob = attachmentContentBlob;
        return this;
    }

    public void setAttachmentContentBlob(byte[] attachmentContentBlob) {
        this.attachmentContentBlob = attachmentContentBlob;
    }

    public String getAttachmentContentBlobContentType() {
        return attachmentContentBlobContentType;
    }

    public TreatmentInfo attachmentContentBlobContentType(String attachmentContentBlobContentType) {
        this.attachmentContentBlobContentType = attachmentContentBlobContentType;
        return this;
    }

    public void setAttachmentContentBlobContentType(String attachmentContentBlobContentType) {
        this.attachmentContentBlobContentType = attachmentContentBlobContentType;
    }

    public Boolean isActiveStatus() {
        return activeStatus;
    }

    public TreatmentInfo activeStatus(Boolean activeStatus) {
        this.activeStatus = activeStatus;
        return this;
    }

    public void setActiveStatus(Boolean activeStatus) {
        this.activeStatus = activeStatus;
    }


    public Long getCreateBy() {
        return createBy;
    }

    public TreatmentInfo createBy(Long createBy) {
        this.createBy = createBy;
        return this;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }


    public Long getUpdateBy() {
        return updateBy;
    }

    public TreatmentInfo updateBy(Long updateBy) {
        this.updateBy = updateBy;
        return this;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public PersonalInfo getPatientInfo() {
        return patientInfo;
    }

    public TreatmentInfo patientInfo(PersonalInfo PersonalInfo) {
        this.patientInfo = PersonalInfo;
        return this;
    }

    public void setPatientInfo(PersonalInfo PersonalInfo) {
        this.patientInfo = PersonalInfo;
    }

    public DoctorInfo getDoctorInfo() {
        return doctorInfo;
    }

    public TreatmentInfo doctorInfo(DoctorInfo DoctorInfo) {
        this.doctorInfo = DoctorInfo;
        return this;
    }

    public void setDoctorInfo(DoctorInfo DoctorInfo) {
        this.doctorInfo = DoctorInfo;
    }

    public MedicalCenterInfo getMedicalCenterInfo() {
        return medicalCenterInfo;
    }

    public TreatmentInfo medicalCenterInfo(MedicalCenterInfo MedicalCenterInfo) {
        this.medicalCenterInfo = MedicalCenterInfo;
        return this;
    }

    public void setMedicalCenterInfo(MedicalCenterInfo MedicalCenterInfo) {
        this.medicalCenterInfo = MedicalCenterInfo;
    }

    public Boolean getActiveStatus() {
        return activeStatus;
    }

    public Date getTreatmentDate() {
        return treatmentDate;
    }

    public void setTreatmentDate(Date treatmentDate) {
        this.treatmentDate = treatmentDate;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Set<TreatmentDtl> getTreatmentDtls() {
        return treatmentDtls;
    }

    public Set<TreatmentDiagoDtl> getDiagoDtls() {
        return diagoDtls;
    }

    public void setDiagoDtls(Set<TreatmentDiagoDtl> diagoDtls) {
        this.diagoDtls = diagoDtls;
    }

    public void setTreatmentDtls(Set<TreatmentDtl> treatmentDtls) {
        this.treatmentDtls = treatmentDtls;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TreatmentInfo treatmentInfo = (TreatmentInfo) o;
        if (treatmentInfo.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), treatmentInfo.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "TreatmentInfo{" +
            "id=" + getId() +
            ", symptom='" + getSymptom() + "'" +
            ", contactNo='" + getContactNo() + "'" +
            ", disease='" + getDisease() + "'" +
            ", remarks='" + getRemarks() + "'" +
            ", attachmentContentBlob='" + getAttachmentContentBlob() + "'" +
            ", attachmentContentBlobContentType='" + getAttachmentContentBlobContentType() + "'" +
            ", activeStatus='" + isActiveStatus() + "'" +
            ", createDate='" + getCreateDate() + "'" +
            ", createBy=" + getCreateBy() +
            ", updateDate='" + getUpdateDate() + "'" +
            ", updateBy=" + getUpdateBy() +
            "}";
    }
}
