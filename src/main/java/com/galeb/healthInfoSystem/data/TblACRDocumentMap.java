/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.galeb.healthInfoSystem.data;

import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author ITMCS-1
 */
//@Entity
//@Table(name="TBL_ACRDOCMAP")
public class TblACRDocumentMap {
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
//    @SequenceGenerator(name = "ACRDOCPK", sequenceName = "ACRDOC_SEQ")
    private Long id;
    
    @ManyToOne
    @JoinColumn(name = "ACR_PK")
    private TblACR acrPk;
    
    @Column(name = "DOCUMENT_TITLE")
    private String documentTitle;
    
    @Column(name = "ATTACHMENT")
    private String attachment;
    
    @Column(name = "IS_ACTIVE")
    private int isActive;
    
    @ManyToOne
    @JoinColumn(name = "CREATED_BY")
    private TblUser createdBy;

    @Column(name = "CREATED_ON")
    private Date createdOn;

    @ManyToOne
    @JoinColumn(name = "UPDATED_BY")
    private TblUser updatedBy;

    @Column(name = "UPDATED_ON")
    private Date updatedOn;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TblACR getAcrPk() {
        return acrPk;
    }

    public void setAcrPk(TblACR acrPk) {
        this.acrPk = acrPk;
    }

    
    public String getDocumentTitle() {
		return documentTitle;
	}

	public void setDocumentTitle(String documentTitle) {
		this.documentTitle = documentTitle;
	}

	public String getAttachment() {
		return attachment;
	}

	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}

	public TblUser getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(TblUser createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public TblUser getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(TblUser updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }
    
}
