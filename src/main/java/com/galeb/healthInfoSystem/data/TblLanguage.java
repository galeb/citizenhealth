package com.galeb.healthInfoSystem.data;

import java.math.BigInteger;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="LANGUAGEMAP")
public class TblLanguage {
	
	 @Id
	 @GeneratedValue(strategy = GenerationType.SEQUENCE)
//	 @SequenceGenerator(name = "LANGAUGEMAPPK", sequenceName = "LANGUAGEMAP_SEQ")
	 private Long id;
	 
	 
	 @Column(name = "ENGLISH_NAME")
	 private String englishName;
	
	 @Column(name = "BANGALI_NAME")
	 private String bengaliName;
	 
	 @Column(name = "IS_ACTIVE")
	 private int isActive;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEnglishName() {
		return englishName;
	}

	public void setEnglishName(String englishName) {
		this.englishName = englishName;
	}

	public String getBengaliName() {
		return bengaliName;
	}

	public void setBengaliName(String bengaliName) {
		this.bengaliName = bengaliName;
	}

	public int getIsActive() {
		return isActive;
	}

	public void setIsActive(int isActive) {
		this.isActive = isActive;
	}
}
