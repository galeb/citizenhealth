/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.galeb.healthInfoSystem.data;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author ITMCS-1
 */
@Entity
@Table(name="LANGUAGE_INFO")
public class TblLanguageInfo implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
//    @SequenceGenerator(name = "LANGUAGEINFOPK", sequenceName = "LANGUAGEINFO_SEQ")
    private Long id;
    
    @Column(name = "LANGUAGE")
    private String language;
    
    @Column(name = "PROFICIENCY_LEVEL")
    private String proficiencylevel;
    
    @Column(name = "REMARK")
    private String remarks;
    
    @ManyToOne
    @JoinColumn(name = "CREATED_BY")
    private TblUser createdBy;

    @Column(name = "CREATED_ON")
    private Date createdOn;

    @ManyToOne
    @JoinColumn(name = "UPDATED_BY")
    private TblUser updatedBy;

    @Column(name = "UPDATED_ON")
    private Date updatedOn;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getProficiencylevel() {
        return proficiencylevel;
    }

    public void setProficiencylevel(String proficiencylevel) {
        this.proficiencylevel = proficiencylevel;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public TblUser getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(TblUser createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public TblUser getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(TblUser updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }
}
