package com.galeb.healthInfoSystem.data;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 * A TreatmentDtl.
 */
@Entity
@Table(name = "treatment_dtl")
//@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class TreatmentDtl implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "treatment_dtl_sequence")
    @SequenceGenerator(name = "treatment_dtl_sequence", sequenceName = "treatment_dtl_sequence", allocationSize = 1, initialValue = 1)
    private Long id;

    @NotNull
    @Column(name = "doses", nullable = false)
    private String doses;

    @Column(name = "duration")
    private String duration;

    @Column(name = "remarks")
    private String remarks;

    @Column(name = "active_status")
    private Boolean activeStatus;

    @Column(name = "create_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;

    @Column(name = "create_by")
    private Long createBy;

    @Column(name = "update_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;

    @Column(name = "update_by")
    private Long updateBy;

    @ManyToOne
    @JsonBackReference
    private TreatmentInfo treatmentInfo;

    @ManyToOne
    //@JsonIgnoreProperties("treatmentDtls")
    private MedicineInfo medicineInfo;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDoses() {
        return doses;
    }

    public TreatmentDtl doses(String doses) {
        this.doses = doses;
        return this;
    }

    public void setDoses(String doses) {
        this.doses = doses;
    }

    public String getDuration() {
        return duration;
    }

    public TreatmentDtl duration(String duration) {
        this.duration = duration;
        return this;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getRemarks() {
        return remarks;
    }

    public TreatmentDtl remarks(String remarks) {
        this.remarks = remarks;
        return this;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Boolean isActiveStatus() {
        return activeStatus;
    }

    public TreatmentDtl activeStatus(Boolean activeStatus) {
        this.activeStatus = activeStatus;
        return this;
    }

    public void setActiveStatus(Boolean activeStatus) {
        this.activeStatus = activeStatus;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public TreatmentDtl createBy(Long createBy) {
        this.createBy = createBy;
        return this;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public TreatmentDtl updateBy(Long updateBy) {
        this.updateBy = updateBy;
        return this;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public TreatmentInfo getTreatmentInfo() {
        return treatmentInfo;
    }

    public TreatmentDtl treatmentInfo(TreatmentInfo TreatmentInfo) {
        this.treatmentInfo = TreatmentInfo;
        return this;
    }

    public void setTreatmentInfo(TreatmentInfo TreatmentInfo) {
        this.treatmentInfo = TreatmentInfo;
    }

    public MedicineInfo getMedicineInfo() {
        return medicineInfo;
    }

    public TreatmentDtl medicineInfo(MedicineInfo MedicineInfo) {
        this.medicineInfo = MedicineInfo;
        return this;
    }

    public Boolean getActiveStatus() {
        return activeStatus;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public void setMedicineInfo(MedicineInfo MedicineInfo) {
        this.medicineInfo = MedicineInfo;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TreatmentDtl treatmentDtl = (TreatmentDtl) o;
        if (treatmentDtl.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), treatmentDtl.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "TreatmentDtl{" +
            "id=" + getId() +
            ", doses='" + getDoses() + "'" +
            ", duration='" + getDuration() + "'" +
            ", remarks='" + getRemarks() + "'" +
            ", activeStatus='" + isActiveStatus() + "'" +
            ", createDate='" + getCreateDate() + "'" +
            ", createBy=" + getCreateBy() +
            ", updateDate='" + getUpdateDate() + "'" +
            ", updateBy=" + getUpdateBy() +
            "}";
    }
}
