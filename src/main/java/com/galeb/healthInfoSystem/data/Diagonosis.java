package com.galeb.healthInfoSystem.data;


import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 * A Diagonosis.
 */
@Entity
@Table(name = "diagonosis")
//@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Diagonosis implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "diagnosis_sequence")
    @SequenceGenerator(name = "diagnosis_sequence", sequenceName = "diagnosis_sequence", allocationSize = 1, initialValue = 1)
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "min_value")
    private String minValue;

    @Column(name = "min_value_male")
    private String minValueMale;

    @Column(name = "min_value_female")
    private String minValueFemale;

    @Column(name = "max_value")
    private String maxValue;

    @Column(name = "max_value_male")
    private String maxValueMale;

    @Column(name = "max_value_female")
    private String maxValueFemale;

    @Column(name = "jhi_type")
    private String type;

    @Column(name = "remarks")
    private String remarks;

    @Column(name = "active_status")
    private Boolean activeStatus;

    @Column(name = "create_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;

    @Column(name = "create_by")
    private Long createBy;

    @Column(name = "update_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;

    @Column(name = "update_by")
    private Long updateBy;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Diagonosis name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public Diagonosis description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMinValue() {
        return minValue;
    }

    public Diagonosis minValue(String minValue) {
        this.minValue = minValue;
        return this;
    }

    public void setMinValue(String minValue) {
        this.minValue = minValue;
    }

    public String getMinValueMale() {
        return minValueMale;
    }

    public Diagonosis minValueMale(String minValueMale) {
        this.minValueMale = minValueMale;
        return this;
    }

    public void setMinValueMale(String minValueMale) {
        this.minValueMale = minValueMale;
    }

    public String getMinValueFemale() {
        return minValueFemale;
    }

    public Diagonosis minValueFemale(String minValueFemale) {
        this.minValueFemale = minValueFemale;
        return this;
    }

    public void setMinValueFemale(String minValueFemale) {
        this.minValueFemale = minValueFemale;
    }

    public String getMaxValue() {
        return maxValue;
    }

    public Diagonosis maxValue(String maxValue) {
        this.maxValue = maxValue;
        return this;
    }

    public void setMaxValue(String maxValue) {
        this.maxValue = maxValue;
    }

    public String getMaxValueMale() {
        return maxValueMale;
    }

    public Diagonosis maxValueMale(String maxValueMale) {
        this.maxValueMale = maxValueMale;
        return this;
    }

    public void setMaxValueMale(String maxValueMale) {
        this.maxValueMale = maxValueMale;
    }

    public String getMaxValueFemale() {
        return maxValueFemale;
    }

    public Diagonosis maxValueFemale(String maxValueFemale) {
        this.maxValueFemale = maxValueFemale;
        return this;
    }

    public void setMaxValueFemale(String maxValueFemale) {
        this.maxValueFemale = maxValueFemale;
    }

    public String getType() {
        return type;
    }

    public Diagonosis type(String type) {
        this.type = type;
        return this;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getRemarks() {
        return remarks;
    }

    public Diagonosis remarks(String remarks) {
        this.remarks = remarks;
        return this;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Boolean isActiveStatus() {
        return activeStatus;
    }

    public Diagonosis activeStatus(Boolean activeStatus) {
        this.activeStatus = activeStatus;
        return this;
    }

    public void setActiveStatus(Boolean activeStatus) {
        this.activeStatus = activeStatus;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public Diagonosis createDate(Date createDate) {
        this.createDate = createDate;
        return this;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public Diagonosis createBy(Long createBy) {
        this.createBy = createBy;
        return this;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public Diagonosis updateDate(Date updateDate) {
        this.updateDate = updateDate;
        return this;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public Diagonosis updateBy(Long updateBy) {
        this.updateBy = updateBy;
        return this;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Diagonosis diagonosis = (Diagonosis) o;
        if (diagonosis.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), diagonosis.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Diagonosis{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            ", minValue='" + getMinValue() + "'" +
            ", minValueMale='" + getMinValueMale() + "'" +
            ", minValueFemale='" + getMinValueFemale() + "'" +
            ", maxValue='" + getMaxValue() + "'" +
            ", maxValueMale='" + getMaxValueMale() + "'" +
            ", maxValueFemale='" + getMaxValueFemale() + "'" +
            ", type='" + getType() + "'" +
            ", remarks='" + getRemarks() + "'" +
            ", activeStatus='" + isActiveStatus() + "'" +
            ", createDate='" + getCreateDate() + "'" +
            ", createBy=" + getCreateBy() +
            ", updateDate='" + getUpdateDate() + "'" +
            ", updateBy=" + getUpdateBy() +
            "}";
    }
}
