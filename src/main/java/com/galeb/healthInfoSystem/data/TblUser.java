package com.galeb.healthInfoSystem.data;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.math.BigInteger;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

@Entity
@Table(name="S_USER")
public class TblUser {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_sequence")
	@SequenceGenerator(name = "user_sequence", sequenceName = "user_sequence", allocationSize = 1, initialValue = 1)
	private Long id;
	
	@Column(name = "NAME")
	 private String name;
	
	@Column(name = "EMAIL")
	 private String email;
	
	 @Column(name = "USER_NAME")
	 private String userName;
	 
	 @Column(name = "PASSWORD")
	 private String password;
	 
	 @ManyToOne
	@JoinColumn(name = "ROLE_FK")
	private TblRole roleFK;
	 
	 @Column(name = "IS_ACTIVE")
	 private int isActive;

	 @ManyToOne
	 @JoinColumn(name = "CREATED_BY")
	 private TblUser createdBy;
	 
	 @Column(name = "CREATED_ON")
	 private Date createdOn;

	 @ManyToOne
	 @JoinColumn(name = "UPDATED_BY")
	 private TblUser updatedBy;

	 @Column(name = "UPDATED_ON")
	 private Date updatedOn;

	 /*@JsonIgnore
	@ManyToMany(fetch = FetchType.LAZY,
			cascade = {
					CascadeType.PERSIST,
					CascadeType.MERGE
			},
			mappedBy = "users")
	private Set<MedicalCenterInfo> medicalCenters = new HashSet<>();*/

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}


	public TblRole getRoleFK() {
		return roleFK;
	}

	public void setRoleFK(TblRole roleFK) {
		this.roleFK = roleFK;
	}

	public int getIsActive() {
		return isActive;
	}

	public void setIsActive(int isActive) {
		this.isActive = isActive;
	}

	public TblUser getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(TblUser createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public TblUser getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(TblUser updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	/*public Set<MedicalCenterInfo> getMedicalCenters() {
		return medicalCenters;
	}

	public void setMedicalCenters(Set<MedicalCenterInfo> medicalCenters) {
		this.medicalCenters = medicalCenters;
	}
*/
}
