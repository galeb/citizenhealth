/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.galeb.healthInfoSystem.data;

import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author ITMCS-1
 */
//@Entity
//@Table(name="TBL_ACRTYPE")
public class TblACRType {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
//    @SequenceGenerator(name = "ACRTYPEPK", sequenceName = "ACRTYPE_SEQ")
    private Long id;
    
    @Column(name="ACR_TYPE_NAME")
    private String acrTypeName;
    
    @Column(name="ACR_TYPE_NAME_BANGLA")
    private String acrTypeNameBangla;
    
    @Column(name="REMARKS")
    private String remarks;
    
    @Column(name="DISPLAY_ORDER")
    private int displayorder;
    
    @Column(name = "IS_ACTIVE")
    private int isActive;

    @ManyToOne
    @JoinColumn(name = "CREATED_BY")
    private TblUser createdBy;

    @Column(name = "CREATED_ON")
    private Date createdOn;

    @ManyToOne
    @JoinColumn(name = "UPDATED_BY")
    private TblUser updatedBy;

    @Column(name = "UPDATED_ON")
    private Date updatedOn;

    public String getAcrTypeNameBangla() {
        return acrTypeNameBangla;
    }

    public void setAcrTypeNameBangla(String acrTypeNameBangla) {
        this.acrTypeNameBangla = acrTypeNameBangla;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAcrTypeName() {
        return acrTypeName;
    }

    public void setAcrTypeName(String acrTypeName) {
        this.acrTypeName = acrTypeName;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public int getDisplayorder() {
        return displayorder;
    }

    public void setDisplayorder(int displayorder) {
        this.displayorder = displayorder;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }

    public TblUser getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(TblUser createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public TblUser getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(TblUser updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    
    
    
    
}
