package com.galeb.healthInfoSystem.data;

import javax.persistence.*;
import java.math.BigInteger;
import java.util.Date;

@Entity
@Table(name="SHASTHO_BARTA")
public class ShasthoBarta {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "shastho_barta_sequence")
	@SequenceGenerator(name = "shastho_barta_sequence", sequenceName = "shastho_barta_sequence", allocationSize = 1, initialValue = 1)
	private Long id;

	@Column(name = "title")
	private String title;

	@Column(name = "BARTA")
	private String barta;

	@Column(name = "IS_ACTIVE")
	private int isActive;

	@ManyToOne
	@JoinColumn(name = "CREATED_BY")
	private TblUser createdBy;

	@Column(name = "CREATED_ON")
	private Date createdOn;

	@ManyToOne
	@JoinColumn(name = "UPDATED_BY")
	private TblUser updatedBy;

	@Column(name = "UPDATED_ON")
	private Date updatedOn;

	@Column(name = "STATUS")
	private String status;

	@Column (name = "image_path")
	private String imagePath;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBarta() {
		return barta;
	}

	public void setBarta(String barta) {
		this.barta = barta;
	}

	public int getIsActive() {
		return isActive;
	}

	public void setIsActive(int isActive) {
		this.isActive = isActive;
	}

	public TblUser getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(TblUser createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public TblUser getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(TblUser updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}
}


