package com.galeb.healthInfoSystem.data;


import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 * A MedicineInfo.
 */
@Entity
@Table(name = "medicine_info")
//@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class MedicineInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "medicine_sequence")
    @SequenceGenerator(name = "medicine_sequence", sequenceName = "medicine_sequence", allocationSize = 1, initialValue = 1)
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "generic_name")
    private String genericName;

    @Column(name = "manufacture_name")
    private String manufactureName;

    @NotNull
    @Column(name = "medicine_type", nullable = false)
    private String medicineType;

    @Column(name = "active_status")
    private Boolean activeStatus;

    @Column(name = "create_date")
    @Temporal(TemporalType.TIMESTAMP)    private Date createDate;

    @Column(name = "create_by")
    private Long createBy;

    @Column(name = "update_date")
    @Temporal(TemporalType.TIMESTAMP)    private Date updateDate;

    @Column(name = "update_by")
    private Long updateBy;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public MedicineInfo name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGenericName() {
        return genericName;
    }

    public MedicineInfo genericName(String genericName) {
        this.genericName = genericName;
        return this;
    }

    public void setGenericName(String genericName) {
        this.genericName = genericName;
    }

    public String getManufactureName() {
        return manufactureName;
    }

    public MedicineInfo manufactureName(String manufactureName) {
        this.manufactureName = manufactureName;
        return this;
    }

    public void setManufactureName(String manufactureName) {
        this.manufactureName = manufactureName;
    }

    public String getMedicineType() {
        return medicineType;
    }

    public MedicineInfo medicineType(String medicineType) {
        this.medicineType = medicineType;
        return this;
    }

    public void setMedicineType(String medicineType) {
        this.medicineType = medicineType;
    }

    public Boolean isActiveStatus() {
        return activeStatus;
    }

    public MedicineInfo activeStatus(Boolean activeStatus) {
        this.activeStatus = activeStatus;
        return this;
    }

    public void setActiveStatus(Boolean activeStatus) {
        this.activeStatus = activeStatus;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public MedicineInfo createBy(Long createBy) {
        this.createBy = createBy;
        return this;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }


    public Long getUpdateBy() {
        return updateBy;
    }

    public MedicineInfo updateBy(Long updateBy) {
        this.updateBy = updateBy;
        return this;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove


    public Boolean getActiveStatus() {
        return activeStatus;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MedicineInfo medicineInfo = (MedicineInfo) o;
        if (medicineInfo.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), medicineInfo.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "MedicineInfo{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", genericName='" + getGenericName() + "'" +
            ", manufactureName='" + getManufactureName() + "'" +
            ", medicineType='" + getMedicineType() + "'" +
            ", activeStatus='" + isActiveStatus() + "'" +
            ", createBy=" + getCreateBy() +
            ", updateBy=" + getUpdateBy() +
            "}";
    }
}
