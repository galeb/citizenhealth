package com.galeb.healthInfoSystem.data;

import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="LOOKUP")
public class TblLookUp {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
//	@SequenceGenerator(name = "LOOK_UP_PK", sequenceName = "LOOKUP_SEQ")
	@Column(name = "id")
	private Long id;
	
	@Column(name = "LOOKUP_NAME")
	 private String lookUpName;
	
	@Column(name = "LOOKUP_NATIVE_NAME")
	 private String lookUpNativeName;
	
	@Column(name = "KEYWORD")
	 private String keyword;
	
	@Column(name = "ABBREVIATION")
	 private String abbreviation;
	
	@Column(name = "ABBREVIATION_NATIVE")
	 private String abbreviationNative;
	
	@Column(name = "SI_NO")
	 private String siNo;
	
	@Column(name = "IS_ACTIVE")
	 private int isActive;
	
	@Column(name = "STATUS")
	private String status;
	
	 @ManyToOne
	 @JoinColumn(name = "CREATED_BY")
	 private TblUser createdBy;
	 
	 @Column(name = "CREATED_ON")
	 private Date createdOn;
	 
	 @ManyToOne
	 @JoinColumn(name = "UPDATED_BY")
	 private TblUser updatedBy;
	 
	 @Column(name = "UPDATED_ON")
	 private Date updatedOn;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLookUpName() {
		return lookUpName;
	}

	public void setLookUpName(String lookUpName) {
		this.lookUpName = lookUpName;
	}

	public String getLookUpNativeName() {
		return lookUpNativeName;
	}

	public void setLookUpNativeName(String lookUpNativeName) {
		this.lookUpNativeName = lookUpNativeName;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public String getAbbreviation() {
		return abbreviation;
	}

	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
	}

	public String getAbbreviationNative() {
		return abbreviationNative;
	}

	public void setAbbreviationNative(String abbreviationNative) {
		this.abbreviationNative = abbreviationNative;
	}

	public String getSiNo() {
		return siNo;
	}

	public void setSiNo(String siNo) {
		this.siNo = siNo;
	}

	public int getIsActive() {
		return isActive;
	}

	public void setIsActive(int isActive) {
		this.isActive = isActive;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public TblUser getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(TblUser createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public TblUser getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(TblUser updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
}
