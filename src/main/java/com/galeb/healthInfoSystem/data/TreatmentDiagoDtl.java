package com.galeb.healthInfoSystem.data;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 * A TreatmentDiagoDtl.
 */
@Entity
@Table(name = "treatment_diago_dtl")
//@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class TreatmentDiagoDtl implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "treatment_diago_sequence")
    @SequenceGenerator(name = "treatment_diago_sequence", sequenceName = "treatment_diago_sequence", allocationSize = 1, initialValue = 1)
    private Long id;

    @Column(name = "test_details")
    private String testDetails;

    @Column(name = "result")
    private String result;

    @Column(name = "remarks")
    private String remarks;

    @Lob
    @Column(name = "attachment")
    private byte[] attachmentContentBlob;

    @Column(name = "attachment_type")
    private String attachmentContentBlobContentType;

    @Column(name = "active_status")
    private Boolean activeStatus;

    @Column(name = "create_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;

    @Column(name = "test_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date testDate;

    @Column(name = "report_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date reportDate;

    @Column(name = "create_by")
    private Long createBy;

    @Column(name = "update_date")
    @Temporal(TemporalType.TIMESTAMP)    private Date updateDate;

    @Column(name = "update_by")
    private Long updateBy;

    @Column(name = "dioago_date")
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    @Temporal(TemporalType.TIMESTAMP)
    private Date diagoDate;

    @Column(name = "result_date")
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    @Temporal(TemporalType.TIMESTAMP)
    private Date resultDate;


    @ManyToOne
    @JsonBackReference
    private TreatmentInfo treatmentInfo;

    @ManyToOne
    //@JsonIgnoreProperties("treatmentDiagoDtls")
    private Diagonosis diagonosis;

    @ManyToOne
    @JsonIgnoreProperties("treatmentInfos")
    private PersonalInfo patientInfo;

    @ManyToOne
    @JsonIgnoreProperties("treatmentInfos")
    private DoctorInfo doctorInfo;

    @ManyToOne
    @JsonIgnoreProperties("treatmentInfos")
    private MedicalCenterInfo medicalCenterInfo;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTestDetails() {
        return testDetails;
    }

    public TreatmentDiagoDtl testDetails(String testDetails) {
        this.testDetails = testDetails;
        return this;
    }

    public void setTestDetails(String testDetails) {
        this.testDetails = testDetails;
    }

    public String getResult() {
        return result;
    }

    public TreatmentDiagoDtl result(String result) {
        this.result = result;
        return this;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getRemarks() {
        return remarks;
    }

    public TreatmentDiagoDtl remarks(String remarks) {
        this.remarks = remarks;
        return this;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public byte[] getAttachmentContentBlob() {
        return attachmentContentBlob;
    }

    public TreatmentDiagoDtl attachmentContentBlob(byte[] attachmentContentBlob) {
        this.attachmentContentBlob = attachmentContentBlob;
        return this;
    }

    public void setAttachmentContentBlob(byte[] attachmentContentBlob) {
        this.attachmentContentBlob = attachmentContentBlob;
    }

    public String getAttachmentContentBlobContentType() {
        return attachmentContentBlobContentType;
    }

    public TreatmentDiagoDtl attachmentContentBlobContentType(String attachmentContentBlobContentType) {
        this.attachmentContentBlobContentType = attachmentContentBlobContentType;
        return this;
    }

    public void setAttachmentContentBlobContentType(String attachmentContentBlobContentType) {
        this.attachmentContentBlobContentType = attachmentContentBlobContentType;
    }

    public Boolean isActiveStatus() {
        return activeStatus;
    }

    public TreatmentDiagoDtl activeStatus(Boolean activeStatus) {
        this.activeStatus = activeStatus;
        return this;
    }

    public void setActiveStatus(Boolean activeStatus) {
        this.activeStatus = activeStatus;
    }


     public TreatmentDiagoDtl createBy(Long createBy) {
        this.createBy = createBy;
        return this;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public Boolean getActiveStatus() {
        return activeStatus;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getTestDate() {
        return testDate;
    }

    public void setTestDate(Date testDate) {
        this.testDate = testDate;
    }

    public Date getReportDate() {
        return reportDate;
    }

    public void setReportDate(Date reportDate) {
        this.reportDate = reportDate;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public TreatmentDiagoDtl updateBy(Long updateBy) {
        this.updateBy = updateBy;
        return this;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public TreatmentInfo getTreatmentInfo() {
        return treatmentInfo;
    }

    public TreatmentDiagoDtl treatmentInfo(TreatmentInfo TreatmentInfo) {
        this.treatmentInfo = TreatmentInfo;
        return this;
    }

    public void setTreatmentInfo(TreatmentInfo TreatmentInfo) {
        this.treatmentInfo = TreatmentInfo;
    }

    public Diagonosis getDiagonosis() {
        return diagonosis;
    }

    public TreatmentDiagoDtl diagonosis(Diagonosis Diagonosis) {
        this.diagonosis = Diagonosis;
        return this;
    }

    public void setDiagonosis(Diagonosis Diagonosis) {
        this.diagonosis = Diagonosis;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove


    public Date getDiagoDate() {
        return diagoDate;
    }

    public void setDiagoDate(Date diagoDate) {
        this.diagoDate = diagoDate;
    }

    public Date getResultDate() {
        return resultDate;
    }

    public void setResultDate(Date resultDate) {
        this.resultDate = resultDate;
    }

    public PersonalInfo getPatientInfo() {
        return patientInfo;
    }

    public void setPatientInfo(PersonalInfo patientInfo) {
        this.patientInfo = patientInfo;
    }

    public DoctorInfo getDoctorInfo() {
        return doctorInfo;
    }

    public void setDoctorInfo(DoctorInfo doctorInfo) {
        this.doctorInfo = doctorInfo;
    }

    public MedicalCenterInfo getMedicalCenterInfo() {
        return medicalCenterInfo;
    }

    public void setMedicalCenterInfo(MedicalCenterInfo medicalCenterInfo) {
        this.medicalCenterInfo = medicalCenterInfo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TreatmentDiagoDtl treatmentDiagoDtl = (TreatmentDiagoDtl) o;
        if (treatmentDiagoDtl.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), treatmentDiagoDtl.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "TreatmentDiagoDtl{" +
            "id=" + getId() +
            ", testDetails='" + getTestDetails() + "'" +
            ", result='" + getResult() + "'" +
            ", remarks='" + getRemarks() + "'" +
            ", attachmentContentBlob='" + getAttachmentContentBlob() + "'" +
            ", attachmentContentBlobContentType='" + getAttachmentContentBlobContentType() + "'" +
            ", activeStatus='" + isActiveStatus() + "'" +
            ", createDate='" + getCreateDate() + "'" +
            ", testDate='" + getTestDate() + "'" +
            ", reportDate='" + getReportDate() + "'" +
            ", createBy=" + getCreateBy() +
            ", updateDate='" + getUpdateDate() + "'" +
            ", updateBy=" + getUpdateBy() +
            "}";
    }
}
