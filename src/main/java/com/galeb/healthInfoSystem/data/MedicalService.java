package com.galeb.healthInfoSystem.data;

import javax.persistence.*;
import java.math.BigInteger;
import java.util.Date;

@Entity
@Table(name="MEDICAL_SERVICE")
public class MedicalService {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "medical_service_sequence")
	@SequenceGenerator(name = "medical_service_sequence", sequenceName = "medical_service_sequence", allocationSize = 1, initialValue = 1)
	private Long id;
	
	 @Column(name = "SERVICE_NAME")
	 private String serviceName;

	 @Column(name = "IS_ACTIVE")
	 private int isActive;

	 @ManyToOne
	 @JoinColumn(name = "CREATED_BY")
	 private TblUser createdBy;
	 
	 @Column(name = "CREATED_ON")
	 private Date createdOn;
	 
	 @ManyToOne
	 @JoinColumn(name = "UPDATED_BY")
	 private TblUser updatedBy;
	 
	 @Column(name = "UPDATED_ON")
	 private Date updatedOn;

	@Column(name = "STATUS")
	private String status;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public int getIsActive() {
		return isActive;
	}

	public void setIsActive(int isActive) {
		this.isActive = isActive;
	}

	public TblUser getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(TblUser createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public TblUser getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(TblUser updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}


}
