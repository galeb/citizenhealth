package com.galeb.healthInfoSystem.common.utility.service;

import java.util.HashSet;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;


@Component
public class CustomInterceptor implements HandlerInterceptor
{

	Set<String> skipUrls = new HashSet<>();
	
	public CustomInterceptor() {
		skipUrls.add("/signup");
		skipUrls.add("/login");
		skipUrls.add("/docbean");
		skipUrls.add("/user/authenticate");
	}
	private static final Logger LOGGER = LoggerFactory.getLogger(CustomInterceptor.class);
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		try {                    
			if(isPageSkipped(request.getRequestURI(), request.getContextPath())) {                          
				return true;
			}else {
				if (request.getSession().getAttribute("AUTHSESSION") == null) {
					response.sendRedirect(request.getContextPath() + "/login");				
				}
			}
		} catch (Exception e) {
			LOGGER.error("Server Error", e);
		}
		return true;
	}
	
	@Override
	public void afterCompletion(HttpServletRequest request,
			HttpServletResponse response, Object object, Exception arg3)
			throws Exception {
		LOGGER.info("Request is complete");
	}

	@Override
	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object object, ModelAndView model)
			throws Exception {
		LOGGER.info("Handler execution is complete");
	}
	
	 boolean isPageSkipped(String uri, String contextName) {
	        boolean pageSkipped = false;
	        if (isPage(uri,contextName)) {
	            pageSkipped = true;
	        } else {
	            for (String str : skipUrls) {
	                if (uri.contains(str)) {
	                    pageSkipped = true;
	                    break;
	                }
	            }
	        }
	        return pageSkipped;
	    }
	 
	 boolean isPage(String uri,String contextName){
	        return isPage1(uri);
	    }
	    
	    boolean isPage1(String uri){
	        return uri.contains("js") || uri.contains("css") || uri.contains("fonts") || uri.contains("resources");
	    }

}
