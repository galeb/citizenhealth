package com.galeb.healthInfoSystem.common.utility.service;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.galeb.healthInfoSystem.bean.SessionBean;
import com.galeb.healthInfoSystem.data.TblLanguage;
import com.galeb.healthInfoSystem.data.TblLookUp;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.galeb.healthInfoSystem.data.TblMenu;
import com.galeb.healthInfoSystem.data.TblUser;

public class CommonUtility {

	private static String UPLOADED_FOLDER = "/home/PIMSUploads/";
	//private static String UPLOADEDIMAGE_FOLDER = "/home/PIMSUploads/Profile/";
	private static String UPLOADEDIMAGE_FOLDER = "D://demo/";

	public static String convertToJsonString(Object obj) {

		Gson gson = new GsonBuilder().create();
		// Gson gson = new GsonBuilder().disableHtmlEscaping().create();
		return gson.toJson(obj);
	}

	public static void setToSession(HttpServletRequest request, String key, Object value) {
		HttpSession httpSession = request.getSession();
		httpSession.setAttribute(key, value);

	}

	public static String buildLanguageValJSON(List<TblLanguage> lstLanguageElement) {
		Map<String, Object> mapMainKey = new HashMap();
		for (TblLanguage tblLanguage : lstLanguageElement) {
			Map<String, Object> dataSet = new HashMap();
			dataSet.put("en", tblLanguage.getEnglishName());
			dataSet.put("bg", tblLanguage.getBengaliName());
			mapMainKey.put(tblLanguage.getEnglishName(), dataSet);
		}
		return convertToJsonString(mapMainKey);
	}

	private static String getFileExtension(String fileName) {
		if (fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0)
			return fileName.substring(fileName.lastIndexOf(".") + 1);
		else
			return "";
	}

	public static String uploadFile(MultipartFile file) throws IOException {
		String fileUploadMsg = "FILEEMPTY";
		if (!file.isEmpty()) {
			byte[] bytes = file.getBytes();
			File fileDemo = new File(UPLOADED_FOLDER);
			fileDemo.mkdirs();
			Path path = Paths.get(UPLOADED_FOLDER + file.getOriginalFilename());
			Files.write(path, bytes);
			fileUploadMsg = UPLOADED_FOLDER + file.getOriginalFilename();
		}
		return fileUploadMsg;
	}

	public static String uploadImage(MultipartFile file) throws IOException {
		String fileUploadMsg = "FILEEMPTY";
		if (!file.isEmpty()) {
			byte[] bytes = file.getBytes();
			File fileDemo = new File(UPLOADEDIMAGE_FOLDER);
			fileDemo.mkdirs();
			String fileName = generateUniqueFileName(file.getOriginalFilename());
			Path path = Paths.get(UPLOADEDIMAGE_FOLDER + fileName);
			Files.write(path, bytes);
			fileUploadMsg = UPLOADEDIMAGE_FOLDER + fileName;
		}
		return fileUploadMsg;
	}

	public static TblUser fetchUserFromSession(HttpServletRequest request) {

		HttpSession session = request.getSession();
		SessionBean sessionBean = (SessionBean) session.getAttribute("AUTHSESSION");
		TblUser tblUser = sessionBean.getUser();
		return tblUser;
	}

	public static String jsoMenuParentChildwise(List<TblMenu> lstMenu, List<Long> lstChildList) {

		List<TblMenu> lstMenuParents = new ArrayList<TblMenu>();
		List<TblMenu> lstMenuChilds = new ArrayList<TblMenu>();
		List<Long> parentIds = new ArrayList<Long>();

		for (TblMenu tblMenu : lstMenu) {
			if (lstChildList.contains(tblMenu.getId()) && tblMenu.getParent() == 1) {
				lstMenuChilds.add(tblMenu);
				if (!parentIds.contains(tblMenu.getParentSelfId())) {
					parentIds.add(tblMenu.getParentSelfId());
				}
			} else if (tblMenu.getChild() == 0 && tblMenu.getParent() == 0) {
				if (lstChildList.contains(tblMenu.getId()) && !lstMenuParents.contains(tblMenu)) {
					lstMenuParents.add(tblMenu);
					parentIds.add(tblMenu.getId());
				}
			}
			if (tblMenu.getChild() == 1) {
				if (!lstMenuParents.contains(tblMenu)) {
					lstMenuParents.add(tblMenu);
				}
			}
		}

		List<Map<String, Object>> lstMapParentChild = new ArrayList();
		for (TblMenu tblParents : lstMenuParents) {
			if (parentIds.contains(tblParents.getId())) {
				Map<String, Object> mapParentChild = new HashMap();
				mapParentChild.put("parent", tblParents);
				mapParentChild.put("child", "0");
				if (tblParents.getChild() == 1) {
					List<TblMenu> tempListMenu = new ArrayList();
					for (TblMenu tblChilds : lstMenuChilds) {
						if (tblParents.getId().equals(tblChilds.getParentSelfId())) {
							tempListMenu.add(tblChilds);
						}
					}
					mapParentChild.put("child", tempListMenu);
				}
				lstMapParentChild.add(mapParentChild);
			}
		}

		return convertToJsonString(lstMapParentChild);
	}

	public static String getProperty(String propertyName) {
		ResourceBundle resourceBundle = ResourceBundle.getBundle("ProjectProperty");
		return resourceBundle.getString(propertyName);
	}

	public static String generateUniqueFileName(String fileName) {

		String timeStamp = new SimpleDateFormat("yyyyMMdd").format(new Date());
		long millis = System.currentTimeMillis();
		timeStamp = timeStamp.replace(" ", "");
		timeStamp = timeStamp.replace(":", "");
		fileName = timeStamp + millis + "_" + fileName;
		return fileName;

	}

	public static String jsonLookup(List<TblLookUp> lstLookup) {
		List<Map<String, Object>> lstMainMapLookup = new ArrayList<>();

		for (TblLookUp tblLookUp : lstLookup) {
			Map<String, Object> mainMapLookup = new HashMap();
			Map<String, Object> mapLookup = new HashMap();
			mapLookup.put("key", tblLookUp.getLookUpName());
			mapLookup.put("value", tblLookUp.getLookUpName());
			if (!isCollectionMapNullOrEmpty(lstmapContainsKey(lstMainMapLookup, tblLookUp.getKeyword()))) {
				List<Map<String, Object>> lstMapOldLookup = (List<Map<String, Object>>) mainMapLookup.get("values");
				lstMapOldLookup.add(mapLookup);
				mainMapLookup.put("values", lstMapOldLookup);
			} else {
				mainMapLookup.put("keyword", tblLookUp.getKeyword());
				List<Map<String, Object>> lstMapLattestLookup = new ArrayList<>();
				lstMapLattestLookup.add(mapLookup);
				mainMapLookup.put("values", lstMapLattestLookup);
			}
			lstMainMapLookup.add(mainMapLookup);
			System.out.println("After condition mainMapLookup " + mainMapLookup);
			System.out
					.println("After condition lstMainMapLookup " + CommonUtility.convertToJsonString(lstMainMapLookup));
		}

		return CommonUtility.convertToJsonString(lstMainMapLookup);
	}

	private static Map<String, Object> lstmapContainsKey(List<Map<String, Object>> providedList, String key) {
		Map<String, Object> requiredmap = new HashMap();
		if (providedList.isEmpty()) {
			for (Map<String, Object> fetchMapObj : providedList) {
				if (fetchMapObj.containsKey(key)) {
					requiredmap = fetchMapObj;
					break;
				}
			}
		}

		return requiredmap;
	}

	protected static boolean isCollectionMapNullOrEmpty(final Collection<?> c) {
		return c == null || c.isEmpty();
	}

	protected static boolean isCollectionMapNullOrEmpty(final Map<?, ?> m) {
		return m == null || m.isEmpty();
	}

}
