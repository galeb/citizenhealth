package com.galeb.healthInfoSystem.common.utility.service;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.galeb.healthInfoSystem.data.TblUser;
import com.galeb.healthInfoSystem.mastertable.usergroup.UserGroupService;
import com.galeb.healthInfoSystem.repository.ShasthoBartaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.galeb.healthInfoSystem.management.menu.MenuService;

@Controller
public class CommonController {	
	
	@Autowired
	MenuService menuService;

	@Autowired
	ShasthoBartaRepository shasthoBartaRepository;
	//@Qualifier("")
	@Autowired
    UserGroupService roleService;
	
	@RequestMapping("/index")
    public String index(Model model,HttpServletRequest request){	
		
		TblUser tblUser = CommonUtility.fetchUserFromSession(request);
		CommonUtility.setToSession(request, "LSTMAINMENU", jsonMenuResposne(tblUser));
		model.addAttribute("bartaList", shasthoBartaRepository.findAllByIsActive(1));
		return "home.dashboard";
    }
	
	public String jsonMenuResposne(TblUser tblUser) {
		String resposne = "";
		if(tblUser != null) {
			List<Long> lstChildList = roleService.fetchMenuByRoleId(tblUser.getRoleFK().getId());
			resposne = CommonUtility.jsoMenuParentChildwise(menuService.fetchAllMenu(),lstChildList);
		}
		return resposne;
	}
	
	@ResponseBody
	@RequestMapping("/fetchSessionData")
    public String fetchSessionData(HttpServletRequest request){		
		Map<String,Object> sessionMap = new HashMap();
		
		HttpSession session = request.getSession();
		sessionMap.put("LANGUAGEMAPPER", session.getAttribute("LANGUAGEMAPPER"));
		sessionMap.put("LSTMAINMENU",session.getAttribute("LSTMAINMENU"));
						
		return CommonUtility.convertToJsonString(sessionMap);
    }
	
	@RequestMapping("/login")
	public String login(Model model, HttpServletRequest request) {
		if (null != request.getSession().getAttribute("AUTHSESSION")) {
			HttpSession session = request.getSession();
			session.removeAttribute("AUTHSESSION");
			session.invalidate();
		}
		model.addAttribute("userModel", new TblUser());
		model.addAttribute("bartaList", shasthoBartaRepository.findAllByIsActive(1));
		return "beforeAuth/login";
	}	
	
	@RequestMapping("/logout")
	public String logout(Model model, HttpServletRequest request) {
		HttpSession session = request.getSession();
		session.removeAttribute("AUTHSESSION");
		session.invalidate();
		return "beforeAuth/login";
	}
	
	@RequestMapping("/signup")
    public String signup(Model model){		
		model.addAttribute("userModel",new TblUser());
        return "beforeAuth/signUp";
    }
	
}
