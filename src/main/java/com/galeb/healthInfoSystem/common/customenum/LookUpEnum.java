/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.galeb.healthInfoSystem.common.customenum;

/**
 *
 * @author ITMCS-1
 */
public enum LookUpEnum {
    ACRREQUIRED("ACR Required"),
    ACRTYPE("ACR Type"),
    HEALTHREPORT("Health Report"),
    NEGREMARK("If neg Remarks"),
    IFSUGBYORU("IF SUG BY ORU"),
    IFSUGBYCSO("IF SUG BY CSO"),
    DESCISIONFORNEGREMARK("Decision against Neg Remarks");
    String lookUpname;
    
    LookUpEnum(String lookupname){
        this.lookUpname=lookupname;
    }
    
    @Override
    public String toString(){
        return this.lookUpname;
    }
}
