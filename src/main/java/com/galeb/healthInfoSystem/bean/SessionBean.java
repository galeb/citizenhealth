package com.galeb.healthInfoSystem.bean;

import com.galeb.healthInfoSystem.data.TblUser;

public class SessionBean {
	TblUser user;

	public TblUser getUser() {
		return user;
	}

	public void setUser(TblUser user) {
		this.user = user;
	}
}
