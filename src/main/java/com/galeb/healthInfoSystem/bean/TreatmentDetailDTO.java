package com.galeb.healthInfoSystem.bean;

import com.galeb.healthInfoSystem.data.*;

import java.util.List;

public class TreatmentDetailDTO {
    private PersonalInfo personalInfo;
    private TreatmentInfo treatmentInfo;
    private MedicalCenterInfo medicalCenterInfo;
    private DoctorInfo doctorInfo;
    private List<TreatmentDtl> treatmentDtlList;
    private List<TreatmentDiagoDtl> diagoDtlList;

    public PersonalInfo getPersonalInfo() {
        return personalInfo;
    }

    public void setPersonalInfo(PersonalInfo personalInfo) {
        this.personalInfo = personalInfo;
    }

    public TreatmentInfo getTreatmentInfo() {
        return treatmentInfo;
    }

    public void setTreatmentInfo(TreatmentInfo treatmentInfo) {
        this.treatmentInfo = treatmentInfo;
    }

    public MedicalCenterInfo getMedicalCenterInfo() {
        return medicalCenterInfo;
    }

    public void setMedicalCenterInfo(MedicalCenterInfo medicalCenterInfo) {
        this.medicalCenterInfo = medicalCenterInfo;
    }

    public List<TreatmentDtl> getTreatmentDtlList() {
        return treatmentDtlList;
    }

    public void setTreatmentDtlList(List<TreatmentDtl> treatmentDtlList) {
        this.treatmentDtlList = treatmentDtlList;
    }

    public DoctorInfo getDoctorInfo() {
        return doctorInfo;
    }

    public void setDoctorInfo(DoctorInfo doctorInfo) {
        this.doctorInfo = doctorInfo;
    }

    public List<TreatmentDiagoDtl> getDiagoDtlList() {
        return diagoDtlList;
    }

    public void setDiagoDtlList(List<TreatmentDiagoDtl> diagoDtlList) {
        this.diagoDtlList = diagoDtlList;
    }
}
