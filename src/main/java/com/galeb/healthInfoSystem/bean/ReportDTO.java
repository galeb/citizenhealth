package com.galeb.healthInfoSystem.bean;

import com.galeb.healthInfoSystem.data.TreatmentDiagoDtl;
import com.galeb.healthInfoSystem.data.TreatmentDtl;
import com.galeb.healthInfoSystem.data.TreatmentInfo;

import java.util.List;

/**
 * Created by abdullah.galeb on 10/20/2019.
 */
public class ReportDTO {
    private Integer totalPatient;
    private Integer totalDoctor;
    private Integer totalMalePatient;
    private Integer totalFemalePatient;
    private Integer totalMaleDoctor;
    private Integer totalFemaleDoctor;
    private List<KeyValueDTO> objectList;
    private List<TreatmentInfo> treatmentInfoList;
    private List<TreatmentDtl> treatmentDtlList;
    private List<TreatmentDiagoDtl> diagoDtlList;


    public Integer getTotalPatient() {
        return totalPatient;
    }

    public void setTotalPatient(Integer totalPatient) {
        this.totalPatient = totalPatient;
    }

    public Integer getTotalDoctor() {
        return totalDoctor;
    }

    public void setTotalDoctor(Integer totalDoctor) {
        this.totalDoctor = totalDoctor;
    }

    public Integer getTotalMalePatient() {
        return totalMalePatient;
    }

    public void setTotalMalePatient(Integer totalMalePatient) {
        this.totalMalePatient = totalMalePatient;
    }

    public Integer getTotalFemalePatient() {
        return totalFemalePatient;
    }

    public void setTotalFemalePatient(Integer totalFemalePatient) {
        this.totalFemalePatient = totalFemalePatient;
    }

    public Integer getTotalMaleDoctor() {
        return totalMaleDoctor;
    }

    public void setTotalMaleDoctor(Integer totalMaleDoctor) {
        this.totalMaleDoctor = totalMaleDoctor;
    }

    public Integer getTotalFemaleDoctor() {
        return totalFemaleDoctor;
    }

    public void setTotalFemaleDoctor(Integer totalFemaleDoctor) {
        this.totalFemaleDoctor = totalFemaleDoctor;
    }

    public List<KeyValueDTO> getObjectList() {
        return objectList;
    }

    public void setObjectList(List<KeyValueDTO> objectList) {
        this.objectList = objectList;
    }

    public List<TreatmentInfo> getTreatmentInfoList() {
        return treatmentInfoList;
    }

    public void setTreatmentInfoList(List<TreatmentInfo> treatmentInfoList) {
        this.treatmentInfoList = treatmentInfoList;
    }

    public List<TreatmentDtl> getTreatmentDtlList() {
        return treatmentDtlList;
    }

    public void setTreatmentDtlList(List<TreatmentDtl> treatmentDtlList) {
        this.treatmentDtlList = treatmentDtlList;
    }

    public List<TreatmentDiagoDtl> getDiagoDtlList() {
        return diagoDtlList;
    }

    public void setDiagoDtlList(List<TreatmentDiagoDtl> diagoDtlList) {
        this.diagoDtlList = diagoDtlList;
    }
}
