package com.galeb.healthInfoSystem.bean;

import java.math.BigInteger;
import java.util.List;

import com.galeb.healthInfoSystem.data.TblRole;

public class RoleMenuMapBean {

	List<Long> lstResMenu;
	TblRole tblRole;
	
	public List<Long> getLstResMenu() {
		return lstResMenu;
	}
	public void setLstResMenu(List<Long> lstResMenu) {
		this.lstResMenu = lstResMenu;
	}
	public TblRole getTblRole() {
		return tblRole;
	}
	public void setTblRole(TblRole tblRole) {
		this.tblRole = tblRole;
	}
	
}
