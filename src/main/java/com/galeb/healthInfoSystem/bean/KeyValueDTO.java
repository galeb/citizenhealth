package com.galeb.healthInfoSystem.bean;

/**
 * Created by abdullah.galeb on 10/20/2019.
 */
public class KeyValueDTO {
    private String propertyName;
    private String propertyValue;

    public String getPropertyName() {
        return propertyName;
    }

    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    public String getPropertyValue() {
        return propertyValue;
    }

    public void setPropertyValue(String propertyValue) {
        this.propertyValue = propertyValue;
    }
}
