package com.galeb.healthInfoSystem.mastertable.lookup;



import java.math.BigInteger;
import java.util.List;

import com.galeb.healthInfoSystem.common.dal.service.OperationTypeEnum;
import com.galeb.healthInfoSystem.data.TblLookUp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.galeb.healthInfoSystem.common.dal.service.CommonDAO;

@Repository
public class LookUpRepoImpl implements LookUpRepo{

	/*@Autowired
	CommonDAO commonDao;
	
	@Override
	public TblLookUp addLookUp(TblLookUp tblLookUp) {
		commonDao.saveOrUpdate(tblLookUp);
		return tblLookUp;
	}

	@Override
	public TblLookUp fetchLookUpbyLookUpId(BigInteger lookUpID) {
		List<TblLookUp> lstLookUp  = commonDao.findEntity(TblLookUp.class, "lookUpPk", OperationTypeEnum.EQ ,lookUpID);
		TblLookUp dbObjTblLookUp = new TblLookUp();
		if(!lstLookUp.isEmpty()) {
			dbObjTblLookUp =  lstLookUp.get(0);
		}
		return dbObjTblLookUp;
	}

	@Override
	public List<TblLookUp> fetchAllLookUp() {
		List<TblLookUp> lstLookUp  = commonDao.findEntity(TblLookUp.class);
		return lstLookUp;
	}

	@Override
	public boolean deleteLookUp(TblLookUp tblLookUp) {
		commonDao.delete(tblLookUp);
		return true;
	}
	
	@Override
    public List<TblLookUp> fetchLookUpByName(List<String> names) {
        Object[] object = names.toArray();
        List<TblLookUp> lstLookUp  = commonDao.findEntity(TblLookUp.class,"keyword",OperationTypeEnum.IN,object);
            return lstLookUp;
    *///}
	
}
