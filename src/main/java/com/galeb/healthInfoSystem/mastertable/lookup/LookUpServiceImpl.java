package com.galeb.healthInfoSystem.mastertable.lookup;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.galeb.healthInfoSystem.data.TblLanguage;
import com.galeb.healthInfoSystem.data.TblLookUp;
import com.galeb.healthInfoSystem.management.language.LanguageRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LookUpServiceImpl implements LookUpService{

	/*@Autowired
	LookUpRepo userRepo;
	
	@Autowired
    LanguageRepo languageRepo;
	
	@Override
	public TblLookUp addLookUp(TblLookUp tblLookUp) {
		tblLookUp.setCreatedOn(new Date());
		tblLookUp.setUpdatedOn(new Date());
		tblLookUp = userRepo.addLookUp(tblLookUp);
		
		TblLanguage tblLanguage = new TblLanguage();
		tblLanguage.setBengaliName(tblLookUp.getLookUpNativeName());
		tblLanguage.setEnglishName(tblLookUp.getLookUpName());
		tblLanguage.setIsActive(1);
		List<TblLanguage> tblLanguageElement = new ArrayList<>();
		tblLanguageElement.add(tblLanguage);
		languageRepo.addAllLanguageElements(tblLanguageElement);
		
		return tblLookUp;
	}

	@Override
	public List<TblLookUp> fetchAllLookUp() {
		return userRepo.fetchAllLookUp();
	}
	
	@Override
	public String deleteLookUp(BigInteger lookUpId) {
		String response = "INVALIDLOOKUPID";
		TblLookUp tblLookUp = userRepo.fetchLookUpbyLookUpId(lookUpId);
		if(tblLookUp != null) {
			if(userRepo.deleteLookUp(tblLookUp)) {
				response = "DELETEDSUCCSSFULLY";
			}
		}
		return response;
	}

	@Override
	public TblLookUp fetchLookUpbyId(BigInteger lookUpId) {
		return userRepo.fetchLookUpbyLookUpId(lookUpId);
	}
	
	@Override
    public List<TblLookUp> fetchLookUpByKeyword(List<String> keywords) {
        return userRepo.fetchLookUpByName(keywords);
    }
	*/
}
