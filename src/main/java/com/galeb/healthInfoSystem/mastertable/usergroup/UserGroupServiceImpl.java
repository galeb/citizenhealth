package com.galeb.healthInfoSystem.mastertable.usergroup;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.galeb.healthInfoSystem.data.TblRole;
import com.galeb.healthInfoSystem.data.TblRoleMenuMapping;
import com.galeb.healthInfoSystem.repository.RoleMenuRepository;
import com.galeb.healthInfoSystem.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class UserGroupServiceImpl implements UserGroupService{

	@Autowired
    RoleRepository roleRepository;
	
	@Autowired
    RoleMenuRepository roleMenuRepository;

	@Override
	public TblRole addRole(TblRole tblRole) {
		tblRole.setCreatedOn(new Date());
		tblRole.setUpdatedOn(new Date());	
		tblRole.setIsActive(1);
		if(tblRole.getId() != null) {
			List<TblRoleMenuMapping> lstTblRoleMenuMapping = roleMenuRepository.findAll();//userRepo.fetchMenuByRoleId(tblRole.getId());
			if(lstTblRoleMenuMapping != null) {
                for (TblRoleMenuMapping each: lstTblRoleMenuMapping
                     ) {
                    roleMenuRepository.delete(each);
                }
            }
		}
		//return userRepo.addRole(tblRole);
        return tblRole;
	}

	@Override
	public List<TblRole> fetchAllRole() {
		return roleRepository.findAll();
	}
	
	@Override
	public String deleteRole(Long roleId) {
		String response = "INVALIDPAYSCALEID";
		TblRole tblRole = roleRepository.getOne(roleId);
		if(tblRole != null) {
			roleRepository.delete(tblRole);
			/*List<TblRoleMenuMapping> lstTblRoleMenuMapping = //userRepo.fetchMenuByRoleId(tblRole.getId());
			if(lstTblRoleMenuMapping != null) {
				userRepo.deleteAllRoleMenuMapping(lstTblRoleMenuMapping);
			}
			
			if(userRepo.deleteRole(tblRole)) {
				response = "DELETEDSUCCSSFULLY";
			}*/
			response = "DELETEDSUCCSSFULLY";
		}
		return response;
	}

	@Override
	public TblRole fetchRolebyId(Long payScaleId) {
		return roleRepository.getOne(payScaleId);
	}

    @Override
    public List<Long> fetchMenuByRoleId(Long roleId) {
		List<Long> lstMenu = new ArrayList();
		List<TblRoleMenuMapping> lstTblRoleMenuMapping = roleMenuRepository.findAllByRolePk(roleId);
		if(lstTblRoleMenuMapping != null) {
			for(TblRoleMenuMapping tblRoleMenuMapping :lstTblRoleMenuMapping) {
				lstMenu.add(tblRoleMenuMapping.getMenuPk());
			}
		}
		return lstMenu;
    }

	@Override
	public String saveMenuRoleMap(List<Long> lstMenu, TblRole savedRole) {
		List<TblRoleMenuMapping> lstTblRoleMenuMapping = new ArrayList();
		for(Long tblMenu: lstMenu) {
			
			TblRoleMenuMapping tblRoleMenuMapping= new TblRoleMenuMapping();
			tblRoleMenuMapping.setIsActive(1);
			tblRoleMenuMapping.setMenuPk(tblMenu);
			tblRoleMenuMapping.setRolePk(savedRole.getId());
			lstTblRoleMenuMapping.add(tblRoleMenuMapping);
		}

		for (TblRoleMenuMapping each:lstTblRoleMenuMapping
			 ) {
			roleMenuRepository.save(each);
		}
		String resposne = "success";
		return resposne;
	}

	/*@Override
	public List<Long> fetchMenuByRoleId(BigInteger roleId) {
		List<BigInteger> lstMenu = new ArrayList();
		List<TblRoleMenuMapping> lstTblRoleMenuMapping = userRepo.fetchMenuByRoleId(roleId);
		if(lstTblRoleMenuMapping != null) {
			for(TblRoleMenuMapping tblRoleMenuMapping :lstTblRoleMenuMapping) {
				lstMenu.add(tblRoleMenuMapping.getMenuPk());
			}
		}
		return lstMenu;
	}*/
}
