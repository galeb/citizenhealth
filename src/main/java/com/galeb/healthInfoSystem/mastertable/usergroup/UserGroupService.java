package com.galeb.healthInfoSystem.mastertable.usergroup;

import java.math.BigInteger;
import java.util.List;

import com.galeb.healthInfoSystem.data.TblRole;
import org.springframework.stereotype.Service;

//@Service
public interface UserGroupService {

	TblRole addRole(TblRole tblRole);
	
	List<TblRole> fetchAllRole();
	
	String deleteRole(Long payScaleId);
	
	TblRole fetchRolebyId(Long payScaleId);

//	String saveMenuRoleMap(List<Long> lstMenu, TblRole savedRole);

	List<Long> fetchMenuByRoleId(Long roleId);

	String saveMenuRoleMap(List<Long> lstMenu, TblRole savedRole);
}
