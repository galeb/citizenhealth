package com.galeb.healthInfoSystem.mastertable.usergroup;



import java.math.BigInteger;
import java.util.List;

import com.galeb.healthInfoSystem.common.dal.service.OperationTypeEnum;
import com.galeb.healthInfoSystem.data.TblRole;
import com.galeb.healthInfoSystem.data.TblRoleMenuMapping;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.galeb.healthInfoSystem.common.dal.service.CommonDAO;
import com.galeb.healthInfoSystem.common.dal.service.HibernateQueryDao;

@Repository
public class UserGroupRepoImpl implements UserGroupRepo{

	/*@Autowired
	CommonDAO commonDao;
	
	@Autowired
	HibernateQueryDao hibernateQuery;
	
	@Override
	public TblRole addRole(TblRole tblRole) {
		
		commonDao.saveOrUpdate(tblRole);
		return tblRole;
	}

	@Override
	public TblRole fetchRolebyRoleId(BigInteger roleID) {
		List<TblRole> lstRole  = commonDao.findEntity(TblRole.class, "id", OperationTypeEnum.EQ ,roleID);
		TblRole dbObjTblRole = new TblRole();
		if(!lstRole.isEmpty()) {
			dbObjTblRole =  lstRole.get(0);
		}
		return dbObjTblRole;
	}

	@Override
	public List<TblRole> fetchAllRole() {
		List<TblRole> lstRole  = commonDao.findEntity(TblRole.class);
		return lstRole;
	}

	@Override
	public boolean deleteRole(TblRole tblRole) {
		commonDao.delete(tblRole);
		return true;
	}

	@Override
	public String saveMenuRoleMap(List<TblRoleMenuMapping> lstTblRoleMenuMapping) {
		commonDao.saveOrUpdateAll(lstTblRoleMenuMapping);
		return "SuccessFullySaved";
	}

	@Override
	public List<TblRoleMenuMapping> fetchMenuByRoleId(BigInteger roleId) {
		List<TblRoleMenuMapping> lstMenu = commonDao.findEntity(TblRoleMenuMapping.class, "id",OperationTypeEnum.EQ ,roleId);
		if(!lstMenu.isEmpty()) {
			return lstMenu;
		}
		return null;
		
	}

	
	@Override
	public boolean deleteAllRoleMenuMapping(List<TblRoleMenuMapping> lstRoleMenuMap) {
		try {
//			Map<String,Object> varMap = new HashMap();
//			System.out.println("-->"+lstRoleMenuMap.get(0).getRolePk());
//			varMap.put("id", lstRoleMenuMap.get(0).getRolePk());
//			StringBuilder query = new StringBuilder();
//			query.append("DELETE FROM TBL_ROLEMENUMAP where ROLEPK= :id");
//			System.out.println("query:>"+query+" varMap:>"+varMap);
//			int status =hibernateQuery.updateDeleteSQLNewQuery(query.toString(),varMap);
//			System.out.println("status:>"+status);
			//commonDao.deleteAll(lstRoleMenuMap);
			for(TblRoleMenuMapping tblRoleMenuMapping :lstRoleMenuMap) {
				commonDao.delete(tblRoleMenuMapping);
			}
			return true;
		}catch(Exception e) {
			e.printStackTrace();
			return false;
		}
		
		
	}
	
	
	*/
}
