package com.galeb.healthInfoSystem.mastertable.usergroup;

import java.math.BigInteger;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.galeb.healthInfoSystem.common.utility.service.CommonUtility;
import com.galeb.healthInfoSystem.data.TblRole;
import com.galeb.healthInfoSystem.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.galeb.healthInfoSystem.bean.RoleMenuMapBean;
import com.galeb.healthInfoSystem.common.utility.service.CommonController;
import com.galeb.healthInfoSystem.data.TblUser;
import com.galeb.healthInfoSystem.management.menu.MenuService;
import com.galeb.healthInfoSystem.management.user.UserService;

@Controller
@RequestMapping("role")
public class UserGroupController {

	@Autowired
	UserGroupService roleService;
	
	@Autowired
	UserService userService;
	
	@Autowired
	MenuService menuService;
	
	@Autowired
	CommonController commonController;

	@Autowired
	RoleRepository roleRepository;
	
	@RequestMapping(value = "/view", method = RequestMethod.GET)
	public String addRole(HttpServletRequest request, Model model) {
		try {
			model.addAttribute("modelRoleMenuBean", new RoleMenuMapBean());
			model.addAttribute("lstRole", CommonUtility.convertToJsonString(roleService.fetchAllRole()));
			model.addAttribute("lstMenu", menuService.fetchAllChildMenu());
			return "com.healthInfo.role";
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@ResponseBody
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String saveRole(@ModelAttribute("modelRoleMenuBean") RoleMenuMapBean modelRoleMenuBean, HttpServletRequest request, Model model) {
		try {
			if(modelRoleMenuBean!=null) {				
				 TblRole savedRole = roleService.addRole(modelRoleMenuBean.getTblRole());
				 if(savedRole.getId() != null) {
					 roleService.saveMenuRoleMap(modelRoleMenuBean.getLstResMenu(),savedRole);
					 TblUser tblUser = CommonUtility.fetchUserFromSession(request);
					 CommonUtility.setToSession(request, "LSTMAINMENU", commonController.jsonMenuResposne(tblUser));	
				 }				 
				return "SuccessFullySaved";
			}else {
				return "EmptyForm";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;		
	}
	
	@ResponseBody
	@RequestMapping(value = "/fetchAll", method = RequestMethod.GET)
	public String saveRole(HttpServletRequest request, Model model) {
		try {
			String lstRole = CommonUtility.convertToJsonString(roleService.fetchAllRole());
			TblUser tblUser = CommonUtility.fetchUserFromSession(request);
			CommonUtility.setToSession(request, "LSTMAINMENU", commonController.jsonMenuResposne(tblUser));
			return lstRole;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;		
	}
	
	@ResponseBody
	@RequestMapping(value = "/delete/{roleId}", method = RequestMethod.GET)
	public String deleteRole(@PathVariable("roleId")Long roleId,HttpServletRequest request, Model model) {
		try {
			String resposneString =roleService.deleteRole(roleId);
			TblUser tblUser = CommonUtility.fetchUserFromSession(request);
			CommonUtility.setToSession(request, "LSTMAINMENU", commonController.jsonMenuResposne(tblUser));
			return resposneString;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;		
	}
	
	@ResponseBody
	@RequestMapping(value = "/edit/{roleId}", method = RequestMethod.GET)
	public String edit(@PathVariable("roleId")Long id,HttpServletRequest request, Model model) {
		try {
			RoleMenuMapBean roleMenuMapBean = new RoleMenuMapBean();
			//TblRole tblRole =roleService.fetchRolebyId(roleId);
			TblRole tblRole =roleRepository.findById(id).get();
			roleMenuMapBean.setTblRole(tblRole);
			List<Long> lstMenuPk =  roleService.fetchMenuByRoleId(id);
			roleMenuMapBean.setLstResMenu(lstMenuPk);
			return CommonUtility.convertToJsonString(roleMenuMapBean);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;		
	}
	

}
