package com.galeb.healthInfoSystem.controller;

/*
import com.galeb.citizenhealth.domain.MedicineInfo;
import com.galeb.citizenhealth.repository.MedicineInfoRepository;
import com.galeb.citizenhealth.service.UserService;
import com.galeb.citizenhealth.web.rest.errors.BadRequestAlertException;
import com.galeb.citizenhealth.web.rest.util.HeaderUtil;
import com.galeb.citizenhealth.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
*/
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing MedicineInfo.
 */
@RestController
@RequestMapping("/api")
public class MedicineInfoResource {

    private final Logger log = LoggerFactory.getLogger(MedicineInfoResource.class);

    private static final String ENTITY_NAME = "medicineInfo";


    /*@Autowired
    private UserService userService;
*/
    /*private final MedicineInfoRepository medicineInfoRepository;

    public MedicineInfoResource(MedicineInfoRepository medicineInfoRepository) {
        this.medicineInfoRepository = medicineInfoRepository;
    }

    *//**
     * POST  /medicine-infos : Create a new medicineInfo.
     *
     * @param medicineInfo the medicineInfo to create
     * @return the ResponseEntity with status 201 (Created) and with body the new medicineInfo, or with status 400 (Bad Request) if the medicineInfo has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     *//*
    @PostMapping("/medicine-infos")
    public ResponseEntity<MedicineInfo> createMedicineInfo(@Valid @RequestBody MedicineInfo medicineInfo) throws URISyntaxException {
        log.debug("REST request to save MedicineInfo : {}", medicineInfo);
        if (medicineInfo.getId() != null) {
            throw new BadRequestAlertException("A new medicineInfo cannot already have an ID", ENTITY_NAME, "idexists");
        }

        if(medicineInfo.getId() != null){
            medicineInfo.setCreateDate(.now());
            medicineInfo.setCreateBy(userService.getUserWithAuthorities().get().getId());
        }
        MedicineInfo result = medicineInfoRepository.save(medicineInfo);
        return ResponseEntity.created(new URI("/api/medicine-infos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    *//**
     * PUT  /medicine-infos : Updates an existing medicineInfo.
     *
     * @param medicineInfo the medicineInfo to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated medicineInfo,
     * or with status 400 (Bad Request) if the medicineInfo is not valid,
     * or with status 500 (Internal Server Error) if the medicineInfo couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     *//*
    @PutMapping("/medicine-infos")
    public ResponseEntity<MedicineInfo> updateMedicineInfo(@Valid @RequestBody MedicineInfo medicineInfo) throws URISyntaxException {
        log.debug("REST request to update MedicineInfo : {}", medicineInfo);
        if (medicineInfo.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }

        if(medicineInfo.getId() != null){
            medicineInfo.setUpdateDate(.now());
            medicineInfo.setUpdateBy(userService.getUserWithAuthorities().get().getId());
        }
        MedicineInfo result = medicineInfoRepository.save(medicineInfo);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, medicineInfo.getId().toString()))
            .body(result);
    }

    *//**
     * GET  /medicine-infos : get all the medicineInfos.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of medicineInfos in body
     *//*
    @GetMapping("/medicine-infos")
    public ResponseEntity<List<MedicineInfo>> getAllMedicineInfos(Pageable pageable) {
        log.debug("REST request to get a page of MedicineInfos");
        Page<MedicineInfo> page = medicineInfoRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/medicine-infos");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    *//**
     * GET  /medicine-infos/:id : get the "id" medicineInfo.
     *
     * @param id the id of the medicineInfo to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the medicineInfo, or with status 404 (Not Found)
     *//*
    @GetMapping("/medicine-infos/{id}")
    public ResponseEntity<MedicineInfo> getMedicineInfo(@PathVariable Long id) {
        log.debug("REST request to get MedicineInfo : {}", id);
        Optional<MedicineInfo> medicineInfo = medicineInfoRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(medicineInfo);
    }

    *//**
     * DELETE  /medicine-infos/:id : delete the "id" medicineInfo.
     *
     * @param id the id of the medicineInfo to delete
     * @return the ResponseEntity with status 200 (OK)
     *//*
    @DeleteMapping("/medicine-infos/{id}")
    public ResponseEntity<Void> deleteMedicineInfo(@PathVariable Long id) {
        log.debug("REST request to delete MedicineInfo : {}", id);
        medicineInfoRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }*/
}
