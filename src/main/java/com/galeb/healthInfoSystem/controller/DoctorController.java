/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.galeb.healthInfoSystem.controller;


import com.galeb.healthInfoSystem.bean.SessionBean;
import com.galeb.healthInfoSystem.common.utility.service.CommonUtility;
import com.galeb.healthInfoSystem.data.DoctorInfo;
import com.galeb.healthInfoSystem.data.PersonalInfo;
import com.galeb.healthInfoSystem.data.TblUser;
import com.galeb.healthInfoSystem.repository.DoctorInfoRepository;
import com.galeb.healthInfoSystem.repository.PersonalInfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/doctor-info")
public class DoctorController {

    @Autowired
    DoctorInfoRepository doctorInfoRepository;
    @Autowired
    PersonalInfoRepository personalInfoRepository;
  


    @RequestMapping(value="/index", method = RequestMethod.GET)
    public String view(Model model, RedirectAttributes redirectAttributes, HttpServletRequest request){
        if(request.isUserInRole("ROLE_ADMIN")){
            System.out.println("Role admin checked");
        }
        model.addAttribute("modelMedicine",new DoctorInfo());
        model.addAttribute("doctorInfoList", doctorInfoRepository.findAll());
        return "com.healthInfo.doctorInfo.index";
    }

    @RequestMapping(value = "/view")
    public String view(Model model) {
        model.addAttribute("modelDoctorInfo", new DoctorInfo());
        List<DoctorInfo> doctorInfoList = doctorInfoRepository.findAll();
        model.addAttribute("doctorInfoList",doctorInfoList);

        return "com.healthInfo.doctorInfo.view";
    }

    @RequestMapping(value = "/show/{id}")
    public String show(Model model,@PathVariable("id") Long id) {
        model.addAttribute("doctorInfo", doctorInfoRepository.findById(id));

        return "com.healthInfo.doctorInfo.show";
    }

    @RequestMapping(value = "/form")
    public String form(Model model) {
        model.addAttribute("modelDoctorInfo", new DoctorInfo());
        List<DoctorInfo> doctorInfoList = doctorInfoRepository.findAll();
        model.addAttribute("doctorInfoList",doctorInfoList);

        return "com.healthInfo.doctorInfo.form";
    }

    @RequestMapping(value = "/form-self")
    public String formSelf(Model model,HttpServletRequest request) {
        model.addAttribute("modelDoctorInfo", new DoctorInfo());
        SessionBean sessionBean = (SessionBean) request.getSession(false).getAttribute("AUTHSESSION");
        TblUser tblUser = sessionBean.getUser();
        DoctorInfo doctorInfo = new DoctorInfo();
        PersonalInfo personalInfo = personalInfoRepository.findOneByUserInfo(tblUser);
        if(personalInfo != null){
            doctorInfo = doctorInfoRepository.findOneByPersonalInfo(personalInfo);
        }

        model.addAttribute("personalInfo",personalInfo);
        model.addAttribute("doctorInfo",doctorInfo);

        return "com.healthInfo.doctorInfo.formSelf";
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String save(@ModelAttribute("modelDoctorInfo") DoctorInfo doctorInfo, HttpServletRequest request, RedirectAttributes redirectAttributes) {
        SessionBean sessionBean = (SessionBean) request.getSession(false).getAttribute("AUTHSESSION");
        TblUser tblUser = sessionBean.getUser();

        if (doctorInfo.getId() != null) {
            DoctorInfo oldDoctorInfo = doctorInfoRepository.getOne(doctorInfo.getId());
            doctorInfo.setCreateBy(oldDoctorInfo.getCreateBy());
            doctorInfo.setCreateDate(oldDoctorInfo.getCreateDate());
            doctorInfo.setUpdateBy(tblUser.getId());
            doctorInfo.setUpdateDate(new Date());
        }else{
            doctorInfo.setCreateBy(tblUser.getId());
            doctorInfo.setCreateDate(new Date());
        }
        doctorInfo = doctorInfoRepository.save(doctorInfo);
        

        redirectAttributes.addFlashAttribute("successMsg", "DoctorInfo Successfully saved");
        return "redirect:/doctor-info/index";
    }

    @RequestMapping(value = "/save-self-info", method = RequestMethod.POST)
    public String saveSelfInfo(@ModelAttribute("modelDoctorInfo") DoctorInfo doctorInfo, HttpServletRequest request, RedirectAttributes redirectAttributes) {
        SessionBean sessionBean = (SessionBean) request.getSession(false).getAttribute("AUTHSESSION");
        TblUser tblUser = sessionBean.getUser();

        PersonalInfo personalInfo = doctorInfo.getPersonalInfo();
        if(personalInfo == null){
            personalInfo.setUserInfo(tblUser);
            personalInfo.setCreateBy(tblUser.getId());
            personalInfo = personalInfoRepository.save(personalInfo);
        }else{
            personalInfo = personalInfoRepository.findOneByUserInfo(tblUser);
        }
        if (doctorInfo.getId() != null) {
            //DoctorInfo oldDoctorInfo = doctorInfoRepository.getOne(doctorInfo.getId());
            doctorInfo.setUpdateBy(tblUser.getId());
            doctorInfo.setUpdateDate(new Date());
        }else{
            doctorInfo.setPersonalInfo(personalInfo);
            doctorInfo.setCreateBy(tblUser.getId());
            doctorInfo.setCreateDate(new Date());
        }
        doctorInfo = doctorInfoRepository.save(doctorInfo);


        redirectAttributes.addFlashAttribute("successMsg", "DoctorInfo Successfully saved");
        return "redirect:/doctor-info/index";
    }

    @ResponseBody
    @RequestMapping(value = "/fetchAll")
    public String fetchAll() {
        return CommonUtility.convertToJsonString(doctorInfoRepository.findAll());
    }

    //@ResponseBody
    @RequestMapping(value = "/edit/{id}")
    public String edit(@PathVariable("id") Long id, Model model) {
        model.addAttribute("modelDoctorInfo", doctorInfoRepository.getOne(id));
        List<DoctorInfo> doctorInfoList = doctorInfoRepository.findAll();
        model.addAttribute("doctorInfoList",doctorInfoList);

        return "com.healthInfo.doctorInfo.edit";
        //return CommonUtility.convertToJsonString(DoctorInfoMstService.findById(productPk));
    }

    @ResponseBody
    @RequestMapping(value = "/delete/{id}")
    public String delete(@PathVariable("id") Long id, HttpServletRequest request) {
        SessionBean sessionBean = (SessionBean) request.getSession(false).getAttribute("AUTHSESSION");
        TblUser tblUser = sessionBean.getUser();
        DoctorInfo doctorInfo = doctorInfoRepository.getOne(id);
        if(doctorInfo != null){
            doctorInfoRepository.delete(doctorInfo);
            return "DELETEDSUCCSSFULLY";
        }
        return "FAILED";
        
    }

    @ResponseBody
    @RequestMapping(value = "/fetchByRegno", method = RequestMethod.GET)
    public String fetchByPID(HttpServletRequest request) {
        String pid = request.getParameter("regno");
        if (pid.trim().isEmpty()) {
            return "emptyCode";
        } else {
            DoctorInfo personalInfo = doctorInfoRepository.findOneByRegInfo(pid);
            return personalInfo != null ? CommonUtility.convertToJsonString(personalInfo) : "notFound";
        }
    }



}
