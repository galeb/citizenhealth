package com.galeb.healthInfoSystem.controller;

import com.galeb.healthInfoSystem.repository.DoctorInfoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing DoctorInfo.
 */
@RestController
@RequestMapping("/api")
public class DoctorInfoResource {

    private final Logger log = LoggerFactory.getLogger(DoctorInfoResource.class);

    private static final String ENTITY_NAME = "doctorInfo";

    //private final DoctorInfoRepository doctorInfoRepository;

    /*@Autowired
    private UserService userService;

    public DoctorInfoResource(DoctorInfoRepository doctorInfoRepository) {
        this.doctorInfoRepository = doctorInfoRepository;
    }

    *//**
     * POST  /doctor-infos : Create a new doctorInfo.
     *
     * @param doctorInfo the doctorInfo to create
     * @return the ResponseEntity with status 201 (Created) and with body the new doctorInfo, or with status 400 (Bad Request) if the doctorInfo has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     *//*
    @PostMapping("/doctor-infos")
    public ResponseEntity<DoctorInfo> createDoctorInfo(@Valid @RequestBody DoctorInfo doctorInfo) throws URISyntaxException {
        log.debug("REST request to save DoctorInfo : {}", doctorInfo);
        if (doctorInfo.getId() != null) {
            throw new BadRequestAlertException("A new doctorInfo cannot already have an ID", ENTITY_NAME, "idexists");
        }
        if(doctorInfo.getId() == null){
            doctorInfo.setCreateDate(.now());
            doctorInfo.setCreateBy(userService.getUserWithAuthorities().get().getId());
        }
        DoctorInfo result = doctorInfoRepository.save(doctorInfo);
        return ResponseEntity.created(new URI("/api/doctor-infos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    *//**
     * PUT  /doctor-infos : Updates an existing doctorInfo.
     *
     * @param doctorInfo the doctorInfo to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated doctorInfo,
     * or with status 400 (Bad Request) if the doctorInfo is not valid,
     * or with status 500 (Internal Server Error) if the doctorInfo couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     *//*
    @PutMapping("/doctor-infos")
    public ResponseEntity<DoctorInfo> updateDoctorInfo(@Valid @RequestBody DoctorInfo doctorInfo) throws URISyntaxException {
        log.debug("REST request to update DoctorInfo : {}", doctorInfo);
        if (doctorInfo.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }

        if(doctorInfo.getId() != null){
            doctorInfo.setUpdateDate(.now());
            doctorInfo.setUpdateBy(userService.getUserWithAuthorities().get().getId());
        }
        DoctorInfo result = doctorInfoRepository.save(doctorInfo);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, doctorInfo.getId().toString()))
            .body(result);
    }

    *//**
     * GET  /doctor-infos : get all the doctorInfos.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of doctorInfos in body
     *//*
    @GetMapping("/doctor-infos")
    public ResponseEntity<List<DoctorInfo>> getAllDoctorInfos(Pageable pageable) {
        log.debug("REST request to get a page of DoctorInfos");
        Page<DoctorInfo> page = doctorInfoRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/doctor-infos");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    *//**
     * GET  /doctor-infos/:id : get the "id" doctorInfo.
     *
     * @param id the id of the doctorInfo to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the doctorInfo, or with status 404 (Not Found)
     *//*
    @GetMapping("/doctor-infos/{id}")
    public ResponseEntity<DoctorInfo> getDoctorInfo(@PathVariable Long id) {
        log.debug("REST request to get DoctorInfo : {}", id);
        Optional<DoctorInfo> doctorInfo = doctorInfoRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(doctorInfo);
    }

    *//**
     * DELETE  /doctor-infos/:id : delete the "id" doctorInfo.
     *
     * @param id the id of the doctorInfo to delete
     * @return the ResponseEntity with status 200 (OK)
     *//*
    @DeleteMapping("/doctor-infos/{id}")
    public ResponseEntity<Void> deleteDoctorInfo(@PathVariable Long id) {
        log.debug("REST request to delete DoctorInfo : {}", id);
        doctorInfoRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }*/
}
