/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.galeb.healthInfoSystem.controller;


import com.galeb.healthInfoSystem.bean.SessionBean;
import com.galeb.healthInfoSystem.common.utility.service.CommonUtility;
import com.galeb.healthInfoSystem.data.MedicineInfo;
import com.galeb.healthInfoSystem.data.TblUser;
import com.galeb.healthInfoSystem.repository.MedicineInfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/medicines")
public class MedicineInfoController {

    @Autowired
    MedicineInfoRepository medicineRepository;
  


    @RequestMapping(value="/index", method = RequestMethod.GET)
    public String view(Model model, RedirectAttributes redirectAttributes){
        model.addAttribute("modelMedicine",new MedicineInfo());
        model.addAttribute("medicineList", medicineRepository.findAll());
        return "com.healthInfo.medicine.index";
    }

    @RequestMapping(value = "/view")
    public String view(Model model) {
        model.addAttribute("modelMedicine", new MedicineInfo());
        List<MedicineInfo> medicineList = medicineRepository.findAll();
        model.addAttribute("medicineList",medicineList);

        return "com.healthInfo.medicine.view";
    }

    @RequestMapping(value = "/show/{id}")
    public String show(Model model,@PathVariable("id") Long id) {
        model.addAttribute("medicine", medicineRepository.findById(id));

        return "com.healthInfo.medicine.show";
    }

    @RequestMapping(value = "/form")
    public String form(Model model) {
        model.addAttribute("modelMedicine", new MedicineInfo());
        List<MedicineInfo> medicineList = medicineRepository.findAll();
        model.addAttribute("medicineList",medicineList);

        return "com.healthInfo.medicine.form";
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String save(@ModelAttribute("modelMedicine") MedicineInfo medicine, HttpServletRequest request, RedirectAttributes redirectAttributes) {
        SessionBean sessionBean = (SessionBean) request.getSession(false).getAttribute("AUTHSESSION");
        TblUser tblUser = sessionBean.getUser();

        if (medicine.getId() != null) {
            MedicineInfo oldMedicine = medicineRepository.getOne(medicine.getId());
            medicine.setCreateBy(oldMedicine.getCreateBy());
            medicine.setCreateDate(oldMedicine.getCreateDate());
            medicine.setUpdateBy(tblUser.getId());
            medicine.setUpdateDate(new Date());
        }else{
            medicine.setCreateBy(tblUser.getId());
            medicine.setCreateDate(new Date());
        }
        medicine = medicineRepository.save(medicine);
        

        redirectAttributes.addFlashAttribute("successMsg", "Medicine Successfully saved");
        return "redirect:/medicines/index";
    }

    @ResponseBody
    @RequestMapping(value = "/fetchAll")
    public String fetchAll() {
        return CommonUtility.convertToJsonString(medicineRepository.findAll());
    }

    //@ResponseBody
    @RequestMapping(value = "/edit/{id}")
    public String edit(@PathVariable("id") Long id, Model model) {
        model.addAttribute("modelMedicine", medicineRepository.getOne(id));
        List<MedicineInfo> medicineList = medicineRepository.findAll();
        model.addAttribute("medicineList",medicineList);

        return "com.healthInfo.medicine.edit";
        //return CommonUtility.convertToJsonString(MedicineMstService.findById(productPk));
    }

    @ResponseBody
    @RequestMapping(value = "/delete/{id}")
    public String delete(@PathVariable("id") Long id, HttpServletRequest request) {
        SessionBean sessionBean = (SessionBean) request.getSession(false).getAttribute("AUTHSESSION");
        TblUser tblUser = sessionBean.getUser();
        MedicineInfo medicine = medicineRepository.getOne(id);
        if(medicine != null){
            medicineRepository.delete(medicine);
            return "DELETEDSUCCSSFULLY";
        }
        return "FAILED";
        
    }

    /*@ResponseBody
    @RequestMapping(value = "/childs/{id}")
    public List<Medicine> getChildMedicinesByParent(@PathVariable("id") Long id, HttpServletRequest request) {
        List<Medicine> childList = medicineRepository.findByParentMedicineId(id);
        return childList;
    }*/
    
}
