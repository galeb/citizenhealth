package com.galeb.healthInfoSystem.controller;

/*
import com.galeb.citizenhealth.domain.TreatmentInfo;
import com.galeb.citizenhealth.repository.TreatmentInfoRepository;
import com.galeb.citizenhealth.service.UserService;
import com.galeb.citizenhealth.web.rest.errors.BadRequestAlertException;
import com.galeb.citizenhealth.web.rest.util.HeaderUtil;
import com.galeb.citizenhealth.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
*/
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing TreatmentInfo.
 */
@RestController
@RequestMapping("/api")
public class TreatmentInfoResource {

    private final Logger log = LoggerFactory.getLogger(TreatmentInfoResource.class);

    private static final String ENTITY_NAME = "treatmentInfo";

    /*@Autowired
    private UserService userService;

    private final TreatmentInfoRepository treatmentInfoRepository;

    public TreatmentInfoResource(TreatmentInfoRepository treatmentInfoRepository) {
        this.treatmentInfoRepository = treatmentInfoRepository;
    }

    *//**
     * POST  /treatment-infos : Create a new treatmentInfo.
     *
     * @param treatmentInfo the treatmentInfo to create
     * @return the ResponseEntity with status 201 (Created) and with body the new treatmentInfo, or with status 400 (Bad Request) if the treatmentInfo has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     *//*
    @PostMapping("/treatment-infos")
    public ResponseEntity<TreatmentInfo> createTreatmentInfo(@Valid @RequestBody TreatmentInfo treatmentInfo) throws URISyntaxException {
        log.debug("REST request to save TreatmentInfo : {}", treatmentInfo);
        if (treatmentInfo.getId() != null) {
            throw new BadRequestAlertException("A new treatmentInfo cannot already have an ID", ENTITY_NAME, "idexists");
        }

        if(treatmentInfo.getId() != null){
            treatmentInfo.setCreateDate(.now());
            treatmentInfo.setCreateBy(userService.getUserWithAuthorities().get().getId());
        }
        TreatmentInfo result = treatmentInfoRepository.save(treatmentInfo);
        return ResponseEntity.created(new URI("/api/treatment-infos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    *//**
     * PUT  /treatment-infos : Updates an existing treatmentInfo.
     *
     * @param treatmentInfo the treatmentInfo to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated treatmentInfo,
     * or with status 400 (Bad Request) if the treatmentInfo is not valid,
     * or with status 500 (Internal Server Error) if the treatmentInfo couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     *//*
    @PutMapping("/treatment-infos")
    public ResponseEntity<TreatmentInfo> updateTreatmentInfo(@Valid @RequestBody TreatmentInfo treatmentInfo) throws URISyntaxException {
        log.debug("REST request to update TreatmentInfo : {}", treatmentInfo);
        if (treatmentInfo.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }

        if(treatmentInfo.getId() != null){
            treatmentInfo.setUpdateDate(.now());
            treatmentInfo.setUpdateBy(userService.getUserWithAuthorities().get().getId());
        }
        TreatmentInfo result = treatmentInfoRepository.save(treatmentInfo);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, treatmentInfo.getId().toString()))
            .body(result);
    }

    *//**
     * GET  /treatment-infos : get all the treatmentInfos.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of treatmentInfos in body
     *//*
    @GetMapping("/treatment-infos")
    public ResponseEntity<List<TreatmentInfo>> getAllTreatmentInfos(Pageable pageable) {
        log.debug("REST request to get a page of TreatmentInfos");
        Page<TreatmentInfo> page = treatmentInfoRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/treatment-infos");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    *//**
     * GET  /treatment-infos/:id : get the "id" treatmentInfo.
     *
     * @param id the id of the treatmentInfo to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the treatmentInfo, or with status 404 (Not Found)
     *//*
    @GetMapping("/treatment-infos/{id}")
    public ResponseEntity<TreatmentInfo> getTreatmentInfo(@PathVariable Long id) {
        log.debug("REST request to get TreatmentInfo : {}", id);
        Optional<TreatmentInfo> treatmentInfo = treatmentInfoRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(treatmentInfo);
    }

    *//**
     * DELETE  /treatment-infos/:id : delete the "id" treatmentInfo.
     *
     * @param id the id of the treatmentInfo to delete
     * @return the ResponseEntity with status 200 (OK)
     *//*
    @DeleteMapping("/treatment-infos/{id}")
    public ResponseEntity<Void> deleteTreatmentInfo(@PathVariable Long id) {
        log.debug("REST request to delete TreatmentInfo : {}", id);
        treatmentInfoRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }*/
}
