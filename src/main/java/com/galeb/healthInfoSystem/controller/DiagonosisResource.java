package com.galeb.healthInfoSystem.controller;

import com.galeb.healthInfoSystem.repository.DiagonosisRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Diagonosis.
 */
@RestController
@RequestMapping("/api")
public class DiagonosisResource {

    private final Logger log = LoggerFactory.getLogger(DiagonosisResource.class);

    private static final String ENTITY_NAME = "diagonosis";

    //private final DiagonosisRepository diagonosisRepository;

    /*@Autowired
    private UserService userService;
*/
   /* public DiagonosisResource(DiagonosisRepository diagonosisRepository) {
        this.diagonosisRepository = diagonosisRepository;
    }*/

    /**
     * POST  /diagonoses : Create a new diagonosis.
     *
     * @param diagonosis the diagonosis to create
     * @return the ResponseEntity with status 201 (Created) and with body the new diagonosis, or with status 400 (Bad Request) if the diagonosis has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    /*@PostMapping("/diagonoses")
    public ResponseEntity<Diagonosis> createDiagonosis(@Valid @RequestBody Diagonosis diagonosis) throws URISyntaxException {
        log.debug("REST request to save Diagonosis : {}", diagonosis);
        if (diagonosis.getId() != null) {
            throw new BadRequestAlertException("A new diagonosis cannot already have an ID", ENTITY_NAME, "idexists");
        }
        if(diagonosis.getId() == null){
            diagonosis.setCreateDate(.now());
            diagonosis.setCreateBy(userService.getUserWithAuthorities().get().getId());
        }
        Diagonosis result = diagonosisRepository.save(diagonosis);
        return ResponseEntity.created(new URI("/api/diagonoses/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    *//**
     * PUT  /diagonoses : Updates an existing diagonosis.
     *
     * @param diagonosis the diagonosis to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated diagonosis,
     * or with status 400 (Bad Request) if the diagonosis is not valid,
     * or with status 500 (Internal Server Error) if the diagonosis couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     *//*
    @PutMapping("/diagonoses")
    public ResponseEntity<Diagonosis> updateDiagonosis(@Valid @RequestBody Diagonosis diagonosis) throws URISyntaxException {
        log.debug("REST request to update Diagonosis : {}", diagonosis);
        if (diagonosis.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }

        if(diagonosis.getId() != null){
            diagonosis.setUpdateDate(.now());
            diagonosis.setUpdateBy(userService.getUserWithAuthorities().get().getId());
        }
        Diagonosis result = diagonosisRepository.save(diagonosis);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, diagonosis.getId().toString()))
            .body(result);
    }

    *//**
     * GET  /diagonoses : get all the diagonoses.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of diagonoses in body
     *//*
    @GetMapping("/diagonoses")
    public ResponseEntity<List<Diagonosis>> getAllDiagonoses(Pageable pageable) {
        log.debug("REST request to get a page of Diagonoses");
        Page<Diagonosis> page = diagonosisRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/diagonoses");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    *//**
     * GET  /diagonoses/:id : get the "id" diagonosis.
     *
     * @param id the id of the diagonosis to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the diagonosis, or with status 404 (Not Found)
     *//*
    @GetMapping("/diagonoses/{id}")
    public ResponseEntity<Diagonosis> getDiagonosis(@PathVariable Long id) {
        log.debug("REST request to get Diagonosis : {}", id);
        Optional<Diagonosis> diagonosis = diagonosisRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(diagonosis);
    }

    *//**
     * DELETE  /diagonoses/:id : delete the "id" diagonosis.
     *
     * @param id the id of the diagonosis to delete
     * @return the ResponseEntity with status 200 (OK)
     *//*
    @DeleteMapping("/diagonoses/{id}")
    public ResponseEntity<Void> deleteDiagonosis(@PathVariable Long id) {
        log.debug("REST request to delete Diagonosis : {}", id);
        diagonosisRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }*/
}
