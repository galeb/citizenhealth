package com.galeb.healthInfoSystem.controller;

import com.galeb.healthInfoSystem.repository.TreatmentDtlRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing TreatmentDtl.
 */
@RestController
@RequestMapping("/api")
public class TreatmentDtlResource {

    private final Logger log = LoggerFactory.getLogger(TreatmentDtlResource.class);

    private static final String ENTITY_NAME = "treatmentDtl";

    /*@Autowired
    private UserService userService;
*/
    /*private final TreatmentDtlRepository treatmentDtlRepository;

    public TreatmentDtlResource(TreatmentDtlRepository treatmentDtlRepository) {
        this.treatmentDtlRepository = treatmentDtlRepository;
    }*/

    /**
     * POST  /treatment-dtls : Create a new treatmentDtl.
     *
     * @param treatmentDtl the treatmentDtl to create
     * @return the ResponseEntity with status 201 (Created) and with body the new treatmentDtl, or with status 400 (Bad Request) if the treatmentDtl has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    /*@PostMapping("/treatment-dtls")
    public ResponseEntity<TreatmentDtl> createTreatmentDtl(@Valid @RequestBody TreatmentDtl treatmentDtl) throws URISyntaxException {
        log.debug("REST request to save TreatmentDtl : {}", treatmentDtl);
        if (treatmentDtl.getId() != null) {
            return (ResponseEntity<TreatmentDtl>) ResponseEntity.badRequest();
        }

        if(treatmentDtl.getId() != null){
            treatmentDtl.setCreateDate(.now());
//            treatmentDtl.setCreateBy(userService.getUserWithAuthorities().get().getId());
        }
        TreatmentDtl result = treatmentDtlRepository.save(treatmentDtl);
        return ResponseEntity.created(new URI("/api/treatment-dtls/" + result.getId()))
            .headers(HttpHeaders.EMPTY)
            .body(result);
    }

    *//**
     * PUT  /treatment-dtls : Updates an existing treatmentDtl.
     *
     * @param treatmentDtl the treatmentDtl to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated treatmentDtl,
     * or with status 400 (Bad Request) if the treatmentDtl is not valid,
     * or with status 500 (Internal Server Error) if the treatmentDtl couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     *//*
    @PutMapping("/treatment-dtls")
    public ResponseEntity<TreatmentDtl> updateTreatmentDtl(@Valid @RequestBody TreatmentDtl treatmentDtl) throws URISyntaxException {
        log.debug("REST request to update TreatmentDtl : {}", treatmentDtl);
        if (treatmentDtl.getId() == null) {
            return (ResponseEntity<TreatmentDtl>) ResponseEntity.badRequest();
        }

        if(treatmentDtl.getId() != null){
            treatmentDtl.setUpdateDate(.now());
            //treatmentDtl.setUpdateBy(userService.getUserWithAuthorities().get().getId());
        }
        TreatmentDtl result = treatmentDtlRepository.save(treatmentDtl);
        return ResponseEntity.ok()
            .headers(HttpHeaders.EMPTY)
            .body(result);
    }

    *//**
     * GET  /treatment-dtls : get all the treatmentDtls.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of treatmentDtls in body
     *//*
    @GetMapping("/treatment-dtls")
    public ResponseEntity<List<TreatmentDtl>> getAllTreatmentDtls(Pageable pageable) {
        log.debug("REST request to get a page of TreatmentDtls");
        Page<TreatmentDtl> page = treatmentDtlRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/treatment-dtls");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    *//**
     * GET  /treatment-dtls/:id : get the "id" treatmentDtl.
     *
     * @param id the id of the treatmentDtl to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the treatmentDtl, or with status 404 (Not Found)
     *//*
    @GetMapping("/treatment-dtls/{id}")
    public ResponseEntity<TreatmentDtl> getTreatmentDtl(@PathVariable Long id) {
        log.debug("REST request to get TreatmentDtl : {}", id);
        Optional<TreatmentDtl> treatmentDtl = treatmentDtlRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(treatmentDtl);
    }

    *//**
     * DELETE  /treatment-dtls/:id : delete the "id" treatmentDtl.
     *
     * @param id the id of the treatmentDtl to delete
     * @return the ResponseEntity with status 200 (OK)
     *//*
    @DeleteMapping("/treatment-dtls/{id}")
    public ResponseEntity<Void> deleteTreatmentDtl(@PathVariable Long id) {
        log.debug("REST request to delete TreatmentDtl : {}", id);
        treatmentDtlRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }*/
}
