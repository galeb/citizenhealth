/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.galeb.healthInfoSystem.controller;


import com.galeb.healthInfoSystem.bean.SessionBean;
import com.galeb.healthInfoSystem.bean.TreatmentDetailDTO;
import com.galeb.healthInfoSystem.common.utility.service.CommonUtility;
import com.galeb.healthInfoSystem.data.*;
import com.galeb.healthInfoSystem.management.language.LanguageBean;
import com.galeb.healthInfoSystem.repository.*;
import com.galeb.healthInfoSystem.service.PersonalInfoService;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.NumberToTextConverter;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;

@Controller
@RequestMapping("/personal-info")
public class PersonalInfoController {

    @Autowired
    PersonalInfoRepository personalInfoRepository;

    @Autowired
    PersonalInfoService personalInfoService;

    @Autowired
    LocationRepository locationRepository;

    @Autowired
    AddressRepository addressRepository;

    @Autowired
    DoctorInfoRepository doctorInfoRepository;

    @Autowired
    MedicalCenterInfoRepository medicalCenterInfoRepository;

    @Autowired
    TreatmentInfoRepository treatmentInfoRepository;

    @Autowired
    TreatmentDtlRepository treatmentDtlRepository;

    @Autowired
    MedicineInfoRepository medicineInfoRepository;

    @Autowired
    DiagonosisRepository diagonosisRepository;

    @Autowired
    TreatmentDiagoDtlRepository treatmentDiagoDtlRepository;

    @Autowired
    GeneralInfoXlsRepository generalInfoXlsRepository;

    @Autowired
    DivisionRepository divisionRepository;

    @Autowired
    DistrictRepository districtRepository;

    @Autowired
    ThanaRepository thanaRepository;


    @RequestMapping(value = "/view", method = RequestMethod.GET)
    public String view(Model model, RedirectAttributes redirectAttributes) {
        model.addAttribute("modelPersonalInfo", new PersonalInfo());
        model.addAttribute("divisionList", divisionRepository.findAllByOrderByNameAsc());
        model.addAttribute("personalInfoList", personalInfoRepository.findAll());
        return "com.healthInfo.personalInfoList";
    }

     @RequestMapping(value = "/profile", method = RequestMethod.GET)
    public String profile(Model model, RedirectAttributes redirectAttributes, HttpServletRequest req) {

     SessionBean sessionBean = (SessionBean) req.getSession(false).getAttribute("AUTHSESSION");
     TblUser tblUser = sessionBean.getUser();
         PersonalInfo personalInfo = personalInfoRepository.findOneByUserInfo(tblUser);

         //model.addAttribute("modelPersonalInfo", new PersonalInfo());
        model.addAttribute("divisionList", divisionRepository.findAllByOrderByNameAsc());
        model.addAttribute("personalInfo", personalInfo);
        return "com.healthInfo.personalInfoProfile";
    }

    @RequestMapping(value = "/patient-entry", method = RequestMethod.GET)
    public String patientEntry(Model model, RedirectAttributes redirectAttributes) {
        model.addAttribute("modelTreatmentDetailDTO", new TreatmentDetailDTO());
        model.addAttribute("locationList", locationRepository.findByLocationType("Division"));
        model.addAttribute("medicineList", medicineInfoRepository.findAll());
        model.addAttribute("diagonisisList", diagonosisRepository.findAll());
        // model.addAttribute("personalInfoList", personalInfoRepository.findAll());
        return "com.healthInfo.patientEntry";
    }


    @RequestMapping(value = "/upload-information", method = RequestMethod.GET)
    public String uploadInformation(Model model, RedirectAttributes redirectAttributes) {

        model.addAttribute("dto", new TreatmentDetailDTO());
        //model.addAttribute("medicalCenter", medicalCenterInfoRepository.findAll());
        return "com.healthInfo.informationUpload";
    }

    @PostMapping("/upload") // //new annotation since 4.3
    public String singleFileUpload(@ModelAttribute("modelTreatmentDetailDTO") TreatmentDetailDTO treatmentDetailDTO,
                                   @RequestParam("file") MultipartFile file, @RequestParam("file1") MultipartFile file1, @RequestParam("file2") MultipartFile file2,
                                   RedirectAttributes redirectAttributes,
                                   HttpServletRequest request) {

        if (file.isEmpty() || file1.isEmpty() || file2.isEmpty()) {
            redirectAttributes.addFlashAttribute("message", "Please select all files to upload");
            return "redirect:/personal-info/upload-information";
        }

        try {

            // Get the file and save it somewhere
            //byte[] bytes = file.getBytes();
            /*Path path = Paths.get(UPLOADED_FOLDER + file.getOriginalFilename());
            Files.write(path, bytes);*/
            InputStream it = file.getInputStream();
            XSSFWorkbook workbook = new XSSFWorkbook(it);
            XSSFSheet sheet = workbook.getSheetAt(0);
            ArrayList<GeneralInfoXls> generalInfoList = new ArrayList<>();
            System.out.println(sheet.getFirstRowNum());
            System.out.println(sheet.getLastRowNum());
            for(int i=sheet.getFirstRowNum()+1;i<=sheet.getLastRowNum();i++){
                GeneralInfoXls e= new GeneralInfoXls();
                e.setCreateDate(new Date());
                e.setCreateBy(CommonUtility.fetchUserFromSession(request).getId());
                e.setStatus(Boolean.FALSE);
                Row ro=sheet.getRow(i);
                for(int j=ro.getFirstCellNum();j<=ro.getLastCellNum();j++){
                    Cell ce = ro.getCell(j);
                    if(j==18){
                        //If you have Header in text It'll throw exception because it won't get NumericValue
                        if(ce.getCellType() == HSSFCell.CELL_TYPE_STRING){
                            e.setPatientID(ce.getStringCellValue());
                        }else if(ce.getCellType() == HSSFCell.CELL_TYPE_NUMERIC){
                            e.setPatientID(NumberToTextConverter.toText(ce.getNumericCellValue()));
                        }

                    }else
                    if(j==21){
                        if(ce.getCellType() == HSSFCell.CELL_TYPE_STRING){
                            e.setPatientNid(ce.getStringCellValue());
                        }else if(ce.getCellType() == HSSFCell.CELL_TYPE_NUMERIC){
                            e.setPatientNid(NumberToTextConverter.toText(ce.getNumericCellValue()));
                        }
                    }else
                    if(j==13){
                        if(ce.getCellType() == HSSFCell.CELL_TYPE_STRING){
                            e.setPatientBid(ce.getStringCellValue());
                        }else if(ce.getCellType() == HSSFCell.CELL_TYPE_NUMERIC){
                            e.setPatientBid(NumberToTextConverter.toText(ce.getNumericCellValue()));
                        }
                    }else
                    if(j==20){
                        if(ce.getCellType() == HSSFCell.CELL_TYPE_STRING){
                            e.setPatientName(ce.getStringCellValue());
                        }else if(ce.getCellType() == HSSFCell.CELL_TYPE_NUMERIC){
                            e.setPatientName(NumberToTextConverter.toText(ce.getNumericCellValue()));
                        }

                    }else
                    if(j==14){
                        if(ce.getCellType() == HSSFCell.CELL_TYPE_STRING){
                            e.setPatientContactNo(ce.getStringCellValue());
                        }else if(ce.getCellType() == HSSFCell.CELL_TYPE_NUMERIC){
                            e.setPatientContactNo(NumberToTextConverter.toText(ce.getNumericCellValue()));
                        }
                    }else
                    if(j==11){
                        if(ce.getCellType() == HSSFCell.CELL_TYPE_STRING){
                            e.setPateintDob(ce.getStringCellValue());
                        }else if(ce.getCellType() == HSSFCell.CELL_TYPE_NUMERIC){
                            e.setPateintDob(NumberToTextConverter.toText(ce.getNumericCellValue()));
                        }
                    }else
                    if(j==17){
                        if(ce.getCellType() == HSSFCell.CELL_TYPE_STRING){
                            e.setPatientGender(ce.getStringCellValue());
                        }else if(ce.getCellType() == HSSFCell.CELL_TYPE_NUMERIC){
                            e.setPatientGender(NumberToTextConverter.toText(ce.getNumericCellValue()));
                        }
                    }else
                    if(j==6){
                        if(ce.getCellType() == HSSFCell.CELL_TYPE_STRING){
                            e.setDoctorRegInfo(ce.getStringCellValue());
                        }else if(ce.getCellType() == HSSFCell.CELL_TYPE_NUMERIC){
                            e.setDoctorRegInfo(NumberToTextConverter.toText(ce.getNumericCellValue()));
                        }
                    }else
                    if(j==23){
                        if(ce.getCellType() == HSSFCell.CELL_TYPE_STRING){
                            e.setTreatmentDate(ce.getStringCellValue());
                        }else if(ce.getCellType() == HSSFCell.CELL_TYPE_NUMERIC){
                            e.setTreatmentDate(NumberToTextConverter.toText(ce.getNumericCellValue()));
                        }
                    }else
                    if(j==22){
                        if(ce.getCellType() == HSSFCell.CELL_TYPE_STRING){
                            e.setSymptom(ce.getStringCellValue());
                        }else if(ce.getCellType() == HSSFCell.CELL_TYPE_NUMERIC){
                            e.setSymptom(NumberToTextConverter.toText(ce.getNumericCellValue()));
                        }
                    }else
                    if(j==1){
                        if(ce.getCellType() == HSSFCell.CELL_TYPE_STRING){
                            e.setDisease(ce.getStringCellValue());
                        }else if(ce.getCellType() == HSSFCell.CELL_TYPE_NUMERIC){
                            e.setDisease(NumberToTextConverter.toText(ce.getNumericCellValue()));
                        }
                    }else
                    if(j==10){
                        if(ce.getCellType() == HSSFCell.CELL_TYPE_STRING){
                            e.setMedicalRegInfo(ce.getStringCellValue());
                        }else if(ce.getCellType() == HSSFCell.CELL_TYPE_NUMERIC){
                            e.setMedicalRegInfo(NumberToTextConverter.toText(ce.getNumericCellValue()));
                        }
                    }
                }
                generalInfoList.add(e);
            }

            if(generalInfoList.size() > 0){
                for (GeneralInfoXls each:generalInfoList) {
                    try{
                        generalInfoXlsRepository.save(each);
                    }catch(Exception e){
                        e.printStackTrace();
                        redirectAttributes.addFlashAttribute("message",
                                "Failed to upload '" + file.getOriginalFilename() + "'");

                    }

                }
                redirectAttributes.addFlashAttribute("message",
                        "You successfully uploaded '" + file.getOriginalFilename() + "'");
            }else {
                redirectAttributes.addFlashAttribute("message",
                        "Sheet is empty in '" + file.getOriginalFilename() + "'");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return "redirect:/personal-info/upload-information";
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public ModelAndView savePersonalInfo(@ModelAttribute("modelPersonalInfo") PersonalInfo personalInfo, HttpServletRequest req, RedirectAttributes redirectAttributes) {

        ModelAndView mv = new ModelAndView();

        Date date = new Date();
        SessionBean sessionBean = (SessionBean) req.getSession(false).getAttribute("AUTHSESSION");
        TblUser tblUser = sessionBean.getUser();
        if(personalInfo.getId() != null){
            PersonalInfo dbData = personalInfoRepository.getOne(personalInfo.getId());
            personalInfo.setUpdateBy(tblUser.getId());
            personalInfo.setUpdateDate(new Date());
            personalInfo.setpID(dbData.getpID());
        }else{
            personalInfo.setCreateBy(tblUser.getId());
            personalInfo.setCreateDate(new Date());
            personalInfo.setpID(String.valueOf(new Date().getTime()));
        }



        personalInfoService.save(personalInfo);

        redirectAttributes.addFlashAttribute("successMsg", "Personal Info Successfully saved");

        mv.setViewName("redirect:/personal-info/view");
        return mv;
    }
    @RequestMapping(value = "/save-profile", method = RequestMethod.POST)
    public ModelAndView saveProfilePersonalInfo(@ModelAttribute("personalInfo") PersonalInfo personalInfo, HttpServletRequest req, RedirectAttributes redirectAttributes) {

        ModelAndView mv = new ModelAndView();

        Date date = new Date();
        SessionBean sessionBean = (SessionBean) req.getSession(false).getAttribute("AUTHSESSION");
        TblUser tblUser = sessionBean.getUser();
        if(personalInfo.getId() != null){
            PersonalInfo dbData = personalInfoRepository.getOne(personalInfo.getId());
            personalInfo.setUpdateBy(tblUser.getId());
            personalInfo.setUpdateDate(new Date());
            personalInfo.setUserInfo(dbData.getUserInfo());
            personalInfo.setpID(dbData.getpID());
        }else{
            personalInfo.setCreateBy(tblUser.getId());
            personalInfo.setCreateDate(new Date());
            personalInfo.setpID(String.valueOf(new Date().getTime()));
            personalInfo.setUserInfo(tblUser);
        }



        personalInfoService.save(personalInfo);

        redirectAttributes.addFlashAttribute("successMsg", "Personal Info Successfully saved");

        mv.setViewName("redirect:/personal-info/profile");
        return mv;
    }

    @ResponseBody
    @RequestMapping(value = "/fetchAll", method = RequestMethod.GET)
    public String FetchalldataofAcr(HttpServletRequest req) {
        SessionBean sessionBean = (SessionBean) req.getSession(false).getAttribute("AUTHSESSION");
        TblUser tblUser = sessionBean.getUser();
        String code = req.getParameter("empCode");
        List<PersonalInfo> lstacrbyuserid = personalInfoRepository.findAll();
        return CommonUtility.convertToJsonString(lstacrbyuserid);
    }

    @ResponseBody
    @RequestMapping(value = "/syncXlsToDb", method = RequestMethod.GET)
    public String syncXlsTodb(HttpServletRequest req) {
        SessionBean sessionBean = (SessionBean) req.getSession(false).getAttribute("AUTHSESSION");
        TblUser tblUser = sessionBean.getUser();
        String response = "fail";
        List<GeneralInfoXls> unprocessedList = generalInfoXlsRepository.findAllByStatus(Boolean.FALSE);
        for (GeneralInfoXls each: unprocessedList) {
            PersonalInfo pateint = null;
            DoctorInfo doctor = null;

            if(pateint == null && each.getPatientNid() != null){
                pateint = personalInfoRepository.findOneByNid(each.getPatientNid());
            }
            if(pateint == null && each.getPatientBid() != null){
                pateint = personalInfoRepository.findOneByBid(each.getPatientBid());
            }
            if(pateint == null && each.getPatientID() != null){
                pateint = personalInfoRepository.findOneByPID(each.getPatientID());
            }
            if(pateint == null){
                pateint = new PersonalInfo();
                pateint.setCreateDate(new Date());
                pateint.setAddress(each.getPatientAddress());
                pateint.setBid(each.getPatientBid());
                pateint.setNid(each.getPatientNid());
                String pid = String.valueOf(new Date().getTime());

                while(personalInfoRepository.findOneByPID(pid) != null){
                    pid = String.valueOf(new Date().getTime())+Math.random();
                }
                pateint.setpID(pid);
                pateint.setName(each.getPatientName());
                pateint.setContactNo(each.getPatientContactNo());
                if(each.getPateintDob() != null && !each.getPateintDob().isEmpty()){
                    try {
                        Date dob=new SimpleDateFormat("dd/MM/yyyy").parse(each.getPateintDob());
                        pateint.setDob(dob);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
                String gender = each.getPatientGender();
                if(gender != null && !gender.isEmpty()){
                    if(gender.equalsIgnoreCase("male") != gender.equalsIgnoreCase("m")){
                        gender = "Male";
                    }else if(gender.equalsIgnoreCase("female") != gender.equalsIgnoreCase("f") || gender.contains("f")){
                        gender = "Female";
                    }else {
                        gender = "Other";
                    }
                }
                pateint.setGender(gender);
                personalInfoRepository.save(pateint);

            }
            if(each.getDoctorRegInfo() != null){
                doctor= doctorInfoRepository.findOneByRegInfo(each.getDoctorRegInfo());
            }

            if(doctor == null){
                PersonalInfo personalInfo = new PersonalInfo();
                personalInfo.setCreateDate(new Date());
                personalInfo.setAddress(each.getDoctorAddress());
                //personalInfo.setBid(each.getDoctorBid());
                //personalInfo.setNid(each.getDoc());
                String pid = String.valueOf(new Date().getTime())+Math.random();

                while(personalInfoRepository.findOneByPID(pid) != null){
                    pid = String.valueOf(new Date().getTime())+Math.random();
                }
                personalInfo.setpID(pid);
                personalInfo.setName(each.getDoctorName());
                personalInfo.setContactNo(each.getPatientContactNo());
                /*if(each.getPateintDob() != null && !each.getPateintDob().isEmpty()){
                    try {
                        Date dob=new SimpleDateFormat("dd/MM/yyyy").parse(each.getDoctor());
                        personalInfo.setDob(dob);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }*/
                /*String gender = each.getDoctorGender();
                if(gender != null && !gender.isEmpty()){
                    if(gender.equalsIgnoreCase("male") != gender.equalsIgnoreCase("m")){
                        gender = "Male";
                    }else if(gender.equalsIgnoreCase("female") != gender.equalsIgnoreCase("f") || gender.contains("f")){
                        gender = "Female";
                    }else {
                        gender = "Other";
                    }
                }*/
                //personalInfo.setGender(gender);
                personalInfo = personalInfoRepository.save(personalInfo);
                doctor = new DoctorInfo();
                doctor.setPersonalInfo(personalInfo);
                doctor.setRegInfo(each.getDoctorRegInfo());
                doctor.setName(each.getDoctorName());
                doctor.setAddress(each.getDoctorAddress());
                doctor.setContactNo(each.getDoctorContactNo());
                doctor.setDepartment(each.getDoctorDepartment());
                doctor.setCreateDate(new Date());
                doctor.setSpecility(each.getDoctorSpecility());
                doctor.setPersonalInfo(personalInfo);
               doctor= doctorInfoRepository.save(doctor);

                TreatmentInfo treatment = new TreatmentInfo();
                treatment.setPatientInfo(pateint);
                treatment.setDoctorInfo(doctor);
                treatment.setCreateDate(new Date());
                treatment.setDisease(each.getDisease());
                treatment.setSymptom(each.getSymptom());
                if(each.getTreatmentDate() != null && !each.getTreatmentDate().isEmpty()){
                    try {
                        Date treatmentDate=new SimpleDateFormat("dd/MM/yyyy").parse(each.getTreatmentDate());
                        treatment.setTreatmentDate(treatmentDate);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }else {
                    Calendar cal = Calendar.getInstance();
                    //get yesterday date
                    cal.add(Calendar.DATE, -1);
                    treatment.setTreatmentDate(cal.getTime());
                }
                treatmentInfoRepository.save(treatment);



            }

            each.setUpdateDate(new Date());
            each.setStatus(Boolean.TRUE);
            generalInfoXlsRepository.save(each);
            response = "success";
        }


        return response;
    }

    @ResponseBody
    @RequestMapping(value = "/patient-treatment-save", method = RequestMethod.POST)
    public String patientTreatmentSave(@ModelAttribute("modelTreatmentDetailDTO") TreatmentDetailDTO treatmentDetailDTO, HttpServletRequest request, Model model) {
        try {
            SessionBean sessionBean = (SessionBean) request.getSession(false).getAttribute("AUTHSESSION");
            TblUser tblUser = sessionBean.getUser();
            PersonalInfo personalInfo = treatmentDetailDTO.getPersonalInfo();
            if(personalInfo.getId() != null){
                personalInfo = personalInfoRepository.getOne(personalInfo.getId());
            }else {
                personalInfo = personalInfoRepository.save(personalInfo);
            }
            DoctorInfo doctorInfo = treatmentDetailDTO.getDoctorInfo();
            if(doctorInfo.getId() != null){
                doctorInfo = doctorInfoRepository.getOne(doctorInfo.getId());
            }else {
                doctorInfo = doctorInfoRepository.save(doctorInfo);
            }
            MedicalCenterInfo medicalCenterInfo = treatmentDetailDTO.getMedicalCenterInfo();
            if(medicalCenterInfo.getId() != null){
                medicalCenterInfo = medicalCenterInfoRepository.getOne(medicalCenterInfo.getId());
            }else {
                medicalCenterInfo = medicalCenterInfoRepository.save(medicalCenterInfo);

            }

            TreatmentInfo treatmentInfo = treatmentDetailDTO.getTreatmentInfo();
            if(treatmentInfo.getTreatmentDate() != null ){
                treatmentInfo.setPatientInfo(personalInfo);
                treatmentInfo.setDoctorInfo(doctorInfo);
                treatmentInfo.setMedicalCenterInfo(medicalCenterInfo);
                treatmentInfo.setCreateDate(new Date());
                treatmentInfo.setCreateBy(tblUser.getId());
                treatmentInfo = treatmentInfoRepository.save(treatmentInfo);
                List<TreatmentDtl> treatmentDtls = treatmentDetailDTO.getTreatmentDtlList();
                for (TreatmentDtl each : treatmentDtls) {
                    each.setTreatmentInfo(treatmentInfo);
                    treatmentDtlRepository.save(each);
                }
            }
            List<TreatmentDiagoDtl> diagoDtls = treatmentDetailDTO.getDiagoDtlList();
            for (TreatmentDiagoDtl each : diagoDtls) {
                each.setPatientInfo(personalInfo);
                each.setDoctorInfo(doctorInfo);
                each.setMedicalCenterInfo(medicalCenterInfo);
                if(treatmentInfo.getId() != null){
                    each.setTreatmentInfo(treatmentInfo);
                }
                treatmentDiagoDtlRepository.save(each);
            }



            return "SUCCESS";
        } catch (Exception e) {
            return "FAIL";
        }
    }

    @ResponseBody
    @RequestMapping(value = "/fetchByNid", method = RequestMethod.GET)
    public String fetchByEmpCode(HttpServletRequest request) {
        String nid = request.getParameter("nid");
        if (nid.trim().isEmpty()) {
            return "emptyCode";
        } else {
            PersonalInfo personalInfo = personalInfoRepository.findOneByNid(nid);
            return personalInfo != null ? CommonUtility.convertToJsonString(personalInfo) : "notFound";
        }
    }


    @ResponseBody
    @RequestMapping(value = "/fetchByPid", method = RequestMethod.GET)
    public String fetchByPID(HttpServletRequest request) {
        String pid = request.getParameter("pid");
        if (pid.trim().isEmpty()) {
            return "emptyCode";
        } else {
            PersonalInfo personalInfo = personalInfoRepository.findOneByPID(pid);
            return personalInfo != null ? CommonUtility.convertToJsonString(personalInfo) : "notFound";
        }
    }


    @ResponseBody
    @RequestMapping(value = "/fetchDoctorByRegInfo", method = RequestMethod.GET)
    public String fetchDoctorByRegInfo(HttpServletRequest request) {
        String pid = request.getParameter("regInfo");
        if (pid.trim().isEmpty()) {
            return "regInfo";
        } else {
            DoctorInfo dorcotInfo = doctorInfoRepository.findOneByRegInfo(pid);
            return dorcotInfo != null ? CommonUtility.convertToJsonString(dorcotInfo) : "notFound";
        }
    }


    @ResponseBody
    @RequestMapping(value = "/fetchMedicalCentryByRegInfo", method = RequestMethod.GET)
    public String fetchMedicalCenterByRegInfo(HttpServletRequest request) {
        String pid = request.getParameter("regInfo");
        if (pid.trim().isEmpty()) {
            return "regInfo";
        } else {
            MedicalCenterInfo centerInfo = medicalCenterInfoRepository.findOneByRegInfo(pid);
            return centerInfo != null ? CommonUtility.convertToJsonString(centerInfo) : "notFound";
        }
    }


    @ResponseBody
    @RequestMapping(value = "/delete/{pid}", method = RequestMethod.GET)
    public String deleteAcr(@PathVariable("pid") Long pid) {
        personalInfoRepository.deleteById(pid);
        return "DELETEDSUCCSSFULLY";
    }

    @ResponseBody
    @RequestMapping(value = "/find/{pid}", method = RequestMethod.GET)
    public String editAcr(@PathVariable("pid") Long pid) {

        PersonalInfo personalInfo = personalInfoRepository.getOne(pid);
        return CommonUtility.convertToJsonString(personalInfo);
    }

    @ResponseBody
    @RequestMapping(value = "/fetchDistrictByDivision", method = RequestMethod.GET)
    public List<District> fetchDistrictsByDivisionId(HttpServletRequest request) {
        String id = request.getParameter("id");
        if (id.trim().isEmpty()) {
            return null;
        } else {
            List<District> districts = districtRepository.findAllByDivisionId(Long.valueOf(id));
            return districts;
        }
    }

    @ResponseBody
    @RequestMapping(value = "/fetchThanasByDistrict", method = RequestMethod.GET)
    public List<Thana> fetchThanasByDistrict(HttpServletRequest request) {
        String id = request.getParameter("id");
        if (id.trim().isEmpty()) {
            return null;
        } else {
            List<Thana> thanas = thanaRepository.findAllByDistrictId(Long.valueOf(id));
            return thanas;
        }
    }


}
