/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.galeb.healthInfoSystem.controller;


import com.galeb.healthInfoSystem.bean.SessionBean;
import com.galeb.healthInfoSystem.common.utility.service.CommonUtility;
import com.galeb.healthInfoSystem.data.MedicalCenterInfo;
import com.galeb.healthInfoSystem.data.TblUser;
import com.galeb.healthInfoSystem.repository.MedicalCenterInfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.Date;

@Controller
@RequestMapping("/medical-centers")
public class MedicalCenterController {

    @Autowired
    MedicalCenterInfoRepository medicalCenterRepository;
  


    @RequestMapping(value="/index", method = RequestMethod.GET)
    public String view(Model model, RedirectAttributes redirectAttributes){
        model.addAttribute("modelMedicalCenter",new MedicalCenterInfo());
        model.addAttribute("medicalCenterList", medicalCenterRepository.findAll());
        return "com.healthInfo.medicalCenter.index";
    }

    @RequestMapping(value = "/view")
    public String view(Model model) {
        model.addAttribute("modelMedicalCenter", new MedicalCenterInfo());
        List<MedicalCenterInfo> medicalCenterList = medicalCenterRepository.findAll();
        model.addAttribute("medicalCenterList",medicalCenterList);

        return "com.healthInfo.medicalCenter.view";
    }

    @RequestMapping(value = "/show/{id}")
    public String show(Model model,@PathVariable("id") Long id) {
        model.addAttribute("medicalCenter", medicalCenterRepository.findById(id));

        return "com.healthInfo.medicalCenter.show";
    }

    @RequestMapping(value = "/form")
    public String form(Model model, HttpServletRequest request) {
        SessionBean sessionBean = (SessionBean) request.getSession(false).getAttribute("AUTHSESSION");
        TblUser tblUser = sessionBean.getUser();

        model.addAttribute("modelMedicalCenter", new MedicalCenterInfo());
        List<MedicalCenterInfo> medicalCenterList = medicalCenterRepository.findAll();
        model.addAttribute("medicalCenterList",medicalCenterList);
        /*MedicalCenterInfo medicalCenterInfo = medicalCenterRepository.findOneByUsers(tblUser);
        model.addAttribute("medicalCenterInfo",medicalCenterInfo);
*/
        return "com.healthInfo.medicalCenter.form";
    }

    @RequestMapping(value = "/form-self")
    public String formSelf(Model model, HttpServletRequest request) {
        SessionBean sessionBean = (SessionBean) request.getSession(false).getAttribute("AUTHSESSION");
        TblUser tblUser = sessionBean.getUser();

        model.addAttribute("modelMedicalCenter", new MedicalCenterInfo());
        List<MedicalCenterInfo> medicalCenterList = medicalCenterRepository.findAll();
        model.addAttribute("medicalCenterList",medicalCenterList);
        MedicalCenterInfo medicalCenterInfo = medicalCenterRepository.findOneByUsers(tblUser);
        model.addAttribute("medicalCenterInfo",medicalCenterInfo);

        return "com.healthInfo.medicalCenter.formSelf";
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String save(@ModelAttribute("modelMedicalCenter") MedicalCenterInfo medicalCenter, HttpServletRequest request, RedirectAttributes redirectAttributes) {
        SessionBean sessionBean = (SessionBean) request.getSession(false).getAttribute("AUTHSESSION");
        TblUser tblUser = sessionBean.getUser();

        if (medicalCenter.getId() != null) {
            MedicalCenterInfo oldMedicalCenter = medicalCenterRepository.getOne(medicalCenter.getId());
            medicalCenter.setCreateBy(oldMedicalCenter.getCreateBy());
            medicalCenter.setCreateDate(oldMedicalCenter.getCreateDate());
            medicalCenter.setUpdateBy(tblUser.getId());
            medicalCenter.setUpdateDate(new Date());
        }else{
            medicalCenter.setCreateBy(tblUser.getId());
            medicalCenter.setCreateDate(new Date());
        }
        medicalCenter = medicalCenterRepository.save(medicalCenter);
        

        redirectAttributes.addFlashAttribute("successMsg", "MedicalCenter Successfully saved");
        return "redirect:/medical-centers/index";
    }

    @RequestMapping(value = "/save-self-center", method = RequestMethod.POST)
    public String saveSelfCenter(@ModelAttribute("modelMedicalCenter") MedicalCenterInfo medicalCenter, HttpServletRequest request, RedirectAttributes redirectAttributes) {
        SessionBean sessionBean = (SessionBean) request.getSession(false).getAttribute("AUTHSESSION");
        TblUser tblUser = sessionBean.getUser();

        if (medicalCenter.getId() != null) {
            MedicalCenterInfo oldMedicalCenter = medicalCenterRepository.getOne(medicalCenter.getId());
            medicalCenter.setCreateBy(oldMedicalCenter.getCreateBy());
            medicalCenter.setCreateDate(oldMedicalCenter.getCreateDate());
            medicalCenter.setUpdateBy(tblUser.getId());
            medicalCenter.setUpdateDate(new Date());
        }else{
            medicalCenter.setCreateBy(tblUser.getId());
            medicalCenter.setCreateDate(new Date());
        }
        medicalCenter = medicalCenterRepository.save(medicalCenter);
        if(medicalCenter.getUsers() == null || medicalCenter.getUsers().size() == 0){
           /* Set<TblUser> users = new LinkedHashSet<TblUser>();
            users.add(tblUser);*/
            medicalCenter.getUsers().add(tblUser);
            medicalCenterRepository.save(medicalCenter);
        }

        redirectAttributes.addFlashAttribute("successMsg", "MedicalCenter Successfully saved");
        return "redirect:/medical-centers/form-self";
    }

    @ResponseBody
    @RequestMapping(value = "/fetchAll")
    public String fetchAll() {
        return CommonUtility.convertToJsonString(medicalCenterRepository.findAll());
    }

    //@ResponseBody
    @RequestMapping(value = "/edit/{id}")
    public String edit(@PathVariable("id") Long id, Model model) {
        model.addAttribute("modelMedicalCenter", medicalCenterRepository.getOne(id));
        List<MedicalCenterInfo> medicalCenterList = medicalCenterRepository.findAll();
        model.addAttribute("medicalCenterList",medicalCenterList);

        return "com.healthInfo.medicalCenter.edit";
        //return CommonUtility.convertToJsonString(MedicalCenterMstService.findById(productPk));
    }

    @ResponseBody
    @RequestMapping(value = "/delete/{id}")
    public String delete(@PathVariable("id") Long id, HttpServletRequest request) {
        SessionBean sessionBean = (SessionBean) request.getSession(false).getAttribute("AUTHSESSION");
        TblUser tblUser = sessionBean.getUser();
        MedicalCenterInfo medicalCenter = medicalCenterRepository.getOne(id);
        if(medicalCenter != null){
            medicalCenterRepository.delete(medicalCenter);
            return "DELETEDSUCCSSFULLY";
        }
        return "FAILED";
        
    }

    /*@ResponseBody
    @RequestMapping(value = "/childs/{id}")
    public List<MedicalCenter> getChildMedicalCentersByParent(@PathVariable("id") Long id, HttpServletRequest request) {
        List<MedicalCenter> childList = medicalCenterRepository.findByParentMedicalCenterId(id);
        return childList;
    }*/
    
}
