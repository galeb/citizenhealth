package com.galeb.healthInfoSystem.controller;

/*
import com.galeb.citizenhealth.domain.TreatmentDiagoDtl;
import com.galeb.citizenhealth.repository.TreatmentDiagoDtlRepository;
import com.galeb.citizenhealth.service.UserService;
import com.galeb.citizenhealth.web.rest.errors.BadRequestAlertException;
import com.galeb.citizenhealth.web.rest.util.HeaderUtil;
import com.galeb.citizenhealth.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
*/
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing TreatmentDiagoDtl.
 */
@RestController
@RequestMapping("/api")
public class TreatmentDiagoDtlResource {

    private final Logger log = LoggerFactory.getLogger(TreatmentDiagoDtlResource.class);

    private static final String ENTITY_NAME = "treatmentDiagoDtl";

    /*@Autowired
    private UserService userService;

    private final TreatmentDiagoDtlRepository treatmentDiagoDtlRepository;

    public TreatmentDiagoDtlResource(TreatmentDiagoDtlRepository treatmentDiagoDtlRepository) {
        this.treatmentDiagoDtlRepository = treatmentDiagoDtlRepository;
    }

    *//**
     * POST  /treatment-diago-dtls : Create a new treatmentDiagoDtl.
     *
     * @param treatmentDiagoDtl the treatmentDiagoDtl to create
     * @return the ResponseEntity with status 201 (Created) and with body the new treatmentDiagoDtl, or with status 400 (Bad Request) if the treatmentDiagoDtl has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     *//*
    @PostMapping("/treatment-diago-dtls")
    public ResponseEntity<TreatmentDiagoDtl> createTreatmentDiagoDtl(@RequestBody TreatmentDiagoDtl treatmentDiagoDtl) throws URISyntaxException {
        log.debug("REST request to save TreatmentDiagoDtl : {}", treatmentDiagoDtl);
        if (treatmentDiagoDtl.getId() != null) {
            throw new BadRequestAlertException("A new treatmentDiagoDtl cannot already have an ID", ENTITY_NAME, "idexists");
        }

        if(treatmentDiagoDtl.getId() != null){
            treatmentDiagoDtl.setCreateDate(.now());
            treatmentDiagoDtl.setCreateBy(userService.getUserWithAuthorities().get().getId());
        }
        TreatmentDiagoDtl result = treatmentDiagoDtlRepository.save(treatmentDiagoDtl);
        return ResponseEntity.created(new URI("/api/treatment-diago-dtls/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    *//**
     * PUT  /treatment-diago-dtls : Updates an existing treatmentDiagoDtl.
     *
     * @param treatmentDiagoDtl the treatmentDiagoDtl to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated treatmentDiagoDtl,
     * or with status 400 (Bad Request) if the treatmentDiagoDtl is not valid,
     * or with status 500 (Internal Server Error) if the treatmentDiagoDtl couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     *//*
    @PutMapping("/treatment-diago-dtls")
    public ResponseEntity<TreatmentDiagoDtl> updateTreatmentDiagoDtl(@RequestBody TreatmentDiagoDtl treatmentDiagoDtl) throws URISyntaxException {
        log.debug("REST request to update TreatmentDiagoDtl : {}", treatmentDiagoDtl);
        if (treatmentDiagoDtl.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }

        if(treatmentDiagoDtl.getId() != null){
            treatmentDiagoDtl.setUpdateDate(.now());
            treatmentDiagoDtl.setUpdateBy(userService.getUserWithAuthorities().get().getId());
        }
        TreatmentDiagoDtl result = treatmentDiagoDtlRepository.save(treatmentDiagoDtl);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, treatmentDiagoDtl.getId().toString()))
            .body(result);
    }

    *//**
     * GET  /treatment-diago-dtls : get all the treatmentDiagoDtls.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of treatmentDiagoDtls in body
     *//*
    @GetMapping("/treatment-diago-dtls")
    public ResponseEntity<List<TreatmentDiagoDtl>> getAllTreatmentDiagoDtls(Pageable pageable) {
        log.debug("REST request to get a page of TreatmentDiagoDtls");
        Page<TreatmentDiagoDtl> page = treatmentDiagoDtlRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/treatment-diago-dtls");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    *//**
     * GET  /treatment-diago-dtls/:id : get the "id" treatmentDiagoDtl.
     *
     * @param id the id of the treatmentDiagoDtl to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the treatmentDiagoDtl, or with status 404 (Not Found)
     *//*
    @GetMapping("/treatment-diago-dtls/{id}")
    public ResponseEntity<TreatmentDiagoDtl> getTreatmentDiagoDtl(@PathVariable Long id) {
        log.debug("REST request to get TreatmentDiagoDtl : {}", id);
        Optional<TreatmentDiagoDtl> treatmentDiagoDtl = treatmentDiagoDtlRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(treatmentDiagoDtl);
    }

    *//**
     * DELETE  /treatment-diago-dtls/:id : delete the "id" treatmentDiagoDtl.
     *
     * @param id the id of the treatmentDiagoDtl to delete
     * @return the ResponseEntity with status 200 (OK)
     *//*
    @DeleteMapping("/treatment-diago-dtls/{id}")
    public ResponseEntity<Void> deleteTreatmentDiagoDtl(@PathVariable Long id) {
        log.debug("REST request to delete TreatmentDiagoDtl : {}", id);
        treatmentDiagoDtlRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }*/
}
