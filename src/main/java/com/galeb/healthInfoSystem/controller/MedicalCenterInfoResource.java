package com.galeb.healthInfoSystem.controller;

/*
import com.galeb.citizenhealth.domain.MedicalCenterInfo;
import com.galeb.citizenhealth.repository.MedicalCenterInfoRepository;
import com.galeb.citizenhealth.service.UserService;
import com.galeb.citizenhealth.web.rest.errors.BadRequestAlertException;
import com.galeb.citizenhealth.web.rest.util.HeaderUtil;
import com.galeb.citizenhealth.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
*/
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing MedicalCenterInfo.
 */
@RestController
@RequestMapping("/api")
public class MedicalCenterInfoResource {

    private final Logger log = LoggerFactory.getLogger(MedicalCenterInfoResource.class);

    private static final String ENTITY_NAME = "medicalCenterInfo";

   /* @Autowired
    private UserService userService;

    private final MedicalCenterInfoRepository medicalCenterInfoRepository;

    public MedicalCenterInfoResource(MedicalCenterInfoRepository medicalCenterInfoRepository) {
        this.medicalCenterInfoRepository = medicalCenterInfoRepository;
    }

    *//**
     * POST  /medical-center-infos : Create a new medicalCenterInfo.
     *
     * @param medicalCenterInfo the medicalCenterInfo to create
     * @return the ResponseEntity with status 201 (Created) and with body the new medicalCenterInfo, or with status 400 (Bad Request) if the medicalCenterInfo has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     *//*
    @PostMapping("/medical-center-infos")
    public ResponseEntity<MedicalCenterInfo> createMedicalCenterInfo(@Valid @RequestBody MedicalCenterInfo medicalCenterInfo) throws URISyntaxException {
        log.debug("REST request to save MedicalCenterInfo : {}", medicalCenterInfo);
        if (medicalCenterInfo.getId() != null) {
            throw new BadRequestAlertException("A new medicalCenterInfo cannot already have an ID", ENTITY_NAME, "idexists");
        }

        if(medicalCenterInfo.getId() != null){
            medicalCenterInfo.setCreateDate(.now());
            medicalCenterInfo.setCreateBy(userService.getUserWithAuthorities().get().getId());
        }
        MedicalCenterInfo result = medicalCenterInfoRepository.save(medicalCenterInfo);
        return ResponseEntity.created(new URI("/api/medical-center-infos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    *//**
     * PUT  /medical-center-infos : Updates an existing medicalCenterInfo.
     *
     * @param medicalCenterInfo the medicalCenterInfo to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated medicalCenterInfo,
     * or with status 400 (Bad Request) if the medicalCenterInfo is not valid,
     * or with status 500 (Internal Server Error) if the medicalCenterInfo couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     *//*
    @PutMapping("/medical-center-infos")
    public ResponseEntity<MedicalCenterInfo> updateMedicalCenterInfo(@Valid @RequestBody MedicalCenterInfo medicalCenterInfo) throws URISyntaxException {
        log.debug("REST request to update MedicalCenterInfo : {}", medicalCenterInfo);
        if (medicalCenterInfo.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }

        if(medicalCenterInfo.getId() != null){
            medicalCenterInfo.setUpdateDate(.now());
            medicalCenterInfo.setUpdateBy(userService.getUserWithAuthorities().get().getId());
        }
        MedicalCenterInfo result = medicalCenterInfoRepository.save(medicalCenterInfo);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, medicalCenterInfo.getId().toString()))
            .body(result);
    }

    *//**
     * GET  /medical-center-infos : get all the medicalCenterInfos.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of medicalCenterInfos in body
     *//*
    @GetMapping("/medical-center-infos")
    public ResponseEntity<List<MedicalCenterInfo>> getAllMedicalCenterInfos(Pageable pageable) {
        log.debug("REST request to get a page of MedicalCenterInfos");
        Page<MedicalCenterInfo> page = medicalCenterInfoRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/medical-center-infos");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    *//**
     * GET  /medical-center-infos/:id : get the "id" medicalCenterInfo.
     *
     * @param id the id of the medicalCenterInfo to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the medicalCenterInfo, or with status 404 (Not Found)
     *//*
    @GetMapping("/medical-center-infos/{id}")
    public ResponseEntity<MedicalCenterInfo> getMedicalCenterInfo(@PathVariable Long id) {
        log.debug("REST request to get MedicalCenterInfo : {}", id);
        Optional<MedicalCenterInfo> medicalCenterInfo = medicalCenterInfoRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(medicalCenterInfo);
    }

    *//**
     * DELETE  /medical-center-infos/:id : delete the "id" medicalCenterInfo.
     *
     * @param id the id of the medicalCenterInfo to delete
     * @return the ResponseEntity with status 200 (OK)
     *//*
    @DeleteMapping("/medical-center-infos/{id}")
    public ResponseEntity<Void> deleteMedicalCenterInfo(@PathVariable Long id) {
        log.debug("REST request to delete MedicalCenterInfo : {}", id);
        medicalCenterInfoRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }*/
}
