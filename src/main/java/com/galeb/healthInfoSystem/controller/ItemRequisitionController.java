/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.galeb.healthInfoSystem.controller;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.math.BigInteger;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ITMCS-1
 */
@Controller
@RequestMapping("/itemRequisition")
public class ItemRequisitionController {
   /* @Autowired
    ItemRequisitionMstService itemRequisitionMstService;

    @Autowired
    ItemRequisitionDtlService itemRequisitionDtlService;

    @Autowired
    ProductService productService;

    static final ObjectMapper om = new ObjectMapper();

    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public ModelAndView index(Model model, HttpServletRequest request) {
        //SessionBean sessionBean = (SessionBean) request.getSession(false).getAttribute("AUTHSESSION");
        //TblUser tblUser = sessionBean.getUser();
        List<TblStrItemRequisitionMaster> itemRequisitionMasterList = itemRequisitionMstService.fechAll();
        ModelAndView map = new ModelAndView("com.pims.store.itemRequisition.index");
        map.addObject("requisitionList", itemRequisitionMasterList);
        return map;
    }
    @RequestMapping(value = "/view")
    public String view(Model model) {
        model.addAttribute("modelItemRequisition", new TblStrItemRequisitionMaster());
        List<TblStrProduct> productList = productService.fechAll();
        model.addAttribute("productList",productList);

        return "com.pims.store.itemRequisition";
    }

    @RequestMapping(value = "/show/{id}")
    public String show(Model model,@PathVariable("id") BigInteger id) {
        model.addAttribute("itemRequisition", itemRequisitionMstService.findById(id));

        return "com.pims.store.itemRequisition.show";
    }

    @RequestMapping(value = "/form")
    public String form(Model model) {
        model.addAttribute("modelItemRequisition", new TblStrItemRequisitionMaster());
        List<TblStrProduct> productList = productService.fechAll();
        model.addAttribute("productList",productList);

        return "com.pims.store.itemRequisition";
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String save(@ModelAttribute("modelProduct") TblStrItemRequisitionMaster tblStrItemRequisitionMaster, HttpServletRequest request, RedirectAttributes redirectAttributes) {
        SessionBean sessionBean = (SessionBean) request.getSession(false).getAttribute("AUTHSESSION");
        TblUser tblUser = sessionBean.getUser();
        List<TblStrItemRequisitionDetail> tblStrItemRequisitionDetails = tblStrItemRequisitionMaster.getTblStrItemRequisitionDetails();
        tblStrItemRequisitionMaster.setTblStrApprovalStatus(TblStrApprovalStatus.ENTRY);

        tblStrItemRequisitionMaster.setTblStrItemRequisitionDetails(null);
        if (tblStrItemRequisitionMaster.getId() != null) {
            TblStrItemRequisitionMaster requisitionOld = itemRequisitionMstService.findById(tblStrItemRequisitionMaster.getId());
            tblStrItemRequisitionMaster.setActionBy(tblUser);
            tblStrItemRequisitionMaster.setActionDate(new Date());
            tblStrItemRequisitionMaster.setEntryBy(requisitionOld.getEntryBy());
            tblStrItemRequisitionMaster.setTranscDate(requisitionOld.getTranscDate());

        }else{
            tblStrItemRequisitionMaster.setEntryBy(tblUser);
            tblStrItemRequisitionMaster.setTranscDate(new Date());
         }
       tblStrItemRequisitionMaster = itemRequisitionMstService.saveItemRequisitionMst(tblStrItemRequisitionMaster);

        for (TblStrItemRequisitionDetail each:tblStrItemRequisitionDetails) {
            each.setTblStrItemRequisitionMaster(tblStrItemRequisitionMaster);
            itemRequisitionDtlService.saveItemRequisitionDtl(each);
        }

        redirectAttributes.addFlashAttribute("successMsg", "Requisition Successfully saved");
        return "redirect:/itemRequisition/index";
    }

    @ResponseBody
    @RequestMapping(value = "/fetchAll")
    public String fetchAll() {
        return CommonUtility.convertToJsonString(itemRequisitionMstService.fechAll());
    }

    //@ResponseBody
    @RequestMapping(value = "/edit/{id}")
    public String edit(@PathVariable("id") BigInteger productPk,Model model) {
        model.addAttribute("modelItemRequisition", itemRequisitionMstService.findById(productPk));
        List<TblStrProduct> productList = productService.fechAll();
        model.addAttribute("productList",productList);

        return "com.pims.store.itemRequisition.edit";
        //return CommonUtility.convertToJsonString(itemRequisitionMstService.findById(productPk));
    }

    @ResponseBody
    @RequestMapping(value = "/delete/{id}")
    public String delete(@PathVariable("id") BigInteger productPk, HttpServletRequest request) {
        SessionBean sessionBean = (SessionBean) request.getSession(false).getAttribute("AUTHSESSION");
        TblUser tblUser = sessionBean.getUser();
        itemRequisitionMstService.deleteItemRequisitionMst(productPk, tblUser);
        return "DELETEDSUCCSSFULLY";
    }


    @ResponseBody
    @RequestMapping(value = "/dtl/delete/{id}")
    public String deleteDtl(@PathVariable("id") BigInteger id, HttpServletRequest request) {
        SessionBean sessionBean = (SessionBean) request.getSession(false).getAttribute("AUTHSESSION");
        TblUser tblUser = sessionBean.getUser();
        itemRequisitionDtlService.deleteItemRequisitionDtl(id, tblUser);
        return "DELETEDSUCCSSFULLY";
    }

    @ResponseBody
    @RequestMapping(value = "/takeAction", method = RequestMethod.GET)
    public String takeAction(@RequestParam("i") BigInteger tblStrItemRequisitonId, @RequestParam("t") String actionType, @RequestParam("d") TblStrDriverInfo tblStrDriverInfo, @RequestParam("r") String remarks, HttpServletRequest request) {
        String status;
        Map<String, Object> m = new HashMap();
        try {
            TblStrItemRequisitionMaster tblStrItemRequisitionMaster = itemRequisitionMstService.findById(tblStrItemRequisitonId);

            TblUser tblUser = CommonUtility.fetchUserFromSession(request);
            if(tblStrItemRequisitionMaster != null && actionType.equals("SUBMIT")){
                tblStrItemRequisitionMaster.setTblStrApprovalStatus(TblStrApprovalStatus.SUBMITTED);
            }
            else if(tblStrItemRequisitionMaster != null && actionType.equals("APPROVE")){
                tblStrItemRequisitionMaster.setTblStrApprovalStatus(TblStrApprovalStatus.APPROVED);
            }
            else if(tblStrItemRequisitionMaster != null && actionType.equals("REJECT")){
                tblStrItemRequisitionMaster.setTblStrApprovalStatus(TblStrApprovalStatus.REJECTED);
            }
            tblStrItemRequisitionMaster.setActionRemarks(remarks);
            tblStrItemRequisitionMaster.setActionBy(tblUser);
            tblStrItemRequisitionMaster.setActionDate(new Date());
            itemRequisitionMstService.saveItemRequisitionMst(tblStrItemRequisitionMaster);
            status = "OK";
        } catch (Exception e){
            status = "ERROR";
            m.put("error", e.getMessage());
            System.out.println("Error : " + e);
            e.printStackTrace();
        }
        m.put("status", status);

        String json = null;
        try {
            json = om.writeValueAsString(m);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return json;
    }*/
}
