/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.galeb.healthInfoSystem.controller;


import com.galeb.healthInfoSystem.bean.SessionBean;
import com.galeb.healthInfoSystem.common.utility.service.CommonUtility;
import com.galeb.healthInfoSystem.data.MedicalService;
import com.galeb.healthInfoSystem.data.TblUser;
import com.galeb.healthInfoSystem.repository.MedicalServiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/medical-services")
public class MedicalServiceController {

    @Autowired
    MedicalServiceRepository medicalServiceRepository;
  


    @RequestMapping(value="/index", method = RequestMethod.GET)
    public String view(Model model, RedirectAttributes redirectAttributes){
        model.addAttribute("modelMedicalService",new MedicalService());
        model.addAttribute("medicalServiceList", medicalServiceRepository.findAll());
        return "com.healthInfo.medicalService.index";
    }

    @RequestMapping(value = "/view")
    public String view(Model model) {
        model.addAttribute("modelMedicalService", new MedicalService());
        List<MedicalService> medicalServiceList = medicalServiceRepository.findAll();
        model.addAttribute("medicalServiceList",medicalServiceList);

        return "com.healthInfo.medicalService.view";
    }

    @RequestMapping(value = "/show/{id}")
    public String show(Model model,@PathVariable("id") Long id) {
        model.addAttribute("medicalService", medicalServiceRepository.findById(id));

        return "com.healthInfo.medicalService.show";
    }

    @RequestMapping(value = "/form")
    public String form(Model model) {
        model.addAttribute("modelMedicalService", new MedicalService());
        List<MedicalService> medicalServiceList = medicalServiceRepository.findAll();
        model.addAttribute("medicalServiceList",medicalServiceList);

        return "com.healthInfo.medicalService.form";
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String save(@ModelAttribute("modelMedicalService") MedicalService medicalService, HttpServletRequest request, RedirectAttributes redirectAttributes) {
        SessionBean sessionBean = (SessionBean) request.getSession(false).getAttribute("AUTHSESSION");
        TblUser tblUser = sessionBean.getUser();

        if (medicalService.getId() != null) {
            MedicalService oldMedicalService = medicalServiceRepository.getOne(medicalService.getId());
            medicalService.setCreatedBy(oldMedicalService.getCreatedBy());
            medicalService.setCreatedOn(oldMedicalService.getCreatedOn());
            medicalService.setUpdatedBy(tblUser);
            medicalService.setUpdatedOn(new Date());
        }else{
            medicalService.setCreatedBy(tblUser);
            medicalService.setCreatedOn(new Date());
        }
        medicalService = medicalServiceRepository.save(medicalService);
        

        redirectAttributes.addFlashAttribute("successMsg", "MedicalService Successfully saved");
        return "redirect:/medical-services/index";
    }

    @ResponseBody
    @RequestMapping(value = "/fetchAll")
    public String fetchAll() {
        return CommonUtility.convertToJsonString(medicalServiceRepository.findAll());
    }

    //@ResponseBody
    @RequestMapping(value = "/edit/{id}")
    public String edit(@PathVariable("id") Long id, Model model) {
        model.addAttribute("modelMedicalService", medicalServiceRepository.getOne(id));
        List<MedicalService> medicalServiceList = medicalServiceRepository.findAll();
        model.addAttribute("medicalServiceList",medicalServiceList);

        return "com.healthInfo.medicalService.edit";
        //return CommonUtility.convertToJsonString(MedicalServiceMstService.findById(productPk));
    }

    @ResponseBody
    @RequestMapping(value = "/delete/{id}")
    public String delete(@PathVariable("id") Long id, HttpServletRequest request) {
        SessionBean sessionBean = (SessionBean) request.getSession(false).getAttribute("AUTHSESSION");
        TblUser tblUser = sessionBean.getUser();
        MedicalService medicalService = medicalServiceRepository.getOne(id);
        if(medicalService != null){
            medicalServiceRepository.delete(medicalService);
            return "DELETEDSUCCSSFULLY";
        }
        return "FAILED";
        
    }

    /*@ResponseBody
    @RequestMapping(value = "/childs/{id}")
    public List<MedicalService> getChildMedicalServicesByParent(@PathVariable("id") Long id, HttpServletRequest request) {
        List<MedicalService> childList = medicalServiceRepository.findByParentMedicalServiceId(id);
        return childList;
    }*/
    
}
