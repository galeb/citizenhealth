/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.galeb.healthInfoSystem.controller;


import com.galeb.healthInfoSystem.bean.SessionBean;
import com.galeb.healthInfoSystem.common.utility.service.CommonUtility;
import com.galeb.healthInfoSystem.data.Diagonosis;
import com.galeb.healthInfoSystem.data.TblUser;
import com.galeb.healthInfoSystem.repository.DiagonosisRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/diagonosis")
public class DiagonosisController {

    @Autowired
    DiagonosisRepository diagonosisRepository;
  


    @RequestMapping(value="/index", method = RequestMethod.GET)
    public String view(Model model, RedirectAttributes redirectAttributes){
        model.addAttribute("modelMedicine",new Diagonosis());
        model.addAttribute("diagonosisList", diagonosisRepository.findAll());
        return "com.healthInfo.diagonosis.index";
    }

    @RequestMapping(value = "/view")
    public String view(Model model) {
        model.addAttribute("modelDiagonosis", new Diagonosis());
        List<Diagonosis> diagonosisList = diagonosisRepository.findAll();
        model.addAttribute("diagonosisList",diagonosisList);

        return "com.healthInfo.diagonosis.view";
    }

    @RequestMapping(value = "/show/{id}")
    public String show(Model model,@PathVariable("id") Long id) {
        model.addAttribute("diagonosis", diagonosisRepository.findById(id));

        return "com.healthInfo.diagonosis.show";
    }

    @RequestMapping(value = "/form")
    public String form(Model model) {
        model.addAttribute("modelDiagonosis", new Diagonosis());
        List<Diagonosis> diagonosisList = diagonosisRepository.findAll();
        model.addAttribute("diagonosisList",diagonosisList);

        return "com.healthInfo.diagonosis.form";
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String save(@ModelAttribute("modelDiagonosis") Diagonosis diagonosis, HttpServletRequest request, RedirectAttributes redirectAttributes) {
        SessionBean sessionBean = (SessionBean) request.getSession(false).getAttribute("AUTHSESSION");
        TblUser tblUser = sessionBean.getUser();

        if (diagonosis.getId() != null) {
            Diagonosis oldDiagonosis = diagonosisRepository.getOne(diagonosis.getId());
            diagonosis.setCreateBy(oldDiagonosis.getCreateBy());
            diagonosis.setCreateDate(oldDiagonosis.getCreateDate());
            diagonosis.setUpdateBy(tblUser.getId());
            diagonosis.setUpdateDate(new Date());
        }else{
            diagonosis.setCreateBy(tblUser.getId());
            diagonosis.setCreateDate(new Date());
        }
        diagonosis = diagonosisRepository.save(diagonosis);
        

        redirectAttributes.addFlashAttribute("successMsg", "Diagonosis Successfully saved");
        return "redirect:/diagonosis/index";
    }

    @ResponseBody
    @RequestMapping(value = "/fetchAll")
    public String fetchAll() {
        return CommonUtility.convertToJsonString(diagonosisRepository.findAll());
    }

    //@ResponseBody
    @RequestMapping(value = "/edit/{id}")
    public String edit(@PathVariable("id") Long id, Model model) {
        model.addAttribute("modelDiagonosis", diagonosisRepository.getOne(id));
        List<Diagonosis> diagonosisList = diagonosisRepository.findAll();
        model.addAttribute("diagonosisList",diagonosisList);

        return "com.healthInfo.diagonosis.edit";
        //return CommonUtility.convertToJsonString(DiagonosisMstService.findById(productPk));
    }

    @ResponseBody
    @RequestMapping(value = "/delete/{id}")
    public String delete(@PathVariable("id") Long id, HttpServletRequest request) {
        SessionBean sessionBean = (SessionBean) request.getSession(false).getAttribute("AUTHSESSION");
        TblUser tblUser = sessionBean.getUser();
        Diagonosis diagonosis = diagonosisRepository.getOne(id);
        if(diagonosis != null){
            diagonosisRepository.delete(diagonosis);
            return "DELETEDSUCCSSFULLY";
        }
        return "FAILED";
        
    }

    /*@ResponseBody
    @RequestMapping(value = "/childs/{id}")
    public List<Diagonosis> getChildDiagonosissByParent(@PathVariable("id") Long id, HttpServletRequest request) {
        List<Diagonosis> childList = diagonosisRepository.findByParentDiagonosisId(id);
        return childList;
    }*/
    
}
