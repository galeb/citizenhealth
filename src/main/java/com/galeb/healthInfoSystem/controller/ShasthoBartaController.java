/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.galeb.healthInfoSystem.controller;


import com.galeb.healthInfoSystem.bean.SessionBean;
import com.galeb.healthInfoSystem.common.utility.service.CommonUtility;
import com.galeb.healthInfoSystem.data.DoctorInfo;
import com.galeb.healthInfoSystem.data.PersonalInfo;
import com.galeb.healthInfoSystem.data.ShasthoBarta;
import com.galeb.healthInfoSystem.data.TblUser;
import com.galeb.healthInfoSystem.repository.DoctorInfoRepository;
import com.galeb.healthInfoSystem.repository.PersonalInfoRepository;
import com.galeb.healthInfoSystem.repository.ShasthoBartaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/shastho-barta")
public class ShasthoBartaController {

//    @Autowired
//    DoctorInfoRepository shasthoBartaRepository;
    @Autowired
    ShasthoBartaRepository shasthoBartaRepository;
    @Autowired
    PersonalInfoRepository personalInfoRepository;
  


    @RequestMapping(value="/index", method = RequestMethod.GET)
    public String view(Model model, RedirectAttributes redirectAttributes, HttpServletRequest request){
        model.addAttribute("modelShasthoBarta",new ShasthoBarta());
        model.addAttribute("bartaList", shasthoBartaRepository.findAll());
        return "com.healthInfo.shasthoBarta.index";
    }

    @RequestMapping(value = "/show/{id}")
    public String show(Model model,@PathVariable("id") Long id) {
        model.addAttribute("shasthoBarta", shasthoBartaRepository.findById(id));

        return "com.healthInfo.shasthoBarta.show";
    }

    @RequestMapping(value = "/form")
    public String form(Model model) {
        model.addAttribute("modelShasthoBarta", new ShasthoBarta());
        List<ShasthoBarta> shasthoBartaList = shasthoBartaRepository.findAll();
        model.addAttribute("shasthoBartaList",shasthoBartaList);

        return "com.healthInfo.shasthoBarta.form";
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String save(@ModelAttribute("modelShasthoBarta") ShasthoBarta shasthoBarta, HttpServletRequest request, RedirectAttributes redirectAttributes) {
        SessionBean sessionBean = (SessionBean) request.getSession(false).getAttribute("AUTHSESSION");
        TblUser tblUser = sessionBean.getUser();

        if (shasthoBarta.getId() != null) {
            shasthoBarta.setUpdatedBy(tblUser);
            shasthoBarta.setUpdatedOn(new Date());
        }else{
            shasthoBarta.setCreatedBy(tblUser);
            shasthoBarta.setCreatedOn(new Date());
        }
        shasthoBartaRepository.save(shasthoBarta);
        

        redirectAttributes.addFlashAttribute("successMsg", "ShasthoBarta Successfully saved");
        return "redirect:/shastho-barta/index";
    }

    @ResponseBody
    @RequestMapping(value = "/fetchAll")
    public String fetchAll() {
        return CommonUtility.convertToJsonString(shasthoBartaRepository.findAll());
    }

    //@ResponseBody
    @RequestMapping(value = "/edit/{id}")
    public String edit(@PathVariable("id") Long id, Model model) {
        model.addAttribute("modelShasthoBarta", shasthoBartaRepository.getOne(id));
        List<ShasthoBarta> shasthoBartaList = shasthoBartaRepository.findAll();
        model.addAttribute("shasthoBartaList",shasthoBartaList);

        return "com.healthInfo.shasthoBarta.edit";
        //return CommonUtility.convertToJsonString(ShasthoBartaMstService.findById(productPk));
    }

    @ResponseBody
    @RequestMapping(value = "/delete/{id}")
    public String delete(@PathVariable("id") Long id, HttpServletRequest request) {
        SessionBean sessionBean = (SessionBean) request.getSession(false).getAttribute("AUTHSESSION");
        TblUser tblUser = sessionBean.getUser();
        ShasthoBarta shasthoBarta = shasthoBartaRepository.getOne(id);
        if(shasthoBarta != null){
            shasthoBartaRepository.delete(shasthoBarta);
            return "DELETEDSUCCSSFULLY";
        }
        return "FAILED";
        
    }

    
}
