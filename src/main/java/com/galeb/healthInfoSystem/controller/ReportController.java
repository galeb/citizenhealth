/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.galeb.healthInfoSystem.controller;


import com.galeb.healthInfoSystem.bean.KeyValueDTO;
import com.galeb.healthInfoSystem.bean.ReportDTO;
import com.galeb.healthInfoSystem.bean.SessionBean;
import com.galeb.healthInfoSystem.bean.TreatmentDetailDTO;
import com.galeb.healthInfoSystem.common.utility.service.CommonUtility;
import com.galeb.healthInfoSystem.data.*;
import com.galeb.healthInfoSystem.repository.*;
import com.galeb.healthInfoSystem.service.PersonalInfoService;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.mapping.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
@RequestMapping("/reports")
public class ReportController {

    @Autowired
    PersonalInfoRepository personalInfoRepository;

    @Autowired
    PersonalInfoService personalInfoService;

    @Autowired
    LocationRepository locationRepository;

    @Autowired
    AddressRepository addressRepository;

    @Autowired
    DoctorInfoRepository doctorInfoRepository;

    @Autowired
    MedicalCenterInfoRepository medicalCenterInfoRepository;

    @Autowired
    TreatmentInfoRepository treatmentInfoRepository;

    @Autowired
    TreatmentDtlRepository treatmentDtlRepository;

    @Autowired
    MedicineInfoRepository medicineInfoRepository;

    @Autowired
    DiagonosisRepository diagonosisRepository;

    @Autowired
    TreatmentDiagoDtlRepository treatmentDiagoDtlRepository;

    @Autowired
    GeneralInfoXlsRepository generalInfoXlsRepository;


    @RequestMapping(value = "/view", method = RequestMethod.GET)
    public String view(Model model, RedirectAttributes redirectAttributes) {
        model.addAttribute("modelPersonalInfo", new PersonalInfo());
        model.addAttribute("locationList", locationRepository.findByLocationType("Division"));
        model.addAttribute("personalInfoList", personalInfoRepository.findAll());
        return "com.healthInfo.reports.view";
    }


    @RequestMapping(value = "/patient-detail-report", method = RequestMethod.GET)
    public String patientDetailReport(Model model, RedirectAttributes redirectAttributes) {

        return "com.healthInfo.reports.patientDetailReport";
    }

    @RequestMapping(value = "/doctor-detail-report", method = RequestMethod.GET)
    public String doctorDetailReport(Model model, RedirectAttributes redirectAttributes) {

        return "com.healthInfo.reports.doctorDetailReport";
    }

    @RequestMapping(value = "/disease-wise-report", method = RequestMethod.GET)
    public String diseaseWiseReport(Model model, RedirectAttributes redirectAttributes) {
        model.addAttribute("diseaseList", treatmentInfoRepository.findDiseaseList());
        return "com.healthInfo.reports.diseaseDetailReport";
    }


    @ResponseBody
    @RequestMapping(value = "/find-by-value", method = RequestMethod.GET)
    public ReportDTO findByValue(@RequestParam("fromDate") String fromDate,@RequestParam("toDate") String toDate,@RequestParam("reportType") String reportType) {
        if(fromDate != null && ! fromDate.isEmpty() && toDate != null && !toDate.isEmpty()){
            try {
                Date fDate=new SimpleDateFormat("dd/MM/yyyy").parse(fromDate);
                Date tDate=new SimpleDateFormat("dd/MM/yyyy").parse(toDate);
                ReportDTO report = new ReportDTO();
                if(reportType.equals("general")){
                    report.setTotalPatient(treatmentInfoRepository.findTotalPatientByTwoDate(fDate,tDate));
                    report.setTotalDoctor(treatmentInfoRepository.findTotalDoctorPatientByTwoDate(fDate,tDate));
                    List<java.util.Map> patientByGender = (List<java.util.Map>) treatmentInfoRepository.findTotalGroupByGender(fDate,tDate);
                    List<java.util.Map> doctorByGender = (List<java.util.Map>) treatmentInfoRepository.findTotalDoctorGroupByGender(fDate,tDate);
                    String key = "";
                    Object value = null;
                    for (java.util.Map<String, Object> each:patientByGender) {
                        for (java.util.Map.Entry<String, Object> entry : each.entrySet()) {
                            if(entry.getKey().equals("gender")){
                                key=entry.getValue().toString();
                            }else {
                                value = entry.getValue();
                            }
                        }
                        if(key.equals("Male")){
                            report.setTotalMalePatient(((Long) value).intValue());
                        }else if(key.equals("Female")){
                            report.setTotalFemalePatient(((Long) value).intValue());
                        }else {
                            //
                        }


                    }
                    for (java.util.Map<String, Object> each:doctorByGender) {
                        for (java.util.Map.Entry<String, Object> entry : each.entrySet()) {
                            if(entry.getKey().equals("gender")){
                                key=entry.getValue() != null ? entry.getValue().toString():null;
                            }else {
                                value = entry.getValue();
                            }
                        }
                        if(key.equals("Male")){
                            report.setTotalMaleDoctor(((Long) value).intValue());
                        }else if(key.equals("Female")){
                            report.setTotalFemaleDoctor(((Long) value).intValue());
                        }else {
                            //
                        }


                    }

                } else if(reportType.equals("division_wise_patient")){
                    List<KeyValueDTO> list = new ArrayList<>();
                    List<java.util.Map> patientByDivision = (List<java.util.Map>) treatmentInfoRepository.findPatientGroupByDivision(fDate,tDate);

                    for (java.util.Map<String, Object> each:patientByDivision) {
                        String key = "";
                        Object value = null;
                        for (java.util.Map.Entry<String, Object> entry : each.entrySet()) {
                            if (entry.getKey().equals("division")) {
                                key = entry.getValue().toString();
                            } else {
                                value = entry.getValue();
                            }
                        }
                        KeyValueDTO object = new KeyValueDTO();
                        object.setPropertyName(key);
                        object.setPropertyValue(value.toString());
                        list.add(object);
                    }
                    report.setObjectList(list);
                } else if(reportType.equals("district_wise_patient")){
                        List<java.util.Map> patientByDistrict = (List<java.util.Map>) treatmentInfoRepository.findPatientGroupByDistrict(fDate,tDate);
                        List<KeyValueDTO> list = new ArrayList<>();

                    for (java.util.Map<String, Object> each:patientByDistrict) {
                            String key = "";
                            Object value = null;
                            for (java.util.Map.Entry<String, Object> entry : each.entrySet()) {
                                if (entry.getKey().equals("district")) {
                                    key = entry.getValue().toString();
                                } else {
                                    value = entry.getValue();
                                }
                            }
                            KeyValueDTO object = new KeyValueDTO();
                            object.setPropertyName(key);
                            object.setPropertyValue(value.toString());
                            list.add(object);
                        }
                    report.setObjectList(list);
                }

                return report;
            } catch (ParseException e) {
                e.printStackTrace();
                return null;
            }
        }

        return null;
    }

    @ResponseBody
    @RequestMapping(value = "/find-patient-report", method = RequestMethod.GET)
    public List<TreatmentInfo> findPateintReport(@RequestParam("fromDate") String fromDate,@RequestParam("toDate") String toDate,@RequestParam("pkid") String pkid) {
        if(pkid != null && ! pkid.isEmpty() && fromDate != null && ! fromDate.isEmpty() && toDate != null && !toDate.isEmpty()){
            try {
                Date fDate=new SimpleDateFormat("dd/MM/yyyy").parse(fromDate);
                Date tDate=new SimpleDateFormat("dd/MM/yyyy").parse(toDate);
                ReportDTO report = new ReportDTO();
                Long id = Long.valueOf(pkid);
                List<TreatmentInfo> treatmentInfos = treatmentInfoRepository.findByPatientIdAndDate(id,fDate,tDate);
               /* List<TreatmentDtl> treatmentDtlsInfos = treatmentDtlRepository.findByPatientIdAndDate(id,fDate,tDate);
                List<TreatmentDiagoDtl> diagoDtlsInfos = treatmentDiagoDtlRepository.findByPatientIdAndDate(id,fDate,tDate);

                report.setTreatmentInfoList(treatmentInfos);
                report.setTreatmentDtlList(treatmentDtlsInfos);
                report.setDiagoDtlList(diagoDtlsInfos);*/
                return treatmentInfos;
            } catch (ParseException e) {
                e.printStackTrace();
                return null;
            }
        }

        return null;
    }
/*
    @ResponseBody
    @RequestMapping(value = "/find-doctor-report", method = RequestMethod.GET)
    public ReportDTO findDoctorReport(@RequestParam("fromDate") String fromDate,@RequestParam("toDate") String toDate,@RequestParam("pkid") String pkid) {
        if(pkid != null && ! pkid.isEmpty() && fromDate != null && ! fromDate.isEmpty() && toDate != null && !toDate.isEmpty()){
            try {
                Date fDate=new SimpleDateFormat("dd/MM/yyyy").parse(fromDate);
                Date tDate=new SimpleDateFormat("dd/MM/yyyy").parse(toDate);
                ReportDTO report = new ReportDTO();
                Long id = Long.valueOf(pkid);
                List<TreatmentInfo> treatmentInfos = treatmentInfoRepository.findByPatientIdAndDate(id,fDate,tDate);
                *//*List<TreatmentDtl> treatmentDtlsInfos = treatmentDtlRepository.findByPatientIdAndDate(id,fDate,tDate);
                List<TreatmentDiagoDtl> diagoDtlsInfos = treatmentDiagoDtlRepository.findByPatientIdAndDate(id,fDate,tDate);
*//*
                report.setTreatmentInfoList(treatmentInfos);
                *//*report.setTreatmentDtlList(treatmentDtlsInfos);
                report.setDiagoDtlList(diagoDtlsInfos);*//*
                return report;
            } catch (ParseException e) {
                e.printStackTrace();
                return null;
            }
        }

        return null;
    }*/

    @ResponseBody
    @RequestMapping(value = "/find-treatment-report-on-type", method = RequestMethod.GET)
    public List<TreatmentInfo> findMedicalCentreReport(@RequestParam("fromDate") String fromDate,@RequestParam("toDate") String toDate,@RequestParam("pkid") String pkid,@RequestParam("reportType") String reportType) {
        if(pkid != null && ! pkid.isEmpty() && fromDate != null && ! fromDate.isEmpty() && toDate != null && !toDate.isEmpty()){
            try {
                Date fDate=new SimpleDateFormat("dd/MM/yyyy").parse(fromDate);
                Date tDate=new SimpleDateFormat("dd/MM/yyyy").parse(toDate);
                ReportDTO report = new ReportDTO();
                Long id = Long.valueOf(pkid);
                List<TreatmentInfo> treatmentInfos = new ArrayList<>();

                if(reportType != null && reportType.equals("doctor")){
                    treatmentInfos = treatmentInfoRepository.findByDoctorIdAndDate(id,fDate,tDate);
                } else if(reportType != null && reportType.equals("medicalCentre")){
                    treatmentInfos = treatmentInfoRepository.findByMedicalCenterIdAndDate(id,fDate,tDate);
                } else {

                }
                //report.setTreatmentInfoList(treatmentInfos);
                return treatmentInfos;
            } catch (ParseException e) {
                e.printStackTrace();
                return null;
            }
        }

        return null;
    }

    @ResponseBody
    @RequestMapping(value = "/find-treatment-by-disease", method = RequestMethod.GET)
    public List<TreatmentInfo> findTreatmentByDisease(@RequestParam("fromDate") String fromDate,@RequestParam("toDate") String toDate,@RequestParam("disease") String disease) {
        if(disease != null && ! disease.isEmpty() && fromDate != null && ! fromDate.isEmpty() && toDate != null && !toDate.isEmpty()){
            try {
                Date fDate=new SimpleDateFormat("dd/MM/yyyy").parse(fromDate);
                Date tDate=new SimpleDateFormat("dd/MM/yyyy").parse(toDate);
                List<TreatmentInfo> treatmentInfos = new ArrayList<>();

                treatmentInfos = treatmentInfoRepository.findByDiseaseAndDate(disease.toUpperCase(),fDate,tDate);
                return treatmentInfos;
            } catch (ParseException e) {
                e.printStackTrace();
                return null;
            }
        }

        return null;
    }




}
