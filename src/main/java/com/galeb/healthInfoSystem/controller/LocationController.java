/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.galeb.healthInfoSystem.controller;


import com.galeb.healthInfoSystem.bean.SessionBean;
import com.galeb.healthInfoSystem.common.utility.service.CommonUtility;
import com.galeb.healthInfoSystem.data.Location;
import com.galeb.healthInfoSystem.data.PersonalInfo;
import com.galeb.healthInfoSystem.data.TblUser;
import com.galeb.healthInfoSystem.repository.AddressRepository;
import com.galeb.healthInfoSystem.repository.HealthInsuranceInfoRepository;
import com.galeb.healthInfoSystem.repository.LocationRepository;
import com.galeb.healthInfoSystem.repository.PersonalInfoRepository;
import com.galeb.healthInfoSystem.service.PersonalInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/locations")
public class LocationController {

    @Autowired
    LocationRepository locationRepository;
  


    @RequestMapping(value="/index", method = RequestMethod.GET)
    public String view(Model model, RedirectAttributes redirectAttributes){
        model.addAttribute("modelLocation",new Location());
        model.addAttribute("locationList", locationRepository.findAll());
        return "com.healthInfo.location.index";
    }

    @RequestMapping(value = "/view")
    public String view(Model model) {
        model.addAttribute("modelLocation", new Location());
        List<Location> locationList = locationRepository.findAll();
        model.addAttribute("locationList",locationList);

        return "com.healthInfo.location.view";
    }

    @RequestMapping(value = "/show/{id}")
    public String show(Model model,@PathVariable("id") Long id) {
        model.addAttribute("location", locationRepository.findById(id));

        return "com.healthInfo.location.show";
    }

    @RequestMapping(value = "/form")
    public String form(Model model) {
        model.addAttribute("modelLocation", new Location());
        List<Location> locationList = locationRepository.findAll();
        model.addAttribute("locationList",locationList);

        return "com.healthInfo.location.form";
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String save(@ModelAttribute("modelLocation") Location location, HttpServletRequest request, RedirectAttributes redirectAttributes) {
        SessionBean sessionBean = (SessionBean) request.getSession(false).getAttribute("AUTHSESSION");
        TblUser tblUser = sessionBean.getUser();

        if (location.getId() != null) {
            Location oldLocation = locationRepository.getOne(location.getId());
            location.setCreatedBy(oldLocation.getCreatedBy());
            location.setCreatedOn(oldLocation.getCreatedOn());
            location.setUpdatedBy(tblUser);
            location.setUpdatedOn(new Date());
        }else{
            location.setCreatedBy(tblUser);
            location.setCreatedOn(new Date());
        }
        location = locationRepository.save(location);
        

        redirectAttributes.addFlashAttribute("successMsg", "Location Successfully saved");
        return "redirect:/locations/index";
    }

    @ResponseBody
    @RequestMapping(value = "/fetchAll")
    public String fetchAll() {
        return CommonUtility.convertToJsonString(locationRepository.findAll());
    }

    //@ResponseBody
    @RequestMapping(value = "/edit/{id}")
    public String edit(@PathVariable("id") Long id, Model model) {
        model.addAttribute("modelLocation", locationRepository.getOne(id));
        List<Location> locationList = locationRepository.findAll();
        model.addAttribute("locationList",locationList);

        return "com.healthInfo.location.edit";
        //return CommonUtility.convertToJsonString(LocationMstService.findById(productPk));
    }

    @ResponseBody
    @RequestMapping(value = "/delete/{id}")
    public String delete(@PathVariable("id") Long id, HttpServletRequest request) {
        SessionBean sessionBean = (SessionBean) request.getSession(false).getAttribute("AUTHSESSION");
        TblUser tblUser = sessionBean.getUser();
        Location location = locationRepository.getOne(id);
        if(location != null){
            locationRepository.delete(location);
            return "DELETEDSUCCSSFULLY";
        }
        return "FAILED";
        
    }

    @ResponseBody
    @RequestMapping(value = "/childs/{id}")
    public List<Location> getChildLocationsByParent(@PathVariable("id") Long id, HttpServletRequest request) {
        List<Location> childList = locationRepository.findByParentLocationId(id);
        return childList;
    }
    
}
